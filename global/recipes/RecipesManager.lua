require('calypso.core');
require('calypso.class');
require('foundation.log');
local Recipe = require('global.recipes.Recipe');
local ids = require('foundation.ids');


-- *****************************************************************************
-- * RecipesManager
-- *
-- *  abstract class RecipesManager
-- *****************************************************************************

--- Classe statique qui gère les recettes contenues dans le jeu.
---@class RecipesManager
local RecipesManager = {
	_recipes = {}; ---@type table<string,Recipe>
};

--- Retourne toutes les recettes enregistrées, dans une liste avec en clef leur
--- identifiant et en valeur elles-mêmes.
---@return table<string, Recipe>
function RecipesManager.getRecipes()
	return RecipesManager._recipes;
end

--- Vérifie si une recette est enregistrée, _via_ son identifiant complet.
---@param id string
---@return boolean
function RecipesManager.hasRecipe(id)
	local id = tostring(id);
	return RecipesManager._recipes[id] ~= nil;
end

--- Retourne une recette enregistrée, _via_ son identifiant complet. Si aucune
--- recette n'existe sous cet ID, retourne `nil`.
---@see RecipesManager.getRecipe_nilsafe
---@param id string
---@return Recipe?
function RecipesManager.getRecipe(id)
	local id = ids.ensureIdModule(id);
	if RecipesManager.hasRecipe(id) then
		return RecipesManager._recipes[id];
	end
end

--- Retourne une recette enregistrée, _via_ son identifiant complet. Si aucune
--- recette n'existe sous cet ID, lève une erreur.
---
--- _@throws_ `Errors.UnknownRecipe`
---@see RecipesManager.getRecipe
---@param id string
---@return Recipe
function RecipesManager.getRecipe_nilsafe(id)
	local i = RecipesManager.getRecipe(id);
	if not i then
		throw(Errors.UnknownRecipe, 'Unknown recipe ' .. id);
	else
		return i;
	end
end

--- Enregistre une recette. Si une recette existe déjà sous son identifiant,
--- lève une erreur.
---
--- _@throws_ `Errors.DuplicatedId`
---@param recipe Recipe
function RecipesManager.registerRecipe(recipe)
	if not RecipesManager.hasRecipe(recipe:getFullyQualifiedId()) then
		RecipesManager._recipes[recipe:getFullyQualifiedId()] = recipe;
	else
		throw(Errors.DuplicatedId, string.format('Cannot register recipe %s, for there is already a recipe under this ID', recipe:getFullyQualifiedId()));
	end
end

--- Désenregistre une recette, si elle existe, et la retourne. Sinon, lève une
--- erreur.
---
--- _@throws_ `Errors.UnknownRecipe`
---@param id string L'identifiant complet de la recette à désenregistrer.
---@return Recipe
function RecipesManager.unregisterRecipe(id)
	local id = tostring(id);
	if RecipesManager.hasRecipe(id) then
		local r = RecipesManager.getRecipe_nilsafe(id);
		RecipesManager._recipes[id] = nil;
		return r;
	else
		throw(Errors.UnknownRecipe, 'Unknown recipe : ' .. id);
	end
end

--- Compte le nombre de recettes enregistrées. Si le paramètre `mid` est
--- fourni, ne prend en compte que les recettes provenant de ce module.
---@param mid? string Un identifiant de module
---@return integer
function RecipesManager.countRecipes(mid)
	if not mid then
		return map.size(RecipesManager._recipes);
	else
		local i = 0;
		for _,r in pairs(RecipesManager._recipes) do
			if r:getModule():getId() == mid then
				i = i + 1;
			end
		end
		return i;
	end
end


return RecipesManager;