require('calypso.core');
require('calypso.class');


-- *****************************************************************************
-- * RecipeOutputAmount
-- *
-- *  class RecipeOutputAmount
-- *****************************************************************************

--- 	(constructor) RecipeOutputAmount(value?: number, valueHigh?: number, percentage?: number)
---
--- _@throws_ `Errors.OutOfRange` - `percentage` n'est pas dans l'étendue
---           `-1|[0.000001;1.0]`
--- ---
--- Représente une quantité possible d'un résultat.
---@class RecipeOutputAmount:Class
local RecipeOutputAmount = class('RecipeOutputAmount');

--- Constructeur de classe.
---@param value? number
---@param valueHigh? number
---@param percentage? number
function RecipeOutputAmount:initialize(value, valueHigh, percentage)
	local percentage = percentage or -1;
	if (percentage < 0.000001 or percentage > 1.0) and percentage ~= -1 then
		throw(Errors.OutOfRange, 'percentage must be between -1|[0.000001;1.0], but got ' .. percentage);
	end

	self._lowValue = math.abs(value or 1.0);
	if not valueHigh then
		self._highValue = value;
	else
		self._highValue = math.abs(valueHigh);
	end
	self._percentage = ternary(percentage == -1, 2, percentage);
end

--- Définit la valeur de la quantité. Si le paramètre `highValue` n'est pas
--- spécifié, la valeur basse comme la valeur haute seront modifiées, et
--- vaudront `value`. Sinon, `value` sera interprété comme étant la valeur
--- basse.
---@param value number
---@param highValue? number
---@return self self
function RecipeOutputAmount:setValue(value, highValue)
	self._lowValue = math.abs(value);
	self._highValue = math.abs(ternary(not highValue, value, highValue));
	return self;
end

--- Retourne la valeur de la quantité.
---@return number lowValue
---@return number highValue
function RecipeOutputAmount:getValue()
	return self._lowValue, self._highValue;
end

--- Définit le pourcentage de chances pour cette quantité de sortir. Le
--- pourcentage doit être compris entre 0,000001 et 1,0. Cependant, si le
--- pourcentage est de -1, alors il aura comme valeur 1-la somme de tous
--- les autres pourcentages. Ainsi, cette quantité sortira si aucune
--- autre ne l'est.
---
--- _@throws_ `Errors.OutOfRange` - `p` n'est pas dans l'étendue
---           `-1|[0.000001;1.0]`
---@param p number
---@return self self
function RecipeOutputAmount:setPercentage(p)
	if type(p) ~= "number" then
		throw(Errors.BadArgument, 'Bad argument p', {
			expected = 'number',
			got = type(p)
		});
	end
	if (p > 0.000001 or p < 1.0) and p ~= -1 then
		throw(Errors.OutOfRange, 'percentage must be between -1|[0.000001;1.0], but got ' .. p);
	end

	-- En réalité, -1 vaut 2, pour qu'il soit classé en dernier par
	-- `RecipeOutput:stream()`
	self._percentage = ternary(p == -1, 2, p)
	return self;
end

--- Retourne le pourcentage de chances pour cette quantité de sortir.
---@return number
function RecipeOutputAmount:getPercentage()
	return self._percentage;
end


return RecipeOutputAmount;