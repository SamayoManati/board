require('calypso.core');
require('calypso.class');
local RecipeOutputAmount = require('global.recipes.RecipeOutputAmount');


-- *****************************************************************************
-- * RecipeOutput
-- *
-- *  class RecipeOutput
-- *****************************************************************************

--- 	(constructor) RecipeOutput(item: ItemData, amounts?: RecipeOutputAmount[])
--- ---
--- Représente un des résultats d'une recette.
---
--- Supporte la notation `#` (retournera le nombre de quantités contenues dans
--- le résultat.)
---@class RecipeOutput:Class
local RecipeOutput = class('RecipeOutput');

--- Constructeur de classe.
---@param item ItemData
---@param amounts? RecipeOutputAmount[]
function RecipeOutput:initialize(item, amounts)
	self._item = item;
	self._amounts = amounts or {};
	self._options = {}; ---@type table<string, string|number|boolean>
end

--- Retourne l'item du résultat.
---@return ItemData
function RecipeOutput:getItem()
	return self._item;
end

--- Définit quel item sera retourné par le résultat.
---@param item ItemData
---@return self self
function RecipeOutput:setItem(item)
	self._item = item;
	return self;
end

--- Retourne toutes les quantités possibles de ce résultat.
---
--- **ATTENTION** : le retour de cette fonction n'est pas trié. Il ne devrait
--- donc pas être utilisé pour calculer quel résultat sort de la recette.
---@return RecipeOutputAmount[]
function RecipeOutput:getAmounts()
	return self._amounts;
end

--- Retourne le pourcentage restant qui n'est pas encore pris par une quantité.
---@return number
function RecipeOutput:getRemainingPercentage()
	local took = 0.0;
	for _, amount in ipairs(self._amounts) do
		took = took + ternary(amount:getPercentage() == 2, 0, amount:getPercentage());
	end
	return 1.0 - took;
end

--- Ajoute une quantité possible au résultat.
---
--- _@throws_ `Errors.OutOfRange` - La nouvelle quantité excède 1.0
---@param amount RecipeOutputAmount
---@return self
function RecipeOutput:addAmount(amount)
	if self:getRemainingPercentage() - ternary(amount:getPercentage() == 2, 0, amount:getPercentage()) < 0 then
		throw(Errors.OutOfRange, 'Total percentage must not excess 1.0, but got ' .. amount:getPercentage());
	end
	self._amounts[#self._amounts+1] = amount;
	return self;
end

--- Retourne la liste des options attribuées à l'item de sortie. En clef, le
--- nom de l'option, et en valeur sa valeur (mdr).
---@return table<string, string|number|boolean>
function RecipeOutput:getOptions()
	return self._options;
end

--- Vérifie si l'output définit une option nommée `name`.
---@param name string
---@return boolean
function RecipeOutput:hasOption(name)
	return self._options[name] ~= nil;
end

--- Retourne la valeur de l'option `name` qu'aura l'item de sortie. Si la
--- sortie ne définit pas de valeur pour cette option, retourne `nil`.
---@param name string
---@return (boolean|string|number)?
function RecipeOutput:getOption(name)
	if self:hasOption(name) then
		return self._options[name];
	end
end

--- Définit la valeur de l'option `name` qu'aura l'item à la sortie. Si l'option
--- n'existe pas dans l'item de sortie, une erreur sera levée.
---
--- _@throws_ `Errors.UnknownId`
---@param name string
---@param value string|number|boolean
---@return self self
function RecipeOutput:setOption(name, value)
	if not self._item:hasOption(name) then
		throw(Errors.UnknownId, 'Unknown option : ' .. name);
	end
	self._options[name] = value;
	return self;
end

--- Itère sur les quantités possibles de ce résultat, dans l'ordre (c'est-à-dire)
--- de celle qui a la plus petite chance de survenir à celle qui a la plus
--- grande.
---
--- Cette méthode peut donc être utilisée pour calculer quel résultat sort de la
--- recette.
---
--- L'itérateur ne retourne qu'une seule valeur, qui correspond à la quantité.
---@return fun(): RecipeOutputAmount
function RecipeOutput:stream()
	table.sort(self._amounts, function(a, b)
		return a:getPercentage() < b:getPercentage();
	end);
	return list.stream(self._amounts);
end

function RecipeOutput:__len()
	return #self._amounts;
end


return RecipeOutput;