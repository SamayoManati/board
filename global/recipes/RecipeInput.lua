require('calypso.core');
require('calypso.class');
local ItemOptionType = require('global.items.ItemOptionType');
local ItemData       = require('global.items.ItemData');


-- *****************************************************************************
-- * RecipeInput
-- *
-- *  class RecipeInput
-- *****************************************************************************

--- 	class RecipeInput
--- 		(constructor) RecipeInput(id: string, it?: ItemData[] = {}, amount?: number = 1, persistent?: boolean = false)
--- ---
--- Représente un input de recette. Chaque input de recette contient une liste
--- d'items abstraits qui lui sont associés, ainsi qu'un montant, qui est le
--- nombre d'exemplaires à fournir. Elle peut aussi contenir des options.
---
--- Un input persistant n'est pas consommé pendant la recette.
---@class RecipeInput:Class
local RecipeInput = class("RecipeInput");

--- Constructeur de classe.
---@param id string
---@param it? ItemData[]
---@param amount? number
---@param persistent? boolean
function RecipeInput:initialize(id, it, amount, persistent)
	self._id = id;
	self._items = it or {}; ---@type ItemData[]
	self._amount = amount or 1;
	self._persistent = ternary(persistent ~= nil, persistent, false);
	self._options = {}; ---@type table<string, [ItemOptionType, string|string[]|number|(number|[number, number])[]|boolean]>
end

--- Obtient l'identifiant de l'input de recette. Cet identifiant est unique
--- parmi les identifiants de cette recette.
---@return string
function RecipeInput:getId()
	return self._id;
end

--- Vérifie si l'input accepte un item abstrait.
---@param item ItemData
---@return boolean
function RecipeInput:acceptsItemData(item)
	return list.contains(self._items, item);
end

--- Vérifie si l'input accepte un item en particulier. Retourne en second
--- retour la raison pour laquelle l'item n'est pas accepté.
---
--- 1. Le `ItemData` n'est pas accepté dans l'ingrédient
--- 2. Il manque une option à l'item
--- 3. L'option de l'item n'est pas du bon type
--- 4. La valeur de l'option de l'item n'est pas parmi les valeurs
---    autorisées pour une _enum_
--- 5. La valeur de l'option de l'item est hors de l'étendue autorisée pour
---    un _range_
--- 6. La valeur de l'option de l'item n'est pas la valeur exacte autorisée
---    pour un _range_
--- 7. La valeur de l'option de l'item n'est pas la valeur autorisée pour un
---    _switch_
---@param item Item
---@return boolean
---@return integer? reason
function RecipeInput:accepts(item)
	local itemData = item:getItemData();

	if not self:acceptsItemData(itemData) then
		-- l'item abstrait n'est pas accepté dans l'ingrédient
		return false, 1;
	end

	for optionName, optionValue in pairs(self._options) do
		if not itemData:hasOption(optionName) then
			-- il manque une option à l'item
			return false, 2;
		end

		local option                 = itemData:getOption(optionName);
		local optionType             = optionValue[1];
		local optionAuthorizedValues = optionValue[2];

		if option:getType() ~= optionType then
			-- l'option de l'item n'est pas du bon type
			return false, 3;
		end

		local actualValue = item:getOptionValue(optionName);
		if optionType == ItemOptionType.ENUM then
			if type(optionAuthorizedValues) == "table" then
				if not list.contains(optionAuthorizedValues, actualValue) then
					return false, 4;
				end
			else
				if actualValue ~= optionAuthorizedValues then
					return false, 4;
				end
			end
		elseif optionType == ItemOptionType.RANGE then
			if type(optionAuthorizedValues) == "table" then
				local qualified = false;
				for i in list.stream(optionAuthorizedValues) do
					if type(i) == "table" then
						if actualValue >= i[1] and actualValue <= i[2] then
							qualified = true;
						end
					else
						if actualValue == i then
							qualified = true;
						end
					end
				end
				if not qualified then
					return false, 5;
				end
			else
				if actualValue ~= optionAuthorizedValues then
					return false, 6;
				end
			end
		elseif optionType == ItemOptionType.SWITCH then
			if actualValue ~= optionAuthorizedValues then
				return false, 7;
			end
		end
	end

	return true;
end

--- Rajoute un item à la liste des items acceptés dans l'input, si celui-ci n'y
--- est pas déjà.
---@param item ItemData
---@return self self
function RecipeInput:accept(item)
	if not self:acceptsItemData(item) then
		self._items[#self._items+1] = item;
	end
	return self;
end

--- Si `item` est accepté, le supprime de la liste des items acceptés et le
--- retourne. Sinon, retourne `nil`.
---@param item ItemData
---@return ItemData?
function RecipeInput:removeItem(item)
	if not item:instanceof(ItemData) then
		throw(Errors.BadArgument, 'Bad argument item', {
			expected = 'ItemData',
			got = type(item)
		});
	end

	local exists, where = list.contains(self._items, item);
	if exists then
		return table.remove(self._items, where);
	end
end

--- Retourne la quantité nécessaire d'exemplaires à fournir.
---@return number
function RecipeInput:getAmount()
	return self._amount;
end

--- Définit la quantité nécessaire d'exemplaires à fournir. Applique `math.abs`
--- dessus.
---@param amount number
---@return self self
function RecipeInput:setAmount(amount)
	self._amount = math.abs(amount);
	return self;
end

--- Vérifie si l'ingrédient est persistant. Un input persistant n'est pas
--- consommé pendant la recette.
---@return boolean
function RecipeInput:isPersistent()
	return self._persistent;
end

--- Définit si l'ingrédient est persistant.
---@param b boolean
---@return self self
function RecipeInput:setPersistent(b)
	self._persistent = b;
	return self;
end

--- Définit une option pour cet ingrédient. Attention, aucun test n'est effectué
--- pour savoir si l'option `id` existe dans les items acceptés. Si vous mettez
--- une option qui n'y est pas, aucun item ne pourra jamais être valide dans cet
--- ingrédient.
---
--- Valeurs possibles selon le type d'option :
---
--- **Enum** :
---  - `string` : Une seule valeur autorisée
---  - `string[]` : Plusieurs valeurs autorisées
---
--- **Range** :
---  - `number` : Une seule valeur autorisée
---  - `number[]` : Plusieurs valeurs autorisées
---  	- `number` : Une valeur exacte
---  	- `number[2]` : Une étendue, avec 1, le minimum compris, et 2 le maximum
---  	  compris
---
--- **Switch** :
---  - `boolean` : Une seule valeur autorisée
---@param id string L'identifiant de l'option
---@param optionType ItemOptionType
---@param authorizedValues boolean|string[]|string|number|(number|[number, number])[]
---@return self self
function RecipeInput:setOption(id, optionType, authorizedValues)
	self._options[id] = {optionType, authorizedValues};
	return self;
end

--- Retourne l'option `id`, avec en premier retour son type, et en second retour
--- ses valeurs autorisées. Si cette option n'est pas définie dans cet input,
--- retourne `nil`.
---@param id string
---@return ItemOptionType?
---@return boolean|string|number|(number|[number, number])[]|string[]?
function RecipeInput:getOption(id)
	local x = self._options[id];
	if not not x then
		return x[1], x[2];
	end
end


return RecipeInput;