require('calypso.core');
require('calypso.class');
require('foundation.log');
local ids = require('foundation.ids');
local Tag = require('global.tags.Tag');
local MachineType = require('global.machines.MachineType');
local MachineModel = require('global.machines.MachineModel');
local RecipeInput = require('global.recipes.RecipeInput');
local RecipeOutput = require('global.recipes.RecipeOutput');
local LocalizationManager = require('global.locales.LocalizationManager');
local ItemData = require('global.items.ItemData');


-- *****************************************************************************
-- * Recipe
-- *
-- *  class Recipe
-- *****************************************************************************

--- 	(constructor) Recipe(id: string, mod: Module)
--- ---
--- Représente une recette disponible dans le jeu.
---
--- Cette classe interagit avec de nombreuses autres. Chaque recette (`Recipe`)
--- possède une liste de machines avec lesquelles elle est compatible
--- (`(MachineType|MachineModel|Tag)[]`). Elle possède également une liste
--- d'ingrédients (`RecipeInput[]`), ou input, une liste d'outputs
--- (`RecipeOutput[]`), et un temps d'usinage (`number`). <br/>
--- Chaque ingrédient de recette (`RecipeInput`) possède une liste d'items
--- abstraits (`ItemData[]`) acceptés dans cet input, ainsi que le nombre
--- d'exemplaires qu'il faut fournir, et si l'ingrédient est, ou non, persistant.
--- Il possède également une liste d'options d'ingrédients, qui représentent
--- chacun, pour une option _x_ d'un item abstrait, la ou les valeur(s) qu'elle
--- doit présenter pour que l'item abstrait soit accepté dans cet input. <br/>
--- Chaque output de recette (`RecipeOutput`) possède un item abstraits
--- (`ItemData`), qui est celui qui sera créé par cet output, une liste
--- d'options qui seront attribuées audit item, et une liste de quantités
--- (`RecipeOutputAmount[]`), qui représentent chacune une des quantités dans
--- lesquelles il est possible d'avoir cet output. <br/>
--- Enfin, chaque recette est enregistrée dans le `RecipesManager` afin d'être
--- présente dans le jeu.
---@class Recipe:Class
local Recipe = class('Recipe');


--- Constructeur de classe.
---@param id string
---@param mod Module
function Recipe:initialize(id, mod)
	self._id = id;
	self._module = mod;
	self._processingTime = 0.0;
	self._machines = {}; ---@type (MachineType|MachineModel|Tag)[]
	self._inputs = {}; ---@type RecipeInput[]
	self._outputs = {}; ---@type RecipeOutput[]
end

--- Retourne l'identifiant de la recette.
---@return string
function Recipe:getId()
	return self._id;
end

--- Retourne une référence vers le module qui a ajouté la recette.
---@return Module
function Recipe:getModule()
	return self._module;
end

--- Retourne le temps de base que met la recette à être créé, en secondes.
--- Note : ce temps est susceptible d'être modifié ultérieurement par
--- différents facteurs.
---@return number
function Recipe:getProcessingTime()
	return self._processingTime;
end

--- Définit le temps de base que met la recette à être créé. Note : ce temps
--- est susceptible d'être modifié ultérieurement par différents facteurs.
---@param seconds number
---@return self self
function Recipe:setProcessingTime(seconds)
	self._processingTime = seconds;
	return self;
end

--- Retourne la liste des types et modèles de machines sur lesquelles la recette
--- est disponible.
---@return (MachineModel|MachineType|Tag)[]
function Recipe:getAvailableFor()
	return self._machines;
end

--- Itère sur la liste des types et modèles de machines sur lesquelles la
--- recette est disponible.
---@return fun(): MachineModel|MachineType|Tag
function Recipe:streamAvailableFor()
	return list.stream(self._machines);
end

--- Vérifie si la recette est disponible sur `m`.
---@param m MachineType|MachineModel
function Recipe:isAvailableFor(m)
	for v in list.stream(self._machines) do
		if v:instanceof(Tag) then
			---@cast v Tag
			if v:contains(m:getFullyQualifiedId()) then
				return true;
			end
		else
			if m == v then
				return true;
			end
		end
	end
	return false;
end

--- Définit la recette comme disponible sur `m`. si `m` est un tag, il doit
--- être un tag de machines, sans quoi la fonction lèvera une erreur. Si la
--- recette est déjà disponible sur `m`, émet simplement un warning.
---
--- _@throws_ `Errors.InvalidTagType` - `m` est un tag, et il n'est pas un tag
---           de machines
---
--- _@throws_ `Errors.BadArgument`
---@param m MachineType|MachineModel|Tag
---@return self self
function Recipe:setAvailableFor(m)
	if m:instanceof(Tag) then
		---@cast m Tag
		if not m:getType() == ids.Specifier.Tag.MACHINES then
			throw(Errors.InvalidTagType, string.format('The tag %s must be a machines tag', m:getFullyQualifiedId()), {
				expected = ids.Specifier.Tag.MACHINES,
				got = m:getType()
			});
		end
	elseif (not m:instanceof(MachineType)) and (not m:instanceof(MachineModel)) then
		throw(Errors.BadArgument, 'Bad parameter m', {
			expected = 'MachineType | MachineModel | Tag',
			got = m.class.name
		});
	end

	if not self:isAvailableFor(m) then
		self._machines[#self._machines+1] = m;
	else
		logwrn(string.format('[Recipe:setAvailableFor] : Ignored %s, already available', m:getFullyQualifiedId()));
	end

	return self;
end

--- Retourne une référence vers la liste des inputs possibles pour la recette.
---@return RecipeInput[]
function Recipe:getInputs()
	return self._inputs;
end

--- Itère sur les ingrédients de la recette. Retourne 1 variable de type
--- `RecipeInput`.
---@return fun(): RecipeInput
function Recipe:streamInputs()
	return list.stream(self._inputs);
end

--- Vérifie si la recette possède un input donné (c'est-à-dire acceptant les
--- mêmes items, dans la même quantité, et avec les mêmes options).
---@param input RecipeInput
---@return boolean
---@return integer? index
function Recipe:hasInput(input)
	return list.contains(self._inputs, input);
end

--- Ajoute un input à la recette.
---
--- _@throws_ `Errors.BadArgument`
---@param input RecipeInput
---@return self self
function Recipe:addInput(input)
	if not input:instanceof(RecipeInput) then
		throw(Errors.BadArgument, 'Bad argument input', {
			expected = 'RecipeInput',
			got = input.class.name
		});
	end

	if not self:hasInput(input) then
		self._inputs[#self._inputs+1] = input;
	end

	return self;
end

--- Supprime un input de la recette, et le retourne. S'il n'existe pas dans la
--- recette, lève une erreur.
---
--- _@throws_ `Errors.BadArgument`
---
--- _@throws_ `Errors.UnknownId`
---@param input RecipeInput
---@return RecipeInput
function Recipe:removeInput(input)
	if not input:instanceof(RecipeInput) then
		throw(Errors.BadArgument, 'Bad argument input', {
			expected = 'RecipeInput',
			got = input.class.name
		});
	end

	local exists, where = self:hasInput(input);
	if exists then
		return table.remove(self._inputs, where);
	else
		throw(Errors.UnknownId, 'Cannot remove an unknown input', {
			input = input
		});
	end
end

--- Vérifie si la recette possède un output avec le même item abstrait.
---@param item ItemData
---@return boolean
function Recipe:hasOutputWithItem(item)
	assertParameterType("Recipe:hasOutputWithItem", "item", {ItemData}, item);

	for output in list.stream(self._outputs) do
		if output:getItem():getFullyQualifiedId() == item:getFullyQualifiedId() then
			return true;
		end
	end
	return false;
end

--- Ajoute un output à la recette.
---@param output RecipeOutput
---@return self self
function Recipe:addOutput(output)
	assertParameterType("Recipe:addOutput", "output", {RecipeOutput}, output);

	self._outputs[#self._outputs+1] = output;
	return self;
end

--- Itère sur les retours de la recette.
---@return fun(): RecipeOutput
function Recipe:streamOutputs()
	return list.stream(self._outputs);
end

--- Retourne la liste de tous les outputs de la recette.
---@return RecipeOutput[]
function Recipe:getOutputs()
	return self._outputs;
end

--- Retourne l'identifiant complet de la recette.
---@return string
function Recipe:getFullyQualifiedId()
	return ids.createId(ids.Qualifier.RECIPE, self:getId(), nil, nil, self:getModule():getId());
end

--- Retourne l'identifiant complet d'un attribut de la recette.
---@param attr RecipeAttributes Le nom de l'attribut
---@return string
function Recipe:getFullyQualifiedAttributeId(attr)
	return ids.createId(ids.Qualifier.RECIPE, self:getId(), attr, nil, self:getModule():getId());
end

--- Retourne la version localisée d'un attribut de la recette, ou `nil` si
--- aucune langue active ne définit de valeur pour celui-ci.
---@param attr RecipeAttributes
---@return string?
function Recipe:getLocalizedAttribute(attr)
	return LocalizationManager.loc(self:getFullyQualifiedAttributeId(attr));
end

--- Retourne la version localisée d'un attribut de la recette, ou son ID si
--- aucune langue active ne définit de valeur pour celui-ci.
---@see Recipe.getLocalizedAttribute
---@param attr RecipeAttributes
---@return string
function Recipe:getSafeLocalizedAttribute(attr)
	return LocalizationManager.safeloc(self:getFullyQualifiedAttributeId(attr));
end


return Recipe;