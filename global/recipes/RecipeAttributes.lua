-- *****************************************************************************
-- * RecipeAttributes
-- *
-- *  enum RecipeAttributes
-- *****************************************************************************

--- Contient les noms d'attributs valides pour une recette.
---@enum RecipeAttributes
local RecipeAttributes =
{
	NAME        = 'name',
	DESCRIPTION = 'description'
};


return RecipeAttributes;