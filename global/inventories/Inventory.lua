require('calypso.core');
require('calypso.class');

-- *****************************************************************************
-- * Inventory
-- *
-- *  class Inventory
-- *****************************************************************************

--- 	(constructor) Inventory(size: number)
---
--- _@param_ `size` - La taille de l'inventaire. On ne peut pas ajouter d'items
---                   au-delà de cette taille.
--- ---
--- Représente un inventaire d'items. Il peut contenir des `ItemStack`, se
--- trier...
---@class Inventory:Class
local Inventory = class('Inventory');

--- Constructeur de classe.
---@param size number
function Inventory:initialize(size)
	self._size = size;
	self._content = {}; ---@type ItemStack[]
end

--- Retourne le contenu de l'inventaire.
---@return ItemStack[]
function Inventory:getAll()
	return self._content;
end

--- Raccourci pour `list.stream(Inventory.getContent())`.
---@return fun():ItemStack
function Inventory:stream()
	return list.stream(self._content);
end

--- Retourne la place occupée dans l'inventaire.
---@return number
function Inventory:getOccupiedSpace()
	local space = 0;
	for slot in list.stream(self._content) do
		space = space + slot:getAmount();
	end
	return space;
end

--- Retourne la place encore libre dans l'inventaire.
---@return number
function Inventory:getRemainingSpace()
	return self._size - self:getOccupiedSpace()
end

--- Ajoute du contenu à l'inventaire. Retourne s'il a été possible de l'ajouter.
--- Si le nombre d'items à ajouter est trop grand par rapport à la place
--- restante, rien ne sera ajouté, et la fonction retournera `false`. Sinon,
--- le stack sera ajouté dans son entièreté, et la fonction retournera `true`.
---@param items ItemStack
---@return boolean success
function Inventory:add(items)
	local hasSimilarItem = false;
	if items:getAmount() <= self:getRemainingSpace() then
		for slot in list.stream(self._content) do
			if items:getItem():isEqualTo(slot:getItem()) then
				hasSimilarItem = true;
				slot:add(items:getAmount());
				break;
			end
		end

		if not hasSimilarItem then
			self._content[#self._content+1] = items;
		end
		return true;
	else
		return false;
	end
end

--- Vérifie si l'inventaire possède le stack avec l'UUID `uid`.
---@param uid string
---@return boolean
function Inventory:hasStack(uid)
	for slot in self:stream() do
		if slot:getUUID() == uid then
			return true;
		end
	end
	return false;
end

--- Supprime de l'inventaire le stack avec l'UUID `uid` et le retourne. S'il
--- n'est pas dans cet inventaire, une erreur sera levée.
---
--- _@throws_ `Errors.UnknownId` - L'UUID est introuvable
---@param uid string
---@return ItemStack
function Inventory:remove(uid)
	local index;

	for i, j in ipairs(self._content) do
		if j:getUUID() == uid then
			index = i;
			break;
		end
	end

	if not index then
		throw(Errors.UnknownId, 'Unknown UUID', {uuid = uid});
	else
		return table.remove(self._content, index);
	end
end

--- Retourne le stack avec l'UUID fourni. Si celui-ci n'est pas dans cet
--- inventaire, retourne `nil`.
---@param uid string
---@return ItemStack?
function Inventory:get(uid)
	for i = 1, #self._content do
		if self._content[i]:getUUID() == uid then
			return self._content[i];
		end
	end
end

--- Se trie lui-même, selon l'ordre fourni par `order`.
---@param order
--- | 'a>' Quantité décroissante (du stack le plus grand à celui le moins grand)
--- | 'a<' Quantité croissante (du stack le moins grand à celui le plus grand)
--- | 'n<' Alphabétique (nom de l'item de A à Z)
--- | 'n>' Antialphabétique (nom de l'item de Z à A)
---@return self self
function Inventory:sort(order)
	if order == 'a>' then
		table.sort(self._content, function(a, b)
			return a:getAmount() > b:getAmount();
		end);
	elseif order == 'a<' then
		table.sort(self._content, function (a, b)
			return a:getAmount() < b:getAmount();
		end);
	elseif order == 'n<' then
		table.sort(self._content, function (a, b)
			return a:getItem():getItemData():getSafeLocalizedName() < b:getItem():getItemData():getSafeLocalizedName();
		end);
	elseif order == 'n>' then
		table.sort(self._content, function (a, b)
			return a:getItem():getItemData():getSafeLocalizedName() > b:getItem():getItemData():getSafeLocalizedName();
		end);
	end
	return self;
end

--- Modifie la taille de l'inventaire. Si `size` est inférieur à zéro, lève une
--- erreur.
---
--- _@throws_ `Errors.OutOfRange`
---@param size number
---@return self self
function Inventory:setSize(size)
	if size < 0 then
		throw(Errors.OutOfRange, 'parameter size cannot be lesser than 0', {size = tostring(size)});
	end
	self._size = size;
	return self;
end

--- Retourne la taille de l'inventaire.
---@return number
function Inventory:getSize()
	return self._size;
end

--- Compte le nombre d'items dans l'inventaire.
---@return number
function Inventory:getFillLevel()
	local i = 0;
	for stack in self:stream() do
		i = i + stack:getAmount();
	end
	return i;
end


return Inventory;