-- *****************************************************************************
-- * TagAttributes
-- *
-- *  enum TagAttributes
-- *****************************************************************************

---@enum TagAttributes
local TagAttributes =
{
	NAME = 'name'
};


return TagAttributes;