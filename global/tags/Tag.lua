require('calypso.core');
require('calypso.class');

local TagAttributes       = require('global.tags.TagAttributes');
local ids                 = require('foundation.ids');
local LocalizationManager = require('global.locales.LocalizationManager');
local ItemsManager        = require('global.items.ItemsManager');
local MachinesManager     = require('global.machines.MachinesManager');
require('foundation.log');


-- *****************************************************************************
-- * Tag
-- *
-- *  class Tag
-- *****************************************************************************

--- 	(constructor) Tag(id: string, type: ids.Specifier.Tag, mod: Module)
--- ---
--- Représente un tag du jeu. Les tags sont chargés au démarrage depuis les
--- modules, et enregistrés dans le `TagsManager`.
---@class Tag:Class
local Tag = class('Tag');

--- Constructeur de classe.
---@param id string
---@param type Specifier.Tag
---@param mod Module
function Tag:initialize(id, type, mod)
	self._id      = id;
	self._type    = type;
	self._module  = mod;
	self._content = {}; ---@type string[]
	self._unseen  = false;
end

--- Retourne l'identifiant du tag (seulement le nom).
---@return string
function Tag:getId()
	return self._id;
end

--- Retourne le type du tag.
---@return Specifier.Tag
function Tag:getType()
	return self._type;
end

--- Retourne le module duquel provient le tag.
---@return Module
function Tag:getModule()
	return self._module;
end

--- Vérifie si le tag possède la propriété "invisible". Un tag marqué comme tel
--- ne sera pas affiché sur les objets le possédant.
---@return boolean
function Tag:isUnseen()
	return self._unseen;
end

--- Définit si le tag est invisible ou non. Un tag invisible ne sera pas affiché
--- sur le objets le possédant.
---@param unseen boolean
---@return self self
function Tag:setUnseen(unseen)
	self._unseen = unseen;
	return self;
end

--- Retourne tout le contenu du tag, sous forme d'un tableau de `string`
--- contenant tous les identifiants.
---@return string[]
function Tag:getContent()
	return self._content;
end

--- Vérifie si `id` est contenu dans le tag.
---@param id string
---@return boolean
function Tag:contains(id)
	return list.contains(self._content, id);
end

--- Ajoute du contenu au tag. L'identifiant doit être celui d'un objet du même
--- type que le tag (exemple : un item pour un tag d'items).
---
--- _@throws_ `Errors.UnknownItem` - L'item `id` n'existe pas
---
--- _@throws_ `Errors.UnknownId` - Le modèle/type de machine `id` n'existe pas
---
--- _@throws_ `Errors.TypesIncompatibility` - Le tag n'est pas adapté à l'item
---@param id string
---@return self self
function Tag:add(id)
	local id = ids.ensureIdModule(id);
	local cookedId = ids.extractId(id);
	if self:getType() == ids.Specifier.Tag.ITEMS and cookedId.qualifier == ids.Qualifier.ITEM then
		if not ItemsManager.hasItem(id) then
			throw(Errors.UnknownItem, 'Unknown item', {
				item = id
			});
		end
		if not self:contains(id) then
			self._content[#self._content+1] = id;
		else
			logwrn(string.format('Ignored item registration : item %s is already in tag %s', id, self._id));
		end
	elseif (self:getType() == ids.Specifier.Tag.MACHINES and cookedId.qualifier == ids.Qualifier.MACHINE_TYPE)
	or (self:getType() == ids.Specifier.Tag.MACHINES and cookedId.qualifier == ids.Qualifier.MACHINE_MODEL) then
		if not MachinesManager.hasMachineType(id) then
			throw(Errors.UnknownId, 'Unknown Machine Model or Machine Type', {
				machine = id
			});
		end
		if not self:contains(id) then
			self._content[#self._content+1] = id;
		else
			logwrn(string.format('Ignored machine registration : machine %s is already in tag %s', id, self._id));
		end
	else
		throw(Errors.TypesIncompatibility, string.format('Bad match between tag %s and item %s', self._id, id), {
			tag = self._id,
			item = id
		});
	end
	return self;
end

--- Supprime un contenu du tag.
---@param id string
---@return self self
function Tag:remove(id)
	local id = ids.ensureIdModule(id);
	local exists, firstIndex = list.contains(self._content, id);
	if exists then
		table.remove(self._content, firstIndex);
	end
	return self;
end

--- Itère sur le contenu du tag. Retourne une variable de type `string` qui
--- correspond à une entrée du tag.
---@return fun(): string
function Tag:stream()
	return list.stream(self._content);
end

--- Retourne l'identifiant complet du tag.
---@return string
function Tag:getFullyQualifiedId()
	return ids.createId(ids.Qualifier.TAG, self:getId(), nil, self:getType(), self:getModule():getId());
end

--- Retourne l'identifiant complet pour un attribut du tag.
---@param attr TagAttributes
---@return string
function Tag:getFullyQualifiedAttributeId(attr)
	return ids.createId(ids.Qualifier.TAG, self:getId(), attr, self:getType(), self:getModule():getId());
end

---Retourne le nom localisé du tag, ou `nil` si aucun nom localisé n'a pu être
--- trouvé.
---@return string?
function Tag:getLocalizedName()
	return LocalizationManager.loc(self:getFullyQualifiedAttributeId(TagAttributes.NAME));
end

---Retourne le nom localisé du tag, ou son ID si aucun nom localisé n'a pu être
--- trouvé.
---@return string
function Tag:getSafeLocalizedName()
	return LocalizationManager.safeloc(self:getFullyQualifiedAttributeId(TagAttributes.NAME));
end


return Tag;