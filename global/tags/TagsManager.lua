require('calypso.core');

local ids = require('foundation.ids');


-- *****************************************************************************
-- * TagsManager
-- *
-- *  abstract class TagsManager
-- *****************************************************************************

--- Classe statique qui gère les tags enregistrés dans le jeu.
---@class TagsManager
local TagsManager =
{
	_tags = {}; ---@type table<string, Tag>
};

--- Vérifie s'il existe un tag sous cet identifiant.
---@param id string
---@return boolean
function TagsManager.hasTag(id)
	return TagsManager._tags[ids.ensureIdModule(id)] ~= nil;
end

--- Retourne un `Tag` selon son identifiant. Si aucun tag n'existe sous cet
--- identifiant, retourne `nil`.
---@param id string
---@return Tag?
function TagsManager.getTag(id)
	local id = ids.ensureIdModule(id);
	return ternary(TagsManager.hasTag(id), TagsManager._tags[id], nil);
end

--- Fais la même chose que `getTag`, mais au lieu de retourner `nil` si le tag
--- n'existe pas, lève une erreur.
---
--- _@throws_ `Errors.UnknownTag`
---@param id string
---@return Tag
function TagsManager.getTag_nilsafe(id)
	local t = TagsManager.getTag(id);
	if not t then
		throw(Errors.UnknownTag, 'Unknown tag ' .. id);
	else
		return t;
	end
end

--- Retourne une table contenant tous les tags enregistrés dans le jeu.
---@return table<string, Tag>
function TagsManager.getAllTags()
	return TagsManager._tags;
end

--- Enregistre un nouveau tag, si aucun tag de ce module avec cet ID n'est déjà
--- enregistré. Sinon, lève une erreur.
---
--- _@throws_ `Errors.DuplicatedId`
---@param tag Tag
function TagsManager.registerTag(tag)
	local completeId = ids.createId(ids.Qualifier.TAG, tag:getId(), nil, tag:getType(), tag:getModule():getId());
	if not TagsManager.hasTag(completeId) then
		TagsManager._tags[completeId] = tag;
	else
		throw(Errors.DuplicatedId, string.format('Cannot register the tag %s, for there is already one under this ID', completeId));
	end
end

--- Compte le nombre de tags enregistrés. Si l'argument `moduleId` est présent,
--- ne compte que ceux enregistrés par ce module.
---@param moduleId? string
---@return integer
function TagsManager.countTags(moduleId)
	if not moduleId then
		return map.size(TagsManager._tags);
	else
		local i = 0;
		for _, v in pairs(TagsManager._tags) do
			if v:getModule():getId() == moduleId then
				i = i + 1;
			end
		end
		return i;
	end
end


return TagsManager;