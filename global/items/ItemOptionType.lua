require('calypso.core');


-- *****************************************************************************
-- * ItemOptionType
-- *
-- *  enum ItemOption
-- *****************************************************************************

---@enum ItemOptionType
local ItemOptionType =
{
	ENUM   = 1,
	RANGE  = 2,
	SWITCH = 3
};

--- Convertit une chaîne de caractères en `ItemOptionType`. Lève une erreur si
--- la chaîne ne peut pas être traduite.
---
--- _@throws_ `Errors.InvalidEnumValue` - La chaîne ne peut pas être traduite
---@param s string
---@return ItemOptionType
function ItemOptionType.fromString(s)
	local s = string.lower(s);
	if     s == 'enum'   then return ItemOptionType.ENUM;
	elseif s == 'range'  then return ItemOptionType.RANGE;
	elseif s == 'switch' then return ItemOptionType.SWITCH;
	else
		throw(Errors.InvalidEnumValue, 'Invalid enum value : ' .. s, {
			value = s
		});
	end
end

--- Convertit une `ItemOptionType` en chaîne de caractères.
---
--- _@throws_ `Errors.BadArgument` `i` n'est pas une valeur de `ItemOptionType`
---@param i ItemOptionType
---@return string
function ItemOptionType.toString(i)
	if     i == ItemOptionType.ENUM   then return 'enum';
	elseif i == ItemOptionType.RANGE  then return 'range';
	elseif i == ItemOptionType.SWITCH then return 'switch';
	else
		throw(Errors.BadArgument, 'i', {
			expected = 'ItemOptionType',
			got = tostring(i)
		});
	end
end


return ItemOptionType;