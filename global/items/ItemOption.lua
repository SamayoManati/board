require('calypso.core');
require('calypso.class');
local ids = require('foundation.ids');

local ItemOptionType = require('global.items.ItemOptionType');


-- *****************************************************************************
-- * ItemOption
-- *
-- *  class ItemOption
-- *****************************************************************************

--- 	(constructor) ItemOption(id: string, optType: ItemOptionType)
---
--- _@param_ `id` - L'identifiant de l'option. Doit être unique parmi les
---                 identifiants des options de l'item auquel elle est associée
---
--- _@param_ `optType` - Le type d'option, issu de l'enum `ItemOptionType`
---
--- ---
--- Représente une option d'item.
---
--- Chaque option peut être d'un des types suivants :
---
--- - **range**, possède une valeur minimale et une valeur maximale. La valeur
---              est choisie entre ces deux nombres compris. Peut accepter, ou
---              non, les nombres à virgule. L'étendue par défaut est la
---              suivante : _[0;0]_
--- - **enum**, possède une liste fermée de valeurs autorisée. La valeur est
---             choisie parmi elles. Ces valeurs sont représentées par des
---             identifiants, afin qu'elles puissent être localisées. Par
---             défaut, elle n'a aucune valeur possible.
--- - **switch**, ne peut être que vrai ou faux.
---
--- L'option peut avoir une valeur par défaut. Lorsque l'instance est créée,
--- puisqu'il n'y a aucune valeur possible, la valeur par défaut est `nil`.
---
--- Il n'y a qu'une seule classe pour tous les types d'options d'items. Elle
--- contient donc les fonctions pour tous les types. Cependant, si on utilise
--- une fonction qui n'est pas relative au type de l'option, une erreur fatale
--- sera levée, afin de ne pas laisser de doute quant à quelle fonction sert à
--- quel type d'option. Les correspondances sont les suivantes :
---
--- **Pour _range_ :**
--- - `setRange`
--- - `getRange`
--- - `getMin`
--- - `getMax`
--- - `isFloat`
---
--- **Pour _enum_ :**
--- - `getValues`
--- - `setValues`
--- - `addValue`
--- - `hasValue`
---@class ItemOption:Class
local ItemOption = class('ItemOption');

--- Constructeur de classe.
---@param id string
---@param optType ItemOptionType
function ItemOption:initialize(id, optType)
	self._id = id;
	self._type = optType;
	self._item = nil; ---@type ItemData
	self._enumValues = {}; ---@type string[]
	self._rangeMin = 0; ---@type number
	self._rangeMax = 0; ---@type number
	self._default = nil;
	self._rangeFloat = false;
end

--- Retourne l'identifiant (seulement le nom de sous-clef) de l'option.
---@return string
function ItemOption:getId()
	return self._id;
end

--- Retourne une référence vers l'item qui accueille l'option, ou `nil` si
--- l'option n'est pas accueillie par un item.
---@return ItemData?
function ItemOption:getItem()
	return self._item;
end

--- Retourne le type de l'option.
---@return ItemOptionType
function ItemOption:getType()
	return self._type;
end

--- Définit l'étendue acceptée en tant que valeur, si l'option est de type
--- _range_. Cette fonction n'est utile que si l'option est de type _range_.
--- Dans le cas contraire, elle n'aura aucun effet, et une erreur sera lancée.
---
--- _@throws_ `Errors.TypesIncompatibility` - L'option n'est pas de type
---           _range_
---@param min number Valeur minimum, ce nombre inclu
---@param max number Valeur maximum, ce nombre inclu
---@param float? boolean L'option accepte-t-elle les nombres à virgule ? Si omi, conserve l'état actuel
---@return self self
function ItemOption:setRange(min, max, float)
	if self:getType() == ItemOptionType.RANGE then
		self._rangeMin = min;
		self._rangeMax = max;
		if float ~= nil then
			self._rangeFloat = float;
		end
	else
		throw(Errors.TypesIncompatibility, 'Trying to set range on a non-range option : ' .. self._id, {
			option = self._id,
			type = ItemOptionType.toString(self._type)
		});
	end
	return self;
end

--- Retourne la valeur minimale acceptée dans une option de type _range_, ce
--- nombre compris. Si l'option n'est pas de type _range_, une erreur sera
--- jetée.
---
--- _@throws_ `Errors.TypesIncompatibility` - L'option n'est pas de type
---           _range_
---@return number
function ItemOption:getMin()
	if self:getType() == ItemOptionType.RANGE then
		return self._rangeMin;
	else
		throw(Errors.TypesIncompatibility, 'Trying to get range minimum on a non-range option : ' .. self._id, {
			option = self._id,
			type = ItemOptionType.toString(self._type)
		});
	end
end

--- Retourne la valeur maximale acceptée dans une option de type _range_, ce
--- nombre compris. Si l'option n'est pas du type _range_, une erreur sera
--- levée.
---
--- _@throws_ `Errors.TypesIncompatibility` - L'option n'est pas de type
---           _range_
---@return number
function ItemOption:getMax()
	if self:getType() == ItemOptionType.RANGE then
		return self._rangeMax;
	else
		throw(Errors.TypesIncompatibility, 'Trying to get range maximum on a non-range option : ' .. self._id, {
			option = self._id,
			type = ItemOptionType.toString(self._type)
		});
	end
end

--- Vérifie si l'option de type _range_ accepte les nombres à virgule. Si
--- l'option n'est pas du type _range_, lève une erreur.
---
--- _@throws_ `Errors.TypesIncompatibility` - L'option n'est pas de type
---           _range_
---@return boolean
function ItemOption:isFloat()
	if self:getType() == ItemOptionType.RANGE then
		return self._rangeFloat;
	else
		throw(Errors.TypesIncompatibility, 'Trying to get range-related value on a non-range option : ' .. self._id, {
			option = self._id,
			type = ItemOptionType.toString(self._type)
		});
	end
end

--- Retourne les données relatives à l'option de type _range_ : sa valeur
--- maximale (ce nombre inclu), sa valeur minimale (ce nombre inclu), et si
--- elle accepte les nombres à virgule. Si l'option n'est pas de type _range_,
--- une erreur sera levée.
---
--- _@throws_ `Errors.TypesIncompatibility` - L'option n'est pas de type
---           _range_
---@return number min
---@return number max
---@return boolean float
function ItemOption:getRange()
	if self:getType() == ItemOptionType.RANGE then
		return self._rangeMin, self._rangeMax, self._rangeFloat;
	else
		throw(Errors.TypesIncompatibility, 'Trying to get range-related values on a non-range option : ' .. self._id, {
			option = self._id,
			type = ItemOptionType.toString(self._type)
		});
	end
end

--- Retourne la liste des valeurs possibles pour l'option de type _enum_. Si
--- l'option n'est pas de type _enum_, lève une erreur.
---
--- Puisque lua ne fonctionne que par références, manipuler le retour de cette
--- méthode manipulera la liste des valeurs possibles pour l'option.
---
--- _@throws_ `Errors.TypesIncompatibility` - L'option n'est pas de type _enum_
---@return string[]
function ItemOption:getValues()
	if self:getType() == ItemOptionType.ENUM then
		return self._enumValues;
	else
		throw(Errors.TypesIncompatibility, 'Trying to get enum values on a non-enum option : ' .. self._id, {
			option = self._id,
			type = ItemOptionType.toString(self._type)
		});
	end
end

--- Ajoute une valeur possible pour l'option de type _enum_. Si l'option n'est
--- pas de type _enum_, ou qu'il existe déjà une valeur possible avec le même
--- identifiant, une erreur sera levée.
---
--- _@throws_ `Errors.TypesIncompatibility` - L'option n'est pas de type _enum_
---
--- _@throws_ `Errors.DuplicatedValue` - Cette valeur existe déjà
---@param v string
---@return self self
function ItemOption:addValue(v)
	if self:getType() == ItemOptionType.ENUM then
		if not list.contains(self._enumValues, v) then
			self._enumValues[#self._enumValues+1] = v;
		else
			throw(Errors.DuplicatedValue, string.format('There is already a %s value in the enum option %s', v, self._id), {
				option = self._id,
				value = v
			});
		end
	else
		throw(Errors.TypesIncompatibility, 'Trying to add enum value on a non-enum option : ' .. self._id, {
			option = self._id,
			type = ItemOptionType.toString(self._type)
		});
	end
	return self;
end

--- Redéfinit toutes les valeurs possibles pour l'option de type _enum_. Si
--- l'option n'est pas du type _enum_, lève une erreur.
---
--- _@throws_ `Errors.TypesIncompatibility` - L'option n'est pas de type _enum_
---@param vs string[]
---@return self self
function ItemOption:setValues(vs)
	if self:getType() == ItemOptionType.ENUM then
		self._enumValues = vs;
	else
		throw(Errors.TypesIncompatibility, 'Trying to set enum values on a non-enum option : ' .. self._id, {
			option = self._id,
			type = ItemOptionType.toString(self._type)
		});
	end
	return self;
end

--- Vérifie si l'option de type _enum_ possède une valeur avec l'identifiant
--- `v`. Si l'option n'est pas du type _enum_, jette une erreur.
---
--- _@throws_ `Errors.TypesIncompatibility` - L'option n'est pas de type _enum_
---@param v string
---@return boolean
function ItemOption:hasValue(v)
	if self:getType() == ItemOptionType.ENUM then
		return list.contains(self._enumValues, v);
	else
		throw(Errors.TypesIncompatibility, 'Trying to get enum value on a non-enum option : ' .. self._id, {
			option = self._id,
			type = ItemOptionType.toString(self._type)
		});
	end
end

--- Vérifie si `v` est une valeur valide pour l'option. `v` doit être un
--- `string` si l'option est une _enum_, un `number` si c'est un _range_, et un
--- `boolean` si c'est un _switch_. `v` sera casté en fonction de cela.
---@param v string|number|boolean
---@return boolean
function ItemOption:accepts(v)
	if self:getType() == ItemOptionType.ENUM then
		local v = tostring(v);
		return self:hasValue(v);
	elseif self:getType() == ItemOptionType.RANGE then
		local v = tonumber(v);
		if not self:isFloat() then
			if not isInteger(v) then
				return false;
			end
		end
		return v >= self:getMin() and v <= self:getMax();
	elseif self:getType() == ItemOptionType.SWITCH then
		if type(v) == "boolean" then
			return true;
		end
		local v = tostring(v):lower();
		if v == 'yes' or v == 'no' then
			return true;
		else
			return false;
		end
	end
end

--- Définit la valeur par défaut de l'option. `v` doit être valide au regard du
--- type d'option, et des restrictions définies, ou une erreur sera levée.
---
--- - Si l'option est de type **range**, `v` doit être comprise entre le maximum
---   et le minimum défini (compris). En fonction de `isFloat()`, `v` peut être,
---   ou ne doit pas être, un nombre à virgule.
--- - Si l'option est de type **enum**, `v` doit être une chaîne de caractères
---   existante dans `getValues()`.
--- - Si l'option est de type **switch**, `v` doit être une valeur booléenne.
---
--- _@throws_ `Errors.BadArgument` - `v` n'est pas du bon type, ou bien l'option
---           n'accepte pas les nombres à virgule
---
--- _@throws_ `Errors.OutOfRange` - `v` est en-dehors de l'étendue autorisée par
---           l'option
---
--- _@throws_ `Errors.InvalidEnumValue` - `v` ne fait pas partie des valeurs
---           autorisées pour l'option de type _enum_
---@param v number|string|boolean
---@return self self
function ItemOption:setDefault(v)
	if self._type == ItemOptionType.RANGE then
		local v = tonumber(v);
		if not v then
			throw(Errors.BadArgument, 'v : expected number, in option ' .. self._id, {
				option = self._id,
				expected = 'number'
			});
		end
		if v >= self._rangeMin and v <= self._rangeMax then
			if math.type(v) == "float" and not self._rangeFloat then
				throw(Errors.BadArgument, string.format('Option %s does not accepts floats', self._id));
			end
			self._default = v;
		else
			throw(Errors.OutOfRange, string.format('Expected a value un the range [%d;%d], but got %d', self._rangeMin, self._rangeMax, v), {
				option = self._id
			});
		end
	elseif self._type == ItemOptionType.ENUM then
		local v = tostring(v);
		if self:hasValue(v) then
			self._default = v;
		else
			throw(Errors.InvalidEnumValue, 'Invalid value ' .. v, {
				option = self._id,
				expected = 'One of ' .. table.concat(self._enumValues, ' | ')
			});
		end
	elseif self._type == ItemOptionType.SWITCH then
		if type(v) == "boolean" then
			self._default = v;
		else
			throw(Errors.BadArgument, 'v : expected boolean, in option ' .. self._id, {
				option = self._id,
				expected = 'boolean',
				got = type(v)
			});
		end
	end
	return self;
end

--- Retourne la valeur par défaut de l'option.
---@return boolean|string|number
function ItemOption:getDefault()
	return self._default;
end

--- Retourne l'identifiant complet de l'option. Attention, l'option doit être
--- assignée à un item, sans quoi une erreur sera levée.
---
--- _@throws_ `Errors.NilValue` - L'option n'est pas assignée à un item
---@return string
function ItemOption:getFullyQualifiedId()
	if self._item ~= nil then
		return ids.createId(ids.Qualifier.ITEM, self._item:getId(), 'option', nil, self._item:getModule():getId(), self._id);
	else
		throw(Errors.NilValue, string.format('Cannot build ID because the option %s is not assigned to an item', self._id));
	end
end

--- Retourne l'identifiant complet pour une valeur possible de l'option, si
--- cette dernière est de type _enum_. Sinon, lève une erreur. `v` doit être une
--- valeur valide, et l'option doit être assignée à un item, sans quoi une
--- erreur sera aussi levée.
---
--- _@throws_ `Errors.TypesIncompatibility` - L'option n'est pas de type _enum_
---
--- _@throws_ `Errors.NilValue` - L'option n'est pas assignée à un item
---
--- _@throws_ `Errors.InvalidEnumValue` - `v` n'existe pas dans l'option
---@param v string
function ItemOption:getFullyQualifiedEnumValueId(v)
	if self._type == ItemOptionType.ENUM then
		if self:hasValue(v) then
			if self._item ~= nil then
				return ids.createId(
					ids.Qualifier.ITEM,
					self._item:getId(),
					'option',
					nil,
					self._item:getModule():getId(),
					self._id,
					v
				);
			else
				throw(Errors.NilValue, string.format('Cannot build ID because the option %s is not assigned to an item', self._id));
			end
		else
			throw(Errors.InvalidEnumValue, string.format('Invalid value %s in option %s', v, self._id), {
				expected = 'One of ' .. table.concat(self._enumValues, ' | ')
			});
			error("[ItemOption:getFullyQualifiedEnumValueId] Invalid value " .. v .. " in option " .. self._id, 2);
		end
	else
		throw(Errors.TypesIncompatibility, 'Cannot use this method on a non-enum option', {
			option = self._id,
			type = ItemOptionType.toString(self._type)
		});
	end
end


return ItemOption;
