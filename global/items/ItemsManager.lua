require('calypso.core');
local ids = require('foundation.ids');


-- *****************************************************************************
-- * ItemsManager
-- *
-- *  abstract class ItemsManager
-- *****************************************************************************

--- Gère les items enregistrés en jeu.
---
--- Dans la documentation de cette classe, il est souvent fait référence à un
--- "item". Il s'agit d'un item du jeu, et non de la classe `Item`. Ainsi,
--- lorsqu'il est dit que la fonction "vérifie si un item est enregistré dans le
--- jeu", par exemple, cela veut dire qu'elle vérifie si un _item du jeu_ est
--- enregistré, _via_ son identifiant d'item. S'il est dit qu'une autre fonction
--- "enregistre un item", cela signifie qu'elle enregistre un _item du jeu_,
--- mais elle le fait en prenant un `ItemData`, et non un `Item`, puisque la
--- classe `ItemData` représente une abstraction de l'item de jeu, globale au
--- jeu, tandis que la classe `Item` représente un item physique, local à la
--- partie. Pour plus d'informations, se référer à la documentation de la classe
--- `ItemData`.
---
---@see ItemData
---@class ItemsManager
local ItemsManager = {};

ItemsManager._items = {}; ---@type table<string, ItemData>

--- Une fonction interne pour parser les ID.
---
--- _@throws_ `Errors.InvalidId` - L'ID n'est pas celle d'un item
---@param id string
---@return string
function ItemsManager.internalProcessId(id)
	local completeId = ids.ensureIdModule(id)
	if ids.extractId(completeId).qualifier ~= ids.Qualifier.ITEM then
		throw(Errors.InvalidId, 'Non_Item ID : ' .. id);
	end
	return completeId;
end

--- Vérifie si l'item dont on a donné l'ID existe.
---@param id string Doit être l'ID d'un item
---@return boolean
function ItemsManager.hasItem(id)
	return ItemsManager._items[ItemsManager.internalProcessId(id)] ~= nil;
end

--- Retourne un `ItemData` selon son ID. Si aucun item n'existe sous cet ID,
--- retourne `nil`.
---@param id string Doit être l'ID d'un item
---@return ItemData?
function ItemsManager.getItem(id)
	local processedId = ItemsManager.internalProcessId(id);
	if ItemsManager.hasItem(processedId) then
		return ItemsManager._items[processedId];
	end
end

--- Enregistre un item. Si un item existe déjà sous cet ID dans ce module, lève
--- une erreur.
---
--- _@throws_ `Errors.DuplicatedId` - Un item existe déjà sous cet identifiant
---@param item ItemData
function ItemsManager.registerItem(item)
	local id = ids.createId(ids.Qualifier.ITEM, item:getId(), nil, nil, item:getModule():getId());
	if not ItemsManager.hasItem(id) then
		ItemsManager._items[id] = item;
	else
		throw(Errors.DuplicatedId, 'There is already an item with the ID ' .. id);
	end
end

--- Compte le nombre d'items enregistrés.
---@param moduleId? string Si spécifié, compte seulement les items de ce module
---@return integer
function ItemsManager.countItems(moduleId)
	if not moduleId then
		return map.size(ItemsManager._items);
	else
		local i = 0;
		for k in pairs(ItemsManager._items) do
			if ids.extractId(k).module == moduleId then
				i = i + 1;
			end
		end
		return i;
	end
end


return ItemsManager;