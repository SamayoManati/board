require('calypso.core');


-- *****************************************************************************
-- * ItemType
-- *
-- *  enum ItemType
-- *****************************************************************************

---@enum ItemType
local ItemType =
{
	SOLID  = 1,
	LIQUID = 2,
	POWDER = 3
};

--- Convertit une chaîne de caractère en `ItemType`. Si la chaîne de caractères
--- ne peut pas être convertie et que `nilsafe` est vrai, lance une erreur. Si
--- elle ne peut pas être convertie et que `nilsafe` est faux, retourne `nil`.
---
--- _@throws_ `Errors.InvalidEnumValue` - `nilsafe` est vrai, et `s` ne peut pas
---           être converti en `ItemType`
---@param s string
---@param nilsafe? boolean `false` par défaut
---@return ItemType?
function ItemType.fromString(s, nilsafe)
	nilsafe = ternary(nilsafe == nil, false, nilsafe);
	local str = s:lower();
	if str == "solid" then
		return ItemType.SOLID;
	elseif str == "liquid" then
		return ItemType.LIQUID;
	elseif str == "powder" then
		return ItemType.POWDER;
	else
		if nilsafe then
			throw(Errors.InvalidEnumValue, 'Invalid enum value : ' .. str);
		end
	end
end

--- Transforme un `ItemType` en chaîne de caractères.
---
--- _@throws_ `Errors.BadArgument` - `it` n'est pas une valeur de `ItemType`
---@param it ItemType
---@return string
function ItemType.toString(it)
	if     it == ItemType.SOLID  then return 'solid';
	elseif it == ItemType.LIQUID then return 'liquid';
	elseif it == ItemType.POWDER then return 'powder';
	else
		throw(Errors.BadArgument, 'it', {
			expected = 'ItemType',
			got = type(it)
		});
	end
end


return ItemType;