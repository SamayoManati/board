require('calypso.core');
require('calypso.class');

local ids                 = require('foundation.ids');
local LocalizationManager = require('global.locales.LocalizationManager');
local ItemAttributes      = require('global.items.ItemAttributes');


-- *****************************************************************************
-- * ItemData
-- *
-- *  class ItemData
-- *****************************************************************************

--- 	(constructor) ItemData(id: string, type: ItemType, mod: Module, unit: ItemUnit, size?: number, icon?: string)
---
--- ---
--- Représente un item disponible dans le jeu.
---
--- Une instance de cette classe définit un item existant (par exemple, du fil).
--- Les items que l'on voit dans le jeu sont, quant à eux, définis par un
--- `ItemStack`, qui contient un `Item` et une quantité. Les `Item` contiennent
--- quant à eux un `ItemData` et, le cas échéant, des valeurs pour les options
--- définies dans `ItemData`. Pour reprendre l'exemple de tout à l'heure :
---
--- | `ItemData`                       | `Item`        | `ItemStack`     |
--- |----------------------------------|---------------|-----------------|
--- | Du fil. Peut être rouge ou vert. | Du fil rouge. | 12 fils rouges. |
---
--- Les `ItemData` définissent leurs options _via_ des instances de
--- `ItemOption`.
---
--- Chaque `ItemData` définit une unité de mesure, de type `ItemUnit`, ainsi
--- qu'une taille individuelle, qui s'exprime en termes de l'unité de mesure.
--- Le calcul du prix s'effectue ensuite selon la formule suivante :
--- `prix * quantité * taille`. Cela permet d'avoir des items qui se vendent
--- à l'unité, mais qui se chiffrent dans une autre unité (exemple : les
--- panneaux de MDF, qui se vendent à l'unité, mais se chiffrent au m2). Dans
--- tel cas, la taille vaudra la superficie du panneau. Dans la plupart des
--- cas de figure, la taille vaut `1`.
---@class ItemData:Class
local ItemData = class('ItemData');

---@param id string
---@param type ItemType
---@param mod Module
---@param unit ItemUnit
---@param size? number
---@param icon? string
function ItemData:initialize(id, type, mod, unit, size, icon)
	self._id = id;
	self._icon = icon or id;
	self._type = type;
	self._module = mod;
	self._unit = unit;
	self._size = size or 1.0;
	self._options = {}; ---@type table<string,ItemOption>
end

--- Retourne l'ID de l'item (uniquement le nom)
---@return string
function ItemData:getId()
	return self._id;
end

--- Retourne le nom du fichier qui sert d'icône à l'item. Retourne un nom de
--- fichier, sans extension ni chemin d'accès. Le jeu se chargera de les
--- rajouter en interne.
---@return string
function ItemData:getIcon()
	return self._icon;
end

--- Définit le nom du fichier qui sert d'icône à l'item. Prend en paramètre un
--- nom de fichier sans extension ni chemin d'accès.
---@param filename string
---@return self self
function ItemData:setIcon(filename)
	self._icon = filename;
	return self;
end

--- Retourne le type de l'item.
---@return ItemType
function ItemData:getType()
	return self._type;
end

--- Retourne le `Module` dont provient l'`ItemData`.
---@return Module
function ItemData:getModule()
	return self._module;
end

--- Retourne la liste des options de l'item.
---@return table<string, ItemOption>
function ItemData:getOptions()
	return self._options;
end

--- Vérifie si l'item possède une option avec le nom `name`.
---@param name string
---@return boolean
function ItemData:hasOption(name)
	return map.contains(self._options, name);
end

--- Obtient une option définie par l'item, si elle existe. Sinon, lève une
--- erreur.
---
--- _@throws_ `Errors.UnknownItemOption` - L'option `name` n'existe pas
---@param name string
---@return ItemOption
function ItemData:getOption(name)
	if self:hasOption(name) then
		return self._options[name];
	else
		throw(Errors.UnknownItemOption, string.format('Unknown option %s in item %s', name, self._id), {
			item = self._id,
			option = name
		});
	end
end

--- Retourne l'unité de mesure de l'item.
---@return ItemUnit
function ItemData:getUnit()
	return self._unit;
end

--- Définit l'unité de mesure de l'item.
---@param u ItemUnit
---@return self self
function ItemData:setUnit(u)
	self._unit = u;
	return self;
end

--- Retourne la taille de l'item.
---@return number
function ItemData:getSize()
	return self._size;
end

--- Définit la taille de l'item.
---@param s number
---@return self self
function ItemData:setSize(s)
	self._size = s;
	return self;
end

--- Enregistre une nouvelle option à l'item, si aucune option avec un nom
--- similaire n'est déjà enregistrée. Sinon, lève une erreur.
---
--- _@throws_ `Errors.DuplicatedId` - Une option avec un nom similaire est déjà
---           enregistrée dans cet item
---@param opt ItemOption
---@return self self
function ItemData:registerOption(opt)
	if not self:hasOption(opt:getId()) then
		self._options[opt:getId()] = opt;
		opt._item = self;
	else
		throw(
			Errors.DuplicatedId,
			string.format('There is already an option under the name %s in item %s', opt:getId(), self._id),
			{
				item = self._id,
				option = opt:getId()
			});
	end
	return self;
end

--- S'il existe une option nommée `name` dans l'item, la supprime. Sinon, lève
--- une erreur.
---
--- _@throws_ `Errors.UnknownItemOption` - L'option n'existe pas dans cet item
---@param name string
---@return self self
function ItemData:unregisterOption(name)
	if self:hasOption(name) then
		self._options[name] = nil;
	else
		throw(
			Errors.UnknownItemOption,
			string.format('Unknown option %s in item %s', name, self._id),
			{
				item = self._id,
				option = name
			});
	end
	return self;
end

--- Retourne l'identifiant complet de l'item.
---@return string
function ItemData:getFullyQualifiedId()
	return ids.createId(ids.Qualifier.ITEM, self:getId(), nil, nil, self:getModule():getId());
end

--- Retourne l'identifiant complet d'un attribut de l'item.
---@param attr ItemAttributes
---@return string
function ItemData:getFullyQualifiedAttributeId(attr)
	return ids.createId(ids.Qualifier.ITEM, self:getId(), attr, nil, self:getModule():getId());
end

--- Retourne le nom localisé de l'item, ou `nil` si aucun nom localisé n'a pu
--- être trouvé.
---@return string?
function ItemData:getLocalizedName()
	return LocalizationManager.loc(self:getFullyQualifiedAttributeId(ItemAttributes.NAME));
end

--- Retourne le nom localisé de l'item, ou son ID si aucun nom localisé n'a pu
--- être trouvé.
---@return string
function ItemData:getSafeLocalizedName()
	return LocalizationManager.safeloc(self:getFullyQualifiedAttributeId(ItemAttributes.NAME));
end

--- Retourne la description localisée de l'item, ou `nil` si aucune description
--- localisée n'a pu être trouvée.
---@return string?
function ItemData:getLocalizedDescription()
	return LocalizationManager.loc(self:getFullyQualifiedAttributeId(ItemAttributes.DESCRIPTION));
end

--- Retourne la description localisée de l'item, ou son ID si aucune description
--- localisée n'a pu être trouvée.
---@return string
function ItemData:getSafeLocalizedDescription()
	return LocalizationManager.safeloc(self:getFullyQualifiedAttributeId(ItemAttributes.DESCRIPTION));
end


return ItemData;