require('calypso.core');

-- *****************************************************************************
-- * ItemUnit
-- *
-- *  enum ItemUnit
-- *****************************************************************************

--- La liste des unités de mesure disponibles pour les items.
---@enum ItemUnit
local ItemUnit =
{
	KG = 'kg',
	LITRE = 'l',
	M1 = 'm1',
	M2 = 'm2',
	M3 = 'm3',
	UNIT = 'u'
};

--- Vérifie si la chaîne de caractères `s` est une `ItemUnit` existante.
---@param s string
---@return boolean
function ItemUnit.isValid(s)
	for _, v in pairs(ItemUnit) do
		if v == s then
			return true;
		end
	end
	return false;
end


return ItemUnit;