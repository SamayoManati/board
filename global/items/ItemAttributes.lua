-- *****************************************************************************
-- * ItemAttributes
-- *
-- *  enum ItemAttributes
-- *****************************************************************************

--- Les attributs valides pour un item.
---@enum ItemAttributes
local ItemAttributes =
{
	NAME        = 'name',
	DESCRIPTION = 'description'
};


return ItemAttributes;