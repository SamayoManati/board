require('calypso.core');
require('calypso.class');
local ids = require('foundation.ids');


-- *****************************************************************************
-- * LocalizationManager
-- *
-- *  abstract class LocalizationManager
-- *****************************************************************************

--- La classe qui sert à interagir avec les langues enregistrées.
---
--- Elle a une liste de langues activées, et, lorsque l'on demande une
--- traduction, elle va demander à chacune des langues activées, depuis l'index
--- 1, jusqu'à ce que l'une d'elles lui en fournisse une.
---@class LocalizationManager
local LocalizationManager =
{
	_locales = {}; ---@type table<string, Locale>
	_activeLocales = {}; ---@type string[]
};

--- Enregistre une nouvelle langue. Si aucune langue n'est actuellement active,
--- la fonction l'activera automatiquement.
---@param locale Locale
function LocalizationManager.registerLocale(locale)
	LocalizationManager._locales[locale:getCode()] = locale;
	if #LocalizationManager._activeLocales == 0 then
		LocalizationManager.activateLocale(locale:getCode());
	end
end

--- Vérifie s'il existe une langue enregistrée avec le code fourni.
---@param code string
---@return boolean
function LocalizationManager.hasLocale(code)
	for k in pairs(LocalizationManager._locales) do
		if k == code then
			return true;
		end
	end
	return false;
end

--- Retourne une langue enregistrée, _via_ son code. Si aucune langue n'est
--- enregistrée sous ce code, retourne `nil`.
---@see LocalizationManager.getLocale_nilsafe
---@param code string
---@return Locale?
function LocalizationManager.getLocale(code)
	return ternary(LocalizationManager.hasLocale(code), LocalizationManager._locales[code], nil);
end

--- Retourne une langue enregistrée, _via_ son code. Si aucune langue n'est
--- enregistrée sous ce code, lève une erreur.
---
--- _@throws_ `Errors.UnknownLocale`
---@see LocalizationManager.getLocale
---@param code string
---@return Locale
function LocalizationManager.getLocale_nilsafe(code)
	local i = LocalizationManager.getLocale(code);
	if not i then
		throw(Errors.UnknownLocale, 'Unknown locale : ' .. code);
	else
		return i;
	end
end

--- Retourne la table contenant toutes les langues enregistrées dans le jeu.
---@return table<string, Locale>
function LocalizationManager.getAllLocales()
	return LocalizationManager._locales;
end

--- Vérifie si la langue dont on donne le code est activée ou non. Si elle
--- l'est, retourne en premier résultat `true`, et en deuxième sa position dans
--- la pile des langues activées. Si elle ne l'est pas, ou qu'aucune langue
--- n'est enregistrée sous ce code, retourne simplement `false`.
---@param code string
---@return boolean active
---@return integer? position
function LocalizationManager.isLocaleActive(code)
	local a, b = list.contains(LocalizationManager._activeLocales, code);
	return a, b;
end

--- Ajoute une langue à la liste des langues activées, si elle ne l'est pas
--- déjà. Si aucune langue n'est enregistrée sous ce code, lève une erreur.
---
--- _@throws_ `Errors.UnknownLocale`
---@param code string
---@param index? integer
function LocalizationManager.activateLocale(code, index)
	if not LocalizationManager.hasLocale(code) then
		throw(Errors.UnknownLocale, 'Unknown locale : ' .. code);
	end
	if not LocalizationManager.isLocaleActive(code) then
		if not index then
			LocalizationManager._activeLocales[#LocalizationManager._activeLocales+1] = code;
		else
			table.insert(LocalizationManager._activeLocales, index, code);
		end
	end
end

--- Désactive une langue activée. Si aucune langue n'est enregistrée sous ce
--- code, lève une erreur.
---
--- _@throws_ `Errors.UnknownLocale`
---@param code string
function LocalizationManager.desactivateLocale(code)
	if not LocalizationManager.hasLocale(code) then
		throw(Errors.UnknownLocale, 'Unknown locale : ' .. code);
	end
	local isActive, index = LocalizationManager.isLocaleActive(code);
	if isActive then
		table.remove(LocalizationManager._activeLocales, index);
	end
end

--- Cherche dans les langues activées la clef de traduction pour l'ID donné.
--- Si la clef n'est trouvée dans aucune langue, retourne `nil`.
---@see LocalizationManager.safeloc
---@param id string
---@return string?
function LocalizationManager.loc(id)
	local localized = nil;
	local id = ids.ensureIdModule(id);
	for code in list.stream(LocalizationManager._activeLocales) do
		localized = LocalizationManager._locales[code]:get(id);
		if localized ~= nil then
			return localized;
		end
	end
end

--- Même fonction que `loc`, mais si la clef de traduction n'est pas trouvée,
--- retourne l'ID au lieu de retourner `nil`.
---@see LocalizationManager.loc
---@param id string
---@return string
function LocalizationManager.safeloc(id)
	return LocalizationManager.loc(id) or id;
end

--- Compte le nombre de langues enregistrées.
---@return integer
function LocalizationManager.countLocales()
	return map.size(LocalizationManager._locales);
end

--- Compte le nombre de langues actives.
---@return integer
function LocalizationManager.countActiveLocales()
	return #LocalizationManager._activeLocales;
end


return LocalizationManager;