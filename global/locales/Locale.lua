require('calypso.class');
require('calypso.core');


-- *****************************************************************************
-- * Locale
-- *
-- *  class Locale
-- *****************************************************************************

--- 	(constructor) Locale()
--- ---
--- Représente une langue.
---@class Locale:Class
local Locale = class('Locale');

function Locale:initialize()
	self.classname = "locales:Locale";
	self._code = "";
	self._keys = {}; ---@type table<string, string>
	self._name = "";
end

--- Définit le code de la langue (exemple : `fr_FR`).
---@param code string
---@return self self
function Locale:setCode(code)
	self._code = code;
	return self;
end

--- Retourne le code de la langue.
---@return string
function Locale:getCode()
	return self._code;
end

--- Définit le nom affichable de la langue.
---@param name string
---@return self self
function Locale:setName(name)
	self._name = name;
	return self;
end

--- Retourne le nom affichable de la langue.
---@return string
function Locale:getName()
	return self._name;
end

--- Retourne la clef de traduction de l'identifiant donné, ou `nil` si la clef
--- n'existe pas dans cette langue.
---@param id string
---@return string?
function Locale:get(id)
	return self._keys[id];
end

--- Définit la valeur de la clef de traduction pour un ID donné.
---@param id string
---@param value string
---@return self self
function Locale:set(id, value)
	self._keys[id] = value;
	return self;
end


return Locale;