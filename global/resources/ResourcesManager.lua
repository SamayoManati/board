require('calypso.core');
require('calypso.class');


-- *****************************************************************************
-- * ResourcesManager
-- *
-- *  abstract class ResourcesManager
-- *****************************************************************************

--- Gère les ressources utilisées par le jeu.
---@class ResourcesManager
local ResourcesManager =
{
	--- Les polices utilisées par le jeu.
	fonts = {
		default = 0 ---@type love.Font
	};

	--- Les informations sur chaque police du jeu. Utilisé au chargement du jeu,
	--- par `GameData.loadFonts`. Chaque entrée porte le même nom que la police
	--- qui sera créée.
	---
	--- Format de chaque entrée :
	---
	--- 	[1]: string -- Nom du fichier, sans l'extension
	--- 	[2]: integer -- Taille de la police
	--- 	[3]: string -- Hinting Mode
	--- 		| 'normal'
	--- 		| 'light'
	--- 		| 'mono'
	--- 		| 'none'
	fontsInfos = {
		default = {'default', 16, 'normal'}
	};
}


return ResourcesManager;