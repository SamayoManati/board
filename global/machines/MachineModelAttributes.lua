-- *****************************************************************************
-- * MachineModelAttributes
-- *
-- *  enum MachineModelAttributes
-- *****************************************************************************

---@enum MachineModelAttributes
local MachineModelAttributes =
{
	NAME        = 'name',
	DESCRIPTION = 'description'
};


return MachineModelAttributes;