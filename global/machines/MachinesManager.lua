require('calypso.core');
require('calypso.class');
local ids = require('foundation.ids');


-- *****************************************************************************
-- * MachinesManager
-- *
-- *  abstract class MachinesManager
-- *****************************************************************************

--- Classe statique qui gère les types de machines et les modèles de machines
--- du jeu.
---@class MachinesManager
local MachinesManager =
{
	_machinesTypes  = {}, ---@type table<string,MachineType>
	_machinesModels = {}  ---@type table<string,MachineModel>
};

--- Vérifie s'il existe un type de machine enregistrée sous cet ID.
---@param id string
---@return boolean
function MachinesManager.hasMachineType(id)
	return MachinesManager._machinesTypes[id] ~= nil;
end

--- Vérifie s'il existe un modèle de machine enregistré sous cet ID.
---@param id string
---@return boolean
function MachinesManager.hasMachineModel(id)
	return MachinesManager._machinesModels[id] ~= nil;
end

--- Retourne la `MachineType` enregistrée sous cet identifiant, ou `nil` si
--- aucun type de machine n'a été trouvé.
---@see MachinesManager.getMachineType_nilsafe
---@param id string
---@return MachineType?
function MachinesManager.getMachineType(id)
	if MachinesManager.hasMachineType(id) then
		return MachinesManager._machinesTypes[id];
	end
end

--- Retourne la `MachineType` enregistrée sous cet ID. Si aucun type de machine
--- n'a pu être trouvé, lève une erreur.
---
--- _@throws_ `Errors.UnknownMachineType`
---@see MachinesManager.getMachineType
---@param id string
---@return MachineType
function MachinesManager.getMachineType_nilsafe(id)
	local uwu = MachinesManager.getMachineType(id);
	if not uwu then
		throw(Errors.UnknownMachineType, 'Unknown machine type : ' .. id);
	else
		return uwu;
	end
end

--- Retourne le `MachineModel` enregistré sous cet ID. Si aucun modèle de
--- machines n'a pu être trouvé, retourne `nil`.
---@see MachinesManager.getMachineModel_nilsafe
---@param id string
---@return MachineModel?
function MachinesManager.getMachineModel(id)
	local id = ids.ensureIdModule(id);
	if MachinesManager.hasMachineModel(id) then
		return MachinesManager._machinesModels[id];
	end
end

--- Retourne le `MachineModel` enregistré sous cet ID. Si aucun modèle de
--- machine n'a pu être trouvé, lève une erreur.
---
--- _@throws_ `Errors.UnknownMachineModel`
---@see MachinesManager.getMachineModel
---@param id string
---@return MachineModel
function MachinesManager.getMachineModel_nilsafe(id)
	local awa = MachinesManager.getMachineModel(id);
	if not awa then
		throw(Errors.UnknownMachineModel, 'Unknown machine model : ' .. id);
	else
		return awa;
	end
end

--- Enregistre un nouveau type de machine, si aucun n'est encore enregistré
--- sous son ID.
---@param mtype MachineType Le type de machine
function MachinesManager.registerMachineType(mtype)
	local builtId = ids.createId(ids.Qualifier.MACHINE_TYPE, mtype:getId(), nil, nil, mtype:getModule():getId());
	if not MachinesManager.hasMachineType(builtId) then
		MachinesManager._machinesTypes[builtId] = mtype;
	end
end

--- Enregistre un nouveau modèle de machine, si aucun n'est encore enregistré
--- sous son ID.
---@param mmodel MachineModel Le modèle de machine à enregistrer
function MachinesManager.registerMachineModel(mmodel)
	local builtId = ids.createId(ids.Qualifier.MACHINE_MODEL, mmodel:getId(), nil, nil, mmodel:getModule():getId());
	if not MachinesManager.hasMachineModel(builtId) then
		MachinesManager._machinesModels[builtId] = mmodel;
	end
end

--- Supprime un type de machine puis le retourne, s'il est enregistré. S'il
--- ne l'est pas, retourne `nil`.
---@param id string
---@return MachineType?
function MachinesManager.unregisterMachineType(id)
	if MachinesManager.hasMachineType(id) then
		local r = MachinesManager._machinesTypes[id];
		MachinesManager._machinesTypes[id] = nil;
		return r;
	end
end

--- Supprime un modèle de machine puis le retourne, s'il est enregistré. S'il
--- ne l'est pas, retourne `nil`.
---@param id string
---@return MachineModel?
function MachinesManager.unregisterMachineModel(id)
	if MachinesManager.hasMachineModel(id) then
		local r = MachinesManager._machinesModels[id];
		MachinesManager._machinesModels[id] = nil;
		return r;
	end
end

--- Compte le nombre de `MachineType` enregistrés.
---@param moduleId? string Si spécifié, restreint le décompte aux types de machines provenant de ce module
---@return integer
function MachinesManager.countMachinesType(moduleId)
	if not moduleId then
		return map.size(MachinesManager._machinesTypes);
	else
		local i = 0;
		for _, v in pairs(MachinesManager._machinesTypes) do
			if v:getModule():getId() == moduleId then
				i = i + 1;
			end
		end
		return i;
	end
end

--- Compte le nombre de `MachineModel` enregistrés. Si le paramètre `moduleId`
--- est spécifié, le décompte sera restreint aux modèles de machines provenant
--- de ce module.
---@param moduleId string
---@return integer
function MachinesManager.countMachinesModels(moduleId)
	if not moduleId then
		return map.size(MachinesManager._machinesModels);
	else
		local i = 0;
		for _, v in pairs(MachinesManager._machinesModels) do
			if v:getModule():getId() == moduleId then
				i = i + 1;
			end
		end
		return i;
	end
end


return MachinesManager;
