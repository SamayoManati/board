-- *****************************************************************************
-- * MachineTypeAttributes
-- *
-- *  enum MachineTypeAttributes
-- *****************************************************************************

---@enum MachineTypeAttributes
local MachineTypeAttributes =
{
	NAME        = 'name',
	DESCRIPTION = 'description'
};


return MachineTypeAttributes;