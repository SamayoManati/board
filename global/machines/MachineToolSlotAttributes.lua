-- *****************************************************************************
-- * MachineToolSlotAttributes
-- *
-- *  enum MachineToolSlotAttributes
-- *****************************************************************************

---@enum MachineToolSlotAttributes
local MachineToolSlotAttributes =
{
	NAME        = 'name',
	DESCRIPTION = 'description'
};


return MachineToolSlotAttributes;