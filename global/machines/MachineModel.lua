require('calypso.core');
require('calypso.class');
local LocalizationManager    = require('global.locales.LocalizationManager');
local ids                    = require('foundation.ids');
local MachineModelAttributes = require('global.machines.MachineModelAttributes');


-- *****************************************************************************
-- * MachineModel
-- *
-- *  class MachineModel
-- *****************************************************************************

--- 	(constructor) MachineModel(id: string, mod: Module, machineType: MachineType)
---
--- *@param* `id` - L'ID unique du modèle (exemple : `"martin_ft17"`) <br/>
--- *@param* `mod` - Le module qui ajoute ce modèle de machines       <br/>
--- *@param* `machineType` - Le type de machine
---
--- ---
--- Représente un modèle de machine (exemple : Martin FT-17). Chaque modèle de
--- machine appartient à un `MachineType` (exemple : dégauchisseuse).
---
---@see MachineType
---@class MachineModel:Class
local MachineModel = class('MachineModel');

--- Constructeur de classe.
---@param id string L'ID unique du modèle (exemple : `"martin_ft17"`)
---@param mod Module Le module qui ajoute ce modèle de machines
---@param machineType MachineType Le type de machine
function MachineModel:initialize(id, mod, machineType)
	self._id = id;
	self._module = mod;
	self._machineType = machineType;
	self._price = 0;
end

--- Retourne l'identifiant du modèle.
---@return string
function MachineModel:getId()
	return self._id;
end

--- Retourne le module qui a ajouté ce modèle de machine.
---@return Module
function MachineModel:getModule()
	return self._module;
end

--- Retourne le type de machine auquel appartient le modèle.
---@return MachineType
function MachineModel:getMachineType()
	return self._machineType;
end

--- Retourne le prix d'achat de base de la machine. Notez que ce prix pourra
--- être modifié par plusieurs facteurs comme les réductions ou certains perks.
---@return number
function MachineModel:getPrice()
	return self._price;
end

--- Définit le prix d'achat de base de la machine. Notez que ce prix pourra
--- être modifié par plusieurs facteurs comme les réductions ou certains perks.
---@param price number
---@return self self
function MachineModel:setPrice(price)
	self._price = price;
	return self;
end

--- Retourne l'identifiant complet du modèle de machines.
---@return string
function MachineModel:getFullyQualifiedId()
	return ids.createId(ids.Qualifier.MACHINE_MODEL, self:getId(), nil, nil, self:getModule():getId());
end

--- Retourne l'identifiant complet pour un attribut du modèle de machines.
---@param attr MachineModelAttributes
---@return string
function MachineModel:getFullyQualifiedAttributeId(attr)
	return ids.createId(ids.Qualifier.MACHINE_MODEL, self:getId(), attr, nil, self:getModule():getId());
end

--- Retourne le nom localisé du modèle de machine, ou `nil` si aucun nom
--- localisé n'a pu être trouvé.
---@return string?
function MachineModel:getLocalizedName()
	return LocalizationManager.loc(self:getFullyQualifiedAttributeId(MachineModelAttributes.NAME));
end

--- Retourne le nom localisé du modèle de machine, ou son ID si aucun nom
--- localisé n'a pu être trouvé.
---@return string
function MachineModel:getSafeLocalizedName()
	return LocalizationManager.safeloc(self:getFullyQualifiedAttributeId(MachineModelAttributes.NAME));
end

--- Retourne la description localisée du modèle de machine, ou `nil` si aucune
--- description localisée n'a pu être trouvée.
---@return string?
function MachineModel:getLocalizedDescription()
	return LocalizationManager.loc(self:getFullyQualifiedAttributeId(MachineModelAttributes.DESCRIPTION));
end

--- Retourne la description localisée du modèle de machine, ou son ID si aucune
--- description localisée n'a pu être trouvée.
---@return string
function MachineModel:getSafeLocalizedDescription()
	return LocalizationManager.safeloc(self:getFullyQualifiedAttributeId(MachineModelAttributes.DESCRIPTION));
end


return MachineModel;