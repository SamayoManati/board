require('calypso.core');
require('calypso.class');
local LocalizationManager = require('global.locales.LocalizationManager');
local ids = require('foundation.ids');
local MachineTypeAttributes = require('global.machines.MachineTypeAttributes');


-- *****************************************************************************
-- * MachineType
-- *
-- *  class MachineType
-- *****************************************************************************

--- 	(constructor) MachineType(id: string, mod: Module)
---
--- *@param* `id` - L'identifiant unique du type de machines <br/>
--- *@param* `mod` - Le module qui a défini ce type de machines
--- 
--- ---
--- Chaque instance représente un type de machine (exemple : dégauchisseuse).
--- Les modèles de machines appartiennent chacun à un type de machines.
---
---@see MachineModel
---@class MachineType:Class
local MachineType = class('MachineType');

--- Constructeur de classe.
---@param id string L'identifiant unique du type de machines
---@param mod Module Le module qui a défini ce type de machines
function MachineType:initialize(id, mod)
	self._id = id;
	self._module = mod;
	self._operators = 1;
	self._tools = {}; ---@type table<string,MachineToolSlot>
end

--- Retourne l'identifiant unique du type de machine. Ne contient que le nom.
---@return string
function MachineType:getId()
	return self._id;
end

--- Permet d'accéder au module duquel provient ce type de machines.
---@return Module
function MachineType:getModule()
	return self._module;
end

--- Retourne le nombre d'opérateurs nécessaires au fonctionnement de ce type de
--- machines.
---@return integer
function MachineType:getOperators()
	return self._operators;
end

--- Définit le nombre d'opérateurs nécessaires au fonctionnement de ce type de
--- machines.
---@param op integer
---@return self self
function MachineType:setOperators(op)
	self._operators = op;
	return self;
end

--- Vérifie si le type de machine possède un emplacement d'outil avec un ID
--- donné.
---@param id string
---@return boolean
function MachineType:hasToolSlot(id)
	return self._tools[id] ~= nil;
end

--- Retourne l'emplacement d'outil portant l'ID donné, ou `nil` si l'emplacement
--- n'existe pas.
---@param id string
---@return MachineToolSlot?
function MachineType:getToolSlot(id)
	return self._tools[id] or nil;
end

--- Retourne l'emplacement d'outil portant l'ID donné. Si cet emplacement
--- n'existe pas, lève une erreur.
---
--- _@throws_ `Errors.UnknownId`
---@param id string
---@return MachineToolSlot
function MachineType:getToolSlot_nilsafe(id)
	local t = self:getToolSlot(id);
	if not t then
		throw(Errors.UnknownId, 'Unknown tool slot : ' .. id);
	else
		return t;
	end
end

--- Ajoute un emplacement d'outil au type de machine, si celui-ci n'existe pas
--- déjà.
---@param toolslot MachineToolSlot
---@return self self
function MachineType:addToolSlot(toolslot)
	if not self:hasToolSlot(toolslot:getId()) then
		self._tools[toolslot:getId()] = toolslot;
	end
	return self;
end

--- Supprime un emplacement d'outil, s'il existait, et retourne le
--- `MachineToolSlot` supprimé. S'il n'existait pas, retourne `nil`.
---@param id string
---@return MachineToolSlot?
function MachineType:removeToolSlot(id)
	if self:hasToolSlot(id) then
		local uwu = self._tools[id];
		self._tools[id] = nil;
		return uwu;
	end
end

--- Retourne l'identifiant complet du type de machine.
---@return string
function MachineType:getFullyQualifiedId()
	return ids.createId(ids.Qualifier.MACHINE_TYPE, self:getId(), nil, nil, self:getModule():getId());
end

--- Retourne l'identifiant complet d'un attribut du type de machine.
---@param attr MachineTypeAttributes
---@return string
function MachineType:getFullyQualifiedAttributeId(attr)
	return ids.createId(ids.Qualifier.MACHINE_TYPE, self:getId(), attr, nil, self:getModule():getId());
end

--- Retourne le nom localisé du type de machines, ou `nil` si le nom n'existe
--- pas.
---@return string?
function MachineType:getLocalizedName()
	return LocalizationManager.loc(self:getFullyQualifiedAttributeId(MachineTypeAttributes.NAME));
end

--- Retourne le nom localisé du type de machines, ou son ID si aucun nom
--- localisé n'a pu être trouvée.
---@return string
function MachineType:getSafeLocalizedName()
	return LocalizationManager.safeloc(self:getFullyQualifiedAttributeId(MachineTypeAttributes.NAME));
end

--- Retourne la description localisée pour ce type de machines, ou `nil` si
--- aucune description localisée n'a pu être trouvée.
---@return string?
function MachineType:getLocalizedDescription()
	return LocalizationManager.loc(self:getFullyQualifiedAttributeId(MachineTypeAttributes.DESCRIPTION));
end

--- Retourne la description localisée pour ce type de machines, ou son ID si
--- aucune description localisée n'a pu être trouvée.
---@return string
function MachineType:getSafeLocalizedDescription()
	return LocalizationManager.safeloc(self:getFullyQualifiedAttributeId(MachineTypeAttributes.DESCRIPTION));
end


return MachineType;