require('calypso.class');
require('calypso.core');
local LocalizationManager = require('global.locales.LocalizationManager');
local MachineToolSlotAttributes = require('global.machines.MachineToolSlotAttributes');
local ids = require('foundation.ids');
require('foundation.log');


-- *****************************************************************************
-- * MachineToolSlot
-- *
-- *  class MachineToolSlot
-- *****************************************************************************

--- 	(constructor) MachineToolSlot(id: string, mtype: MachineType)
--- ---
--- Un slot d'outil, dans une machine. Utilisé dans les `MachineType` et les
--- `MachineModel`.
---@class MachineToolSlot:Class
local MachineToolSlot = class('MachineToolSlot');

--- Constructeur de classe.
---@param id string
---@param mtype MachineType
function MachineToolSlot:initialize(id, mtype)
	self._id = id;
	self._accepts = {}; ---@type (Tag|ItemData)[]
	self._amount = 1;
	self._machineType = mtype;
end

--- Retourne l'ID du slot de l'outil (exemple : `"cutter"`).
---@return string
function MachineToolSlot:getId()
	return self._id;
end

--- Retourne le nombre d'outils identiques à monter sur ce slot.
---@return integer
function MachineToolSlot:getAmount()
	return self._amount;
end

--- Définit le nombre d'outils identiques à monter sur ce slot.
---@param a integer
---@return self self
function MachineToolSlot:setAmount(a)
	self._amount = a;
	return self;
end

--- Retourne une référence sur le `MachineType` qui héberge cet emplacement
--- d'outil.
---@return MachineType
function MachineToolSlot:getMachineType()
	return self._machineType;
end

--- Vérifie si un `Item` est accepté dans ce slot.
---@param it ItemData
---@return boolean
function MachineToolSlot:doesAccepts(it)
	for entry in list.stream(self._accepts) do
		if entry == it then
			return true;
		end

		--if entry:instanceof(tags.Tag) then
		if entry.class.name == "Tag" then -- pour éviter les références circulaires entre tags.lua et machines.lua
			---@cast entry Tag
			if entry:contains(it:getFullyQualifiedId()) then
				return true;
			end
		end
	end
	return false;
end

--- Ajoute un `Item` ou un `Tag` d'items dans la liste des items acceptés dans
--- ce slot, s'il n'est pas déjà accepté.
---@param subject ItemData|Tag
---@return self self
function MachineToolSlot:accept(subject)
	for x in list.stream(self._accepts) do
		if subject == x then
			return self;
		end
	end
	self._accepts[#self._accepts+1] = subject;
	return self;
end

--- Retourne l'identifiant complet de l'emplacement d'outil.
---@return string
function MachineToolSlot:getFullyQualifiedId()
	return ids.createId(ids.Qualifier.MACHINE_TYPE, self:getMachineType():getId(), 'tool', nil, self:getMachineType():getModule():getId(), self:getId());
end

--- Retourne l'identifiant complet pour un attribut de l'emplacement d'outil.
---@param attr MachineToolSlotAttributes
---@return string
function MachineToolSlot:getFullyQualifiedAttributeId(attr)
	return ids.createId(ids.Qualifier.MACHINE_TYPE, self:getMachineType():getId(), 'tool', nil, self:getMachineType():getModule():getId(), self:getId(), nil, attr);
end

--- Retourne le nom localisé de l'emplacement d'outil, ou `nil` si aucun nom
--- localisé n'a pu être trouvé.
---@return string?
function MachineToolSlot:getLocalizedName()
	return LocalizationManager.loc(self:getFullyQualifiedAttributeId(MachineToolSlotAttributes.NAME));
end

--- Retourne le nom localisé de l'emplacement d'outil, ou son ID si aucun nom
--- localisé n'a pu être trouvé.
---@return string
function MachineToolSlot:getSafeLocalizedName()
	return LocalizationManager.safeloc(self:getFullyQualifiedAttributeId(MachineToolSlotAttributes.NAME));
end

--- Retourne la description localisée de l'emplacement d'outil, ou `nil` si
--- aucune description localisée n'a pu être trouvée.
---@return string?
function MachineToolSlot:getLocalizedDescription()
	return LocalizationManager.loc(self:getFullyQualifiedAttributeId(MachineToolSlotAttributes.DESCRIPTION));
end

--- Retourne la description localisée de l'emplacement d'outil, ou son ID si
--- aucune description localisée n'a pu être trouvée.
---@return string
function MachineToolSlot:getSafeLocalizedDescription()
	return LocalizationManager.safeloc(self:getFullyQualifiedAttributeId(MachineToolSlotAttributes.DESCRIPTION));
end


return MachineToolSlot;