require('calypso.class');


-- *****************************************************************************
-- * Module
-- *
-- *  class Module
-- *****************************************************************************

--- 	(constructor) Module(id: string)
--- ---
--- Représente un module.
---@class Module:Class
local Module = class('Module');

---@param id string
function Module:initialize(id)
	self._id = id;
	self._name = "";
	self._authors = {}; ---@type string[]
	self._version = "";
end

--- Définit l'identifiant du module. Il doit être unique parmi tous les modules
--- chargés, ne contenir que des caractères alphanumériques, des tirets et
--- des underscores, et faire au moins 1 caractère.
---@param id string
---@return self self
function Module:setId(id)
	self._id = id;
	return self;
end

--- Retourne l'identifiant du module.
---@return string
function Module:getId()
	return self._id;
end

--- Définit le nom du module, tel qu'il sera affiché en jeu.
---@param name string
---@return self self
function Module:setName(name)
	self._name = name;
	return self;
end

--- Retourne le nom du module, tel qu'il sera affiché dans le jeu.
---@return string
function Module:getName()
	return self._name;
end

--- Ajoute un.e aut.eur.rice à la liste.
---@param author string
---@return self self
function Module:addAuthor(author)
	self._authors[#self._authors+1] = author;
	return self;
end

--- Fournit une liste qui sera la liste des aut.eur.rice.s.
---@param authors string[]
---@return self self
function Module:setAuthors(authors)
	self._authors = authors;
	return self;
end

--- Retourne la liste des auteurs du module.
---@return string[]
function Module:getAuthors()
	return self._authors;
end

--- Définit la version du module.
---@param version string
---@return self self
function Module:setVersion(version)
	self._version = version;
	return self;
end

--- Retourne la version du module.
---@return string
function Module:getVersion()
	return self._version;
end


return Module;