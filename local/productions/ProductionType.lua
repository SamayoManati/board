-- *****************************************************************************
-- * ProductionType
-- *
-- *  enum ProductionType
-- *****************************************************************************

--- La liste des types de production possibles.
---@enum ProductionType
local ProductionType =
{
	--- Production à l'unité : 1 exemplaire
	UNIT = 1,
	--- Production en série : 2 exemplaires ou plus
	SERIAL = 2,
	--- Production à la chaîne : en continu
	CONTINUOUS = 3,
	--- Par période : produit pendant un laps de temps donné
	TIMED = 4
};


return ProductionType;