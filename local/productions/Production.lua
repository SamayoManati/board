require('foundation.log');
require('calypso.core');
require('calypso.class');
local ProductionType = require('local.productions.ProductionType');
local Item = require('local.items.Item');
local ItemStack = require('local.items.ItemStack');

-- *****************************************************************************
-- * Production
-- *
-- *  class Production
-- *****************************************************************************

--- 	class Production
--- 		(constructor) Production(recipe: Recipe, machine: Machine, productionType: ProductionType)
---
--- _@param_ `recipe` - La recette associée à la production
---
--- _@param_ `machine` - La machine associée à la production
---
--- _@param_ `productionType` - Le type de production
---
--- ---
--- Représente une production en cours.
---@class Production:Class
local Production = class('Production');

--- Constructeur de classe.
---@param recipe Recipe
---@param machine Machine
---@param productionType ProductionType
function Production:initialize(recipe, machine, productionType)
	Production.super.initialize(self);

	-- Variables relatives à la production
	self._uuid = uuid();
	self._recipe = recipe;
	self._machine = machine;
	self._machine._currentProduction = self;
	self._type = productionType;
	self._inputsSources = {}; ---@type table<string, [Machine|Warehouse, ItemStack]> id de l'input = [où le prendre, dans quel stack taper]
	self._active = true;

	-- Variables relatives au type de production
	self._amountToProduce = ternary(self._type == ProductionType.UNIT, 1, 0); --- La quantité d'exemplaires à produire
	self._productionDuration = 0; --- Pendant combien de temps, en secondes, la production doit-elle durer

	-- Variables relatives à l'exécution de la production
	self._amountProduced = 0; --- La quantité d'exemplaires fabriqués depuis le début de la production
	self._productionSince = 0; --- Le temps au moment où on a commencé la production
	self._loopStartTime = 0; --- Le temps au moment où on a commencé la boucle actuelle de production
	self._ended = false; --- La production est-elle terminée ?
	self.__started = false; --- La production a-t-elle commencée ? Utilisée uniquement en interne
end

--- Retourne l'UUID de la production.
---@return string
function Production:getUUID()
	return self._uuid;
end

--- Définit la quantité d'exemplaires à produire. Doit être définie pour les
--- productions de type `ProductionType.SERIAL`.
---@param a integer
---@return self self
function Production:setAmountToProduce(a)
	self._amountToProduce = a;
	return self;
end

--- Retourne la quantité d'exemplaires à produire. Utilisée par les productions
--- de type `ProductionType.SERIAL`.
---@return integer
function Production:getAmountToProduce()
	return self._amountToProduce;
end

--- Définit pendant combien de temps, en secondes, la production doit-elle
--- durer. Doit être définie pour les productions de type
--- `ProductionType.TIMED`.
---@param seconds number
---@return self self
function Production:setDuration(seconds)
	self._productionDuration = seconds;
	return self;
end

--- Retourne pendant combien de temps, en secondes, la production doit durer.
--- Utilisée par les productions de type `ProductionType.TIMED`.
---@return number
function Production:getDuration()
	return self._productionDuration;
end

--- Retourne le temps de départ de la production. Est utilisée par les
--- productions de type `ProductionType.TIMED`. Peut être utilisée pour calculer
--- combien de temps il reste à la production, ou depuis combien de temps elle
--- tourne.
---@return number
function Production:getProductionSince()
	return self._productionSince;
end

--- Retourne la recette associée à la production.
---@return Recipe
function Production:getRecipe()
	return self._recipe;
end

--- Retourne la machine associée à la production.
---@return Machine
function Production:getMachine()
	return self._machine;
end

--- Retourne le type de production.
---@return ProductionType
function Production:getType()
	return self._type;
end

--- Définit où aller chercher l'input représenté par son ID, et dans quel stack
--- taper.
---@param inputID string
---@param source Machine|Warehouse
---@param stack ItemStack
---@return self self
function Production:setSourceForInput(inputID, source, stack)
	self._inputsSources[inputID] = {source, stack};
	return self;
end

--- Retourne l'endroit où aller chercher l'input représenté par son ID, et dans
--- quel stack taper. Si l'ID n'existe pas dans cette production, retourne
--- `nil`.
---@param inputID string
---@return [(Machine|Warehouse), ItemStack]?
function Production:getSourceForInput(inputID)
	return self._inputsSources[inputID];
end

--- Retourne le temps qu'il sera, en secondes, quand la boucle de production
--- actuelle sera terminée.
---@return number
function Production:getEndTime()
	return self._loopStartTime + self._recipe:getProcessingTime();
end

--- Retourne si la production est active ou non. Une production inactive est
--- comme "mise sur pause" : elle existe toujours, mais elle ne produit rien.
---
--- La mise en pause est implémentée au niveau de la classe `Factory`, pour
--- éviter qu'une sous-classe de `Production` n'outrepasse ce code et ne puisse
--- ainsi pas être mise en pause.
---@return boolean
function Production:isActive()
	return self._active;
end

--- Définit si la production est active ou non. Une production inactive est
--- comme "mise sur pause" : elle existe toujours, mais elle ne produit rien.
---@param a boolean
---@return self self
function Production:setActive(a)
	self._active = a;
	return self;
end

--- Retourne si la production est terminée. Cet interrupteur est modifié par la
--- production quand elle détecte qu'elle est terminée (temps écoulé pour une
--- production en temps limité, ou nombre d'exemplaires à produire atteint). La
--- méthode retourne également `false` si la production n'a jamais démarrée.
---
--- De même que pour la pause, la vérification de la fin d'une production est
--- implémentée au niveau de l'usine, afin d'alléger l'exécution, le code, et
--- d'empêcher une sous-classe de `Production` de ne jamais pouvoir se terminer
--- eut-on oubliés d'implémenter la vérification.
---@return boolean
function Production:isEnded()
	return self._ended or not self.__started;
end

--- Démarre la production depuis zéro. Utilisée pour relancer une production
--- qui s'est terminée, ou pour la lancer pour la première fois après sa
--- création. Pour que la méthode fonctionne, la production doit être à l'arrêt,
--- ou n'avoir jamais été lancée. Sinon, il ne se passera rien.
function Production:startProduction()
	if self:isEnded() then
		self._amountProduced = 0;
		self._productionSince = love.timer.getTime();

		if self._ended then self._ended = false; end
		if not self.__started then self.__started = true; end
		self:_startProductionLoop();
	end
end

--- Termine la production immédiatement, sans attendre la fin de la boucle de
--- production. Les inputs engagés seront détruits.
function Production:endProduction()
	self._ended = true;
end

--- Fonction appelée internalement pour mettre fin à une boucle de production.
function Production:_endProductionLoop()
	self._amountProduced = self._amountProduced + 1;

	for output in list.stream(self._recipe:getOutputs()) do
		local diceRoll = math.random();
		for a in output:stream() do
			local qtLow, qtHigh = a:getValue();
			local qt = math.random(qtLow, qtHigh);
			if diceRoll <= a:getPercentage() then
				local obtainedItem = Item(output:getItem()); ---@type Item
				for optionName, optionValue in pairs(output:getOptions()) do
					obtainedItem:setOptionValue(optionName, optionValue);
				end
				local obtainedItemStack = ItemStack(obtainedItem, qt); ---@type ItemStack
				if not self._machine:getOutputStorage():add(obtainedItemStack) then
					-- On ne peut pas insérer le stack dans l'inventaire de sortie, qui est probablement complet.
					-- il faudra envoyer un message au joueur et mettre la production en pause
				end
				break;
			end
		end
	end
end

--- Fonction appelée internalement pour démarrer une boucle de production.
function Production:_startProductionLoop()
	-- On va prendre les inputs là où il faut
	-- On exclue les inputs persistants
	for input in list.fstream(self._recipe:getInputs(), function (i)
		return not i:isPersistent();
	end) do
		local inputID = input:getId();
		local inputAmount = input:getAmount();
		self._inputsSources[inputID][2]:sub(inputAmount);
	end
	self._loopStartTime = love.timer.getTime();
end

--- Est appelée à chaque update, pour faire tourner la production.
function Production:update()
	if love.timer.getTime() >= self:getEndTime() then
		-- Fin de la boucle de production
		self:_endProductionLoop();
		if self._type == ProductionType.UNIT then
			-- Fin de la production !
			self:endProduction();
		elseif self._type == ProductionType.SERIAL then
			if self._amountProduced < self._amountToProduce then
				self:_startProductionLoop();
			else
				-- Fin de la production !
				self:endProduction();
			end
		elseif self._type == ProductionType.CONTINUOUS then
			self:_startProductionLoop();
		elseif self._type == ProductionType.TIMED then
			if love.timer.getTime() < self._productionSince + self._productionDuration then
				self:_startProductionLoop();
			else
				-- Fin de la production !
				self:endProduction();
			end
		end
	end
end


return Production;