require('calypso.core');
require('foundation.log');

-- *****************************************************************************
-- * Game
-- *
-- *  abstract class Game
-- *****************************************************************************

--- 	abstract class Game
--- Représente la partie en cours.
---@class Game
local Game =
{
	_playerMoney = 0;
	_playerName = '';
	_factories = {}; ---@type Factory[]
}

--- Retourne l'argent que possède le joueur.
---@return number
function Game.getPlayerMoney()
	return Game._playerMoney;
end

--- Modifie la quantité d'argent que possède le joueur.
---@param money number
function Game.setPlayerMoney(money)
	Game._playerMoney = money;
end

--- Retourne le nom du joueur.
---@return string
function Game.getPlayerName()
	return Game._playerName;
end

--- Modifie le nom du joueur.
---@param name string
function Game.setPlayerName(name)
	Game._playerName = name;
end

--- Retourne la liste des usines du joueur.
---@return Factory[]
function Game.getFactories()
	return Game._factories;
end

--- Raccourci pour `list.stream(Game.getFactories())`.
---@return fun():Factory
function Game.streamFactories()
	return list.stream(Game._factories);
end

--- Vérifie s'il existe une usine avec l'UUID fourni.
---@param uid string
---@return boolean
function Game.hasFactory(uid)
	for factory in Game.streamFactories() do
		if factory:getUUID() == uid then
			return true;
		end
	end
	return false;
end

--- Retourne l'usine ayant l'UUID fourni. Si elle n'existe pas, retourne
--- `nil`.
---@param uid string
---@return Factory?
function Game.getFactory(uid)
	for i = 1, #Game._factories do
		if Game._factories[i]:getUUID() == uid then
			return Game._factories[i];
		end
	end
end

--- Enregistre une usine.
---@param f Factory
function Game.registerFactory(f)
	Game._factories[#Game._factories+1] = f;
end

--- Doit être appelée à chaque tick pour permettre au jeu de fonctionner.
function Game.update()
	for factory in Game.streamFactories() do
		factory:update();
	end
end


return Game;