require('calypso.core');
require('calypso.class');

-- *****************************************************************************
-- * MachineTool
-- *
-- *  class MachineTool
-- *****************************************************************************

--- 	(constructor) MachineTool(slot: MachineToolSlot, item: Item)
--- ---
--- Cette classe représente un outil de machine dans son emplacement.
---
--- Chaque fois que l'outil est utilisé, on doit appeler sa méthode `use`.
---
---@see MachineToolSlot
---@class MachineTool:Class
local MachineTool = class('MachineTool');

--- Constructeur de classe.
---@param slot MachineToolSlot
---@param item Item
function MachineTool:initialize(slot, item)
	self._slot = slot;
	self._item = item;
	self._uses = 0;
	self._broken = false;
	self._maxUses = 0;
end

--- Retourne l'emplacement auquel est monté l'outil.
---@return MachineToolSlot
function MachineTool:getToolSlot()
	return self._slot;
end

--- Retourne le nombre d'utilisations de l'outil.
---@return number
function MachineTool:getUses()
	return self._uses;
end

--- Modifie le nombre d'utilisations de l'outil. Attention, modifier ce nombre
--- manuellement impacte la durée de vie de l'outil.
---@param uses number
---@return self self
function MachineTool:setUses(uses)
	self._uses = uses;
	return self;
end

--- Définit le nombre d'utilisations de l'outil avant que celui-ci ne se casse.
--- `amount` doit être un nombre entier supérieur ou égal à 0. S'il vaut 0,
--- l'outil ne se cassera jamais.
---@param amount integer `amount >= 0`
---@return self self
function MachineTool:setMaxUses(amount)
	self._maxUses = math.abs(amount);
	return self;
end

--- Retourne le nombre d'utilisations de l'outil avant que celui-ci ne se casse.
---@return integer # `>= 0`
function MachineTool:getMaxUses()
	return self._maxUses;
end

--- Vérifie si l'outil est cassé ou non.
---@return boolean
function MachineTool:getBroken()
	return self._broken;
end

--- Définit si l'outil est cassé ou non.
---@param b boolean
---@return self self
function MachineTool:setBroken(b)
	self._broken = b;
	return self;
end

--- Retourne l'item utilisé comme outil.
---@return Item
function MachineTool:getItem()
	return self._item;
end

--- Cette fonction doit être appelée chaque fois que l'outil est utilisé.
function MachineTool:use()
	if not self:getBroken() then
		self._uses = self._uses + 1.0;
		if self._maxUses > 0 and self._uses > self._maxUses then
			self:setBroken(true);
		end
	end
end


return MachineTool;