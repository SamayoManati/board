require('calypso.core');
require('calypso.class');
local Inventory = require('global.inventories.Inventory');

-- *****************************************************************************
-- * Machine
-- *
-- *  class Machine
-- *****************************************************************************

--- 	class Machine
--- 		(constructor) Machine(model: MachineModel)
--- ---
--- Représente une machine, dans une usine, qui peut produire des choses.
---@class Machine:Class
local Machine = class('Machine');

Machine.static.MACHINE_INVENTORY_SIZE = 100;

--- Constructeur de classe.
---@param model MachineModel
function Machine:initialize(model)
	Machine.super.initialize(self);

	self._uuid = uuid();
	self._model = model;
	self._tools = {}; ---@type table<string, MachineTool>
	self._factory = nil; ---@type Factory
	self._currentProduction = nil; ---@type Production
	self._outputInventory = Inventory(Machine.static.MACHINE_INVENTORY_SIZE); ---@type Inventory
end

--- Retourne l'UUID de la machine.
---@return string
function Machine:getUUID()
	return self._uuid;
end

--- Retourne l'outil monté sur un slot dont on donne l'identifiant (seulement
--- le nom). S'il n'y a pas d'emplacement d'outil à ce nom dans cette machine,
--- retourne `nil`.
---@param slot string
---@return MachineTool
function Machine:getTool(slot)
	return self._tools[slot];
end

--- Monte un outil dans le slot pour lequel il est fait.
---@param tool MachineTool
---@return self self
function Machine:mountTool(tool)
	self._tools[tool:getToolSlot():getId()] = tool;
	return self;
end

--- Démonte l'outil à l'emplacement `id`, et le retourne. S'il n'y avait aucun
--- outil à l'emplacement désigné, retourne `nil`.
---@param id string
---@return MachineTool?
function Machine:unmountTool(id)
	if self._tools[id] ~= nil then
		local t = self._tools[id];
		self._tools[id] = nil;
		return t;
	end
end

--- Retourne le modèle de machine auquel cette machine est associée.
---@return MachineModel
function Machine:getMachineModel()
	return self._model;
end

--- Retourne l'usine dans laquelle est la machine. Si la machine n'est pas dans
--- une usine, retourne `nil`.
---@return Factory?
function Machine:getFactory()
	return self._factory;
end

--- Retourne la production qu'exécute actuellement la machine. Si la machine
--- n'exécute actuellement pas de production, retourne `nil`.
---@return Production?
function Machine:getCurrentProduction()
	return self._currentProduction;
end

--- Retourne l'inventaire de stockage des sorties de la machine. C'est là où
--- sont stockés les items créés par la machine, en attendant qu'ils soient
--- enlevés par d'autres machines ou entrepôts.
---@return Inventory
function Machine:getOutputStorage()
	return self._outputInventory;
end


return Machine;