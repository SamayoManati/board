require('calypso.core');
require('calypso.class');
require('foundation.log');

-- *****************************************************************************
-- * Factory
-- *
-- *  class Factory
-- *****************************************************************************

--- 	(constructor) Factory(name?: string)
--- ---
--- Représente une usine.
---
--- Chaque usine doit être enregistrée dans la classe `Game`.
---@class Factory:Class
local Factory = class('Factory');

--- Constructeur de classe.
---@param name? string
function Factory:initialize(name)
	Factory.super.initialize(self);

	self._uuid = uuid();
	self._name = name or 'Factory Without a Name';
	self._warehouses = {}; ---@type Warehouse[]
	self._machines = {}; ---@type Machine[]
	self._productions = {}; ---@type Production[]
end

--- Obtient l'UUID de l'usine.
---@return string
function Factory:getUUID()
	return self._uuid;
end

--- Retourne le nom de l'usine.
---@return string
function Factory:getName()
	return self._name;
end

--- Définit le nom de l'usine, affiché en jeu.
---@param name string
---@return self self
function Factory:setName(name)
	self._name = name;
	return self;
end

--- Obtient la liste des entrepôts construits dans l'usine.
---@return Warehouse[]
function Factory:getWarehouses()
	return self._warehouses;
end

--- Raccourci pour `list.stream(Factory.getWarehouses())`.
---@return fun():Warehouse
function Factory:streamWarehouses()
	return list.stream(self._warehouses);
end

--- Ajoute un nouvel entrepôt `w` à l'usine. Retourne si l'ajout a été
--- possible ou non.
---@param w Warehouse
---@return boolean success
function Factory:addWarehouse(w)
	self._warehouses[#self._warehouses+1] = w;
	w._factory = self;
	return true;
end

--- Vérifie si une usine possède l'entrepôt avec l'UUID fourni.
---@param uid string
---@return boolean
function Factory:hasWarehouse(uid)
	for w in list.stream(self._warehouses) do
		if w:getUUID() == uid then
			return true;
		end
	end
	return false;
end

--- Supprime un entrepôt de l'usine et le retourne, basé sur son UUID. Si
--- cet entrepôt n'existe pas dans cette usine, lève une erreur.
---
--- _@throws_ `Errors.UnknownId` - L'UUID n'existe pas
---@param uid string
---@return Warehouse
function Factory:removeWarehouse(uid)
	local index;
	for i, j in ipairs(self._warehouses) do
		if j:getUUID() == uid then
			index = i;
			break;
		end
	end

	if not index then
		throw(Errors.UnknownId, 'Unknown UUID', {uuid = uid});
	else
		return table.remove(self._warehouses, index);
	end
end

--- Retourne les machines existantes dans l'usine.
---@return Machine[]
function Factory:getMachines()
	return self._machines;
end

--- Raccourci pour `list.stream(Factory.getMachines())`.
---@return fun():Machine
function Factory:streamMachines()
	return list.stream(self._machines);
end

--- Ajoute une machine à l'usine. Retourne si l'ajout a pu se faire.
---@param machine Machine
---@return boolean success
function Factory:addMachine(machine)
	self._machines[#self._machines+1] = machine;
	machine._factory = self;
	return true;
end

--- Vérifie si la machine avec l'UUID fourni est dans l'usine ou non.
---@param uid string
---@return boolean
function Factory:hasMachine(uid)
	for m in self:streamMachines() do
		if m:getUUID() == uid then
			return true;
		end
	end
	return false;
end

--- Supprime de l'usine la machine avec l'UUID fourni, et la retourne. Si elle
--- n'était pas dans cette usine, retourne `nil`.
---@param uid string
---@return Machine?
function Factory:removeMachine(uid)
	for i = 1, #self._machines do
		if self._machines[i]:getUUID() == uid then
			return table.remove(self._machines, i);
		end
	end
end

--- Retourne la liste des productions actuellement en cours dans cette usine.
---@return Production[]
function Factory:getProductions()
	return self._productions;
end

--- Raccourci pour `list.stream(Factory:getProductions())`.
---@return fun():Production
function Factory:streamProductions()
	return list.stream(self._productions);
end

--- Vérifie si la production avec l'UUID `uid` existe dans cette usine.
---@param uid string
---@return boolean
function Factory:hasProduction(uid)
	for p in list.stream(self._productions) do
		if p:getUUID() == uid then
			return true;
		end
	end
	return false;
end

--- Ajoute une nouvelle production à l'usine.
---@param prod Production
---@return self self
function Factory:addProduction(prod)
	self._productions[#self._productions+1] = prod;
	return self;
end

--- Supprime une production de l'usine en se basant sur son UUID, et la
--- retourne. S'il n'y a dans cette usine aucune production avec cet UUID,
--- retourne `nil`.
---@param uid string
---@return Production?
function Factory:removeProduction(uid)
	for i = 1, #self._productions do
		if self._productions[i]:getUUID() == uid then
			return table.remove(self._productions, i);
		end
	end
end

--- Cette fonction est appelée à chaque tick pour permettre son fonctionnement.
function Factory:update()
	-- Update des productions
	for prod in list.fstream(self._productions, function (i) return i:isActive() and not i:isEnded(); end) do
		prod:update();
	end
end


return Factory;