require('calypso.class');
require('calypso.core');


-- *****************************************************************************
-- * ItemStack
-- *
-- *  class ItemStack
-- *****************************************************************************

--- 	(constructor) ItemStack(item: Item, amount: number)
---
--- ---
--- Représente un stack d'items, c'est-à-dire un `Item` avec une quantité, qui
--- peut être utilisé dans les emplacements de stockage du jeu. Pour plus
--- d'informations, se référer à la documentation de la classe `ItemData`.
---
---@see ItemData
---@class ItemStack:Class
local ItemStack = class('ItemStack');

--- Constructeur de classe.
---@param item Item
---@param amount number
function ItemStack:initialize(item, amount)
	ItemStack.super.initialize(self);

	self._item = item;
	self._amount = amount;
	self._uuid = uuid();
end

--- Retourne l'item du stack.
---@return Item
function ItemStack:getItem()
	return self._item;
end

--- Retourne la quantité d'items dans le stack.
---@return number
function ItemStack:getAmount()
	return self._amount;
end

--- Définit la quantité d'items dans le stack.
---@param a number
---@return self self
function ItemStack:setAmount(a)
	self._amount = a;
	return self;
end

--- Raccourci pour ajouter un nombre `n` à la quantité d'items dans le stack.
--- Le type de `n` n'est pas vérifié, ainsi une classe implémentant `__add`
--- peut être fournie comme argument sans avoir à être castée en `number`.
---@param n number
---@return self self
function ItemStack:add(n)
	self._amount = self._amount + n;
	return self;
end

--- Raccourci pour retrancher un nombre `n` à la quantité d'items dans le stack.
--- Le type de `n` n'est pas vérifié, ainsi une classe implémentant `__sub`
--- peut être fournie comme argument sans avoir à être castée en `number`.
---@param n number
---@return self self
function ItemStack:sub(n)
	self._amount = self._amount - n;
	return self;
end

--- Retourne l'UUID du stack.
---@return string
function ItemStack:getUUID()
	return self._uuid;
end

--- Retire `amount` items du stack, et retourne un nouvel `ItemStack` les
--- contenant. S'il n'est pas possible d'en prendre autant, retourne
--- `nil`.
---@param amount number
---@return ItemStack?
function ItemStack:get(amount)
	if amount <= self._amount then
		self._amount = self._amount - amount;
		return ItemStack(self._item, amount); ---@type ItemStack
	end
end


return ItemStack;