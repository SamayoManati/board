require('calypso.core');
require('calypso.class');


-- *****************************************************************************
-- * Item
-- *
-- *  class Item
-- *****************************************************************************

--- 	(constructor) Item(item: ItemData)
--- ---
--- Représente un item physique du jeu. Il est la prolongation de la classe
--- `ItemData`. Pour plus d'informations, voir la documentation de la classe
--- `ItemData`.
---
---@see ItemData
---@class Item:Class
local Item = class('Item');

--- Constructeur de classe.
---@param item ItemData
function Item:initialize(item)
	Item.super.initialize(self);

	self._itemdata = item;
	self._options = {}; ---@type table<string, string|boolean|number>
end

--- Retourne l'item abstrait.
---@return ItemData
function Item:getItemData()
	return self._itemdata;
end

--- Définit la valeur pour une option. Si cette option n'existe pas dans l'item
--- abstrait, ou que la valeur fournie n'est pas en accord avec l'option
--- (mauvais type, valeur invalide...), lève une erreur.
---
--- _@throws_ `Errors.UnknownItemOption` - L'option `name` n'existe pas
---
--- _@throws_ `Errors.InvalidValue` - La valeur de `value` est invalide
---@param name string
---@param value string|boolean|number
---@return self self
function Item:setOptionValue(name, value)
	local itm = self._itemdata:getOption(name);

	if itm:accepts(value) then
		self._options[name] = value;
		return self;
	else
		throw(Errors.InvalidValue, 'Invalid value ' .. tostring(value), {
			option = name,
			item = self._itemdata:getFullyQualifiedId()
		});
	end
end

--- Retourne la valeur actuelle de l'option `name`. Si la valeur n'a pas été
--- définie dans `Item`, c'est la valeur par défaut de l'option qui sera
--- retournée. Si l'option n'existe pas, une erreur sera lancée.
---
--- _@throws_ `Errors.UnknownItemOption` - L'option n'existe pas
---@param name string
---@return string|boolean|number
function Item:getOptionValue(name)
	local itm = self._itemdata:getOption(name);

	if self._options[name] ~= nil then
		return self._options[name];
	else
		return itm:getDefault();
	end
end

--- Vérifie si l'item `i` est identique à soi-même; c'est-à-dire s'il fait
--- référence au même ÌtemData`, et si ses options ont toutes la même
--- valeur.
---@param i Item
---@return boolean
function Item:isEqualTo(i)
	if self._itemdata ~= i:getItemData() then
		return false;
	end

	for optionName, optionValue in pairs(self._options) do
		if i:getOptionValue(optionName) ~= optionValue then
			return false;
		end
	end

	return true;
end


return Item;