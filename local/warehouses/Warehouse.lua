require('calypso.core');
require('calypso.class');
local Inventory = require('global.inventories.Inventory');


-- *****************************************************************************
-- * Warehouse
-- *
-- *  class Warehouse
-- *****************************************************************************

--- 	(constructor) Warehouse(kind: ItemType, capacity: number, name?: string)
---
--- _@param_ `kind` - Le type d'items stockables dans l'entrepôt
---
--- _@param_ `capacity` - La taille de l'entrepôt
---
--- ---
--- La classe Warehouse représente un entrepôt.
---@class Warehouse:Class
local Warehouse = class('Warehouse');

--- Constructeur de classe.
---@param kind ItemType
---@param capacity number
---@param name? string
function Warehouse:initialize(kind, capacity, name)
	Warehouse.super.initialize(self);

	self._uuid = uuid();
	self._type = kind;
	self._name = name or 'Warehouse Which Has No Name';
	self._inventory = Inventory(capacity); ---@type Inventory
	self._factory = nil; ---@type Factory
end

--- Retourne l'UUID de l'entrepôt, qui permet de le reconnaître.
---@return string
function Warehouse:getUUID()
	return self._uuid;
end

--- Définit le nom de l'entrepôt, qui est affiché dans le jeu.
---@param name string
---@return self self
function Warehouse:setName(name)
	self._name = name;
	return self;
end

--- Retourne le nom de l'entrepôt, qui est affiché dans le jeu.
---@return string
function Warehouse:getName()
	return self._name;
end

--- Retourne le stockage de l'entrepôt.
---@return Inventory
function Warehouse:getStorage()
	return self._inventory;
end

--- Retourne le type d'items stockable dans cet entrepôt.
---@return ItemType
function Warehouse:getType()
	return self._type;
end

--- Retourne l'usine dans laquelle l'entrepôt est construit, ou `nil` si ce
--- champ n'a pas été renseigné.
---@return Factory?
function Warehouse:getFactory()
	return self._factory;
end


return Warehouse;