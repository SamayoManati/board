math.randomseed(tonumber(tostring(os.time()):reverse():sub(1, 9)));

require('calypso.core');
require('foundation.log');
local themes           = require('calypso.themes');
local scenes           = require('calypso.scenes');
local GameData         = require('gamedata');
local ResourcesManager = require('global.resources.ResourcesManager');
local Game             = require("local.Game");
require('libs.error_explorer'){};


local Scenes =
{
	TestScene            = require('scenes.test'),
	CentralViewScene     = require('scenes.centralView'),
	ProductionLinesScene = require('scenes.ProductionLinesScene'),
	WarehousesScene      = require('scenes.WarehousesScene'),
	GameContent          = require('scenes.gameContent')
};
scenes.ScenesManager.registerScene('test', Scenes.TestScene());
scenes.ScenesManager.registerScene('central_board', Scenes.CentralViewScene());
scenes.ScenesManager.registerScene('production_lines_view', Scenes.ProductionLinesScene());
scenes.ScenesManager.registerScene('warehouses_view', Scenes.WarehousesScene());
scenes.ScenesManager.registerScene('all_game_content', Scenes.GameContent.AllGameContentScene());


MON_ENTREPOT = nil; ---@type Warehouse
MA_MACHINE = nil; ---@type Machine


function love.load()
	love.filesystem.remove('output.log');

	love.window.setFullscreen(true);
	love.graphics.setDefaultFilter("nearest", "nearest");
	do
		-- Lecture des thèmes
		loginf('Registering themes');
		local defaultThemeFileContent = love.filesystem.read("themes/default.xml");
		local themesInDefault = themes.Theme:buildFromXML(defaultThemeFileContent); ---@type table<string,Theme>
		for themeId, themeItself in pairs(themesInDefault) do
			themes.ThemesManager.registerTheme(themeId, themeItself);
			loginf("Theme \"" .. themeItself:getName() .. "\" registered under ID \"" .. themeId .. "\"");
		end
	end;


	GameData.loadModules(true);

	love.graphics.setFont(ResourcesManager.fonts.default);

	scenes.ScenesManager.setCurrentScene('central_board');

	do
		local Factory = require('local.factories.Factory');
		local Warehouse = require('local.warehouses.Warehouse');
		local Production = require('local.productions.Production');
		local ProductionType = require('local.productions.ProductionType');
		local ItemType = require('global.items.ItemType');
		local Machine = require('local.machines.Machine');
		local RecipesManager = require('global.recipes.RecipesManager');
		local MachinesManager = require('global.machines.MachinesManager');
		local ItemStack = require('local.items.ItemStack');
		local Item = require('local.items.Item');
		local ItemsManager = require('global.items.ItemsManager');

		local f = Factory(); ---@type Factory

		MON_ENTREPOT = Warehouse(ItemType.SOLID, 500); ---@type Warehouse
		f:addWarehouse(MON_ENTREPOT);

		local i = Item(ItemsManager.getItem('item.copper_wire')):setOptionValue('color', 'red'); ---@type Item
		local s = ItemStack(i, 120); ---@type ItemStack
		MON_ENTREPOT:getStorage():add(s);
		MON_ENTREPOT:getStorage():add(
			ItemStack(
				Item(
					ItemsManager.getItem('item.copper_wire')
				):setOptionValue('color', 'green'),
				200)
		);

		MA_MACHINE = Machine(MachinesManager.getMachineModel_nilsafe('machine_model.martin_bf19')); ---@type Machine
		f:addMachine(MA_MACHINE);

		local p = Production(RecipesManager.getRecipe_nilsafe('recipe.copper_red_wire_coil'), MA_MACHINE, ProductionType.SERIAL); ---@type Production
		p:setAmountToProduce(10);
		p:setSourceForInput('input1', MON_ENTREPOT, s);
		f:addProduction(p);
		p:startProduction();

		Game.registerFactory(f);
	end;
end

local colors = require('calypso.colors');

function love.draw()
	scenes.ScenesManager.callSceneLoadIfNeeded();
	scenes.ScenesManager.getCurrentScene():internalDraw();

	do
		love.graphics.setColor(colors.red);
		local txt = 'Contenu de l entrepot :\n';
		for x in list.stream(MON_ENTREPOT:getStorage():getAll()) do
			txt = txt .. x:getItem():getItemData():getSafeLocalizedName() .. ' = ' .. tostring(x:getAmount()) .. '\n';
		end
		debug_printf({txt}, 500, 20);

		love.graphics.setColor(colors.green);
		txt = 'Contenu de la machine :\n';
		for x in list.stream(MA_MACHINE:getOutputStorage():getAll()) do
			txt = txt .. x:getItem():getItemData():getSafeLocalizedName() .. ' = ' .. tostring(x:getAmount()) .. '\n';
		end
		debug_printf({txt}, 800, 20);

		love.graphics.setColor({1.0,1.0,1.0, 0.01});
		love.graphics.rectangle("fill", 0, 0, 500, 250);
	end;
end

---@param dt number
function love.update(dt)
	scenes.ScenesManager.getCurrentScene():update(dt);

	Game.update();
end

function love.mousepressed(x, y, button, istouch)
	scenes.ScenesManager.getCurrentScene():internalMousepressed(x, y, button, istouch);
end

function love.mousereleased(x, y, button, istouch)
	scenes.ScenesManager.getCurrentScene():internalMousereleased(x, y, button, istouch);
end

function love.keypressed(key)
	scenes.ScenesManager.getCurrentScene():internalKeypressed(key);
end

function love.keyreleased(key)
	scenes.ScenesManager.getCurrentScene():internalKeyreleased(key);
end

function love.quit()
	loginf("Au revoir.");
end