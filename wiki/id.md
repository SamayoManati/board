# Identifiants

## ID

Les ID sont des identifiants uniques, que possèdent de nombreux objets du jeu afin de leur permettre d'être identifiés. Ils sont composés de plusieurs parties :

* Le module
* Le qualificatif
* La spécification
* Le nom
* L'attribut

Le qualificatif et la spécification forment, ensemble, la qualification. La qualification et le nom mis ensemble forment le coeur.

Les identifiants sont écrits de la manière suivante :

	[module:]qualificatif[@spécification].nom[.attribut]

La présence du module est optionnelle. En son absence, il est présumé que l'on parle du module `base`. La spécification n'est pas toujours présente. Il sert par exemple pour les tags (alors que les items sont qualifiés `item.<truc>`, les tags reçoivent en plus de leur qualification de `tag` une spécification de contenu, par exemple `tag@items` pour les tags recevant des items). L'attribut non plus n'est pas toujours présent.

Exemples :

	base:item.mon_item.name
		-> module = "base"
		-> qualificatif = "item"
		-> nom = "mon_item"
		-> attribut = "name"
	tag@items.ductible/iron
		-> module = "base" (implicite)
		-> qualificatif = "tag"
		-> spécification = "items"
		-> qualification = "tag@items"
		-> nom = "ductible/iron"

Certains identifiants possèdent une sous-clef. Les identifiants dotés d'une sous-clef sont composés de la manière suivante :

	[module:]qualificatif[@spécification].nom[.attribut[@nom-de-sous-clef[/valeur-de-sous-clef][.attribut-de-sous-clef]]]

Exemples :

	base:item.screw.option@head
		-> module = "base"
		-> qualificatif = "item"
		-> nom = "screw"
		-> attribut = "option"
		-> nom de sous-clef = "head"
	base:item.screw.option@head/flat.name
		-> module = "base"
		-> qualificatif = "item"
		-> nom = "screw"
		-> attribut = "option"
		-> nom de sous-clef = "head"
		-> valeur de sous-clef = "flat"
		-> attribut de sous-clef = "name"
	item.screw.option@size/long/very
		-> module = "base" (implicite)
		-> qualificatif = "item"
		-> nom = "screw"
		-> attribut = "option"
		-> nom de sous-clef = "head"
		-> valeur de sous-clef = "long/very"

### Format des Identifiants

Les identifiants ne peuvent contenir que les caractères suivants :

 * Les caractères alphabétiques, de a à z et de A à Z, non accentués ;
 * Les chiffres, de 0 à 9 ;
 * Le tiret (`-`) et le tiret du bas (`_`).

Certaines parties d'un identifiant peuvent contenir d'autres caractères en sus de ceux ci-dessus :

 * Les noms et les valeurs de sous-clefs peuvent contenir des slashs (`/`).

## ID ou UUID ?

Le jeu possède deux systèmes d'identification : les ID et les UUID.

Les ID (communément appelés "identifiants") sont des identifiants tels que définis plus haut. Ils sont définis dans les fichiers de module, et ne changent jamais.

Les UUID, eux, sont des chaînes de caractères de ce type : `e91b539b-3179-4f69-b871-e40c0f0be099`. Ils sont générés aléatoirement par le jeu. Ceux déjà créés ne changent pas, car ils sont inclus dans la sauvegarde.

Ces deux systèmes parallèles sont utilisés selon le type de donnée. Les IDs sont utilisés pour désigner les éléments dits _globaux_, c'est-à-dire ceux qui sont chargés au chargement du jeu, en lisant les modules, et qui restent les mêmes peu importe sur quelle partie on se trouve (par exemple les tags, les recettes...). Quant aux UUIDs, ils sont utilisés pour désigner les éléments _locaux_, c'est-à-dire ceux qui sont chargés au chargement de la partie, en lisant la sauvegarde, et qui n'existent pas dans les autres parties (par exemple un stack d'items, un entrepôt...).
