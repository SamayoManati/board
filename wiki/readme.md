# Documentation

## Sommaire

* [Fonctionnement des Modules](modules.md)
	* [`items.xml`](modules/items.xml.md)
	* [`module.xml`](modules/module.xml.md)
	* [Fichiers de Langue](modules/locales.md)
	* [`recipes.xml`](modules/recipes.xml.md)
	* [`tags.xml`](modules/tags.xml.md)
	* [`machinesTypes.xml`](modules/machinesTypes.xml.md)
	* [`machines.xml`](modules/machines.xml.md)
* [Fonctionnement des Identifiants](id.md)
