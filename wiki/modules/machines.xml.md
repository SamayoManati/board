# Format du Fichier `machines.xml`

Le fichier `machines.xml` décrit les modèles de machines ajoutés par le module. Ces modèles de machines pourront être achetés par le joueur. La présence de ce fichier n'est pas obligatoire.

## Arborescence

- Une astérisque (*) indique que la présence de la balise est obligatoire
- Un plus (+) indique que la balise peut apparaître autant de fois que nécessaire

---

* [`MachinesModels`](#machinesmodels) *
	* [`MachineModel`](#machinesmodelsmachinemodel) +
		* [`MachineType`](#machinesmodelsmachinemodelmachinetype) *
		* [`Price`](#machinesmodelsmachinemodelprice)

## Descriptions des Balises

Chaque balise, sauf si spécifié autrement :

* N'est pas obligatoire
* Ne peut apparaitre qu'une seule fois

### `MachinesModels`

La balise-racine du fichier. Elle est obligatoire.

### `MachinesModels/MachineModel`

Représente un modèle de machines. Peut apparaître autant de fois que nécessaire.

Son attribut obligatoire `Id` lui fournit un identifiant unique qui sera utilisé pour la référencer partout dans le jeu. Cet identifiant doit être unique parmi tous les identifiants de modèles de machines de ce module.

#### Exemple

```xml
<MachinesModels>
	<MachineModel Id="mrz364">
		...
	</MachineModel>
	...
</MachinesModels>
```

### `MachinesModels/MachineModel/MachineType`

Spécifie le type de machine auquel appartient ce modèle. La présence de cette balise est obligatoire. Son contenu doit être un identifiant complet d'un type de machine existant (donc provenant soit du module, soit d'un module déjà chargé).

#### Exemple

```xml
<MachineModel Id="mrz364">
	<MachineType>base:machine_type.jointer</MachineType>
	...
</MachineModel>
```

### `MachinesModels/MachineModel/Price`

Définit le prix d'achat de base du modèle de machine. Gardez à l'esprit que ce prix pourra être modifié par d'autres facteurs. Cette balise est optionnelle, et, si elle n'est pas spécifiée, le modèle de machine aura un prix de 0.0. Son contenu doit être un nombre, entier ou à virgule.

#### Exemple

```xml
<MachineModel Id="mrz364">
	<Price>1206.89</Price>
	...
</MachineModel>
```
