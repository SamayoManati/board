# Format du Fichier `machinesTypes.xml`

Le fichier `machinesTypes.xml` décrit les types de machines créés par le module. Ces types de machines seront utilisés pour en tirer des modèles de machines. La présence de ce fichier dans un module n'est pas obligatoire.

## Arborescence

- Une astérisque (*) indique que la présence de la balise est obligatoire
- Un plus (+) indique que la balise peut apparaître autant de fois que nécessaire

---

* [`MachinesTypes`](#machinestypes) *
	* [`MachineType`](#machinestypesmachinetype) +
		* [`OperatorsAmount`](#machinestypesmachinetypeoperatorsamount)
		* [`Tools`](#machinestypesmachinetypetools)
			* [`Tool`](#machinestypesmachinetypetoolstool) +
				* [`Amount`](#machinestypesmachinetypetoolstoolamount)
				* [`Accepts`](#machinestypesmachinetypetoolstoolaccepts) *
					* [`Id`](#machinestypesmachinetypetoolstoolacceptsid) *+

## Descriptions des Balises

Chaque balise, sauf si spécifié autrement :

* N'est pas obligatoire
* Ne peut apparaitre qu'une seule fois

### `MachinesTypes`

La balise-racine du fichier. Sa présence est obligatoire.

### `MachinesTypes/MachineType`

Décrit un type de machine. Cette balise peut être répétée autant de fois que nécessaire.

Son attribut obligatoire `Id` lui fournit son identifiant unique.

#### Exemple

```xml
<MachinesTypes>
	<MachineType Id="jointer">
		...
	</MachineType>
</MachinesTypes>
```

### `MachinesTypes/MachineType/OperatorsAmount`

Spécifie le nombre d'opérateurs nécessaires au fonctionnement de la machine. Doit être un nombre. Si cette balise n'est pas précisée, la machine aura besoin d'un seul opérateur.

> *Attention, cette balise est vouée à disparaître au profit d'un système plus perfectionné.*

#### Exemple

```xml
<MachineType Id="jointer">
	<OperatorsAmount>3</OperatorsAmount>
	...
</MachineType>
```

### `MachinesTypes/MachineType/Tools`

Cette balise permet de décrire le ou les outil(s) nécessaires au fonctionnement de la machine. Les outils sont des items utilisés par la machine, qu'il faut remplacer au bout d'un moment.

### `MachinesTypes/MachineType/Tools/Tool`

Décrit un outil. Cette balise peut revenir autant de fois que nécessaire.

Son attribut obligatoire `Id` permet de le référencer. Cet identifiant doit être unique au sein des outils de la machine mais, étant compartimenté dans celle-ci, plusieurs types de machines peuvent présenter un outil au nom similaire.

Les outils disposent de clefs de traductions. Pour plus de détails, se reporter à la section traitant des fichiers de traductions.

#### Exemple

```xml
<MachinesTypes>
	<MachineType Id="machine1">
		...
		<Tools>
			<Tool Id="cutter">
				...
			</Tool>
		</Tools>
	</MachineType>
	<MachineType Id="machine2">
		...
		<Tools>
			<Tool Id="cutter">
				...
			</Tool>
		</Tools>
	</MachineType>
</MachinesTypes>
```

### `MachinesTypes/MachineType/Tools/Tool/Amount`

La quantité d'outils identiques à mettre dans cet emplacement. Par défaut, 1.

#### Exemple

```xml
<Tools>
	<Tool Id="outil1">
		...
		<Amount>3</Amount>
	</Tool>
</Tools>
```

### `MachinesTypes/MachineType/Tools/Tool/Accepts`

La liste des identifiants des items et des tags acceptés comme outil à cet emplacement. Sa présence est obligatoire.

### `MachinesTypes/MachineType/Tools/Tool/Accepts/Id`

Décrit un identifiant d'un item ou d'un tag d'items (selon la valeur de son attribut obligatoire `Type`) accepté comme outil à cet emplacement. Il peut y en avoir autant que nécessaire, mais il en faut un minimum 1.

#### Exemple

```xml
<Tool Id="outil1">
	...
	<Accepts>
		<Id Type="item">mon_module:item.fait_pour_etre_un_outil</Id>
		<Id Type="tag">mon_module:tag@items.tools/pour_ma_machine</Id>
		<Id Type="item">base:item.item_vanilla</Id>
		...
	</Accepts>
</Tool>
```
