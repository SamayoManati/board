# Format du Fichier `tags.xml`

Le fichier `tags.xml` décrit les tags créés par le module. Ces tags pourront être utilisés dans de nombreux contextes, comme les recettes ou les vendeurs. La présence de ce fichier dans un module n'est pas obligatoire.

Chaque module peut utiliser les tags qu'il définit lui-même, ainsi que ceux définis pas les modules déjà chargés.

## Arborescence

- Une astérisque (*) indique que la présence de la balise est obligatoire
- Un plus (+) indique que la balise peut apparaître autant de fois que nécessaire

---

* [`Tags`](#tags) *
	* [`Items`](#tagsitems)
		* [`Tag`](#tagstag) +
			* [`Contains`](#tagstagcontains)
				* [`Content`](#tagstagcontainscontent) +
		* [`ExTag`](#tagsextag) +
			* [`Contains`](#tagstagcontains)
				* [`Content`](#tagstagcontainscontent) +
	* [`Machines`](#tagsmachines)
		* [`Tag`](#tagstag) +
			* [`Contains`](#tagstagcontains)
				* [`Content`](#tagstagcontainscontent) +
		* [`ExTag`](#tagsextag) +
			* [`Contains`](#tagstagcontains)
				* [`Content`](#tagstagcontainscontent) +

## Descriptions des Balises

Chaque balise, sauf si spécifié autrement :

* N'est pas obligatoire
* Ne peut apparaitre qu'une seule fois

Le fichier est composé de plusieurs sections, contenant toutes les mêmes balises. Sur cette page, la mention `/*/` dans les chemins de balises est utilisée pour faire référence à l'une de ces sections, indifféremment. Ainsi, la balise `Tags/*/Tag` peut aussi bien se trouver dans la section `Items` que la section `Machines`.

### `Tags`

La balise-racine du fichier. Sa présence est obligatoire.

### `Tags/Items`

Dans cette section seront décrits les tags d'items. Les tags d'items, comme leur nom l'indique, ne peuvent contenir que des items.

### `Tags/Machines`

Dans cette section seront décrits les tags de machines. Les tags de machines peuvent contenir des types de machines et des modèles de machines. Ils sont notamment utiles pour définir des groupes de machines éligibles à telle ou telle recette.

### `Tags/*/Tag`

Représente un tag. Cette balise peut apparaître autant de fois que nécessaire.

L'attribut obligatoire `Id`, spécifie l'identifiant unique du tag, utilisé pour y faire référence. Il ne peut contenir que des caractères alphanumériques, des tirets, des underscores (`_`) et des slashs (`/`).

#### Exemple

```xml
<Tags>
	<Items>
		<Tag Id="mon_tag">
			...
		</Tag>
		<Tag Id="my_machines/tier3">
			...
		</Tag>
		...
	</Items>
	<Machines>
		<Tag Id="mes_machines">
			...
		</Tag>
		...
	</Machines>
</Tags>
```

### `Tags/*/Tag/Contains`

C'est ici que nous préciserons le contenu du tag, en utilisant des balises `<Content>`.

### `Tags/*/Tag/Contains/Content`

Chaque balise `content` représente un contenu du tag. Ce contenu peut être de 2 types : un autre tag, ou un objet. Ce type est précisé en utilisant l'attribut `Type`.

* Un contenu de type **tag** (`Type="tag"`) inclut un autre tag dans celui-ci. Par conséquence, ce tag contiendra tout le contenu du tag inclu.
* Un contenu de type **object** (`Type="obj"`) inclut un objet dans ce tag.

Objets comme tags doivent être référencés par leur ID complet :

* Pour les tags : `<id_du_module>:tag@<type>.<id_du_tag>`
* Pour les items : `<id_du_module>:item.<id_de_l_item>`
* Pour les machines : `<id_du_module>:machine.<id_de_la_machine>`

Il peut bien sûr y avoir autant d'occurences de cette balise que nécessaire.

Faire référence à un objet ou un tag inexistant ne fait pas crasher le jeu. Un avertissement sera simplement envoyé dans le fichier `output.log`.

#### Exemple

```xml
<Tag Id="nom_de_tag">
	...
	<Contains>
		<Content Type="tag">mon_module:tag@items.mon_tag</Content>
		<Content Type="obj">mon_module:item.mon_item</Content>
	</Contains>
</Tag>
```

### `Tags/*/Tag/Unseen`

Si cette balise est présente, le tag sera marqué comme invisible. Un tag invisible n'est pas affiché sur les objets le possédant.

#### Exemple

```xml
<Tag Id="nom/du/tag">
	...
	<Unseen/>
</Tag>
```

### `Tags/*/ExTag`

Un ExTag, ou *Existing Tag*, fait référence à un tag qui a déjà été enregistré, généralement provenant d'un autre module. Cette balise permet d'ajouter du contenu à un tag d'un autre module.

Il peut évidemment y en avoir autant que nécessaire.

L'attribut `Id` définit l'ID du tag auquel on fait référence. Il doit s'agit de son ID complet. Cette balise est obligatoire.

#### Exemple

```xml
<ExTag Id="autre_module:tag@items.tag_pour_items">
	...
</ExTag>
```
