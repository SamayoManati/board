# Format des Fichiers de Langue

Les fichiers de langue doivent être placés dans le dossier `locales/` du module, sans quoi ils ne seront pas lus. Bien que ça ne soit pas obligatoire, il est conseillé de leur donner comme nom le code de la langue qu'ils définissent (par exemple `fr_FR.xml` ou `en_US.xml`).

Les clefs de traduction définies dans chacun de ces fichiers seront ajoutées au dictionnaire de la langue en question. Si une clef déjà existante est redéfinie, son contenu sera modifié. Une clef invalide n'empêche pas le lancement du jeu. Un avertissement sera simplement envoyé dans le log.

## Arborescence

- Une astérisque (*) indique que la balise est obligatoire
- Un plus (+) indique que la balise peut revenir autant de fois que nécessaire

---

* [`Locale`](#locale) *
	* [`Key`](#localekey) +

## Descriptions des Balises

Sauf précisé, chaque balise :

* N'est pas obligatoire
* Ne peut être spécifiée qu'une seule fois

---

### `Locale`

La balise-racine du fichier. Sa présence est obligatoire.

Son attribut obligatoire `Code` permet de spécifier le code de la langue qu'il définit.

#### Exemple

```xml
<Locale Code="fr_FR">
	...
</Locale>
```

### `Locale/Key`

Chaque balise `Key` correspond à la traduction d'un élément. Il peut y en avoir autant que nécessaire. La valeur de la balise `Key` correspond à la valeur de la traduction.

Son attribut obligatoire `Id` définit l'identifiant de la clef de traduction.

L'identifiant de la clef doit être un identifiant valide, comportant un attribut (se référer à la partie sur les [clefs de traduction existantes](#clefs-de-traduction) pour plus d'informations).

#### Exemple

```xml
<Locale Code="fr_FR">
	<Key Id="mon_module:item.mon_item.name">Mon Item</Key>
	<Key Id="mon_module:item.mon_item.description">Ceci est une
description sur plusieurs lignes.</Key>
	<Key Id="mon_module:item.mon_item.option@color.name">Couleur</Key>
	<Key Id="mon_module:item.mon_item.option@color/red.name">Rouge</Key>
	<Key Id="mon_module:item.mon_item.option@color/green.name">Vert</Key>
	<Key Id="mon_module:tag@items.iron.name">En Fer</Key>
	<Key Id="mon_module:recipe.ma_recette.name">Une recette très utile !</Key>
	<Key Id="mon_module:recipe.ma_recette.description">Vraiment, cette recette vous sera très utile.
Il n'y a plus à douter d'elle.</Key>
	<Key Id="mon_module:machine_type.jointer.name">Dégauchisseuse</Key>
	<Key Id="mon_module:machine_type.tool@cutter.name>">Fers</Key>
</Locale>
```

## Clefs de Traduction

Seuls les attributs marqués comme multilignes reconnaissent les retours à la ligne et les interprèteront correctement.

> **Note** : dans les tableaux ci-dessous, `<module>` doit être remplacé par l'identifiant du module, `<item>` par l'identifiant de l'item, etc. `[quelquechose]` signifie que `quelquechose` est optionnel.

### Items

`[<module>:]item.<item>` suivi de :

| Attribut       | Contenu                  | Multilignes ? |
|----------------|--------------------------|---------------|
| `.name`        | Le nom de l'item         | Non           |
| `.description` | La description de l'item | Oui           |

#### Options d'Items

`[<module>:]item.<item>.option@<option>` suivi de :

| Attribut        | Contenu                  | Multilignes ? | Type d'Option       |
|-----------------|--------------------------|---------------|---------------------|
| `.name`         | Le nom de l'option       | Non           | switch, range, enum |
| `/<value>.name` | Le nom de la valeur      | Non           | enum                |

### Tags

`<type>` doit être remplacé par le type de contenu du tag (`items`, `machines`, etc).

`[<module>:]tag@<type>.<tag>` suivi de :

| Attribut | Contenu       | Multilignes ? |
|----------|---------------|---------------|
| `.name`  | Le nom du tag | Non           |

### Recettes

`[<module>:]recipe.<recette>` suivi de :

| Identifiant    | Contenu                      | Multilignes ? |
|----------------|------------------------------|---------------|
| `.name`        | Le nom de la recette         | Non           |
| `.description` | La description de la recette | Oui           |

### Types de Machines

`[<module>:]machine_type.<type de machine>` suivi de :

| Identifiant    | Contenu                           | Multilignes ? |
|----------------|-----------------------------------|---------------|
| `.name`        | Le nom du type de machine         | Non           |
| `.description` | La description du type de machine | Oui           |

### Emplacements d'Outil des Machines

`[<module>:]machine_type.<type de machine>.tool@<outil>` suivi de :

| Identifiant    | Contenu                   | Multilignes ? |
|----------------|---------------------------|---------------|
| `.name`        | Le nom de l'outil         | Non           |
| `.description` | La description de l'outil | Oui           |

### Clefs Spéciales

| Identifiant   | Contenu                                    | Multilignes ? |
|---------------|--------------------------------------------|---------------|
| `locale.name` | Le nom de la langue (exemple : "Français") | Non           |
