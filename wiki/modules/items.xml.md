# Format du Fichier `items.xml`

Ce fichier définit les items du jeu.

## Arborescence

- Une astérisque (*) indique que cette balise est obligatoire
- Un plus (+) indique que cette balise peut apparaître autant de fois que nécessaire

---

* [`Items`](#items) *
	* [`Item`](#itemsitem) +
		* [`Type`](#itemsitemtype) *
		* [`Icon`](#itemsitemicon)
		* [`Options`](#itemsitemoptions)
			* [`Option`](#itemsitemoptionsoption) +
				* [`Value`](#les-options-enum) +

## Descriptions des Balises

Sauf spécifié autrement, chaque balise :

* N'est pas obligatoire
* Ne peut apparaître qu'une seule fois

### `Items`

La balise-racine du fichier. Sa présence est évidemment obligatoire.

### `Items/Item`

Représente un item.

L'attribut `Id`, obligatoire, spécifie l'id unique de l'item. C'est celui qui sera utilisé pour y faire référence. Il ne peut, comme tout ID, contenir que des caractères alphanumériques, des tirets et des underscores (`_`), et contenir au minimum 1 caractère.

Il peut y avoir autant de balises `Item` que nécessaire.

#### Exemple

```xml
<Items>
	<Item Id="mon_item">
		...
	</Item>
	<Item Id="mon_autre_item">
		...
	</Item>
</Items>
```

### `Items/Item/Type`

Le type de l'item. Permet au jeu de savoir à quel type de stockage et quels conditionnements il a accès. Cette balise est obligatoire. Le type peut être une des valeurs suivantes :

* `solid` : pour un item solide
* `liquid` : pour un item liquide
* `powder` : pour une poudre

#### Exemple

```xml
<Item Id="mon_item">
	<Type>solid</Type>
	...
</Item>
```

### `Items/Item/Unit`

L'unité de mesure de l'item. Sa présence est obligatoire. C'est l'unité que l'on utilisera pour compter l'item. Elle doit être l'une des valeurs ci-dessous :

| Unité | Signification  | Exemple                  |
|-------|----------------|--------------------------|
| `kg`  | Kilogramme     | 1 Kg de sable            |
| `l`   | Litre          | 1 L de sable             |
| `m1`  | Mètre linéaire | 1 m<sup>1</sup> de sable |
| `m2`  | Mètre carré    | 1 m<sup>2</sup> de sable |
| `m3`  | Mètre cube     | 1 m<sup>3</sup> de sable |
| `u`   | Unité          | 1 sable                  |

#### Exemple

```xml
<Item Id="cp_board">
	...
	<Unit>m2</Unit>
</Item>
```

### `Items/Item/Size`

La taille de l'item, exprimée selon son unité. Sa présence est optionnelle, et vaut `1.0` si elle n'est pas spécifiée. C'est un nombre entier ou à virgule. Elle est utilisée pour calculer le prix de l'item. Dans la plupart des cas, elle vaudra `1`, sauf pour les items qui existent en une unité, mais se chiffrent en une autre.

Par exemple, imaginons un item "sable", qui se mesure en kilogrammes. Sa taille vaut `1`, de sorte de lorsqu'on vend 4,71 de sable, à 5$ le kilogramme, le calcul du prix total (`prix * quantité * taille`) soit `5 * 4.71 * 1`. <br/>
Maintenant, imaginons un item "panneau de contreplaqué". Les panneaux de contreplaqué se vendent à l'unité : on achète 1 ou 2 panneaux, et non 1,126 panneau. Cependant, les panneaux sont chiffrés au mètre carré, et un panneau ne fait pas forcément 1 m<sup>2</sup>. Disons que nos panneaux mesurent 3 m par 2,5 m. Leur superficie étant donc de 7,5 m<sup>2</sup>, ils auront comme taille `7.5`. Ainsi, lorsqu'on vend 5 panneaux de contreplaqué à 11$ du mètre carré, le calcul du prix total sera : `11 * 5 * 7.5`.

#### Exemple

```xml
<Item Id="cp_board">
	...
	<Unit>m2</Unit>
	<Size>7.5</Size>
</Item>
```

### `Items/Item/Icon`

Le nom de l'image qui servira d'icône à l'item. Il doit s'agit du nom précédé de l'ID du module, et pas d'un chemin d'accès (donc ni extension de fichier, ni chemin). Le jeu cherchera lui-même l'image correspondante dans le dossier adéquat.

Si cette balise n'est pas présente, le jeu cherchera pour une image portant le nom de l'ID de l'item.

#### Exemple

```xml
<Item Id="mon_item">
	<Icon>mon_module:mon_item</Icon> <!-- Bien -->
	<Icon>Resources/Icons/mon_item.png</Icon> <!-- Mauvais -->
</Item>
```

### `Items/Item/Options`

Cette balise rassemble les options possibles pour l'item.

### `Items/Item/Options/Option`

Une option. Peut apparaître autant de fois que nécessaire.

L'attribut obligatoire `Id` fourni à l'option un nom (unique parmi toutes les options de cet item).

L'attribut obligatoire `Type` décide du type de l'option.

L'option n'a pas le même type de contenu selon son type.

 * Les options de type _range_ (`Type="range"`) ont une valeur comprise dans une étendue _\[min; max\]_ (ces nombres compris). Ils peuvent, ou non, autoriser les nombres à virgule.
 * Les options de type _enum_ (`Type="enum"`) ont une valeur tirée d'une liste de valeurs autorisées. Ces valeurs sont représentées par des identifiants, afin de pouvoir les traduire.
 * Les options de type _switch_ (`Type="switch"`) ont une valeur booléenne, qui peut être soit vraie, soit fausse.

Selon le type de l'option, La balise n'est pas la même.

#### Les options _range_

```xml
<Option Id="<ID>" Type="range" Range="<MIN>;<MAX>" Default="<DEFAUT>"[ Float="(yes|no)"]/>
```

L'attribut `Range` fournit l'étendue de l'option. `MIN` et `MAX` doivent être des nombres, entiers ou à virgule. Un signe point-virgule (`;`) les sépare.

L'attribut `Default` spécifie la valeur par défaut. Elle doit être comprise dans l'étendue _\[min; max\]_, sans quoi l'option ne sera pas chargée par le jeu.

L'attribut `Float` est optionnel. Il indique si l'option accepte ou non les nombres à virgule. Attention, si spécifié, il doit être en accord avec l'étendue : une option n'acceptant pas les nombres à virgule, mais dont l'étendue en comprend, ne sera pas chargée par le jeu. Si elle n'est pas spécifiée, les nombres à virgule ne seront pas acceptés.

#### Les options _enum_

```xml
<Option Id="<ID>" Type="enum" Default="<DEFAULT>">
	<Value>IDENTIFIANT VALEUR 1</Value>
	<Value>IDENTIFIANT VALEUR N</Value>
	...
</Option>
```

Les valeurs des options de type _enum_ sont écrites dans des balises `Value`, qui contiennent l'identifiant de cette valeur. Il peut y en avoir autant que nécessaire, mais il doit y en avoir au moins une.

L'attribut `Default` précise la valeur par défaut. Sa valeur doit, évidemment, être une des valeurs possibles spécifiées.

#### Les options _switch_

```xml
<Option Id="<ID>" Type="switch" Default="(yes|no)"/>
```

L'attribut `Default` doit être soit `yes` soit `no`.
