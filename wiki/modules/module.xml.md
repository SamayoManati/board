# Format du Fichier `module.xml`

Le fichier `module.xml` contient les données d'identification relatives au module. Sa présence est obligatoire dans chaque module.

## Arborescence

- Une astérisque (*) après le nom de la balise indique que celle-ci est obligatoire
- Un plus (+) indique que la balise peut apparaître autant de fois que nécessaire

* [`ModuleInfos`](#moduleinfos) *
	* [`Name`](#moduleinfosname)
	* [`Authors`](#moduleinfosauthors)
		* [`Author`](#moduleinfosauthorsauthor) +
	* [`Id`](#moduleinfosid) *
	* [`Version`](#moduleinfosversion)

## Descriptions des Balises

Chaque balise, sauf si spécifié autrement :

* N'est pas obligatoire
* Ne peut apparaitre qu'une seule fois

### `ModuleInfos`

Balise-racine du fichier. Sa présence est obligatoire.

### `ModuleInfos/Name`

Contient le nom du module, tel qu'il sera affiché en jeu. Si cette balise n'est pas précisée, le module sera nommé par son ID.

#### Exemple

```xml
<Name>Mon Module</Name>
```

### `ModuleInfos/Authors`

Contient les noms des aut.eur.rice.s du module, chacun dans une balise `Author`.

#### Exemple

```xml
<Authors>
	<Author>Joe Mama</Author>
	<Author>John Cena</Author>
</Authors>
```

### `ModuleInfos/Authors/Author`

Le nom d'un.e aut.eur.rice.s du module. Peut apparaître autant de fois que nécessaire.

### `ModuleInfos/Id`

Contient l'identifiant du module. Cet identifiant est une chaîne de caractères, qui doit être différente pour chaque module. C'est par cet ID que le jeu fait référence au module.

L'ID ne peut contenir que des caractères alphanumériques, des tirets et des underscores (`_`). Il doit avoir au minimum 1 caractère.

La présence de cette balise est obligatoire.

#### Exemple

```xml
<Id>mon_module</Id>
```

### `ModuleInfos/Version`

Indique la version du module. Le format de la version n'est pas contraint.

#### Exemple

```xml
<Version>1.4.12</Version>
```
