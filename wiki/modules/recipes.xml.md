# Format du Fichier `recipes.xml`

Ce fichier décrit les recettes ajoutées par le module.

## Arborescence

* Une astérisque (*) indique que la présence de la balise est obligatoire
* Un plus (+) indique que la balise peut être présente autant de fois que nécessaire

---

* [`Recipes`](#recipes) *
	* [`Recipe`](#recipesrecipe) +
		* [`Machines`](#recipesrecipemachines)
			* [`Machine`](#recipesrecipemachinesmachine) +
		* [`Inputs`](#recipesrecipeinputs)
			* [`Input`](#recipesrecipeinputsinput) +
				* [`Id`](#recipesrecipeinputsinputid) *+
				* [`Amount`](#recipesrecipeinputsinputamount)
				* [`Options`](#recipesrecipeinputsinputoptions)
					* [`Option`](#recipesrecipeinputsinputoptionsoption) +
						* [`Value`](#recipesrecipeinputsinputoptionsoption) +
		* [`ProcessingTime`](#recipesrecipeprocessingtime) *
		* [`Outputs`](#recipesrecipeoutputs) *
			* [`Output`](#recipesrecipeoutputsoutput) *+
				* [`Id`](#recipesrecipeoutputsoutputid) *
				* [`Amounts`](#recipesrecipeoutputsoutputamounts)
					* [`Amount`](#recipesrecipeoutputsoutputamountsamount) +
				* [`Options`](#recipesrecipeoutputsoutputoptions)
					* [`Option`](#recipesrecipeoutputsoutputoptionsoption) +

## Descriptions des Balises

Sauf si spécifié, chaque balise :

- N'est pas obligatoire
- Ne peut être spécifiée qu'une seule fois

---

### `Recipes`

Il s'agit de la balise-racine du fichier, qui doit être présente.

### `Recipes/Recipe`

Décrit une recette. Peut être présente autant de fois qu'il y a de recettes à ajouter.

L'attribut obligatoire `Id` spécifie l'identifiant unique de la recette.

#### Exemple

```xml
<Recipe Id="ma_recette">
	...
</Recipe>
<Recipe Id="mon_autre_recette">
	...
</Recipe>
```

### `Recipes/Recipe/Machines`

La liste des machines qui pourront utiliser la recette.

#### Exemple

```xml
<Recipe Id="ma_recette">
	...
	<Machines>
		<Machine Type="machine">mon_module:machine_type.ma_machine</Machine>
		<Machine Type="tag">mon_module:tag@machines.mes_machines</Machine>
		...
	</Machines>
</Recipe>
```

### `Recipes/Recipe/Machines/Machine`

Référence une machine pouvant utiliser la recette. Cette balise peut apparaître autant de fois que nécessaire.

L'attribut obligatoire `Type` permet de spécifier ce à quoi on fait référence : un type/modèle de machines (`Type="machine"`) ou un tag de machines (`Type="tag"`). Faire référence à un tag de machines permet à toutes les machines faisant partie de ce tag d'utiliser cette recette. La valeur de cet attribut n'est pas sensible à la casse.

### `Recipes/Recipe/Inputs`

La liste des ingrédients entrant dans la recette.

#### Exemple

```xml
<Recipe Id="ma_recette">
	...
	<Inputs>
		<Input Id="input1">
			<Id Type="item">mon_module:items.mon_item</Id>
			<Amount>1</Amount>
		</Input>
		...
	</Inputs>
</Recipe>
```

### `Recipes/Recipe/Inputs/Input`

Référence un ingrédient de la recette. Cette balise peut apparaître autant de fois que nécessaire.

Son attribut obligatoire `Id` définit l'identifiant de cet input. Il doit être unique parmi les identifiants d'inputs de sa recette.

### `Recipes/Recipe/Inputs/Input/Id`

L'ID de l'item qui servira d'ingrédient dans la recette. Cette balise est obligatoire, mais peut apparaître autant de fois que nécessaire.

L'attribut obligatoire `Type` permet de spécifier ce à quoi on fait référence : un item (`Type="item"`) ou un tag d'items (`Type="tag"`). Faire référence à un tag d'items permet d'accepter comme ingrédient tous les items contenus dedans. La valeur de cet attribut n'est pas sensible à la casse.

#### Exemple

```xml
<Input Id="input1">
	<Id Type="item">mon_module:item.mon_item</Id>
	...
</Input>
```

### `Recipes/Recipe/Inputs/Input/Amount`

La quantité de cet item qui est nécessaire, exprimée dans l'unité de mesure spécifique à son type. Ce doit être un nombre. Si cette balise n'est pas spécifiée, la valeur par défaut sera utilisée.

| Type d'Item	| Unité			| Valeur par défaut | Notes |
|-------------|-----------|-------------------|-------|
| Solide			| Unitaire	| `1`								| Arrondi à l'entier inférieur. |
| Liquide			| Litre			| `1.0`							| |
| Poudre			| Kg				| `1.0`							| |

#### Exemple

```xml
<Input Id="input1">
	...
	<Amount>3.14</Amount>
</Input>
```

### `Recipes/Recipe/Inputs/Input/Options`

Cette balise permet de n'accepter que les items ayant certaines valeurs à une ou plusieurs de leurs options.

### `Recipes/Recipe/Inputs/Input/Options/Option`

Une option. Peut apparaître autant de fois que nécessaire. L'item, pour être considéré comme un ingrédient valide, doit avoir une valeur pour cette option correspondante à celle spécifiée ici.

Son attribut obligatoire `Id` permet de définir à quelle option on fait référence.

Son attribut obligatoire `Type` définit quel est le type d'option attendu. Cela permet de ne pas confondre une option pour une autre : on pourrait autrement faire passer une _enum_ avec une valeur `yes` pour une _switch_. La valeur à mettre est un des types existants pour les options.

Il y a deux manières de spécifier des valeurs. La première est d'utiliser l'attribut `Value`, et de lui fournir la valeur. La deuxième méthode permet d'accepter plusieurs valeurs. Elle s'utilise en ne mettant pas d'attribut `Value`, mais en spécifiant chaque valeur autorisée dans une balise enfant. Si les deux méthodes sont utilisées en même temps, c'est l'attribut qui prévaudra.

#### Exemple

```xml
<Input Id="input1">
	...
	<Options>
		<!-- Pour une enum. Ici, red OU green OU blue -->
		<Option Id="color" Type="enum">
			<Value>red</Value>
			<Value>green</Value>
			<Value>blue</Value>
		</Option>

		<!-- Pour un range. Ici, entre 1 et 20 OU entre 30 et 50 -->
		<Option Id="length" Type="range">
			<Value>1;20</Value>
			<Value>30;50</Value>
		</Option>

		<Option Id="isolated" Type="switch" Value="yes"/>
	</Options>
</Input>
```

### `Recipes/Recipe/ProcessingTime`

Le temps de production de la recette, exprimé en secondes. Ce temps de base sera éventuellement modifié par les bonus/malus dont bénéficie le.a joueu.r.se. Cette balise est obligatoire.

#### Exemple

```xml
<Recipe Id="ma_recette">
	...
	<ProcessingTime>2.5</ProcessingTime>
</Recipe>
```

### `Recipes/Recipe/Outputs`

Une liste des résultats de la recette. Cette balise est obligatoire.

### `Recipes/Recipe/Outputs/Output`

Un des résultat(s) de la recette. Cette balise peut être présente autant de fois que nécessaire, mais il doit y en avoir au moins une.

#### Exemple

```xml
<Outputs>
	<Output>
		<Id>mon_module:item.mon_resultat</Id>
	</Output>
	...
</Outputs>
```

### `Recipes/Recipe/Outputs/Output/Id`

L'identifiant de l'item auquel on fait référence. Cette balise est obligatoire.

### `Recipes/Recipe/Outputs/Output/Amounts`

Une liste des quantités possibles de ce résultat. Si cette balise n'est pas spécifiée, le jeu assumera que l'item est en quantité par défaut (voir les quantités par défaut de [`Recipes/Recipe/Inputs/Input/Amount`](#recipesrecipeinputsinputamount)).

Chaque quantité peut avoir un pourcentage, qui influe sur les chances de l'obtenir. Le total de tous les pourcentages des quantités du résultat ne doit pas dépasser `1.0`. Le résultat est calculé de la manière suivante.

#### Exemple 1

Imaginons ce résultat :

```xml
<Output>
	<Id>mon_module:item.mon_resultat</Id>
	<Amounts>
		<Amount Value="1" Percentage="0.75"/>
		<Amount Value="2" Percentage="0.25"/>
	</Amounts>
</Output>
```

Le jeu va classer les `Amount` selon leur poid, du plus faible au plus fort. Il va ensuite lancer un "dé 100", c'est-à-dire une valeur entre 0.0 et 100.0 (le d100 est arrondi à 4 chiffres après la virgule), puis va interpréter le résultat ainsi :

```
d100 = [0.0; 100.0]
SI d100 <= (0.25 * 100)
=> d100 <= 25.0
ALORS donner 2 items
SINON
	SI d100 <= (0.75 * 100)
	=> d100 <= 75.0
	ALORS donner 1 item
```

Ainsi, dans cet exemple, il y a 75% de chances d'obtenir 2 items, et 25% de chances d'en obtenir un.

#### Exemple 2

```xml
<Output>
	<Id>mon_module:item.mon_resultat</Id>
	<Amounts>
		<Amount Value="1" Percentage="0.75"/>
		<Amount Value="2" Percentage="0.25"/>
	</Amounts>
</Output>
<Output>
	<Id>mon_module:item.mon_resultat</Id>
</Output>
```

Ici, nous avons une deuxième sortie, avec le même item. Puisque la quantité n'a pas été précisée, le jeu considère qu'elle vaut 1.0. Puisque le poid n'a pas non plus été précisé, le jeu considère qu'il vaut 1.0. Le calcul est le suivant :

```
d100 = [0.0; 100.0]
SI d100 <= (0.25 * 100)
=> d100 <= 25.0
ALORS donner 2 items
SINON
	SI d100 <= (0.75 * 100)
	=> d100 <= 75.0
	ALORS donner 1 item

SI d100 <= (1.0 * 100)
=> d100 <= 100.0
ALORS donner 1 item
```

Ainsi, le jeu va donner 1 item quoi qu'il arrive, puis va avoir 25% de chances d'en ajouter 2, et 75% de chances d'en ajouter 1. Dans cet exemple, nous avons 75% de chances d'obtenir 2 items, et 25% de chances d'en obtenir 3.

#### Exemple 3

```xml
<Output>
	<Id>mon_module:item.mon_resultat</Id>
	<Amounts>
		<Amount Value="1" Percentage="0.75"/>
		<Amount Value="2" Percentage="0.2"/>
	</Amounts>
</Output>
```

Ici, le total de nos poids ne donne pas 1.0.

```xml
d100 = [0.0; 100.0]
SI d100 <= (0.2 * 100)
=> d100 <= 20.0
ALORS donner 2 items
SINON
	SI d100 <= (0.75 * 100)
	=> d100 <= 75.0
	ALORS donner 1 item
```

Nous avons donc 75% de chances d'obtenir 1 item, 20% d'en obtenir 2, et 5% de chances de ne rien obtenir du tout.

### `Recipes/Recipe/Outputs/Output/Amounts/Amount`

Une quantité obtensible du résultat. Cette balise peut apparaître autant de fois que nécessaire, mais gardez à l'esprit que tous leurs poids combinés ne doivent pas dépasser 1.0, sinon le jeu refusera de charger le module.

L'attribut `Value` prend un nombre. Il s'agit du montant de la quantité. S'il n'est pas spécifié, le jeu considère qu'il vaut 1.0. Il est possible de spécifier une quantité basse et une quantité haute, en les séparant par un point-virgule (`Value="qtBasse;qtHaute"`). Dans ce cas, la quantité offerte sera une quantité aléatoire entre ces deux bornes. La quantité basse doit obligatoirement être plus petite que la quantité haute, sans quoi le jeu ignorera cet output.

L'attribut `Percentage` prend un nombre entre 0 et 1, avec 6 chiffres après la virgule. Il s'agit du poid de la quantité. S'il n'est pas spécifié, le jeu considère qu'il vaut 1.0.

#### Exemple

```xml
<Amounts>
	<Amount Value="3.26" Percentage="0.33"/>
	<Amount Value="1;6" Percentage="0.47"/>
	...
</Amounts>
```

### `Recipes/Recipe/Outputs/Output/Options`

Cette balise permet de préciser les options qu'aura l'item en sortie.

### `Recipes/Recipe/Outputs/Output/Options/Option`

Une option. Cette balise peut apparaître autant de fois que nécessaire. Elle possède deux attributs obligatoires.

`Id` permet de spécifier l'identifiant de l'option à laquelle on fait ici référence.

`Value` définit la valeur qu'aura l'option. Ce doit évidemment être une valeur valide pour cette option.

Il n'y a pas besoin, ici, de spécifier un type d'option, puisque ces options s'appliquent à un item en particulier, et non à plusieurs.

#### Exemple

```xml
<Output>
	<Id>item.copper_wire</Id>
	<Options>
		<Option Id="color" Value="red"/>
		...
	</Options>
</Output>
```
