# Fonctionnement des Données du Jeu

Les données de jeu sont les données indépendantes de la sauvegarde (items, tags, machines...). Le jeu fonctionne par modules. Chaque module définit des données de jeu. Même les données du jeu de base sont contenues dans un module.

Un module est un dossier. Il contient un ou plusieurs fichiers, souvent au format XML, organisés selon la hiérarchie décrite ci-dessous. Ces fichiers sont lus au lancement pour ajouter leur contenu au jeu. Il n'y a pas de nombre maximal de modules.

Le module `Base` contient les données de base du jeu.

## Hiérarchie d'un Module

Chaque module, afin d'être correctement reconnu et interprété, doit suivre l'arborescence de fichiers décrite ici. Notez que tous les fichiers ne doivent pas obligatoirement être présents.

* `<nom du module>/`
	* `resources/`
		* `icons/`
			* `items/`
			* `machines/`
		* `fonts/`
	* `locales/`
	* `items.xml`
	* `tags.xml`
	* `recipes.xml`
	* `module.xml` Obligatoire
	* `machinesTypes.xml`
	* `machines.xml`

### `items.xml`

Décrit tous les items du jeu.

[Plus d'informations](items.xml.md)

### `tags.xml`

Décrit tous les tags du module.

Chaque module peut utiliser les tags de son module et ceux des modules déjà chargés.

[Plus d'informations](tags.xml.md)

### `recipes.xml`

Décrit les recettes.

[Plus d'informations](recipes.xml.md)

### `module.xml`

`module.xml` contient les données d'identification relatives au module. La présence de ce fichier est obligatoire.

[Plus d'informations](module.xml.md)

### `machinesTypes.xml`

Décrit les types de machines du module.

[Plus d'informations](machinesTypes.xml.md)

### `machines.xml`

Décrit les modèles de machines du module.

[Plus d'informations](machines.xml.md)

### `resources/icons/`

C'est ici qu'il faut placer les images qui serviront d'icônes aux objets, dans les sous-dossiers suivants :

* `items/` pour les items
* `machines/` pour les machines

### `resources/fonts`

C'est ici qu'il faut placer les polices de caractères pour que le jeu les reconnaisse.

Le jeu accepte les polices au format TrueType Font. Les polices OpenType sont partiellement supportées.

Pour que le jeu les utilise correctement, les polices doivent avoir le nom adéquat.

| Nom de la police | Usage               |
|------------------|---------------------|
| `default`        | La police de corps. |

### `locales/`

C'est dans ce dossier qu'il faut placer les fichiers de langues du module. Bien que ça ne soit pas obligatoire, il est conseillé de leur donner comme nom le code de la langue qu'ils définissent (`en_US.xml`, `fr_FR.xml`, ...).

## Où Mettre les Modules ?

Les modules sont à placer dans le dossier `%AppData%\LOVE\board\modules`.

**Attention** : les modules sont des dossiers, et pas des dossiers compressés. Ainsi, le fichier `module.xml` du module de base est au chemin `%AppData%\LOVE\board\modules\Base\module.xml`.

## Ordre de Chargement des Modules

Le jeu va charger en premier le module nommé `Base`, puis va charger tous les autres modules par ordre alphabétique.

S'il ne peut pas trouver de module `Base`, le jeu ne pourra pas se lancer.

Au sein de chaque module, les fichiers sont chargés dans l'ordre suivant :

1. `module.xml`
2. `items.xml`
3. `tags.xml` (Seulement les tags d'items)
4. `machinesTypes.xml`
5. `machines.xml`
6. `tags.xml` (Seulement les tags de machines)
7. `recipes.xml`
8. Les fichiers de langue

> Vous noterez que le fichier `tags.xml` est chargé plusieurs fois. Il est nécessaire de segmenter la lecture de ce fichier en plusieurs fois, selon le type des tags. En effet, si tous les tags étaient chargés après le reste du contenu, il serait impossible, par exemple, de faire référence à un tag d'items défini par le module dans la définition d'un type de machines. Chaque type de tag doit donc être chargé aussitôt après le type de son contenu (les tags d'items juste après les items, les tags de machines juste après les types et modèles de machines...). <br/>
En réalité, le jeu ne lit qu'une seule fois le fichier `tags.xml`, mais segmente son chargement en plusieurs opérations. Chaque opération va chercher, au sein de ce fichier précédemment lu, la section qui la concerne, et ne lit que celle-ci.
