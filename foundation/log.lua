--- Ajoute une ligne de debug au fichier `output.log`.
---@param text any
---@param printDate? boolean
function log(text, printDate)
	if printDate == nil then
		printDate = true;
	end
	local _date = os.date("[%d/%m/%Y %H:%M:%S]");
	local text = tostring(text);
	local txt;
	if printDate then
		txt = _date .. " : " .. text .. "\r\n";
	else
		txt = text .. "\r\n";
	end
	if not love.filesystem.getInfo('output.log') then
		love.filesystem.write('output.log', txt);
	else
		love.filesystem.append('output.log', txt);
	end
end

--- Affiche une info dans le fichier `output.log`.
---@param text any
---@param printDate? boolean
function loginf(text, printDate)
	log('INFO : ' .. tostring(text), printDate);
end

--- Affiche une erreur dans le fichier `output.log`.
---@param err ErrorObject
---@param printDate? boolean
function logerr(err, printDate)
	local text = '';
	local title = '[ERROR]';
	local halfHeader = string.rep('=', (79 - #title) / 2);
	local header = halfHeader .. title .. halfHeader;
	local footer = string.rep('=', 79);

	log(header, false);
	log('ERROR : ' .. err.fullMessage, printDate);
	log(err.traceback, false);
	if err.causeTraceback ~= nil then
		log('', false);
		log(err.causeTraceback, false);
	end
	log(footer, false);
end

--- Affiche un warning dans le fichier `output.log`.
---
--- Si `additionalInformations` est fourni, chaque entrée de cette map sera
--- affichée sur une nouvelle ligne indentée, sous le message de warning.
---@param text any
---@param additionalInformations? table<string, string>
---@param printDate? boolean
function logwrn(text, additionalInformations, printDate)
	log('WARNING : ' .. tostring(text), printDate);

	if additionalInformations ~= nil then
		for name, value in pairs(additionalInformations) do
			log('\t| ' .. name .. ' : ' .. value, false);
		end
	end
end

function debug_printf(textf, x, y)
	love.graphics.printf(textf, x, y, love.graphics.getWidth() - x, "left");
end