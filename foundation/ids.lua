require('calypso.core');


-- Composition des ID :
-- [module:]qualificatif[@spécification].nom[.attribut[@nom[/valeur][.attribut]]]
-- mon_module:item.mon_item.name
--     1        2     4      5
-- mon_module:tag@items.mon/tag.name
--     1       2    3      4     5
-- mon_module:item.mon_item.option@nom_option_enum/valeur1.name
--     1       2      4       5        6             7      8
-- mon_module:machine_type.ma_machine.tool@nom_outil.description
--     1            2         4        5      6         8
--
-- 1: module
-- 2: qualificatif
-- 3: spécification
-- 4: nom
-- 5: attribut
-- 6: nom de sous-clef
-- 7: valeur de sous-clef
-- 8: attribut de sous-clef
-- qualificatif+spécification: qualification
-- qualification+nom: coeur
-- 5+6+7+8: sous-clef

local REGEX_ID_NAME             = "[a-zA-Z0-9-_/]+";
local REGEX_ID_QUALIFICATION    = "[a-zA-Z0-9-_@]+";
local REGEX_ID_MODULE           = "[a-zA-Z0-9-_]+:";
local REGEX_ID_SUBKEY_NAME      = "[a-zA-Z0-9-_]+";
local REGEX_ID_SUBKEY_VALUE     = "[a-zA-Z0-9-_/]+";
local REGEX_ID_SUBKEY_ATTRIBUTE = "[a-zA-Z0-9-_]+";
local REGEX_ID_ATTRIBUTE        = "[a-zA-Z0-9-_]+";
local REGEX_ID_HEART            = REGEX_ID_QUALIFICATION .. "%." .. REGEX_ID_NAME;

local REGEX_SUBKEY_CASE_NAME                 = REGEX_ID_SUBKEY_NAME;
local REGEX_SUBKEY_CASE_NAME_VALUE           = REGEX_SUBKEY_CASE_NAME .. "/" .. REGEX_ID_SUBKEY_VALUE;
local REGEX_SUBKEY_CASE_NAME_ATTRIBUTE       = REGEX_SUBKEY_CASE_NAME .. "%." .. REGEX_ID_SUBKEY_ATTRIBUTE;
local REGEX_SUBKEY_CASE_NAME_VALUE_ATTRIBUTE = REGEX_SUBKEY_CASE_NAME_VALUE .. "%." .. REGEX_ID_SUBKEY_ATTRIBUTE;

local REGEX_CASE_HEART                  = "^"                    .. REGEX_ID_HEART                               .. "$";
local REGEX_CASE_HEART_ATTRIBUTE        = "^"                    .. REGEX_ID_HEART .. "%." .. REGEX_ID_ATTRIBUTE .. "$";
local REGEX_CASE_MODULE_HEART           = "^" .. REGEX_ID_MODULE .. REGEX_ID_HEART                               .. "$";
local REGEX_CASE_MODULE_HEART_ATTRIBUTE = "^" .. REGEX_ID_MODULE .. REGEX_ID_HEART .. "%." .. REGEX_ID_ATTRIBUTE .. "$";
local REGEX_CASE_HEART_ATTRIBUTE_SUBKEY_NAME = "^" .. REGEX_ID_HEART .. "%." .. REGEX_ID_ATTRIBUTE .. "@" .. REGEX_SUBKEY_CASE_NAME .. "$";
local REGEX_CASE_HEART_ATTRIBUTE_SUBKEY_NAME_VALUE = "^" .. REGEX_ID_HEART .. "%." .. REGEX_ID_ATTRIBUTE .. "@" .. REGEX_SUBKEY_CASE_NAME_VALUE .. "$";
local REGEX_CASE_HEART_ATTRIBUTE_SUBKEY_NAME_ATTRIBUTE = "^" .. REGEX_ID_HEART .. "%." .. REGEX_ID_ATTRIBUTE .. "@" .. REGEX_SUBKEY_CASE_NAME_ATTRIBUTE .. "$";
local REGEX_CASE_HEART_ATTRIBUTE_SUBKEY_NAME_VALUE_ATTRIBUTE = "^" .. REGEX_ID_HEART .. "%." .. REGEX_ID_ATTRIBUTE .. "@" .. REGEX_SUBKEY_CASE_NAME_VALUE_ATTRIBUTE .. "$";
local REGEX_CASE_MODULE_HEART_ATTRIBUTE_SUBKEY_NAME = "^" .. REGEX_ID_MODULE .. REGEX_ID_HEART .. "%." .. REGEX_ID_ATTRIBUTE .. "@" .. REGEX_SUBKEY_CASE_NAME .. "$";
local REGEX_CASE_MODULE_HEART_ATTRIBUTE_SUBKEY_NAME_ATTRIBUTE = "^" .. REGEX_ID_MODULE .. REGEX_ID_HEART .. "%." .. REGEX_ID_ATTRIBUTE .. "@" .. REGEX_SUBKEY_CASE_NAME_ATTRIBUTE .. "$";
local REGEX_CASE_MODULE_HEART_ATTRIBUTE_SUBKEY_NAME_VALUE = "^" .. REGEX_ID_MODULE .. REGEX_ID_HEART .. "%." .. REGEX_ID_ATTRIBUTE .. "@" .. REGEX_SUBKEY_CASE_NAME_VALUE .. "$";
local REGEX_CASE_MODULE_HEART_ATTRIBUTE_SUBKEY_NAME_VALUE_ATTRIBUTE = "^" .. REGEX_ID_MODULE .. REGEX_ID_HEART .. "%." .. REGEX_ID_ATTRIBUTE .. "@" .. REGEX_SUBKEY_CASE_NAME_VALUE_ATTRIBUTE .. "$";


-- *****************************************************************************
-- * Qualifier
-- *
-- *  enum Qualifier
-- *****************************************************************************

--- Contient tous les qualificatifs possibles.
---@enum Qualifier
local Qualifier =
{
	ITEM          = 'item',
	TAG           = 'tag',
	RECIPE        = 'recipe',
	MACHINE_TYPE  = 'machine_type',
	MACHINE_MODEL = 'machine_model'
}


-- *****************************************************************************
-- * Specifier
-- *
-- *  enum Specifier
-- *****************************************************************************

--- Contient toutes les spécifications possibles.
---@enum Specifier
local Specifier =
{
	Tag =
	{
		ITEMS    = 'items',
		MACHINES = 'machines'
	}
}


--- Vérifie si une spécification de tag existe ou non.
---@param qualifier Qualifier
---@param sp string
---@return boolean
function Specifier.exists(qualifier, sp)
	if qualifier == Qualifier.TAG then
		return sp == Specifier.Tag.ITEMS
			or sp == Specifier.Tag.MACHINES;
	end
	return false;
end

--- Vérifie si un qualificatif est un contenu valide pour un spécificateur de
--- tags.
---@param sp Specifier.Tag
---@param qu Qualifier
---@return boolean
function Specifier.isQualifierValidTagContent(sp, qu)
	return switch(
		sp, false,
		Specifier.Tag.ITEMS, switch(
			qu, false,
			Qualifier.ITEM, true
		),
		Specifier.Tag.MACHINES, switch(
			qu, false,
			Qualifier.MACHINE_TYPE, true,
			Qualifier.MACHINE_MODEL, true
		)
	);
end

--- Vérifie si un ID fourni possède une forme valide. S'il est valide, retourne
--- sa forme. Sinon, retourne `false`.
---
--- Valeurs de retour :
--- - `false` : invalide
--- - `1` : module-coeur-attribut
--- - `2` : module-coeur
--- - `3` : coeur-attribut
--- - `4` : coeur
--- - `5` : coeur-attribut-sousclef(nom)
--- - `6` : coeur-attribut-sousclef(nom-attribut)
--- - `7` : coeur-attribut-sousclef(nom-valeur)
--- - `8` : coeur-attribut-sousclef(nom-valeur-attribut)
--- - `9` : module-coeur-attribut-sousclef(nom)
--- - `10` : module-coeur-attribut-sousclef(nom-attribut)
--- - `11` : module-coeur-attribut-sousclef(nom-valeur)
--- - `12` : module-coeur-attribut-sousclef(nom-valeur-attribut)
---@param id string
---@return boolean|integer
local function isValid(id)
	if not not string.match(id, REGEX_CASE_MODULE_HEART_ATTRIBUTE) then
		return 1;
	elseif not not string.match(id, REGEX_CASE_MODULE_HEART) then
		return 2;
	elseif not not string.match(id, REGEX_CASE_HEART_ATTRIBUTE) then
		return 3;
	elseif not not string.match(id, REGEX_CASE_HEART) then
		return 4;
	elseif not not string.match(id, REGEX_CASE_HEART_ATTRIBUTE_SUBKEY_NAME) then
		return 5;
	elseif not not string.match(id, REGEX_CASE_HEART_ATTRIBUTE_SUBKEY_NAME_ATTRIBUTE) then
		return 6;
	elseif not not string.match(id, REGEX_CASE_HEART_ATTRIBUTE_SUBKEY_NAME_VALUE) then
		return 7;
	elseif not not string.match(id, REGEX_CASE_HEART_ATTRIBUTE_SUBKEY_NAME_VALUE_ATTRIBUTE) then
		return 8;
	elseif not not string.match(id, REGEX_CASE_MODULE_HEART_ATTRIBUTE_SUBKEY_NAME) then
		return 9;
	elseif not not string.match(id, REGEX_CASE_MODULE_HEART_ATTRIBUTE_SUBKEY_NAME_ATTRIBUTE) then
		return 10;
	elseif not not string.match(id, REGEX_CASE_MODULE_HEART_ATTRIBUTE_SUBKEY_NAME_VALUE) then
		return 11;
	elseif not not string.match(id, REGEX_CASE_MODULE_HEART_ATTRIBUTE_SUBKEY_NAME_VALUE_ATTRIBUTE) then
		return 12;
	else
		return false;
	end
end

--- Crée un ID à partir des éléments fournis. Si le module n'est pas fourni,
--- la fonction utilisera le module `base`.
---
--- _@throws_ `Errors.BadArgument` - Les champs nécessaires ne sont pas fournis
---@param qualifier Qualifier
---@param name string
---@param attribute? string Nécessaire en cas de sous-clef
---@param specifier? Specifier
---@param module? string
---@param subkeyName? string Nécessaire si `subkeyValue` et/ou `subkeyAttribute` sont spécifiés
---@param subkeyValue? string
---@param subkeyAttribute? string
---@return string
local function createId(qualifier, name, attribute, specifier, module, subkeyName, subkeyValue, subkeyAttribute)
	local module = module or "base";
	local qualification = qualifier;
	if specifier ~= nil then
		qualification = qualification .. "@" .. specifier;
	end
	local made_id = module .. ":" .. qualification .. "." .. name;
	if not not attribute then
		made_id = made_id .. "." .. attribute;
	end
	if subkeyName ~= nil or subkeyValue ~= nil or subkeyAttribute ~= nil then
		if not attribute then
			throw(Errors.BadArgument, 'In order to make an ID featuring a subkey, you must provide an attribute');
		end
		if not subkeyName then
			throw(Errors.BadArgument, 'In order to make an ID featuring a subkey, you musty provide a subkey name');
		end
		made_id = made_id .. "@" .. subkeyName;
		if subkeyValue ~= nil then
			made_id = made_id .. "/" .. subkeyValue;
		end
		if subkeyAttribute ~= nil then
			made_id = made_id .. "." .. subkeyAttribute;
		end
	end
	return made_id;
end

--- Extrait les différentes parties d'un ID, et les retourne. Retourne une table
--- contenant toutes les informations sur l'ID. Si l'ID n'est pas valide, lance
--- une erreur.
---
--- _@throws_ `Errors.InvalidId` - L'ID n'est pas valide
---@param id string
---@return {module:string,qualifier:Qualifier,specifier:Specifier?,name:string,attribute:string?,heart:string,qualification:string,subkey:{name:string,value:string?,attribute:string?,values:string[]?}?,subkeystr:string?}
local function extractId(id)
	local infos =
	{
		module = "base",
		qualifier = "",
		specifier = nil,
		name = "",
		attribute = nil,
		subkey = nil,
		subkeystr = nil,
		qualification = "",
		heart = ""
	};
	local withoutModule = "";
	local valid = isValid(id);
	if not valid then
		throw(Errors.InvalidId, 'Invalid ID : ' .. id, {
			id = id
		});
	end
	if valid == 1 or valid == 2 or (valid >= 9 and valid <= 12) then
		-- il y a un module
		infos.module, withoutModule = unpack(string.split(id, ':'));
	else
		withoutModule = id;
	end
	local subkeyAttribute, subkeyName, subkeyValue, attribute;
	infos.qualification, infos.name, attribute, subkeyAttribute = unpack(string.split(withoutModule, '%.'));

	infos.heart = infos.qualification .. "." .. infos.name;
	infos.qualifier, infos.specifier = unpack(string.split(infos.qualification, '@'));
	if attribute ~= nil then
		local subkey;
		infos.attribute, subkey = unpack(string.split(attribute, '@'));
		if subkey ~= nil then
			local t = string.split(subkey, '/');
			subkeyName = t[1];
			if #t > 1 then
				subkeyValue = table.concat(t, '/', 2);
			end
		end
	end

	if subkeyName ~= nil then
		infos.subkey = {};
		infos.subkey.name = subkeyName;
		infos.subkeystr = subkeyName;
		if subkeyValue ~= nil then
			infos.subkey.value = subkeyValue;
			infos.subkeystr = infos.subkeystr .. "/" .. subkeyValue;
		end
		if subkeyAttribute ~= nil then
			infos.subkey.attribute = subkeyAttribute;
			infos.subkeystr = infos.subkeystr .. "." .. subkeyAttribute;
		end
	end
	return infos;
end

--- Vérifie que l'ID contient bien un nom de module, et en rajoute un au besoin.
---
--- _@throws_ `Errors.InvalidId` - l'ID n'est pas valide
---@param id string
---@return string
local function ensureIdModule(id)
	local valid = isValid(id);
	if not valid then
		throw(Errors.InvalidId, 'Invalid ID : ' .. id, {
			id = id
		});
	end
	if valid == 1 or valid == 2 or (valid >= 9 and valid <= 12) then
		return id;
	else
		return "base:" .. id;
	end
end

return
{
	Qualifier = Qualifier,
	Specifier = Specifier,

	isValid        = isValid,
	createId       = createId,
	extractId      = extractId,
	ensureIdModule = ensureIdModule
};