require('calypso.core')
require('calypso.class')
require('foundation.log')


-- *****************************************************************************
-- * Scene
-- *
-- *  class Scene
-- *****************************************************************************

--- 	(constructor) Scene()
--- ---
--- Représente une scène du programme.
---@class Scene:Class
local Scene = class('Scene')

function Scene:initialize()
	self._uiComponents = {} ---@type Component[]
end

--- Le callback appelé au chargement de la scène. Peut être réécrit.
function Scene:load()
end

--- Le callback appelé à chaque frame, pour dessiner la scène. Peut être
--- réécrit.
function Scene:draw()
end

--- Fonction interne qui s'occupe de l'affichage de la scène. Elle appelle
--- l'affichage de tous les composants UI inscrits à la scène, puis appelle
--- `draw`. Il n'y a en principe pas besoin de la réécrire.
function Scene:internalDraw()
	for component in list.stream(self._uiComponents) do
		component:draw()
	end
	self:draw()
end

--- Le callback appelé à chaque frame, pour la logique de la scène. Peut être
--- réécrite.
---@param deltatime number
function Scene:update(deltatime)
end

--- Le callback appelé quand l'utilisat.eur.rice fait un clic de souris. Peut
--- être réécrite.
---@param x number La position X du clic
---@param y number La position Y du clic
---@param button number Quel bouton a été cliqué
---@param istouch boolean Est-ce un appui tactile ?
function Scene:onMousePressed(x, y, button, istouch)
end

--- Cette fonction interne est appelée lorsque l'utilisat.eur.rice fait un clic
--- de souris. C'est elle qui appelle `onMousePressed`. En principe, elle n'a
--- pas besoin d'être réécrite.
---@param x number
---@param y number
---@param button number
---@param istouch boolean
function Scene:internalMousepressed(x, y, button, istouch)
	for component in list.stream(self._uiComponents) do
		component:internalMousepressed(x, y, button, istouch)
	end
	self:onMousePressed(x, y, button, istouch)
end

--- Le callback appelé lorsque l'utilisat.eur.rice relâche un clic de souris.
--- Peut être réécrite.
---@param x number La position X du clic
---@param y number La position Y du clic
---@param button number Quel bouton a été cliqué
---@param istouch boolean Est-ce un appui tactile ?
function Scene:onMouseReleased(x, y, button, istouch)
end

--- Cette fonction interne est appelé lorsque l'utilisat.eur.rice relâche un
--- clic de souris. C'est elle qui appelle `onMouseReleased`. Elle n'a en
--- principe pas besoin d'être réécrite.
---@param x number
---@param y number
---@param button number
---@param istouch boolean
function Scene:internalMousereleased(x, y, button, istouch)
	for  component in list.stream(self._uiComponents) do
		component:internalMousereleased(x, y, button, istouch)
	end
	self:onMouseReleased(x, y, button, istouch)
end

--- Le callback appelé losque l'utilisat.eur.rice appuie sur une touche du
--- clavier. Peut être réécrite.
---@param key string La touche
function Scene:onKeyPressed(key)
end

--- Cette fonction interne est appelée lorsque l'utilisat.eur.rice appuie sur
--- une touche du clavier. C'est elle qui appelle `onKeyPressed`. Elle n'a pas
--- besoin, en principe, d'être réécrite.
---@param key string
function Scene:internalKeypressed(key)
	self:onKeyPressed(key)
end

--- Le callback appelé lorsque l'utilisat.eur.rice relâche une touche du
--- clavier. Peut être réécrite.
---@param key string La touche
function Scene:onKeyReleased(key)
end

--- Cette fonction interne est appelée lorsque l'utilisat.eur.rice relâche une
--- touche du clavier. C'est elle qui appelle `onKeyReleased`. Elle n'a pas
--- besoin, en principe, d'être réécrite.
---@param key string
function Scene:internalKeyreleased(key)
	self:onKeyReleased(key)
end

--- Enregistre un composant d'UI dans la scène, et retourne son index
--- d'enregistrement.
---@param component Component
---@return integer
function Scene:registerUIComponent(component)
	self._uiComponents[#self._uiComponents+1] = component
	return #self._uiComponents
end

--- Supprime un composant d'UI de la scène, en se basant sur son index
--- d'enregistrement obtenu en retour de `registerUIComponent`, et retourne ce
--- composant.
---@param index integer
---@return Component
function Scene:unregisterUIComponent(index)
	return table.remove(self._uiComponents, index)
end

--- Retourne un composant d'UI enregistré sur la scène en se basant sur son
--- index obtenu en retour de `registerUIComponent`. Si cet index n'existe pas,
--- lève une erreur.
---@param index integer
---@return Component
function Scene:getUIComponent(index)
	if self._uiComponents[index] ~= nil then
		return self._uiComponents[index]
	else
		error("[Scene:getUIComponent] No such component, invalid index : " .. tostring(index), 2)
	end
end


-- *****************************************************************************
-- * ScenesManager
-- *
-- *  abstract class ScenesManager
-- *****************************************************************************

--- Gère les scènes du jeu.
---@class ScenesManager
local ScenesManager = {}

ScenesManager._needCallToSceneLoad = false
ScenesManager._scenes              = {} ---@type table<string,Scene>
ScenesManager._currentSceneId      = ""

--- Vérifie s'il existe une `Scene` enregistrée sous l'ID donné.
---@param id string
---@return boolean
function ScenesManager.hasScene(id)
	return ScenesManager._scenes[id] ~= nil
end

--- Enregistre une nouvelle `Scene`.
---@param id string
---@param scene Scene
function ScenesManager.registerScene(id, scene)
	ScenesManager._scenes[id] = scene
end

--- Supprime une scène.
---@param id string
function ScenesManager.unregisterScene(id)
	ScenesManager._scenes[id] = nil
end

--- Retourne l'ID de la scène actuelle.
---@return string
function ScenesManager.getCurrentSceneId()
	return ScenesManager._currentSceneId
end

--- Modifie la scène actuelle.
---@param id string
function ScenesManager.setCurrentScene(id)
	ScenesManager._currentSceneId = id
	ScenesManager._needCallToSceneLoad = true
end

--- Retourne la scène actuelle.
---@return Scene
function ScenesManager.getCurrentScene()
	return ScenesManager._scenes[ScenesManager.getCurrentSceneId()]
end

--- Fonction interne. Appelle le `Scene:load` de la scène actuelle si celui-ci
--- n'a encore jamais été effectué.
function ScenesManager.callSceneLoadIfNeeded()
	if ScenesManager._needCallToSceneLoad then
		ScenesManager.getCurrentScene():load()
	end
	ScenesManager._needCallToSceneLoad = false
end


return
{
	Scene         = Scene,
	ScenesManager = ScenesManager
}