require('calypso.core')


return
{
	--- Encode une table vers une chaîne de caractères au format CSV.
	---@param data string[][]
	---@param sep? string `;` par défaut
	---@param newline? string `\n` par défaut
	---@return string
	encode = function(data, sep, newline)
		local csv = ''
		local sep = sep or ';'
		local newline = newline or '\n'

		for line in list.stream(data) do
			csv = csv .. table.concat(line, sep) .. newline
		end

		return csv
	end,

	--- Décode une chaîne de caractères au format CSV vers une table lua.
	---@param data string
	---@param sep? string `;` par défaut
	---@param newline? string `\n` par défaut
	decode = function(data, sep, newline)
		local sep = sep or ';'
		local newline = newline or '\n'
		local t = {}

		for line in list.stream(string.split(data, newline)) do
			t[#t+1] = {}
			for cell in list.stream(string.split(line, sep)) do
				t[#t][#t[#t]+1] = cell
			end
		end

		return t
	end
}