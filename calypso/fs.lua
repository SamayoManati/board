require('calypso.core');

--- Contient les fonctions relatives aux fichiers et aux chemin de fichiers.
fs =
{
	--- Construit un chemin d'accès constitué de tous les paramètres. Met des `/`
	--- entre chaque paramètre si besoin. Retourne une chaîne de caractères
	--- constituée de la manière suivante :
	---
	--- 	{...}[1] .. '/' .. {...}[n]
	---@param ... string
	---@return string
	path = function(...)
		local components = {...};

		for i = 1, #components do
			local iend = components[i]:sub(-1);
			local istart = components[i]:sub(1, 1);
			if iend == '/' or iend == '\\' then
				components[i] = components[i]:sub(1, -2);
			end
			if istart == '/' or istart == '\\' then
				components[i] = components[i]:sub(2);
			end
		end

		if #components == 0 then
			return '';
		elseif #components == 1 then
			return components[1];
		elseif #components == 2 then
			return components[1] .. '/' .. components[2];
		else
			return fs.path(fs.path(components[1], components[2]), unpack(components, 3));
		end
	end,

	--- Retourne le nom du fichier `s` sans l'extension de fichier.
	---@param s string
	---@return string
	getFileCorename = function(s)
		return string.match(s, '(.*)%.[^.]+$') or s;
	end,

	--- Vérifie si le chemin d'accès `path` existe et est un fichier.
	---@param path string
	---@return boolean
	existsAsFile = function(path)
		local x = love.filesystem.getInfo(path);
		return x ~= nil and x.type == "file";
	end,

	--- Vérifie si le chemin d'accès `path` existe et est un dossier.
	---@param path string
	---@return boolean
	existsAsDir = function(path)
		local x = love.filesystem.getInfo(path);
		return x ~= nil and x.type == "directory";
	end
};