require('foundation.log');


--- Sépare une chaîne de caractères `s` selon une liste de caractères `sep`,
--- et retourne la table ainsi obtenue.
---
--- *Fonction ajoutée par Calypso.*
---@param s string
---@param sep? string Par défaut, `'%s'`
---@return string[]
function string.split(s, sep)
	local s = tostring(s);
	local sep = sep or '%s';
	local t = {};
	for field, s in string.gmatch(s, "([^" .. sep .. "]*)(" .. sep .. "?)") do
		t[#t+1] = field;
		if s == "" then
			return t;
		end
	end
	return t;
end

--- Met la première lettre de `s` en majuscule.
---
--- *Fonction ajoutée par Calypso.*
---@param s string
---@return string
function string.upperf(s)
	local s = tostring(s);
	return s:sub(1, 1):upper() .. s:sub(2);
end

--- Met la première lettre de chaque mot en majuscule.
---
--- *Fonction ajoutée par Calypso.*
---@param s string
---@return string
function string.title(s)
	local s = tostring(s);
	local n = {};
	for _, part in ipairs(s:split()) do
		n[#n+1] = part:upperf();
	end
	return table.concat(n, ' ');
end


---@alias ErrorObject {message:string, causedBy?:ErrorObject, fullMessage:string, traceback:string, fullTraceback:string, causeTraceback?:string, type:Errors, additionalInformations:table<string,string>}


---@enum Errors
Errors =
{
	--- Erreur sans précision
	Error = 'Error',
	--- Une valeur est en-dehors de son étendue autorisée
	OutOfRange = 'OutOfRange',
	--- Une valeur vaut `nil` et ne devrait pas
	NilValue = 'NilValue',

-- *****************************************************************************
-- * ERREURS SUR LES FICHIERS
-- *****************************************************************************

	--- Impossible de trouver un fichier
	FileNotFound = 'FileNotFound',
	--- Impossible de trouver un dossier
	FolderNotFound = 'FolderNotFound',
	--- Le chargement d'un fichier a échouté
	LoadingFailed = 'LoadingFailed',

-- *****************************************************************************
-- * ERREURS SUR LES IDENTIFIANTS
-- *****************************************************************************

	--- Un ID existe déjà
	DuplicatedId = 'DuplicatedId',
	--- Un ID est introuvable
	UnknownId = 'UnknownId',
	--- Un ID n'est pas valide
	InvalidId = 'InvalidId',

-- *****************************************************************************
-- * ERREURS SUR LE DOM
-- *****************************************************************************

	--- Impossible de trouver une balise DOM
	MarkupNotFound = 'MarkupNotFound',
	--- Impossible de trouver un attribut DOM
	MarkupAttributeNotFound = 'MarkupAttributeNotFound',
	--- Une balise DOM est vide et ne devrait pas l'être
	EmptyMarkup = 'EmptyMarkup',
	--- Un attribut d'une balise DOM est vide et ne devrait pas l'être
	EmptyMarkupAttribute = 'EmptyMarkupAttribute',
	--- Une balise DOM n'a pas le nom qu'elle devrait avoir
	WrongMarkupName = 'WrongMarkupName',

-- *****************************************************************************
-- * ERREURS DE TYPES & DE CONTENU
-- *****************************************************************************

	--- Un argument fourni à une fonction n'est pas d'un type espéré
	BadArgument = 'BadArgument',
	--- Deux types de données ne sont pas compatibles ensemble
	TypesIncompatibility = 'TypesIncompatibility',
	--- La valeur fournie n'est pas l'une des valeurs de l'énumération autorisée
	InvalidEnumValue = 'InvalidEnumValue',
	--- La valeur fournie n'est pas valide au regard du contexte
	InvalidValue = 'InvalidValue',

-- *****************************************************************************
-- * ERREURS SUR LES ITEMS
-- *****************************************************************************

	--- Un item n'existe pas
	UnknownItem = 'UnknownItem',
	--- Une option d'un item n'existe pas
	UnknownItemOption = 'UnknownItemOption',

-- *****************************************************************************
-- * ERREURS SUR LES TAGS
-- *****************************************************************************

	--- Un tag n'existe pas
	UnknownTag = 'UnknownTag',
	--- On a fourni un tag d'un mauvais type
	InvalidTagType = 'InvalidTagType',

-- *****************************************************************************
-- * ERREURS SUR LES LANGUES
-- *****************************************************************************

	--- Une langue n'existe pas
	UnknownLocale = 'UnknownLocale',

-- *****************************************************************************
-- * ERREURS SUR LES TYPES DE MACHINES
-- *****************************************************************************

	--- Un type de machines n'existe pas
	UnknownMachineType = 'UnknownMachineType',

-- *****************************************************************************
-- * ERREURS SUR LES MODELES DE MACHINES
-- *****************************************************************************

	--- Un modèle de machines n'existe pas
	UnknownMachineModel = 'UnknownMachineModel',

-- *****************************************************************************
-- * ERREURS SUR LES RECETTES
-- *****************************************************************************

	--- La recette n'existe pas
	UnknownRecipe = 'UnknownRecipe',

-- *****************************************************************************
-- * ERREURS SUR LES STRUCTURES DE DONNEES
-- *****************************************************************************

	--- Une valeur existe en plusieurs exemplaires, et ne devrait pas
	DuplicatedValue = 'DuplicatedValue',
	--- Un index n'existe pas dans une table
	ArrayOutOfBound = 'ArrayOutOfBound'
};


--- Construit une erreur.
---@param kind Errors
---@param msg string
---@param additionalInformations? table<string, string>
---@param causedBy? ErrorObject
---@param level? integer
---@return ErrorObject
function buildError(kind, msg, additionalInformations, causedBy, level)
	local err = {};
	local BASE_LEVEL = 2;
	local level = level;
	if not level then
		level = BASE_LEVEL;
	else
		level = BASE_LEVEL + (level - 1);
	end

	err.message = msg;
	err.causedBy = causedBy;
	err.type = kind;
	err.additionalInformations = additionalInformations or {};
	err.fullMessage = err.type .. ' : ' .. err.message;

	local x = '';
	for k,v in pairs(err.additionalInformations) do
		x = x .. '\t| ' .. string.title(k) .. ' : ' .. v .. '\n';
	end
	err.fullMessage = err.fullMessage .. '\n' .. x;

	err.traceback = debug.traceback('', level);
	err.fullTraceback = err.fullMessage .. err.traceback;

	if causedBy ~= nil then
		err.causeTraceback = 'Caused by :\n' .. causedBy.fullTraceback;
		err.fullTraceback = err.fullTraceback .. '\n' .. err.causeTraceback;
	end

	--return err;
	return setmetatable(err, {
		__tostring = function (t)
			return t.fullTraceback;
		end
	});
end

--- Construit une erreur avec `buildError` et la lance immédiatement.
---
--- Si une erreur est lancée au sein d'un environnement `pcall`, l'erreur
--- retournée sera du type `ErrorObject`. Exemple :
---
--- 	local success, ret = pcall(function_that_throws, arg1, argN);
--- 	if not success then
--- 		-- ret est du type `ErrorObject`, on peut donc faire :
--- 		throw(Errors.Error, 'An error has been caught', {}, ret);
--- 	end
---@param kind Errors
---@param msg string
---@param additionalInformations? table<string, string>
---@param causedBy? ErrorObject
function throw(kind, msg, additionalInformations, causedBy)
	local err = buildError(kind, msg, additionalInformations or {}, causedBy or nil, 2);
	logerr(err);
	error(err);
end


--- Simule une condition ternaire. Si l'argument `cond` est vrai, retourne
--- l'argument `yes`. Sinon, retourne l'argument `no`.
---@generic T, U
---@param cond boolean
---@param yes T
---@param no U
---@return T|U
function ternary(cond, yes, no)
	if cond then
		return yes;
	else
		return no;
	end
end

--- Simule une condition de type switch. Le premier paramètre est le sujet. Le
--- second est la valeur par défaut, qui sera retournée si aucun test n'est
--- concluant. Ensuite, tous les autres arguments sont, par deux, une valeur
--- que peut prendre `subject`, puis la valeur à retourner dans ce cas. La
--- fonction doit avoir un nombre pair d'argument (afin d'éviter un test sans
--- valeur de retour associée), sans quoi une erreur sera levée.
---
--- **Exemple** :
---
--- 	local bar = 2
--- 	local foo = switch(bar, "zéro", 1, "un", 2, "deux", 3, "trois")
--- 	print(foo) --> deux
---
--- _@throws_ `Errors.BadArgument` - La fonction n'a pas un nombre pair
---           d'arguments.
---@param subject any
---@param default any
---@param ... any
---@return any
function switch(subject, default, ...)
	local args = {...};
	if #args % 2 ~= 0 then
		throw(Errors.BadArgument, 'Function switch must have an even number of arguments');
	end
	for i = 1, #args, 2 do
		if subject == args[i] then
			return args[i + 1];
		end
	end
	return default;
end

--- Vérifie si un paramètre est bel et bien d'un des types autorisés. Sinon,
--- lève une erreur.
---@generic T
---@param contextName string Le nom du contexte, c'est-à-dire le nom de la fonction ou de la classe:la méthode
---@param parameterName string Le nom du paramètre
---@param expectedTypes table[] Une liste des types attendus
---@param actualValue T La valeur du paramètre
function assertParameterType(contextName, parameterName, expectedTypes, actualValue)
	local errorMessage = "[" .. tostring(contextName) .. "] : Bad parameter " .. tostring(parameterName)

	for _, v in ipairs(expectedTypes) do
		if actualValue:isInstanceOf(v) then
			return
		end
	end

	error(errorMessage, 3)
end

--- Vérifie qu'un paramètre soit bel et bien de l'un des types autorisés. S'il
--- ne l'est pas, lève une erreur.
---
--- Cette fonction fonctionne avec les types primitifs comme avec les classes.
---
--- _@throws_ `Errors.BadArgument`
---@generic T
---@param parameterName string Le nom du paramètre, utilisé dans le message d'erreur
---@param expected table Une liste des types attendus
---@param got T La valeur du paramètre
function assertArgumentType(parameterName, expected, got)
	for _, t in ipairs(expected) do
		if type(t) == "string" then
			local s, r = pcall(function ()
				---@cast got Class
				return got:instanceof(Class)
			end);
			if not s then
				
			end
		end

		if type(got) == "table" then
			local s, r = pcall(function ()
				---@cast got Class
			end);
		end
	end
end

--- Vérifie si un nombre `n` est un nombre entier.
---@param n number
---@return boolean
function isInteger(n)
	return n % 1 == 0;
end


--- Contient les fonctions relatives aux listes, c'est-à-dire les tables au
--- format `T[]`.
list =
{
	--- Itère sur les valeurs de la liste. Attention, une valeur `nil`
	--- interrompt l'exécution.
	---@generic T
	---@param l T[]
	---@return fun(): T
	stream = function(l)
		local i = 0;
		return function ()
			i = i + 1;
			if l[i] ~= nil then
				return l[i];
			end
		end;
	end,

	--- Itère sur les valeurs de la liste qui passent le filtre. Attention, une
	--- valeur `nil` interrompt l'exécution.
	---@generic T
	---@param l T[]
	---@param filter fun(i: T): boolean
	---@return fun(): T
	fstream = function(l, filter)
		local i = 0;
		return function()
			i = i + 1;
			while l[i] ~= nil do
				if filter(l[i]) then
					return l[i];
				else
					i = i + 1;
				end
			end
		end;
	end,

	--- Itère sur les index et les valeurs de la liste qui passent le filtre.
	--- Attention, une valeur `nil` interrompt l'exécution.
	---@generic T
	---@param l T[]
	---@param filter fun(k: integer, v: T): boolean
	---@return fun(): integer, T
	fmap = function(l, filter)
		local i = 0;
		return function()
			i = i + 1;
			while l[i] ~= nil do
				if filter(i, l[i]) then
					return i, l[i];
				else
					i = i + 1;
				end
			end
		end;
	end,

	--- Vérifie si la liste `l` contient une valeur `v`. Si tel est le cas,
	--- retourne `true` et le premier index de `v` dans `l`. Sinon, retourne
	--- simplement `false`.
	---@generic T
	---@param l T[]
	---@param v T
	---@return boolean exists
	---@return integer? firstIndex
	contains = function(l, v)
		for i, j in ipairs(l) do
			if j == v then
				return true, i;
			end
		end
		return false;
	end,

	--- Vérifie si la liste `l` contient une valeur qui passe le filtre. Si tel
	--- est lecas, retourne `true` et le premier index de cette valeur. Sinon,
	--- retourne simplement `false`. Le filtre prend deux paramètres : en
	--- premier la valeur examinée, et en deuxième son index.
	---@generic T
	---@param l T[]
	---@param filter fun(v: T, k?: integer): boolean
	---@return boolean exists
	---@return integer? firstIndex
	fcontains = function(l, filter)
		for i, j in ipairs(l) do
			if filter(j, i) then
				return true, i;
			end
		end
		return false;
	end,

	--- Ajoute les données de `l2` à la suite de celles de `l1`.
	---@generic T
	---@param l1 T[]
	---@param l2 T[]
	merge = function(l1, l2)
		for i = 1, #l2 do
			l1[#l1+1] = l2[i];
		end
	end,

	--- Fusionne deux listes et retourne le résultat en tant que nouvelle liste.
	---@generic T
	---@param l1 T[]
	---@param l2 T[]
	---@return T[]
	mergec = function(l1, l2)
		local l3 = {};
		list.merge(l3, l1);
		list.merge(l3, l2);
		return l3;
	end,

	--- Ajoute à la fin de `l1` les données de `l2` qui passent le filtre.
	---
	--- Le premier argument du filtre est la valeur examinée, et le deuxième son
	--- index.
	---@generic T
	---@param l1 T[]
	---@param l2 T[]
	---@param filter fun(v: T, k?: integer): boolean
	fmerge = function(l1, l2, filter)
		for i = 1, #l2 do
			if filter(l2[i], i) then
				l1[#l1+1] = l2[i];
			end
		end
	end,

	--- Fusionne les données de `l1` et `l2` qui passent le filtre et retourne
	--- le résultat en tant que nouvelle liste.
	---
	--- Le premier argument du filtre est la valeur examinée, et le deuxième
	--- son index.
	---@generic T
	---@param l1 T[]
	---@param l2 T[]
	---@param filter fun(v: T, k?: integer): boolean
	---@return T[]
	fmergec = function(l1, l2, filter)
		local l3 = {};
		list.fmerge(l3, l1, filter);
		list.fmerge(l3, l2, filter);
		return l3;
	end
};

--- Contient les fonctions utilitaires relatives aux tables au format
--- `table<K, V>`.
map =
{
	--- Vérifie si `m` contient une entrée avec la clef donnée.
	---@generic K, V
	---@param m table<K, V>
	---@param key K
	---@return boolean
	contains = function(m, key)
		return m[key] ~= nil;
	end,

	--- Itère sur les clefs et les valeurs de la map qui passent le filtre.
	---@generic K, V
	---@param m table<K, V>
	---@param filter fun(k: K, v: V): boolean
	---@return fun(t: table<K, V>, k: K): K, V
	---@return table<K, V>
	---@return nil
	fmap = function(m, filter)
		local iteratorFunc, theTableOnWhichWeBeIterating, firstKey = pairs(m);
		return function(theTableOnWhichWeBeIterating, firstKey)
			-- on itère toujours sur la prochaine entrée, parce que la première
			-- vaut nil, nil
			local nextKey, nextValue = iteratorFunc(theTableOnWhichWeBeIterating, firstKey);
			while (nextKey ~= nil) and (nextValue ~= nil) and (not filter(nextKey, nextValue)) do
				-- tant qu'on a pas atteint la fin de la liste, et que
				-- l'entrée sur laquelle on itère ne passe pas le filtre, on
				-- essaye avec l'entrée d'après.
				nextKey, nextValue = iteratorFunc(theTableOnWhichWeBeIterating, nextKey);
			end
			return nextKey, nextValue;
		end, theTableOnWhichWeBeIterating, firstKey;
	end,

	--- Ajoute à `m1` les données de `m2`. Si le filtre de priorité n'est pas
	--- fourni, la priorité sera toujours donnée aux éléments de `m1`.
	---
	--- Le paramètre `priority` permet de définir le comportement à adopter
	--- dans le cas où une entrée avec la même clef est présente à la fois dans
	--- `m1` et dans `m2`. C'est une fonction qui prend en paramètres :
	---  * `k` : La clef
	---  * `v1` : La valeur dans `m1`
	---  * `v2` : La valeur dans `m2`
	--- Elle doit retourner la valeur à conserver. Cela peut être simplement
	--- l'une des deux valeurs fournies, mais des changements peuvent être
	--- appliqués.
	---@generic K, V
	---@param m1 table<K, V>
	---@param m2 table<K, V>
	---@param priority? fun(k: K, v1: V, v2: V): V
	merge = function(m1, m2, priority)
		if priority == nil then
			priority = function(k, v1, v2) return v1; end;
		end

		for k2, v2 in pairs(m2) do
			if map.contains(m1, k2) then
				m1[k2] = priority(k2, m1[k2], v2);
			else
				m1[k2] = v2;
			end
		end
	end,

	--- Fusionne `m1` et `m2` et retourne le résultat en tant que nouvelle
	--- table. Les paramètres de `priority` sont exactement les mêmes que pour
	--- `map.merge`, qui est d'ailleurs utilisée en interne.
	---@generic K, V
	---@param m1 table<K, V>
	---@param m2 table<K, V>
	---@param priority? fun(k: K, v1: V, v2: V): V
	---@return table<K, V>
	mergec = function(m1, m2, priority)
		local m3 = {};
		map.merge(m3, m1, priority);
		map.merge(m3, m2, priority);
		return m3;
	end,

	--- Ajoute à `m1` les données de `m2` qui passent le filtre. Au-delà de ça,
	--- le fonctionnement est identique à celui de `map.merge`.
	---@generic K, V
	---@param m1 table<K, V>
	---@param m2 table<K, V>
	---@param filter fun(k: K, v: V): boolean
	---@param priority? fun(k: K, v1: V, v2: V): V
	fmerge = function(m1, m2, filter, priority)
		if priority == nil then
			priority = function(k, v1, v2) return v1; end;
		end

		for k2, v2 in pairs(m2) do
			if filter(k2, v2) then
				if map.contains(m1, k2) then
					m1[k2] = priority(k2, m1[k2], v2);
				else
					m1[k2] = v2;
				end
			end
		end
	end,

	--- Fusionne les données de `m1` et de `m2` qui passent le filtre, et les
	--- retourne dans une nouvelle table. Le fonctionnement est identique à
	--- celui de `map.fmerge`.
	---@generic K, V
	---@param m1 table<K, V>
	---@param m2 table<K, V>
	---@param filter fun(k: K, v: V): boolean
	---@param priority? fun(k: K, v1: V, v2: V): V
	---@return table<K, V>
	fmergec = function(m1, m2, filter, priority)
		local m3 = {};
		map.fmerge(m3, m1, filter, priority);
		map.fmerge(m3, m2, filter, priority);
		return m3;
	end,

	--- Retourne la taille de `m`.
	---@generic K, V
	---@param m table<K, V>
	---@return integer
	size = function(m)
		local i = 0;
		for _ in pairs(m) do
			i = i + 1;
		end
		return i;
	end
};

--- Génère un UUID.
---
--- N'oubliez pas d'appeler `math.randomseed()` avant la première utilisation de
--- `uuid()`, afin d'éviter d'obtenir plusieurs fois le même UUID dans des
--- exécutions différentes.
---
--- Exemple :
---
--- 	math.randomseed(tonumber(tostring(os.time()):reverse():sub(1, 9)));
---@return string
function uuid()
	local template = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
	return (string.gsub(template, '[xy]', function(c)
		local v = (c == 'x') and math.random(0, 0xf) or math.random(8, 0xb);
		return string.format('%x', v);
	end));
end
