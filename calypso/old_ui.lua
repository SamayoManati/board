require('calypso.core')
require('calypso.class')
local themes = require('calypso.themes')


--- Retourne la position centrée pour un objet.
---@param objWidth number La largeur de l'objet à centrer
---@param frameWidth number La largeur du cadre de l'objet
---@param offset? number Le décalage vers la droite. `0` par défaut
---@return number
local function centered(objWidth, frameWidth, offset)
	local offset = offset or 0
	return offset + ((frameWidth - objWidth) / 2)
end


-- *****************************************************************************
-- * TableReadingMethod
-- *
-- *  enum TableReadingMethod
-- *****************************************************************************

--- Cette enum contient les différentes méthodes d'interprétation possibles
--- pour le contenu des tableaux.
---@see Table.setContent
---@enum TableReadingMethod
local TableReadingMethod =
{
	KEY_VALUE = "KEY_VALUE",
	VALUE     = "VALUE"
}


-- *****************************************************************************
-- * Rect
-- *
-- *  class Rect
-- *****************************************************************************

--- 	(constructor) Rect(x?: number, y?: number, w?: number, h?: number)
---
--- _@param_ `x` - Position X, `0` par défaut
---
--- _@param_ `y` - Position Y, `0` par défaut
---
--- _@param_ `w` - Largeur, `0` par défaut
---
--- _@param_ `h` - Hauteur, `0` par défaut
---
--- ---
--- Une petite classe utilitaire faite pour rassembler les informations
--- nécessaires à la forme d'un rectangle : sa position X et Y, et ses
--- dimensions (largeur et hauteur). Ses propriétés sont publiques.
---@class Rect:Class
local Rect = class('Rect')

--- Constructeur de classe.
---@param x? number position X, `0` par défaut
---@param y? number position Y, `0` par défaut
---@param w? number largeur, `0` par défaut
---@param h? number hauteur, `0` par défaut
function Rect:initialize(x, y, w, h)
	self.x = x or 0
	self.y = y or 0
	self.width = w or 0
	self.height = h or 0
end

--- Vérifie si un point donné via ses coordonnées X et Y est contenu ou non dans
--- le rectangle.
---@param x number
---@param y number
---@return boolean
function Rect:containsPosition(x, y)
	return x >= self.x and x <= self.width + self.x and y >= self.y and y <= self.height + self.y
end


-- *****************************************************************************
-- * Component
-- *
-- *  class Component
-- *****************************************************************************

--- 	(constructor) Component()
--- ---
--- La classe-mère de tous les composants
---@class Component:Class
local Component = class('Component')

function Component:initialize()
	self._location = Rect() ---@type Rect
	self._mouseClickedCallback  = function(x, y, button, istouch) end
	self._mouseReleasedCallback = function(c, y, button, istouch) end
end

--- Retourne la taille du composant.
---@return number width
---@return number height
function Component:getSize()
	return self._location.width, self._location.height
end

--- Retourne la largeur du composant.
---@return number
function Component:getWidth()
	return self._location.width
end

--- Retourne la hauteur du composant.
---@return number
function Component:getHeight()
	return self._location.height
end

--- Modifie la taille du composant.
---@param width number
---@param height number
---@return self self
function Component:setSize(width, height)
	self._location.width = width
	self._location.height = height
	return self
end

--- Retourne la position du composant.
---@return number x
---@return number y
function Component:getPosition()
	return self._location.x, self._location.y
end

--- Retourne la position X du composant.
---@return number
function Component:getPositionX()
	return self._location.x
end

--- Retourne la position Y du composant.
---@return number
function Component:getPositionY()
	return self._location.y
end

--- Modifie la position du composant.
---@param x number
---@param y number
---@return self self
function Component:setPosition(x, y)
	self._location.x = x
	self._location.y = y
	return self
end

--- Retourne le `Rect` qui contient la position et la taille du composant.
---@return Rect
function Component:getLocation()
	return self._location
end

--- Définit un `Rect` qui contient la position et la taille du composant.
---@param rect Rect
---@return self self
function Component:setLocation(rect)
	self._location = rect
	return self
end

--- Définit le callback appelé quand on clic sur le composant. Les paramètres
--- suivants sont passés au callback :
---
--- 1. `number` position X du clic
--- 2. `number` position Y du clic
--- 3. `number` le bouton qui a cliqué
--- 4. `boolean` Est-ce un appui tactile ?
---@param fnt fun(x: number, y: number, btn: number, istouch: boolean)
---@return self self
function Component:onMouseClick(fnt)
	self._mouseClickedCallback = fnt
	return self
end

--- Définit le callback appelé quand on relâche le clic sur le composant. Les
--- paramètres passés sont les mêmes que pour `onMouseClick`.
---@see Component.onMouseClick
---@param fnt fun(x: number, y: number, btn: number, istouch: boolean)
---@return self self
function Component:onMouseRelease(fnt)
	self._mouseReleasedCallback = fnt
	return self
end

function Component:internalMousepressed(x, y, button, istouch)
	self._mouseClickedCallback(x, y, button, istouch)
end

function Component:internalMousereleased(x, y, button, istouch)
	self._mouseReleasedCallback(x, y, button, istouch)
end

function Component:internalKeypressed(key)
end

function Component:internalKeyreleased(key)
end

function Component:internalUpdate(deltatime)
end


-- *****************************************************************************
-- * StyleableComponent
-- *
-- *  class StyleableComponent extends Component
-- *****************************************************************************

--- 	(constructor) StyleableComponent()
--- ---
--- Un composant qui prend son apparence depuis les thèmes actifs. Les composants
--- qui héritent de celui-là peuvent utiliser les thèmes pour déterminer leur
--- apparence visuelle.
---@class StyleableComponent:Component
local StyleableComponent = class('StyleableComponent', Component)

function StyleableComponent:initialize()
	StyleableComponent.super.initialize(self)
	self._styleTarget = ""
end

--- Définit la cible utilisée par le thème pour choisir le style du composant.
---@param target string
---@return self self
function StyleableComponent:setStyleTarget(target)
	self._styleTarget = target
	return self
end

--- Retourne la cible utilisée par le thème pour choisir le style du composant.
---@return string
function StyleableComponent:getStyleTarget()
	return self._styleTarget
end


-- *****************************************************************************
-- * ComponentsGroup
-- *
-- *  class ComponentsGroup extends Component
-- *****************************************************************************

--- 	(constructor) ComponentsGroup()
--- ---
--- Une classe-mère, de laquelle hérite tous les organiseurs de composants.
--- Cet organiseur n'a pas de grille, il place les éléments selon la position
--- et la taille définies dans chacun d'eux.
---@see ComponentsStack
---@see VerticalComponentsStack
---@see HorizontalComponentsStack
---@class ComponentsGroup:Component
local ComponentsGroup = class('ComponentsGroup', Component)

function ComponentsGroup:initialize()
	Component.initialize(self)
	self._content = {} ---@type Component[]
end

--- Enregistre un composant dans le groupe. Les composants sont tracés selon
--- leur ordre d'enregistrement (du plus ancien au plus récent).
---@param component Component
---@return integer # L'index d'enregistrement du composant.
function ComponentsGroup:registerComponent(component)
	self._content[#self._content+1] = component
	return #self._content
end

--- Désenregistre un composant selon son index d'enregistrement.
---@param index integer
---@return Component # Le composant que l'on vient de désenregistrer.
function ComponentsGroup:unregisterComponent(index)
	return table.remove(self._content, index)
end

--- Trace le groupe à la position indiquée et de la taille indiquée. Cette
--- fonction est appelée automatiquement. Les composants qui héritent de cette
--- classe doivent l'appeler dans leur fonction `draw`.
---@param x number
---@param y number
---@param width number
---@param height number
function ComponentsGroup:draw(x, y, width, height)
	for comp in list.stream(self._content) do
		local compRect = comp:getLocation()
		comp:draw(compRect.x + x, compRect.y + y, math.min(compRect.width, width), math.min(compRect.height, height))
	end
end

function ComponentsGroup:internalUpdate(deltatime)
	for component in list.stream(self._content) do
		component:update(deltatime)
	end
end

function ComponentsGroup:internalMousepressed(x, y, button, istouch)
	for comp in list.stream(self._content) do
		if comp:getLocation():containsPosition(x, y) then
			comp:internalMousepressed(x, y, button, istouch)
		end
	end
end

function ComponentsGroup:internalMousereleased(x, y, button, istouch)
	for comp in list.stream(self._content) do
		if comp:getLocation():containsPosition(x, y) then
			comp:internalMousereleased(x, y, button, istouch)
		end
	end
end


-- *****************************************************************************
-- * ComponentsStack
-- *
-- *  class CompontentsStack extends ComponentsGroup
-- *****************************************************************************

--- 	(constructor) ComponentsStack()
--- ---
--- Classe de laquelle héritent tous les organiseurs en stacks. Ils possèdent
--- une propriété `internalMargin`, réglant la largeur de la marge laissée entre
--- chaque composant au tracage.
---
--- Les organiseurs en stacks affichent leurs composants en divisant leur taille
--- de manière égale, de façon à occuper toute la place disponible. Ils est
--- cependant possible de leur attribuer une taille maximale, _via_ `setMaxSize`.
--- Lorsqu'une taille maximale est indiquée, les composants opteront pour la
--- plus petite valeur entre la taille maximale et la taille calculée. Si la
--- taille maximale est plus petite que la taille calculée, l'organiseur
--- centrera les composants.
---@see VerticalComponentsStack
---@see HorizontalComponentsStack
---@class ComponentsStack:ComponentsGroup
local ComponentsStack = class('ComponentsStack', ComponentsGroup)

function ComponentsStack:initialize()
	ComponentsStack.super.initialize(self)
	self._internalMargin = 1
	self._maxSize = nil
end

--- Définit la largeur de la marge laissée entre les composants.
---@param margin number
---@return self self
function ComponentsStack:setInternalMargin(margin)
	self._internalMargin = margin
	return self
end

--- Retourne la largeur de la marge laissée entre les composants.
---@return number
function ComponentsStack:getInternalMargin()
	return self._internalMargin
end

--- Définit la taille maximale des composants. Si une taille maximale est
--- définie, lors du tracage les composants opteront pour la plus petite
--- valeur entre la taille maximale et la taille calculée. Pour supprimer une
--- taille maximale, indiquez `nil`.
---@param size number|nil
---@return self self
function ComponentsStack:setMaxSize(size)
	self._maxSize = size
	return self
end

--- Retourne la taille maximale que peuvent prendre les composants, ou `nil`
--- s'il n'y a pas de taille maximale.
---@return number|nil
function ComponentsStack:getMaxSize()
	return self._maxSize
end


-- *****************************************************************************
-- * VerticalComponentsStack
-- *
-- *  class VerticalComponentsStack extends ComponentsStack
-- *****************************************************************************

--- 	(constructor) VerticalComponentsStack()
--- ---
--- Organise les composants verticalement, en une colonne. Les propriétés de
--- taille et de position des composants contenus dans cette grille sont
--- réécrites automatiquement à chaque traçage.
---@see HorizontalComponentsStack
---@class VerticalComponentsStack:ComponentsStack
local VerticalComponentsStack = class('VerticalComponentsStack', ComponentsStack)

function VerticalComponentsStack:initialize()
	VerticalComponentsStack.super.initialize(self)
end

---@param x number
---@param y number
---@param width number
---@param height number
function VerticalComponentsStack:draw(x, y, width, height)
	local totalMarginsHeight = self:getInternalMargin() * ternary(#self._content > 0, #self._content - 1, 0)
	local heightOfEachComponent = (height - totalMarginsHeight) / ternary(#self._content > 0, #self._content, 1)
	for componentIndex, component in ipairs(self._content) do
		---@cast component Component
		local chosenHeight = heightOfEachComponent
		local calculatedY = y + ((componentIndex - 1) * heightOfEachComponent) + ((componentIndex - 1) * self:getInternalMargin())
		local chosenY = calculatedY
		if self:getMaxSize() ~= nil and self:getMaxSize() < heightOfEachComponent then
			chosenHeight = self:getMaxSize()
			chosenY = calculatedY + ((heightOfEachComponent - chosenHeight) / 2)
		end
		component:setPosition(x, chosenY)
		component:setSize(width, chosenHeight)
		component:draw(x, chosenY, width, chosenHeight)
	end
end


-- *****************************************************************************
-- * HorizontalComponentsStack
-- *
-- *  class HorizontalComponentsStack extends ComponentsStack
-- *****************************************************************************

--- 	(constructor) HorizontalComponentsStack()
--- ---
--- Organise les composants horizontalement, en une seule ligne. Les propriétés
--- de taille et de position des composants contenus dans cet organiseur sont
--- réécrites automatiquement à chaque traçage.
---@see VerticalComponentsStack
---@class HorizontalComponentsStack:ComponentsStack
local HorizontalComponentsStack = class('HorizontalComponentsStack', ComponentsStack)

function HorizontalComponentsStack:initialize()
	HorizontalComponentsStack.super.initialize(self)
end

---@param x number
---@param y number
---@param width number
---@param height number
function HorizontalComponentsStack:draw(x, y, width, height)
	local totalMarginsWidth = self:getInternalMargin() * ternary(#self._content > 0, #self._content - 1, 0)
	local widthOfEachComponent = (width - totalMarginsWidth) / ternary(#self._content > 0, #self._content, 1)
	for componentIndex, component in ipairs(self._content) do
		---@cast component Component
		local chosenWidth = widthOfEachComponent
		local calculatedX = x + ((componentIndex - 1) * widthOfEachComponent) + ((componentIndex - 1) * self:getInternalMargin())
		local chosenX = calculatedX
		if self:getMaxSize() ~= nil and self:getMaxSize() < widthOfEachComponent then
			chosenWidth = self:getMaxSize()
			chosenX = calculatedX + ((widthOfEachComponent - chosenWidth) / 2)
		end
		component:setPosition(chosenX, y)
		component:setSize(chosenWidth, height)
		component:draw(chosenX, y, chosenWidth, height)
	end
end


-- *****************************************************************************
-- * TitlebarComponent
-- *
-- *  class TitlebarComponent extends StyleableComponent
-- *****************************************************************************

--- 	(constructor) TitlebarComponent()
--- ---
--- Permet l'affichage d'une barre de titre, en haut d'une fenêtre.
---
--- La cible par défaut pour le style est `window.titlebar`. La cible utilisée
--- pour le bouton de fermeture est `<cible>.closebtn`.
---@class TitlebarComponent:StyleableComponent
local TitlebarComponent = class('TitlebarComponent', StyleableComponent)

function TitlebarComponent:initialize()
	Component.initialize(self)
	self._text = ""
	self._displaysText = true
	self._displaysCloseButton = true
	self._styleTarget = "window.titlebar"
	self:setSize(self:getWidth(), 20)

	self._drawableText = 0
	self._drawableClosebtn = 0

	self._closebtnShape = {} ---@type number[]
	self._closebtnSelected = false

	self._closeButtonClicked  = function(btn, istouch) end
	self._closeButtonReleased = function(btn, istouch) end
end

--- Définit le texte affiché sur la barre de titre.
---@param text string
---@return self self
function TitlebarComponent:setText(text)
	self._text = text
	self._drawableText = 0
	return self
end

--- Retourne le texte affiché sur la barre de titre.
---@return string
function TitlebarComponent:getText()
	return self._text
end

--- Définit si la barre de titre affichera ou non le titre.
---@param b boolean
---@return self self
function TitlebarComponent:setDisplaysText(b)
	self._displaysText = b
	return self
end

--- Vérifie si la barre de titre affichera ou non le titre.
---@return boolean
function TitlebarComponent:getDisplaysText()
	return self._displaysText
end

--- Définit si la barre de titre possède un bouton de fermeture ou non. Lorsque
--- ce bouton est cliqué, le callback `closeButtonClicked` est appelé.
---@see TitlebarComponent.getDisplaysCloseButton
---@param b boolean
---@return self self
function TitlebarComponent:setDisplaysCloseButton(b)
	self._displaysCloseButton = b
	return self
end

--- Vérifie si la barre de titre possède un bouton de fermeture ou non.
---@see TitlebarComponent.setDisplaysCloseButton
---@return boolean
function TitlebarComponent:getDisplaysCloseButton()
	return self._displaysCloseButton
end

--- Définit le callback appelé quand le bouton de fermeture est cliqué. Les
--- arguments suivants sont passés au callback :
---
--- 1. `number` Le bouton cliqué
--- 2. `boolean` Tactile ?
---@param fnt fun(btn: number, istouch: boolean)
---@return self self
function TitlebarComponent:onCloseButtonClicked(fnt)
	self._closeButtonClicked = fnt
	return self
end

--- Définit le callback appelé quand on relâche un clic souris sur le bouton de
--- fermeture. Les arguments passés sont les mêmes que pour `onCloseButtonClicked`.
---@see TitlebarComponent.onCloseButtonClicked
---@param fnt fun(btn: number, istouch: boolean)
---@return self self
function TitlebarComponent:onCloseButtonReleased(fnt)
	self._closeButtonReleased = fnt
	return self
end

---@param selected boolean
function TitlebarComponent:drawCloseButton(selected)
	local bx = self._closebtnShape[1]
	local y  = self._closebtnShape[2]
	local w  = self._closebtnShape[3]
	local by = self._closebtnShape[4]
	local fgc = themes.ThemesManager.supplyForegroundColor(self:getStyleTarget() .. ".closebtn", themes.TargetState.SELECTED)
	local bgc = themes.ThemesManager.supplyBackgroundColor(self:getStyleTarget() .. ".closebtn", themes.TargetState.SELECTED)
	if selected and bgc ~= nil then
		love.graphics.setColor(bgc)
	else
		love.graphics.setColor(themes.ThemesManager.supplyBackgroundColor(self:getStyleTarget() .. ".closebtn"))
	end
	love.graphics.rectangle("fill", bx, y, w, by)
	if selected and fgc ~= nil then
		love.graphics.setColor(fgc)
	else
		love.graphics.setColor(themes.ThemesManager.supplyForegroundColor(self:getStyleTarget() .. ".closebtn"))
	end
	if self._drawableClosebtn == nil or self._drawableClosebtn == 0 then
		self._drawableClosebtn = love.graphics.newText(love.graphics.getFont(), "X")
	end
	local tx = bx + ((self:getHeight() - self._drawableClosebtn:getWidth()) / 2)
	local ty = (by - self._drawableClosebtn:getHeight()) / 2
	love.graphics.draw(self._drawableClosebtn, tx, ty)
end

function TitlebarComponent:draw(x, y, w, h)
	love.graphics.setColor(themes.ThemesManager.supplyBackgroundColor(self:getStyleTarget()))
	love.graphics.rectangle("fill", x, y, w, self:getHeight())
	if self:getDisplaysText() then
		if self._drawableText == nil or self._drawableText == 0 then
			self._drawableText = love.graphics.newText(love.graphics.getFont(), self:getText())
		end
		local tx = (w - self._drawableText:getWidth()) / 2
		local ty = (self:getHeight() - self._drawableText:getHeight()) / 2
		love.graphics.setColor(themes.ThemesManager.supplyForegroundColor(self:getStyleTarget()))
		love.graphics.draw(self._drawableText, tx, ty)
	end
	if self:getDisplaysCloseButton() then
		local bx = w - self:getHeight()
		local by = self:getHeight()
		self._closebtnShape = {bx, y, w, by}
		self:drawCloseButton(self._closebtnSelected)
	end
end

---@param x number
---@param y number
---@param button number
---@param istouch boolean
function TitlebarComponent:internalMousepressed(x, y, button, istouch)
	if x >= self._closebtnShape[1] and x <= self._closebtnShape[3] and y >= self._closebtnShape[2] and y <= self._closebtnShape[4] then
		self._closebtnSelected = true
		return self._closeButtonClicked(button, istouch)
	end
end

---@param x number
---@param y number
---@param button number
---@param istouch boolean
function TitlebarComponent:internalMousereleased(x, y, button, istouch)
	self._closebtnSelected = false
	if x >= self._closebtnShape[1] and x <= self._closebtnShape[3] and y >= self._closebtnShape[2] and y <= self._closebtnShape[4] then
		return self._closeButtonReleased(button, istouch)
	end
end


-- *****************************************************************************
-- * FooterBar
-- *
-- *  class FooterBar extends StyleableComponent
-- *****************************************************************************

--- 	(constructor) FooterBar()
--- ---
--- Une ligne en bas de la fenêtre, qui peut contenir du texte.
---
--- La cible par défaut pour le style est `footerbar`.
---@class FooterBar:StyleableComponent
local FooterBar = class('FooterBar', StyleableComponent)

function FooterBar:initialize()
	Component.initialize(self)
	self._text = ""
	self._centered = true
	self._styleTarget = "footerbar"
	self._drawableText = 0
	self:setSize(self:getWidth(), 20)
end

--- Définit le texte affiché par le footer.
---@param text string
---@return self self
function FooterBar:setText(text)
	self._text = text
	self._drawableText = 0
	return self
end

--- Retourne le texte affiché par le footer.
---@return string
function FooterBar:getText()
	return self._text
end

--- Définit si le texte s'affichera centré ou non.
---@param b boolean
---@return self self
function FooterBar:setCentered(b)
	self._centered = b
	return self
end

--- Vérifie si le texte s'affichera centré ou non.
---@return boolean
function FooterBar:getCentered()
	return self._centered
end

---@param x number
---@param y number
---@param w number
---@param h number
function FooterBar:draw(x, y, w, h)
	love.graphics.setColor(themes.ThemesManager.supplyBackgroundColor(self:getStyleTarget()))
	love.graphics.rectangle("fill", x, y, w, h)

	if #self:getText() > 0 then
		if self._drawableText == 0 then
			self._drawableText = love.graphics.newText(love.graphics.getFont(), self:getText())
		end
		local tx = ternary(self:getCentered(), centered(self._drawableText:getWidth(), w), 5) + x
		local ty = centered(self._drawableText:getHeight(), h, y)
		love.graphics.setColor(themes.ThemesManager.supplyForegroundColor(self:getStyleTarget()))
		love.graphics.draw(self._drawableText, tx, ty)
	end
end

---@param x number
---@param y number
---@param button number
---@param istouch boolean
function FooterBar:internalMousepressed(x, y, button, istouch)
end


-- *****************************************************************************
-- * Button
-- *
-- *  class Button extends StyleableComponent
-- *****************************************************************************

--- 	(constructor) Button()
--- ---
--- Permet de créer des boutons.
---
--- La cible par défaut pour le style est `button`.
---@class Button:StyleableComponent
local Button = class('Button', StyleableComponent)

function Button:initialize()
	Component.initialize(self)
	self._text = ""
	self._styleTarget = "button"
	self._selected = false

	self._drawableText = 0
end

--- Définit le texte du bouton.
---@param text string
---@return self self
function Button:setText(text)
	self._text = text
	self._drawableText = 0
	return self
end

--- Retourne le texte du bouton.
---@return string
function Button:getText()
	return self._text
end

--- Définit si le bouton est actuellement sélectionné ou non.
---@param b boolean
---@return self self
function Button:setSelected(b)
	self._selected = b
	return self
end

--- Vérifie si le bouton est actuellementt sélectionné ou non.
---@return boolean
function Button:getSelected()
	return self._selected
end

---@param x number
---@param y number
---@param w number
---@param h number
function Button:draw(x, y, w, h)
	--local bx, by = self:getPosition()
	--local bw, bh = self:getSize()
	--bx = bx + x
	--by = by + y
	local bx = x
	local by = y
	local bw = w
	local bh = h
	love.graphics.setColor(themes.ThemesManager.supplySafeBackgroundColor(self:getStyleTarget(), ternary(self:getSelected(), themes.TargetState.SELECTED, themes.TargetState.DEFAULT)))
	love.graphics.rectangle("fill", bx, by, bw, bh)
	local borderColor = themes.ThemesManager.supplySafeBorderColor(self:getStyleTarget(), ternary(self:getSelected(), themes.TargetState.SELECTED, themes.TargetState.DEFAULT))
	if borderColor ~= nil then
		local tmp1, tmp2
		local borderWidth = themes.ThemesManager.supplySafeBorderWidth(self:getStyleTarget(), ternary(self:getSelected(), themes.TargetState.SELECTED, themes.TargetState.DEFAULT))
		if borderWidth ~= nil then
			tmp1 = love.graphics.getLineJoin()
			tmp2 = love.graphics.getLineWidth()
			love.graphics.setLineJoin("miter")
			love.graphics.setLineWidth(borderWidth)
		end
		love.graphics.setColor(borderColor)
		love.graphics.rectangle("line", bx, by, bw, bh)
		if borderWidth ~= nil then
			love.graphics.setLineJoin(tmp1)
			love.graphics.setLineWidth(tmp2)
		end
	end
	if self._drawableText == 0 then
		self._drawableText = love.graphics.newText(love.graphics.getFont(), self:getText())
	end
	tx = centered(self._drawableText:getWidth(), bw, bx)
	ty = centered(self._drawableText:getHeight(), bh, by)
	love.graphics.setColor(themes.ThemesManager.supplySafeForegroundColor(self:getStyleTarget(), ternary(self:getSelected(), themes.TargetState.SELECTED, themes.TargetState.DEFAULT)))
	love.graphics.draw(self._drawableText, tx, ty)
end


-- *****************************************************************************
-- * TableColumn
-- *
-- *  class TableColumn extends StyleableComponent
-- *****************************************************************************

--- 	(constructor) TableColumn()
--- ---
--- Représente une colonne d'un tableau. Cette classe n'est pas faite pour être
--- utilisée indépendamment de la classe `Table`.
---
--- La colonne contient toutes les informations à son sujet (nom, largeur...)
--- et se trace elle-même. Il est donc possible, en héritant de `TableColumn`,
--- d'implémenter son propre affichage de colonne.
--- Puisque cette classe hérite de la classe `Component`, on peut spécifier les
--- même fonctions de callback que pour la plupart des composants.
---
--- <u>A noter que ni la hauteur du composant, optensible _via_, par exemple,
--- `getHeight()`, ni les positions X et Y ne sont utilisées. La largeur, en
--- revanche, est utilisée pour la largeur de la colonne dans son tableau. La
--- valeur est toutefois interprétée comme un pourcentage</u>.
---
--- La cible par défaut pour le thème est `table.column`. La cible pour la
--- cellule d'en-tête est `<cible>.header`.
---@class TableColumn:StyleableComponent
local TableColumn = class('TableColumn', StyleableComponent)

function TableColumn:initialize()
	TableColumn.super.initialize(self)
	self._displayedName = ""
	self._name = ""
	self._styleTarget = "table.column"

	self._drawableColumnName = nil ---@type love.Text
end

--- Définit le nom affiché de la colonne. Il s'agit du nom qui sera affiché en
--- en-tête, si l'en-tête est affiché.
---@param name string
---@return self self
function TableColumn:setDisplayedName(name)
	self._displayedName = name
	return self
end

--- Retourne le nom affiché de la colonne.
---@return string
function TableColumn:getDisplayedName()
	return self._displayedName
end

--- Définit le nom réel de la colonne, celui qui correspond à une clef de chaque
--- entrée du contenu. Utilisé en mode `KEY_VALUE`.
---@param name string
---@return self self
function TableColumn:setName(name)
	self._name = name
	return self
end

--- Retourne le nom réel de la colonne.
---@return string
function TableColumn:getName()
	return self._name
end

--- Trace la case d'en-tête de la colonne, avec son nom. Cette méthode est
--- appelée par sa `Table` au traçage. Redéfinir cette méthode dans une
--- classe-fille permet de personnaliser la manière dont la case d'en-tête de
--- la colonne est tracée.
---@param x number La position X du point haut-gauche de la case
---@param y number La position Y du point haut-gauche de la case
---@param width number La largeur de la case
---@param height number La hauteur de la case
function TableColumn:drawHeaderCell(x, y, width, height)
	love.graphics.setColor(themes.ThemesManager.supplyBackgroundColor(self:getStyleTarget() .. ".header"))
	love.graphics.rectangle("fill", x, y, width, height)
	if self._drawableColumnName == nil then
		self._drawableColumnName = love.graphics.newText(love.graphics.getFont(), self:getDisplayedName())
	end
	love.graphics.setColor(themes.ThemesManager.supplyForegroundColor(self:getStyleTarget() .. ".header"))
	love.graphics.draw(self._drawableColumnName, centered(self._drawableColumnName:getWidth(), width, x), centered(self._drawableColumnName:getHeight(), height, y))
end

--- Trace une case de la colonne. Cette méthode est appelée par sa `Table` au
--- moment du traçage. Redéfinir cette méthode dans une classe-fille permet de
--- personnaliser la manière dont les cases de la colonne sont tracées. Le
--- contenu de la case peut être de n'importe quel type de données. Il n'est
--- pas casté en `string` avant d'être passé, afin de permettre l'utilisation de
--- types complexes, desquels on peut extraire certaines données, et modifier
--- l'apparence de la case en fonction d'elles.
---@param x number La position X du point haut-gauche de la case
---@param y number La position Y du point haut-gauche de la case
---@param width number La largeur de la case
---@param height number La hauteur de la case
---@param content any Le contenu de la case
---@param lineIndex integer L'index de la ligne de la cellule
function TableColumn:drawCell(x, y, width, height, content, lineIndex)
	love.graphics.setColor(themes.ThemesManager.supplyBackgroundColor(self:getStyleTarget()))
	love.graphics.rectangle("fill", x, y, width, height)
	local txt = love.graphics.newText(love.graphics.getFont(), tostring(content))
	love.graphics.setColor(themes.ThemesManager.supplyForegroundColor(self:getStyleTarget()))
	love.graphics.draw(txt, centered(txt:getWidth(), width, x), centered(txt:getHeight(), height, y))
end


-- *****************************************************************************
-- * Table
-- *
-- *  class Table extends StyleableComponent
-- *****************************************************************************

--- 	(constructor) Table()
--- ---
--- Permet d'afficher des tableaux à l'écran.
--- Chaque colonne est représentée par une instance de la classe `TableColumn`,
--- qui est ajoutée au tableau.
---
--- La cible par défaut pour le thème est `table`.
---@class Table:StyleableComponent
local Table = class('Table', StyleableComponent)

function Table:initialize()
	Table.super.initialize(self)

	self:setStyleTarget("table")
	self._readingMethod = TableReadingMethod.VALUE ---@type TableReadingMethod
	self._drawsHeader = true
	self._content = {}
	self._columns = {} ---@type TableColumn[]
end

--- Prend en paramètre une table. Son contenu sera ce qu'affiche le tableau.
---
--- Il existe deux modes d'interprétation des contenus. Le premier, nommé
--- "Clef-Valeur", prend une table composée de la manière suivante :
--- <pre>
--- {
--- 	{
--- 		clef1 = valeur1,
--- 		clefN = valeurN,
--- 		...
--- 	},
--- 	...
--- }
--- </pre>
--- Chaque entrée est constituée d'un dictionnaire clefs-valeurs. Dans ce mode,
--- le tableau lira chaque paire, et enverra la `valeur1` à la colonne qui
--- possède le nom `clef1`. Il est indiqué par la valeur
--- `TableReadingMethod.KEY_VALUE`.
---
--- Le deuxième mode, nommé "Valeur", prend une table composée de la manière
--- suivantes :
--- <pre>
--- {
--- 	{
--- 		valeur1,
--- 		valeurN,
--- 		...
--- 	},
--- 	...
--- }
--- </pre>
--- Chaque entrée est constituée d'un array de valeurs. Dans ce mode, le tableau
--- lira chaque entrée, et enverra chaque valeur, selon son index, à la colonne
--- possédant l'index similaire (index obtenu à l'enregistrement de la colonne).
--- Ainsi, `valeur1` sera envoyé à la colonne possédant l'index 1, etc. Il est
--- indiqué par la valeur `TableReadingMethod.VALUE`.
---@see TableColumn.setName
---@see TableReadingMethod
---@param content table
---@param readingMethod TableReadingMethod
---@return self self
function Table:setContent(content, readingMethod)
	self._content = content
	self._readingMethod = readingMethod
	return self
end

--- Retourne la table dont le contenu est affiché dans le tableau.
---@return table
function Table:getContent()
	return self._content
end

--- Définit la méthode d'interprétation du contenu du tableau. Voir la
--- description du fonctionnement à la méthode `Table:setContent`.
---@see Table.setContent
---@param method TableReadingMethod
---@return self self
function Table:setReadingMethod(method)
	self._readingMethod = method
	return self
end

--- Retourne la méthode d'interprétation du contenu du tableau.
---@return TableReadingMethod
function Table:getReadingMethod()
	return self._readingMethod
end

--- Ajoute une colonne au tableau, et retourne l'index de la nouvelle colonne.
--- Si le paramètre `index` est spécifié, la fonction insèrera la nouvelle
--- colonne ici. Sinon, elle l'insèrera à la fin du tableau.
---@param col TableColumn
---@param index? integer
---@return integer
function Table:addColumn(col, index)
	if index == nil then
		table.insert(self._columns, col)
		return #self._columns
	else
		table.insert(self._columns, index, col)
		return index
	end
end

--- Supprime une colonne du tableau, en se basant sur son index, et la retourne.
---@param index integer
---@return TableColumn
function Table:removeColumn(index)
	return table.remove(self._columns, index)
end

--- Retourne une `TableColumn` selon son index. L'index est obtenu en retour de
--- la méthode `Table:addColumn`.
---@see Table.addColumn
---@param index integer
---@return TableColumn
function Table:getColumn(index)
	return self._columns[index]
end

--- Retourne la largeur du tableau, moins celle de toutes les colonnes. Il reste
--- ainsi la largeur attribuable à de nouvelles colonnes.
---@return number
function Table:getRemainingWidthSpacing()
	local rem = self:getWidth()
	for col in list.stream(self._columns) do
		rem = rem - col:getWidth()
	end
	return rem
end

--- Définit si la table affiche ou non une ligne avec le nom des colonnes.
---@param b boolean
---@return self self
function Table:setDrawsHeader(b)
	self._drawsHeader = b
	return self
end

--- Retourne si la table affiche ou non une ligne avec le nom des colonnes.
---@return boolean
function Table:getDrawsHeader()
	return self._drawsHeader
end

---@param x number
---@param y number
---@param w number
---@param h number
function Table:draw(x, y, w, h)
	local currentx = x
	local currenty = y
	local CELL_HEIGHT = 20
	if self:getDrawsHeader() then
		for colIndex, col in ipairs(self._columns) do
			local width_percent
			if col:getWidth() > 0 then
				width_percent = (w * col:getWidth()) / 100
			else
				width_percent = (w - currentx) / (#self._columns - colIndex + 1)
			end
			col:drawHeaderCell(currentx, y, width_percent, CELL_HEIGHT)
			currentx = currentx + width_percent
		end
		currenty = currenty + CELL_HEIGHT
	end

	if themes.ThemesManager.supplyBorderColor(self:getStyleTarget()) ~= nil then
		love.graphics.setColor(themes.ThemesManager.supplyBorderColor(self:getStyleTarget()))
		love.graphics.rectangle("line", x, y, w, h)
	end

	for lineIndex, line in ipairs(self._content) do
		currentx = x
		local colIndex = 1
		if self._readingMethod == TableReadingMethod.VALUE then
			for columnIndex, column in ipairs(line) do
				local col = self:getColumn(columnIndex)
				local width
				if col:getWidth() > 0 then
					width = (w * col:getWidth()) / 100
				else
					width = (w - currentx) / (#self._columns - colIndex + 1)
				end
				col:drawCell(currentx, currenty, width, CELL_HEIGHT, column, lineIndex)
				currentx = currentx + width
				colIndex = colIndex + 1
			end
		elseif self._readingMethod == TableReadingMethod.KEY_VALUE then
			for columnKey, columnValue in pairs(line) do
				local col = nil ---@type TableColumn
				for c in list.stream(self._columns) do
					if c:getName() == columnKey then
						col = c
						local width
						if col:getWidth() > 0 then
							width = (w * col:getWidth()) / 100
						else
							width = (w - currentx) / (#self._columns - colIndex + 1)
						end

						col:drawCell(currentx, currenty, width, CELL_HEIGHT, columnValue, lineIndex)
						currentx = currentx + width
					end
				end
				colIndex = colIndex + 1
			end
		end
		currenty = currenty + CELL_HEIGHT
	end
end


-- *****************************************************************************
-- * Window
-- *
-- *  class Window extends StyleableComponent
-- *****************************************************************************

--- 	(constructor) Window()
--- ---
--- Crée une fenêtre qui prend tout l'écran.
---
--- La cible par défaut pour le thème est `window.body`.
---@class Window:StyleableComponent
local Window = class('Window', StyleableComponent)

function Window:initialize()
	Window.super.initialize(self)
	self._drawsTitlebar = true
	self._styleTarget = "window.body"

	self._titlebar = TitlebarComponent() ---@type TitlebarComponent
	self._content  = ComponentsGroup()   ---@type ComponentsGroup

	self:getContent():setPosition(0, 0)
	self:setPosition(0, 0)
	self:setSize(0, 0)
end

--- Retourne le composant chargé de l'affichage de la barre de titre de la
--- fenêtre.
---@return TitlebarComponent
function Window:getTitlebar()
	return self._titlebar
end

--- Définit si on doit tracer la barre de titre. `true` par défaut.
---@param bool boolean
---@return self self
function Window:setDrawsTitlebar(bool)
	self._drawsTitlebar = bool
	return self
end

--- Vérifie si on doit tracer la barre de titre.
---@return boolean
function Window:getDrawsTitlebar()
	return self._drawsTitlebar
end

--- Définit le composant qui est en contenu de la fenêtre. Généralement, on met
--- une grille, qui peut, elle, accueillir plusieurs composants.
---@param content ComponentsGroup
---@return self self
function Window:setContent(content)
	self._content = content
	self._content:setPosition(0, ternary(self:getDrawsTitlebar(), self:getPositionY() + self:getTitlebar():getHeight(), self:getPositionY()))
	self._content:setSize(self:getWidth(), self:getHeight())
	return self
end

--- Retourne le composant qui est contenu par la fenêtre.
---@return ComponentsGroup
function Window:getContent()
	return self._content
end

--- Trace la fenêtre à l'écran.
---@param x number
---@param y number
---@param w number
---@param h number
function Window:draw(x, y, w, h)
	local ww, wh = love.graphics.getDimensions()
	self:setPosition(0, 0)
	self:setSize(ww, wh)
	if self:getDrawsTitlebar() then
		self:getContent():setSize(self:getWidth(), self:getHeight() - self:getTitlebar():getHeight())
		self:getContent():setPosition(self:getPositionX(), self:getPositionY() + self:getTitlebar():getHeight())
	else
		self:getContent():setSize(self:getWidth(), self:getHeight())
		self:getContent():setPosition(self:getPositionX(), self:getPositionY())
	end
	love.graphics.setColor(themes.ThemesManager.supplyBackgroundColor(self:getStyleTarget()))
	love.graphics.rectangle("fill", self:getPositionX(), self:getPositionY(), self:getWidth(), self:getHeight())
	if self:getDrawsTitlebar() then
		self:getTitlebar():draw(self:getPositionX(), self:getPositionY(), self:getWidth(), self:getTitlebar():getHeight())
	end
	self:getContent():draw(
		self:getPositionX(),
		ternary(self:getDrawsTitlebar(), self:getPositionY() + self:getTitlebar():getHeight(), self:getPositionY()),
		self:getWidth(),
		self:getHeight()
	)
end

function Window:internalMousepressed(x, y, button, istouch)
	self:getTitlebar():internalMousepressed(x, y, button, istouch)
	self:getContent():internalMousepressed(x, y, button, istouch)
end

function Window:internalMousereleased(x, y, button, istouch)
	self:getTitlebar():internalMousereleased(x, y, button, istouch)
	self:getContent():internalMousereleased(x, y, button, istouch)
end

return
{
	centered = centered,

	Rect = Rect,

	TableReadingMethod = TableReadingMethod,

	Component                 = Component,
	StyleableComponent        = StyleableComponent,
	ComponentsGroup           = ComponentsGroup,
	ComponentsStack           = ComponentsStack,
	VerticalComponentsStack   = VerticalComponentsStack,
	HorizontalComponentsStack = HorizontalComponentsStack,
	TitlebarComponent         = TitlebarComponent,
	FooterBar                 = FooterBar,
	Button                    = Button,
	TableColumn               = TableColumn,
	Table                     = Table,
	Window                    = Window
}