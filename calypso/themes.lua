require('calypso.core')
require('calypso.class')
local dom    = require('calypso.dom')
local colors = require('calypso.colors')


-- *****************************************************************************
-- * TargetState
-- *
-- *  enum TargetState
-- *****************************************************************************

--- Contiens tous les états possibles pour une cible.
---@enum TargetState
local TargetState =
{
	DEFAULT  = "default",
	SELECTED = "selected",
	HOVER    = "hover"
}


-- *****************************************************************************
-- * Theme
-- *
-- *  class Theme
-- *****************************************************************************

--- 	(constructor) Theme()
--- ---
--- Représente un thème.
---@class Theme:Class
local Theme = class('Theme')

function Theme:initialize()
	self._name = ""
	self._vars = {} ---@type table<string,any>
	self._backgroundColor = {} ---@type table<string,number[]>
	self._foregroundColor = {} ---@type table<string,number[]>
	self._borderColor = {} ---@type table<string,number[]>
	self._borderWidth = {} ---@type table<string,number>
end

--- Vérifie si un identifiant de cible est valide.
---
--- *Fonction statique*
---@param identifier string
---@return boolean
function Theme.static:isIdentifierValid(identifier)
	if not string.match(identifier, "^[a-zA-Z0-9._-]+$") then
		return false
	else
		return true
	end
end

--- Construit un identifiant valide pour une cible. Lève une erreur si l'un des
--- arguments n'est pas un identifiant valide.
---
--- *Fonction statique*
---@param target string
---@param state? TargetState
---@return string
function Theme.static:buildTargetIdentifier(target, state)
	if not Theme:isIdentifierValid(target) then
		error("[Theme.static:buildTargetIdentifier] Invalid identifier : " .. target, 2)
	end
	if not state then
		return target .. ":" .. TargetState.DEFAULT
	else
		return target .. ":" .. state
	end
end

--- Modifie la valeur d'une variable, la créant si elle n'existe pas.
---@param name string
---@param value any
---@return self self
function Theme:setVariable(name, value)
	self._vars[name] = value
	return self
end

--- Retourne le contenu d'une variable.
---@param name string
---@return any
function Theme:getVariable(name)
	return self._vars[name]
end

--- Vérifie s'il existe dans le thème une variable avec le nom fourni.
---@param name string
---@return boolean
function Theme:hasVariable(name)
	return self._vars[name] ~= nil
end

--- Définit le nom affiché du thème.
---@param name string
---@return self self
function Theme:setName(name)
	self._name = name
	return self
end

--- Retourne le nom affiché du thème.
---@return string
function Theme:getName()
	return self._name
end

--- Fournit une couleur d'arrière-plan pour une cible spécifique, ou `nil` si
--- cette cible n'a pas de couleur d'arrière-plan.
---@param target string
---@param state? TargetState
---@return number[]?
function Theme:getBackgroundColor(target, state)
	return self._backgroundColor[Theme:buildTargetIdentifier(target, state)]
end

--- Set the background color for a specific target.
---@param target string
---@param c number[] The color
---@param state? TargetState Set for this state of the target, if any
---@return self self
function Theme:setBackgroundColor(target, c, state)
	self._backgroundColor[Theme:buildTargetIdentifier(target, state)] = c
	return self
end

--- Supplies the foreground color for a specific target, or nil if is has no
--- foreground color.
---@param target string
---@param state? TargetState Fetch for this state of the target, if any
---@return number[]?
function Theme:getForegroundColor(target, state)
	return self._foregroundColor[Theme:buildTargetIdentifier(target, state)]
end

--- Set the foreground color for a specific target.
---@param target string
---@param c number[] The color
---@param state? TargetState Set for this state of the target, if any
---@return self self
function Theme:setForegroundColor(target, c, state)
	self._foregroundColor[Theme:buildTargetIdentifier(target, state)] = c
	return self
end

--- Fournit la couleur de la bordure pour une cible donnée, ou `nil` si la cible
--- n'a pas de couleur de bordure définie.
---@param target string
---@param state? TargetState L'état de la cible. `TargetState.DEFAULT` par défaut
---@return number[]?
function Theme:getBorderColor(target, state)
	return self._borderColor[Theme:buildTargetIdentifier(target, state)]
end

--- Définit la couleur de bordure pour une cible spécifiée.
---@param target string
---@param c number[]
---@param state? TargetState `TargetState.DEFAULT` par défaut
---@return self self
function Theme:setBorderColor(target, c, state)
	self._borderColor[Theme:buildTargetIdentifier(target, state)] = c
	return self
end

--- Fournit la largeur de la bordure pour une cible donnée, ou `nil` si la cible
--- n'a pas de largeur de bordure définie.
---@param target string
---@param state? TargetState
---@return number?
function Theme:getBorderWidth(target, state)
	return self._borderWidth[Theme:buildTargetIdentifier(target, state)]
end

--- Définit la largeur de la bordure pour une cible donnée.
---@param target string
---@param width number
---@param state? TargetState
---@return self self
function Theme:setBorderWidth(target, width, state)
	self._borderWidth[Theme:buildTargetIdentifier(target, state)] = width
	return self
end

---@param s string
---@return number[]
function Theme.static:internalConvertColorNameToColor(s)
	if     s == "white"     then return colors.white
	elseif s == "orange"    then return colors.orange
	elseif s == "magenta"   then return colors.magenta
	elseif s == "lightBlue" then return colors.lightBlue
	elseif s == "yellow"    then return colors.yellow
	elseif s == "lime"      then return colors.lime
	elseif s == "pink"      then return colors.pink
	elseif s == "gray"      then return colors.gray
	elseif s == "grey"      then return colors.gray
	elseif s == "lightGray" then return colors.lightGray
	elseif s == "lightGrey" then return colors.lightGray
	elseif s == "cyan"      then return colors.cyan
	elseif s == "purple"    then return colors.purple
	elseif s == "blue"      then return colors.blue
	elseif s == "brown"     then return colors.brown
	elseif s == "green"     then return colors.green
	elseif s == "red"       then return colors.red
	elseif s == "black"     then return colors.black
	else
		error("Unknown color : " .. s)
	end
end

--- Fonction utilisée internalement pour résoudre une balise `<Variable/>` déjà
--- déclarée. Retourne la valeur sur laquelle la variable pointe, sans la caster.
---@param variableNode DOMElement La balise `<Variable/>`
---@return string|number[]
function Theme:internalResolveVariable(variableNode)
	if not variableNode:hasAttributeWithName("Name") then
		error("Trying to resolve a variable without a name.")
	end
	local varname = variableNode:getFirstAttributeByName("Name"):getValue()
	return self:getVariable(varname)
end

--- Fonction interne pour résoudre une balise `<rgb/>`. Retourne une table
--- contenant les données RGBA. Si l'argument convert est `true`, retourne les
--- données sous une forme utilisable par Love2D, entre 0 et 1. Sinon, les
--- retourne sous la forme classique entre 0 et 255.
---@param rgbNode DOMElement
---@param convert? boolean `true` par défaut
---@return number[]
function Theme:internalResolveRGBA(rgbNode, convert)
	if convert == nil then
		convert = true
	end
	local r, g, b
	local a = 255 ---@type number
	if not rgbNode:hasAttributeWithName("r")
	or not rgbNode:hasAttributeWithName("g")
	or not rgbNode:hasAttributeWithName("b") then
		error("Trying to resolve a malformed RGB node.")
	end
	r = tonumber(rgbNode:getFirstAttributeByName("r"):getValue())
	g = tonumber(rgbNode:getFirstAttributeByName("g"):getValue())
	b = tonumber(rgbNode:getFirstAttributeByName("b"):getValue())
	if rgbNode:hasAttributeWithName("a") then
		a = tonumber(rgbNode:getFirstAttributeByName("a"):getValue())
	end
	if convert then
		r, g, b, a = love.math.colorFromBytes(r, g, b, a)
	end
	return {r, g, b, a}
end

--- Fonction interne utilisée pour construire une nouvelle variable. Lit la
--- balise `<Variable/>` fournie, en tire le nom de la variable et son contenu.
--- Si ce contenu pointe sur une autre variable, remonte jusqu'au vrai contenu.
--- Retourne le nom de la variable et sa valeur, mais ne la crée pas dans le
--- thème.
---@param variableNode DOMElement
---@return string name
---@return string|number[] value
function Theme:internalBuildVariable(variableNode)
	if not variableNode:hasAttributeWithName("Name") then
		error("Trying to build a variable without a name.")
	end
	local varname = variableNode:getFirstAttributeByName("Name"):getValue()
	local content = variableNode:getAllElements()
	local varval = nil
	if #content > 0 then
		local firstContent = content[1]
		if firstContent.classname == "calypso.dom:DOMTextElement" then
			-- Si c'est un texte
			---@cast firstContent DOMTextElement
			varval = firstContent:getContent()
		elseif firstContent.classname == "calypso.dom:DOMElement" then
			---@cast firstContent DOMElement
			if firstContent:getName() == "Variable" then
				-- le contenu pointe vers une autre variable
				varval = self:internalResolveVariable(firstContent)
			elseif firstContent:getName():lower() == "rgb" then
				varval = self:internalResolveRGBA(firstContent)
			end
		end
	end
	if varval == nil then
		error("Trying to build an empty variable : " .. varname)
	end
	return varname, varval
end

--- Fonction interne pour parser une propriété de couleur.
---@protected
---@param propNode DOMElement
---@return number[]? # Une couleur, au format RGBA
function Theme:internalParseColorProperty(propNode)
	local parsedContent = propNode:getAllElements()
	if #parsedContent < 1 then
		error("Empty property")
	end
	parsedContent = parsedContent[1]
	if parsedContent:instanceof(dom.DOMTextElement) then
		return Theme:internalConvertColorNameToColor(parsedContent:getContent())
	elseif parsedContent:instanceof(dom.DOMElement) then
		---@cast parsedContent DOMElement
		if parsedContent:getName() == "Variable" then
			local varvalue = self:internalResolveVariable(parsedContent)
			if type(varvalue) == "string" then
				return Theme:internalConvertColorNameToColor(varvalue)
			else
				return varvalue
			end
		elseif string.lower(parsedContent:getName()) == "rgb" then
			return self:internalResolveRGBA(parsedContent)
		end
	end
	return nil
end

--- Une fonction interne qui construit un thème depuis des données XML.
---@param themedom DOMElement
---@return Theme # Le thème construit
---@return string # L'ID sous lequel le thème préférerait être enregistré
function Theme.static:internalBuildThemeFromXML(themedom)
	local theme = Theme() ---@type Theme
	local themeName = ternary(themedom:hasAttributeWithName("Name"), themedom:getFirstAttributeByName("Name"):getValue(), "<UNTITLED>")
	local themeId = ternary(themedom:hasAttributeWithName("Id"), themedom:getFirstAttributeByName("Id"):getValue(), "")
	theme:setName(themeName)

	-- Build variables
	if themedom:hasElementWithName("Definitions") then
		local definitionsNodes = themedom:getElementsByName("Definitions")
		for definitionsNode in list.stream(definitionsNodes) do
			for variable in list.stream(definitionsNode:getElementsByName("Variable")) do
				local varname, varval = theme:internalBuildVariable(variable)
				theme:setVariable(varname, varval)
			end
		end
	end

	-- Build targets
	for targetIndex, target in ipairs(themedom:getElementsByName("Target")) do
		if not target:hasAttributeWithName("Id") then
			error("Missing Id attribute in target " .. tostring(targetIndex) .. " on theme " .. themeName)
		end
		local targetId = target:getFirstAttributeByName("Id"):getValue()

		for state in list.stream(target:getElementsByName("State")) do
			local stateId = TargetState.DEFAULT
			if state:hasAttributeWithName("Id") then
				stateId = state:getFirstAttributeByName("Id"):getValue()
			end

			for rule in list.stream(state:getElements()) do
				if rule:getName() == "ForegroundColor" then
					local _color = theme:internalParseColorProperty(rule)
					if _color ~= nil then
						theme:setForegroundColor(targetId, _color, stateId)
					end
				elseif rule:getName() == "BackgroundColor" then
					local _color = theme:internalParseColorProperty(rule)
					if _color ~= nil then
						theme:setBackgroundColor(targetId, _color, stateId)
					end
				elseif rule:getName() == "Border" then
					if rule:hasElementWithName("Color") then
						local _color = theme:internalParseColorProperty(rule:getFirstElementByName("Color"))
						if _color ~= nil then
							theme:setBorderColor(targetId, _color, stateId)
						end
					end
					if rule:hasElementWithName("Width") then
						local te = rule:getFirstElementByName("Width"):getTextElements()
						if #te >= 0 then
							theme:setBorderWidth(targetId, tonumber(te[1]:getContent()), stateId)
						end
					end
				end
			end
		end
	end
	return theme, themeId
end

--- Crée de nouveaux thèmes à partir des données XML fournies. Cette fonction
--- lira les données XML de thème, et en tirera autant de `Theme` que celles-ci
--- en décrivaient. Cette méthode statique retourne une table contenant, comme
--- clef, l'ID sous lequel le thème préférerait être enregistré, et comme valeur
--- le `Theme` en lui-même.
---@param xml string
---@return table<string,Theme>
function Theme.static:buildFromXML(xml)
	local document = dom.DOMDocument:createFromXML(xml) ---@type DOMDocument
	---@type table<string,Theme>
	local builtThemes = {}
	if document:getRoot():getName() == "Themes" then
		local themes = document:getRoot():getElementsByName("Theme")
		for k, theme in ipairs(themes) do
			local builtTheme, builtThemeId = Theme:internalBuildThemeFromXML(theme)
			if builtThemeId == "" then
				builtThemeId = "THEME-" .. tostring(k)
			end
			builtThemes[builtThemeId] = builtTheme
		end
	end
	return builtThemes
end


-- *****************************************************************************
-- * ThemesManager
-- *
-- *  abstract class ThemesManager
-- *****************************************************************************

--- Gère les thèmes visuels de l'application.
---
--- Les thèmes sont enregistrés sous un ID unique, qui est généralement fourni
--- dans les données XML du thème.
---
--- Fonctionne en cascade : plusieurs thèmes peuvent être activés en même temps.
--- Lorsqu'on demande une valeur, le `ThemesManager` va demander à tous les
--- thèmes activés cette valeur, dans l'ordre croissant de leur index, jusqu'à
--- ce qu'un thème lui fournisse une valeur non-`nil`.
---@class ThemesManager
local ThemesManager = {}
ThemesManager._themes        = {} ---@type table<string,Theme>
ThemesManager._currentThemes = {} ---@type string[]

--- Retourne une liste contenant les ID des thèmes actifs, dans le bon ordre.
---@return string[]
function ThemesManager.getCurrentThemesId()
	return ThemesManager._currentThemes
end

--- Vérifie si un thème est activé ou non, basé sur son ID.
---@param themeId string
---@return boolean active
---@return integer? index
function ThemesManager.isThemeActive(themeId)
	for ti,tid in ipairs(ThemesManager._currentThemes) do
		if tid == themeId then
			return true, ti
		end
	end
	return false
end

--- Active un thème, s'il ne l'est pas déjà, et retourne son index.
---@param themeId string L'identifiant du thème à activer
---@param index? integer L'index où le mettre. Ne rien mettre pour le mettre tout en bas
---@return integer index
function ThemesManager.activateTheme(themeId, index)
	if not ThemesManager.isThemeActive(themeId) then
		if index == nil then
			table.insert(ThemesManager._currentThemes, themeId)
			return #ThemesManager._currentThemes
		else
			table.insert(ThemesManager._currentThemes, index, themeId)
			return index
		end
	end
end

--- Désactive un thème, si celui-ci est activé, et le retourne. Si le thème
--- n'était pas activé, retourne `nil`.
---@param themeId string
---@return Theme?
function ThemesManager.desactivateTheme(themeId)
	local isActive, index = ThemesManager.isThemeActive(themeId)
	if isActive then
		return table.remove(ThemesManager._currentThemes, index)
	end
	return nil
end

--- Retourne un thème en fonction de son ID. Si aucun thème n'existe sous cet
--- identifiant, retourne `nil`.
---@param themeId string
---@return Theme?
function ThemesManager.getTheme(themeId)
	return ThemesManager._themes[themeId]
end

--- Vérifie s'il existe un thème enregistré sous cet ID.
---@param themeId string
---@return boolean
function ThemesManager.hasTheme(themeId)
	return ThemesManager._themes[themeId] ~= nil
end

--- Enregistre un thème. Si aucun thème n'est actif, il sera automatiquement
--- activé. Si un thème existe déjà sous cet ID, il ne sera pas enregistré, et
--- la fonction retournera `false`. Sinon, il le sera, et la fonction
--- retournera `true`.
---@param themeId string L'ID du thème. Doit être unique
---@param theme Theme Le thème en lui-même
---@return boolean
function ThemesManager.registerTheme(themeId, theme)
	if not ThemesManager.hasTheme(themeId) then
		ThemesManager._themes[themeId] = theme
		if #ThemesManager._themes[themeId] == 0 then
			ThemesManager.activateTheme(themeId)
		end
		return true
	else
		return false
	end
end

--- Fournit une couleur d'arrière-plan pour une cible, ou `nil` si aucune
--- couleur d'arrière-plan n'a pu être trouvée pour cette cible. En cas de
--- succès, retourne aussi en deuxième valeur l'ID du thème qui a fournit la
--- couleur.
---@param target string
---@param state? TargetState Si spécifié, cherche la couleur pour cet état
---@return number[]? color
---@return string? themeId En cas de succès, l'ID du thème qui a fournit la couleur. Sinon, `nil`
function ThemesManager.supplyBackgroundColor(target, state)
	for _, tid in ipairs(ThemesManager._currentThemes) do
		local bgc = ThemesManager._themes[tid]:getBackgroundColor(target, state)
		if bgc ~= nil then
			return bgc, tid
		end
	end
	return nil, nil
end

--- Fournit une couleur d'arrière-plan pour une cible. Si aucune couleur n'a pu
--- être trouvée pour cette cible dans cet état, refait une recherche pour la
--- même cible, mais en état normal.
---@see ThemesManager.supplyBackgroundColor
---@param target string
---@param state? TargetState
---@return number[]? color
---@return string? themeId
function ThemesManager.supplySafeBackgroundColor(target, state)
	local bgc, tid = ThemesManager.supplyBackgroundColor(target, state)
	if not bgc and state ~= nil and state ~= TargetState.DEFAULT then
		bgc, tid = ThemesManager.supplyBackgroundColor(target)
	end
	return bgc, tid
end

--- Fournit une couleur de premier-plan pour une cible, ou `nil` si aucune
--- couleur de premier-plan n'a pu être trouvée pour cette cible. En cas de
--- succès, retourne aussi en deuxième valeur l'ID du thème qui a fournit la
--- couleur.
---@param target string
---@param state? TargetState Si spécifié, cherche la couleur pour cet été
---@return number[]? color
---@return string? themeId
function ThemesManager.supplyForegroundColor(target, state)
	for _, tid in ipairs(ThemesManager._currentThemes) do
		local fgc = ThemesManager._themes[tid]:getForegroundColor(target, state)
		if fgc ~= nil then
			return fgc, tid
		end
	end
	return nil, nil
end

--- Fournit une couleur de premier-plan pour une cible. Si aucune couleur n'a pu
--- être trouvée pour cette cible dans cet état, refait une recherche pour la
--- même cible, mais en état normal.
---@see ThemesManager.supplyForegroundColor
---@param target string
---@param state? TargetState
---@return number[]? color
---@return string? themeId
function ThemesManager.supplySafeForegroundColor(target, state)
	local fgc, tid = ThemesManager.supplyForegroundColor(target, state)
	if not fgc and state ~= nil and state ~= TargetState.DEFAULT then
		fgc, tid = ThemesManager.supplyForegroundColor(target)
	end
	return fgc, tid
end

--- Fournit la couleur de bordure pour une cible spécifiée. Si l'argument `state`
--- n'est pas donné, la fonction fournira la couleur pour l'état
--- `TargetState.DEFAULT` de la cible. Si aucune couleur de bordure n'a pu être
--- trouvée, retourne `nil`. En cas de succès, retourne en deuxième valeur l'ID
--- du thème qui a fourni la couleur.
---@param target string
---@param state? TargetState
---@return number[]? color
---@return string? themeId
function ThemesManager.supplyBorderColor(target, state)
	for _, tid in ipairs(ThemesManager._currentThemes) do
		local bc = ThemesManager._themes[tid]:getBorderColor(target, state)
		if bc ~= nil then
			return bc, tid
		end
	end
	return nil, nil
end

--- Fournit une couleur de bordurepour une cible. Si aucune couleur n'a pu
--- être trouvée pour cette cible dans cet état, refait une recherche pour la
--- même cible, mais en état normal.
---@see ThemesManager.supplyBorderColor
---@param target string
---@param state? TargetState
---@return number[]? color
---@return string? themeId
function ThemesManager.supplySafeBorderColor(target, state)
	local bc, tid = ThemesManager.supplyBorderColor(target, state)
	if not bc and state ~= nil and state ~= TargetState.DEFAULT then
		bc, tid = ThemesManager.supplyBorderColor(target)
	end
	return bc, tid
end

--- Fournit la largeur de bordure pour une cible spécifiée, ou `nil` si aucune
--- largeur de bordure pour cette cible dans cet état n'a pu être trouvée. En
--- cas de succès, retourne également en deuxième valeur l'ID du thème qui a
--- fourni la valeur.
---@param target string
---@param state? TargetState
---@return number? width
---@return string? themeId
function ThemesManager.supplyBorderWidth(target, state)
	for _, tid in ipairs(ThemesManager._currentThemes) do
		local bw = ThemesManager._themes[tid]:getBorderWidth(target, state)
		if bw ~= nil then
			return bw, tid
		end
	end
	return nil, nil
end

--- Fournit une largeur de bordure pour une cible. Si aucune valeur n'a pu être
--- trouvée pour cette cible dans cet état, refait une recherche pour la même
--- cible, mais en état normal.
---@see ThemesManager.supplyBorderWidth
---@param target string
---@param state? TargetState
---@return number? color
---@return string? themeId
function ThemesManager.supplySafeBorderWidth(target, state)
	local bw, tid = ThemesManager.supplyBorderWidth(target, state)
	if not bw and state ~= nil and state ~= TargetState.DEFAULT then
		bw, tid = ThemesManager.supplyBorderWidth(target)
	end
	return bw, tid
end


return
{
	TargetState   = TargetState,
	Theme         = Theme,
	ThemesManager = ThemesManager
}