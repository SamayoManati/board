local white     = {love.math.colorFromBytes(240, 240, 240, 255)}; ---@type number[]
local orange    = {love.math.colorFromBytes(242, 178,  51, 255)}; ---@type number[]
local magenta   = {love.math.colorFromBytes(229, 127, 216, 255)}; ---@type number[]
local lightBlue = {love.math.colorFromBytes(153, 178, 242, 255)}; ---@type number[]
local yellow    = {love.math.colorFromBytes(222, 222, 108, 255)}; ---@type number[]
local lime      = {love.math.colorFromBytes(127, 204,  25, 255)}; ---@type number[]
local pink      = {love.math.colorFromBytes(242, 178, 204, 255)}; ---@type number[]
local gray      = {love.math.colorFromBytes( 76,  76,  76, 255)}; ---@type number[]
local lightGray = {love.math.colorFromBytes(153, 153, 153, 255)}; ---@type number[]
local cyan      = {love.math.colorFromBytes( 76, 153, 178, 255)}; ---@type number[]
local purple    = {love.math.colorFromBytes(178, 102, 229, 255)}; ---@type number[]
local blue      = {love.math.colorFromBytes( 51, 102, 204, 255)}; ---@type number[]
local brown     = {love.math.colorFromBytes(127, 102,  76, 255)}; ---@type number[]
local green     = {love.math.colorFromBytes( 87, 166,  78, 255)}; ---@type number[]
local red       = {love.math.colorFromBytes(204,  76,  76, 255)}; ---@type number[]
local black     = {love.math.colorFromBytes( 17,  17,  17, 255)}; ---@type number[]

return
{
	white     = white,
	orange    = orange,
	magenta   = magenta,
	lightBlue = lightBlue,
	yellow    = yellow,
	lime      = lime,
	pink      = pink,
	gray      = gray,
	grey      = gray,
	lightGray = lightGray,
	lightGrey = lightGray,
	cyan      = cyan,
	purple    = purple,
	blue      = blue,
	brown     = brown,
	green     = green,
	red       = red,
	black     = black
};