require('calypso.core');
local coreclass = require('calypso.middleclass.middleclass');


--- Crée une nouvelle classe. Si aucune super-classe n'est fournie, elle
--- héritera automatiquement de `Class`.
---@generic T:Class
---@param name string Le nom de la classe à créer
---@param superclass? T La super-classe, le cas échéant
---@return table
function class(name, superclass)
	return coreclass(name, superclass or Class);
end


--- La classe dont héritent toutes les autres.
---@class Class
---@field public static table Contient les méthodes et les champs statiques
Class = coreclass('Class');

function Class:initialize()
end

--- Vérifie si l'objet est une instance de `aClass`.
---@generic T:Class
---@param aClass T
---@return boolean
function Class:instanceof(aClass)
	return self:isInstanceOf(aClass);
end

