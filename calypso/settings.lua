--- Manages the applications's settings.
---@class SettingsManager
local SettingsManager = {
	---@protected
	---@type table<string,any>
	_settings = {}
}

--- Class constructor.
---@return SettingsManager
function SettingsManager:new()
	local wannabe = setmetatable({
		_settings = {}
	}, self)
	self.__index = self
	return wannabe
end

--- Returns a setting.
---@param setting string The setting's name
---@param group? string The setting's group, if there is one. Defaults to "" (ie no group)
---@return any
function SettingsManager:get(setting, group)
	local group = group or ""
	return self._settings[group][setting]
end

return {
	SettingsManager = SettingsManager
}