require("calypso.class");
require("calypso.core");

-- *****************************************************************************
-- * Rect
-- *
-- *  class Rect
-- *****************************************************************************

--- 	(constructor) Rect(x?: number, y?: number, w?: number, h?: number)
---
--- _@param_ `x` - Position X, `0` par défaut
---
--- _@param_ `y` - Position Y, `0` par défaut
---
--- _@param_ `w` - Largeur, `0` par défaut
---
--- _@param_ `h` - Hauteur, `0` par défaut
---
--- ---
--- Une petite classe utilitaire faite pour rassembler les informations
--- nécessaires à la forme d'un rectangle : sa position X et Y, et ses
--- dimensions (largeur et hauteur). Ses propriétés sont publiques.
---@class Rect:Class
local Rect = class('Rect');

--- Constructeur de classe.
---@param x? number position X, `0` par défaut
---@param y? number position Y, `0` par défaut
---@param w? number largeur, `0` par défaut
---@param h? number hauteur, `0` par défaut
function Rect:initialize(x, y, w, h)
	self.x = x or 0;
	self.y = y or 0;
	self.width = w or 0;
	self.height = h or 0;
end

--- Vérifie si un point donné via ses coordonnées X et Y est contenu ou non dans
--- le rectangle.
---@param x number
---@param y number
---@return boolean
function Rect:containsPosition(x, y)
	return x >= self.x and x <= self.width + self.x and y >= self.y and y <= self.height + self.y;
end


return Rect;