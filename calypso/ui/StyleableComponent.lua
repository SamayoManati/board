require("calypso.core");
require("calypso.class");
local Component = require("calypso.ui.Component");


-- *****************************************************************************
-- * StyleableComponent
-- *
-- *  class StyleableComponent extends Component
-- *****************************************************************************


--- Classe-mère des composants qui peuvent modifier leur style.
---@class StyleableComponent:Component
local StyleableComponent = class("StyleableComponent", Component);


return StyleableComponent;