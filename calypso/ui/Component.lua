require("calypso.class");
require("calypso.core");
local Rect = require("calypso.ui.Rect");


-- *****************************************************************************
-- * Component
-- *
-- *  class Component
-- *****************************************************************************

--- 	(constructor) Component()
--- ---
--- La classe-mère de tous les composants.
---@class Component:Class
local Component = class('Component');

function Component:initialize()
	self._location = Rect(); ---@type Rect
	self._mouseClickedCallback  = function(x, y, button, istouch) end;
	self._mouseReleasedCallback = function(c, y, button, istouch) end;
end

--- Retourne la taille du composant.
---@return number width
---@return number height
function Component:getSize()
	return self._location.width, self._location.height;
end

--- Retourne la largeur du composant.
---@return number
function Component:getWidth()
	return self._location.width;
end

--- Retourne la hauteur du composant.
---@return number
function Component:getHeight()
	return self._location.height;
end

--- Modifie la taille du composant.
---@param width number
---@param height number
---@return self self
function Component:setSize(width, height)
	self._location.width = width;
	self._location.height = height;
	return self;
end

--- Retourne la position du composant.
---@return number x
---@return number y
function Component:getPosition()
	return self._location.x, self._location.y;
end

--- Retourne la position X du composant.
---@return number
function Component:getPositionX()
	return self._location.x;
end

--- Retourne la position Y du composant.
---@return number
function Component:getPositionY()
	return self._location.y;
end

--- Modifie la position du composant.
---@param x number
---@param y number
---@return self self
function Component:setPosition(x, y)
	self._location.x = x;
	self._location.y = y;
	return self;
end

--- Retourne le `Rect` qui contient la position et la taille du composant.
---@return Rect
function Component:getLocation()
	return self._location;
end

--- Définit un `Rect` qui contient la position et la taille du composant.
---@param rect Rect
---@return self self
function Component:setLocation(rect)
	self._location = rect;
	return self;
end

--- Définit le callback appelé quand on clic sur le composant. Les paramètres
--- suivants sont passés au callback :
---
--- 1. `number` position X du clic
--- 2. `number` position Y du clic
--- 3. `number` le bouton qui a cliqué
--- 4. `boolean` Est-ce un appui tactile ?
---@param fnt fun(x: number, y: number, btn: number, istouch: boolean)
---@return self self
function Component:onMouseClick(fnt)
	self._mouseClickedCallback = fnt;
	return self;
end

--- Définit le callback appelé quand on relâche le clic sur le composant. Les
--- paramètres passés sont les mêmes que pour `onMouseClick`.
---@see Component.onMouseClick
---@param fnt fun(x: number, y: number, btn: number, istouch: boolean)
---@return self self
function Component:onMouseRelease(fnt)
	self._mouseReleasedCallback = fnt;
	return self;
end

function Component:internalMousepressed(x, y, button, istouch)
	self._mouseClickedCallback(x, y, button, istouch);
end

function Component:internalMousereleased(x, y, button, istouch)
	self._mouseReleasedCallback(x, y, button, istouch);
end

function Component:internalKeypressed(key)
end

function Component:internalKeyreleased(key)
end

function Component:internalUpdate(deltatime)
end


return Component;