require('calypso.core');
require('calypso.class');
local sxml = require('calypso.slaxml.slaxdom');


--- 	(constructor) DOMTextElement(str?: string)
--- ---
--- Représente un noeud de texte, contenu dans une balise.
---@see DOMElement
---@class DOMTextElement:Class
local DOMTextElement = class('DOMTextElement');


-- *****************************************************************************
-- * DOMAttribute
-- *
-- *  class DOMAttribute
-- *****************************************************************************

--- 	(constructor) DOMAttribute(name: string, value?: string)
--- ---
--- Représente un attribut d'une balise, possédant un nom et une valeur.
---@see DOMElement
---@class DOMAttribute:Class
local DOMAttribute = class('DOMAttribute');

--- Constructeur de classe.
---@param name   string Le nom de l'attribut.
---@param value? string La valeur de l'attribut. Un texte vide par défaut.
function DOMAttribute:initialize(name, value)
	self._name = name;
	self._value = value or "";
end

--- Modifie le nom de l'attribut.
---@param name string
---@return self self
function DOMAttribute:setName(name)
	self._name = name;
	return self;
end

--- Retourne le nom de l'attribut.
---@return string
function DOMAttribute:getName()
	return self._name;
end

--- Modifie la valeur de l'attribut.
---@param value string
---@return self self
function DOMAttribute:setValue(value)
	self._value = value
	return self
end

--- Retourne la valeur de l'attribut.
---@return string
function DOMAttribute:getValue()
	return self._value
end

--- Vérifie si la valeur de l'attribut est vide ou non.
---@return boolean
function DOMAttribute:isEmpty()
	return self._value == ""
end

--- Construit l'attribut en une table sérialisable utilisable par SLAXML.
---@return {type:string, name:string, value:string, nsURI: nil, nsPrefix: nil}
function DOMAttribute:build()
	return {
		type = "attribute",
		name = self._name,
		value = self._value,
		nsURI = nil,
		nsPrefix = nil
	}
end


-- *****************************************************************************
-- * DOMElement
-- *
-- *  class DOMElement
-- *****************************************************************************

--- 	(constructor) DOMElement(name: string)
--- ---
--- Représente une balise, qui peut avoir des attributs, ainsi que des enfants,
--- en tant qu'autres balises ou que noeuds de texte.
---@see DOMAttribute
---@see DOMTextElement
---@class DOMElement:Class
local DOMElement = class('DOMElement')

---@param name string
function DOMElement:initialize(name)
	self._content = {} ---@type (DOMElement|DOMTextElement)[]
	self._name = name
	self._attrs = {} ---@type DOMAttribute[]
end

--- Définit le nom de la balise.
---@param name string
---@return self self
function DOMElement:setName(name)
	self._name = name
	return self
end

--- Retourne le nom de la balise.
---@return string
function DOMElement:getName()
	return self._name
end

--- Ajoute un enfant à la balise. L'enfant peut être soit une balise, qui sera
--- alors ajoutée comme un noeud-enfant, soit un noeud de texte, qui sera alors
--- ajouté comme un nouveau texte à la fin du contenu de la balise.
---@param child DOMElement|DOMTextElement
---@return self self
function DOMElement:addChild(child)
	self._content[#self._content+1] = child
	return self
end

--- Ajoute un attribut à la balise.
---@param attr DOMAttribute
---@return self self
function DOMElement:addAttribute(attr)
	self._attrs[#self._attrs+1] = attr
	return self
end

--- Construit le noeud et son contenu en une table de sérialisation utilisable
--- par SLAXML.
---@return {type:string, name:string, nsURI:nil, nsPrefix:nil, attr:{type:string, name:string, value:string, nsURI: nil, nsPrefix: nil}[], kids:{}[]}
function DOMElement:build()
	local r = {}
	r.type = "element"
	r.name = self._name
	r.nsURI = nil
	r.nsPrefix = nil
	r.attr = {}
	if #self._attrs > 0 then
		for attr in list.stream(self._attrs) do
			r.attr[#r.attr+1] = attr:build()
		end
	end
	if #self._content > 0 then
		r.kids = {}
		for kiddo in list.stream(self._content) do
			r.kids[#r.kids+1] = kiddo:build()
		end
	end

	return r
end

--- Retourne toutes les balises contenues dans celle-ci. Ne retourne pas les
--- noeuds de texte.
---@return DOMElement[]
function DOMElement:getElements()
	local r = {} ---@type DOMElement[]

	for el in list.fstream(self._content, function(i) return i:instanceof(DOMElement) end) do
		r[#r+1] = el
	end

	return r
end

--- Retourne tous les attrbitus de la balise.
---@return DOMAttribute[]
function DOMElement:getAttributes()
	return self._attrs
end

--- Retourne tous les noeuds de texte de la balise.
---@return DOMTextElement[]
function DOMElement:getTextElements()
	local r = {} ---@type DOMTextElement[]

	for el in list.fstream(self._content, function(i) return i:instanceof(DOMTextElement) end) do
		r[#r+1] = el
	end

	return r
end

--- Retourne tous les enfants de la balise, c'est-à-dire à la fois les
--- `DOMElement` et les `DOMTextElement`.
---@return (DOMElement|DOMTextElement)[]
function DOMElement:getAllElements()
	return self._content
end

--- Retourne toutes les balises-enfants dont le nom est un de ceux fournis.
---@param ... string Les noms
---@return DOMElement[]
function DOMElement:getElementsByName(...)
	local r = {} ---@type DOMElement[]
	local args = {...}

	for el in list.fstream(self:getElements(), function(i)
		for name in list.stream(args) do
			if i:getName() == name then
				return true
			end
		end
		return false
	end) do
		r[#r+1] = el
	end

	return r
end

--- Retourne la première balise-enfant dont le nom est un de ceux fournis, ou
--- `nil` s'il n'y en a pas.
---@param ... string
---@return DOMElement?
function DOMElement:getFirstElementByName(...)
	for i in list.stream(self:getElementsByName(...)) do
		return i
	end
end

--- Retourne toutes les balises-enfants ayant un attribut avec un nom et une
--- valeur précise.
---@param name string Le nom de l'attribut
---@param value string La valeur de l'attribut
---@param elementName? string Si spécifié, restreint la recherche aux balises portant ce nom
---@return DOMElement[]
function DOMElement:getElementsByAttribute(name, value, elementName)
	return list.fmergec({}, self:getElements(), function (el)
		if elementName ~= nil and el:getName() ~= elementName then
			return false
		end
		if #list.fmergec({}, el:getAttributes(), function (attr)
			return attr:getName() == name and attr:getValue() == value
		end) == 0 then
			return false
		end
		return true
	end)
end

--- Retourne la première balise-enfant ayant un attribut avec un nom et une
--- valeur précise. Si rien n'a été trouvé, retourne `nil`.
---@param name string Le nom de l'attribut
---@param value string La valeur de l'attribut
---@param elementName? string Si spécifié, restreint la recherche aux balises portant ce nom
---@return DOMElement?
function DOMElement:getFirstElementByAttribute(name, value, elementName)
	for el in list.stream(self:getElements()) do
		if elementName ~= nil and el:getName() ~= elementName then
			goto continue
		end
		if #list.fmergec({}, el:getAttributes(), function (attr)
			return attr:getName() == name and attr:getValue() == value
		end) > 0 then
			return el
		end
	    ::continue::
	end
end

--- Retourne toutes les balises enfant qui ont un attribut dont le nom est l'un
--- de ceux fournis.
---@param ... string
---@return DOMElement[]
function DOMElement:getElementsByAttributeName(...)
	local args = {...}
	local els = {}

	for el in list.stream(self:getElements()) do
		for attr in list.stream(el:getAttributes()) do
			for name in list.stream(args) do
				if attr:getName() == name then
					els[#els+1] = el
				end
			end
		end
	end

	return els
end

--- Retourne la première balise-enfant possédant un attribut dont le nom est un
--- de ceux spécifiés, ou `nil` si aucune n'a été trouvée.
---@param ... string
---@return DOMElement?
function DOMElement:getFirstElementByAttributeName(...)
	local args = {...}
	for el in list.stream(self:getElements()) do
		for attr in list.stream(el:getAttributes()) do
			for name in list.stream(args) do
				if attr:getName() == name then
					return el
				end
			end
		end
	end
end

--- Retourne toutes les balises enfant ayant un attribut dont la valeur est
--- l'une de celles fournies.
---@param ... string
---@return DOMElement[]
function DOMElement:getElementsByAttributeValue(...)
	local els = {}
	for el in list.stream(self:getElements()) do
		for attr in list.stream(el:getAttributes()) do
			for value in list.stream({...}) do
				if attr:getValue() == value then
					els[#els+1] = el
				end
			end
		end
	end
	return els
end

--- Retourne la première balise-enfant ayant un attribut dont la valeur est
--- l'une de celles fournies, ou `nil` si aucune n'a été trouvée.
---@param ... string
---@return DOMElement?
function DOMElement:getFirstElementByAttributeValue(...)
	for el in list.stream(self:getElements()) do
		for attr in list.stream(el:getAttributes()) do
			for value in list.stream({...}) do
				if attr:getValue() == value then
					return el
				end
			end
		end
	end
end

--- Retourne tous les attributs de cette balise dont le nom est l'un de ceux
--- fournis.
---@param ... string
---@return DOMAttribute[]
function DOMElement:getAttributesByName(...)
	local args = {...}
	return list.fmergec({}, self:getAttributes(), function(attr)
		for name in list.stream(args) do
			if attr:getName() == name then
				return true
			end
		end
		return false
	end)
end

--- Retourne le premier attribut trouvé dont le nom correspond à un de ceux
--- fournis, ou `nil` si aucun n'a été trouvé.
---@param ... string Les noms
---@return DOMAttribute?
function DOMElement:getFirstAttributeByName(...)
	for attr in list.stream(self:getAttributes()) do
		for name in list.stream({...}) do
			if attr:getName() == name then
				return attr
			end
		end
	end
end

--- Retourne tous les attributs de cette balise dont la valeur est l'une de
--- celles fournies.
---@param ... string
---@return DOMAttribute[]
function DOMElement:getAttributesByValue(...)
	local attrs = {}
	for  attr in list.stream(self:getAttributes()) do
		for  value in list.stream({...}) do
			if attr:getValue() == value then
				attrs[#attrs+1] = attr
			end
		end
	end
	return attrs
end

--- Retourne le premier attribut de cette balise dont la valeur est l'une de
--- celles fournies, ou `nil` si aucune n'a été trouvée.
---@param ... string
---@return DOMAttribute?
function DOMElement:getFirstAttributeByValue(...)
	for  attr in list.stream(self:getAttributes()) do
		for value in list.stream({...}) do
			if attr:getValue() == value then
				return attr
			end
		end
	end
end

--- Retourne tous les noeuds de texte de cette balise dont le contenu est l'un
--- de ceux fournis.
---@param ... string
---@return DOMTextElement[]
function DOMElement:getTextElementsByContent(...)
	local els = {}
	for el in list.stream(self:getTextElements()) do
		for content in list.stream({...}) do
			if el:getContent() == content then
				els[#els+1] = el
			end
		end
	end
	return els
end

--- Retourne le premier noeud de texte de cette balise dont le contenu est l'un
--- de ceux fournis, ou `nil` si aucun n'a pu être trouvé.
---@param ... string
---@return DOMTextElement?
function DOMElement:getFirstTextElementByContent(...)
	for  el in list.stream(self:getTextElements()) do
		for content in list.stream({...}) do
			if el:getContent() == content then
				return el
			end
		end
	end
end

--- Vérifie si cette balise possède une balise-enfant dont le nom est l'un de
--- ceux fournis.
---@param ... string
---@return boolean
function DOMElement:hasElementWithName(...)
	local args = {...}
	local b = list.fcontains(self:getElements(), function(el)
		for name in list.stream(args) do
			if el:getName() == name then
				return true
			end
		end
		return false
	end)
	return b
end

--- Vérifie si cette balise possède un attribut dont le nom est l'un de ceux
--- fournis.
---@param ... string
---@return boolean
function DOMElement:hasAttributeWithName(...)
	local args = {...}
	local b = list.fcontains(self:getAttributes(), function(attr)
		for name in list.stream(args) do
			if attr:getName() == name then
				return true
			end
		end
		return false
	end)
	return b
end

--- Vérifie si cette balise possède un attribut dont la valeur est l'une de
--- celles fournies.
---@param ... string
---@return boolean
function DOMElement:hasAttributeWithValue(...)
	local args = {...}
	local b = list.fcontains(self:getAttributes(), function(attr)
		for value in list.stream(args) do
			if attr:getValue() == value then
				return true
			end
		end
		return false
	end)
	return b
end

--- Vérifie si cette balise possède un attribut avec une valeur, et dont le nom
--- est l'un de ceux fournis.
---@param ... string
---@return boolean
function DOMElement:hasNonEmptyAttributeWithName(...)
	for attr in list.stream(self._attrs) do
		if attr:getValue() ~= "" then
			for name in list.stream({...}) do
				if attr:getName() == name then
					return true
				end
			end
		end
	end
	return false
end

--- Vérifie si la balise possède au moins une balise enfant avec un attribut
--- ayant la valeur indiquée.
---@param name string Le nom de l'attribut
---@param value string La valeur de l'attribut
---@param elementName? string Si spécifié, restreint la recherche aux balises enfant portant ce nom
---@return boolean
function DOMElement:hasElementWithAttribute(name, value, elementName)
	local els
	if elementName ~= nil then
		els = self:getElementsByName(elementName)
	else
		els = self:getElements()
	end
	for el in list.stream(els) do
		for attr in list.stream(el:getAttributesByName(name)) do
			if attr:getValue() == value then
				return true
			end
		end
	end
	return false
end

--- Compte le nombre de balises enfant dont le nom est l'un de ceux fournis.
---@param ... string
---@return integer
function DOMElement:countElementsWithName(...)
	local c = 0
	for  el in list.stream(self:getElements()) do
		for  name in list.stream({...}) do
			if el:getName() == name then
				c = c + 1
			end
		end
	end
	return c
end

--- Compte le nombre d'attributs dans cette balise dont le nom est l'un de ceux
--- fournis.
---@param ... string
---@return integer
function DOMElement:countAttributesWithName(...)
	local c = 0
	for attr in list.stream(self:getAttributes()) do
		for name in list.stream({...}) do
			if attr:getName() == name then
				c = c + 1
			end
		end
	end
	return c
end

--- Compte le nombre d'attributs dans cette balise dont la valeur est l'une de
--- celles fournies.
---@param ... string
---@return integer
function DOMElement:countAttributesWithValue(...)
	local c = 0
	for attr in list.stream(self:getAttributes()) do
		for value in list.stream({...}) do
			if attr:getValue() == value then
				c = c + 1
			end
		end
	end
	return c
end

--- Scanne non-récursivement tous les textes contenus dans cette balise, mais
--- pas ses enfants, et les retourne sous forme d'un `string`.
---@return string
function DOMElement:getShallowTextContent()
	local s = ""
	for el in list.stream(self:getTextElements()) do
		s = s .. el:getContent()
	end
	return s
end

--- Scanne non-récursivement tous les textes contenus dans cette balise, mais
--- pas ses enfants, et les retourne sous forme d'une table.
---@return string[]
function DOMElement:getShallowTextContentTbl()
	local t = {} ---@type string[]
	for el in list.stream(self:getTextElements()) do
		t[#t+1] = el:getContent()
	end
	return t
end

--- Scanne récursivement tous les textes contenus dans cette balise et ses
--- enfants, les concatène et les retourne sous forme d'un `string`.
---@return string
function DOMElement:getTextContent()
	local s = ""
	for el in list.stream(self:getAllElements()) do
		if el:instanceof(DOMElement) then
			s = s .. el:getTextContent()
		elseif el:instanceof(DOMTextElement) then
			s = s .. el:getContent()
		end
	end
	return s
end

--- Scanne récursivement tous les textes contenus dans cette balise et ses
--- enfants, et les retourne sous forme d'une table.
---@return string[]
function DOMElement:getTextContentTbl()
	local t = {} ---@type string[]
	for el in list.stream(self:getAllElements()) do
		if el:instanceof(DOMElement) then
			---@cast el DOMElement
			list.merge(t, el:getTextContentTbl())
		elseif el:instanceof(DOMTextElement) then
			---@cast el DOMTextElement
			t[#t+1] = el:getContent()
		end
	end
	return t
end


-- *****************************************************************************
-- * DOMTextElement
-- *
-- *  class DOMTextElement
-- *****************************************************************************

---@param str? string Le contenu textuel du noeud. `""` par défaut.
function DOMTextElement:initialize(str)
	local str = str or ""
	self._textContent = str
	self._name = "#text"
end

--- Définit le contenu textuel du noeud.
---@param str string
---@return self self
function DOMTextElement:setContent(str)
	self._textContent = str
	return self
end

--- Retourne le contenu textuel du noeud.
---@return string
function DOMTextElement:getContent()
	return self._textContent
end

--- Construit le noeud en une table de sérialization utilisable par SLAXML.
---@return {type:string, name:string, cdata:false, value:string}
function DOMTextElement:build()
	return {
		type = "text",
		name = "#text",
		cdata = false,
		value = self._textContent
	}
end


-- *****************************************************************************
-- * DOMDocument
-- *
-- *  class DOMDocument
-- *****************************************************************************

--- 	(constructor) DOMDocument()
--- ---
--- Représente un Document Object Model, ou DOM. Il est la base de toute
--- hiérarchie DOM. Il contient une seule balise-racine, de type `DOMElement`,
--- qui peut quant à elle contenir aussi bien des balises que des noeuds de
--- texte, en quantité désirée.
---@class DOMDocument:Class
local DOMDocument = class('DOMDocument')

function DOMDocument:initialize()
	self._content = nil ---@type DOMElement
end

--- Définit la balise-racine du document.
---@param root DOMElement
---@return self self
function DOMDocument:setRoot(root)
	self._content = root
	return self
end

--- Retourne la balise-racine du document.
---@return DOMElement
function DOMDocument:getRoot()
	return self._content
end

--- Construit le DOM en une chaîne de caractères XML.
---@param indent? number|string Le(s) caractère(s) utilisé(s) pour l'indentation, si un `string` est passé; ou le nombre d'espaces, si un `number` est passé. Par défaut, `'\t'`
---@return string
function DOMDocument:toXML(indent)
	local indent = indent or '\t'
	local r = {
		type = "document",
		name = "#doc",
		kids = {},
		root = {}
	}
	r.kids[1] = self._content:build()
	return sxml:xml(r, {
		indent = indent
	})
end

--- Crée un attribut à partir d'une table désérialisée par SLAXML.
---@protected
---@param data table
---@return DOMAttribute
function DOMDocument.static:createAttribute(data)
	return DOMAttribute(data.name, data.value)
end

--- Crée une balise enfant à partir d'une table désérialisée par SLAXML.
---@protected
---@param data table
---@return DOMElement
function DOMDocument.static:createNodeChild(data)
	local el = DOMElement(data.name)
	if #data.kids > 0 then
		for _, v in ipairs(data.kids) do
			el:addChild(DOMDocument:createChild(v))
		end
	end
	if #data.attr > 0 then
		for _, v in ipairs(data.attr) do
			el:addAttribute(DOMDocument:createAttribute(v))
		end
	end
	return el
end

--- Crée un noeud textuel à partir d'une table désérialisée par SLAXML.
---@protected
---@param data table
---@return DOMTextElement
function DOMDocument.static:createTextChild(data)
	return DOMTextElement(data.value)
end

--- Crée un noeud enfant à partir d'une table désérialisée par SLAXML.
---@protected
---@param data table
---@return DOMElement|DOMTextElement
function DOMDocument.static:createChild(data)
	if data.type == "element" then
		return DOMDocument:createNodeChild(data)
	elseif data.type == "text" then
		return DOMDocument:createTextChild(data)
	end
end

--- Crée un nouveau `DOMDocument` en parsant une chaîne de caractères contenant
--- des données XML.
---@param xml string
---@return DOMDocument
function DOMDocument.static:createFromXML(xml)
	local parsed = sxml:dom(xml, {
		stripWhitespace = true
	})
	local rootMarkup = DOMDocument:createChild(parsed.root) ---@type DOMElement

	local document = DOMDocument() ---@type DOMDocument
	document:setRoot(rootMarkup)
	return document
end

return {
	DOMAttribute   = DOMAttribute,
	DOMElement     = DOMElement,
	DOMTextElement = DOMTextElement,
	DOMDocument    = DOMDocument
}