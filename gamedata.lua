require('calypso.core');
require('calypso.fs');
local dom                 = require('calypso.dom');
local LocalizationManager = require('global.locales.LocalizationManager');
local Locale              = require('global.locales.Locale');
local Module              = require('global.Module');
local ItemData            = require('global.items.ItemData');
local ItemOption          = require('global.items.ItemOption');
local ItemOptionType      = require('global.items.ItemOptionType');
local ItemsManager        = require('global.items.ItemsManager');
local ItemType            = require('global.items.ItemType');
local ItemUnit            = require('global.items.ItemUnit');
local ids                 = require('foundation.ids');
local Tag                 = require('global.tags.Tag');
local TagsManager         = require('global.tags.TagsManager');
local MachineType         = require('global.machines.MachineType');
local MachineToolSlot     = require('global.machines.MachineToolSlot');
local MachinesManager     = require('global.machines.MachinesManager');
local MachineModel        = require('global.machines.MachineModel');
local Recipe              = require('global.recipes.Recipe');
local RecipeInput         = require('global.recipes.RecipeInput');
local RecipeOutput        = require('global.recipes.RecipeOutput');
local RecipeOutputAmount  = require('global.recipes.RecipeOutputAmount');
local RecipesManager      = require('global.recipes.RecipesManager');
local ResourcesManager    = require('global.resources.ResourcesManager');
require('foundation.log');


-- *****************************************************************************
-- * GameData
-- *
-- * abstract class GameData
-- *****************************************************************************

--- Une classe statique à partir de laquelle on peut accéder au contenu du jeu
--- qui n'est pas dépendant de la partie : items, tags, machines...
---@class GameData
local GameData = {
	BASE_MODULE_NAME = 'Base';
	_modules = {}; ---@type table<string, Module>
}

--- Vérifie si un module existe sous un ID donné.
---@param id string
---@return boolean
function GameData.hasModule(id)
	local id = tostring(id);
	return GameData._modules[id] ~= nil;
end

--- Enregistre un module. Si un module existe déjà avec cet ID, lance une
--- erreur.
---
--- _@throws_ `DuplicatedIdError` - Un module existe déjà sous cet identifiant
---@param id string
---@param mod Module
function GameData.registerModule(id, mod)
	local id = tostring(id);
	if not GameData.hasModule(id) then
		GameData._modules[id] = mod;
	else
		throw(Errors.DuplicatedId, 'A module already exists under the ID ' .. id);
	end
end

--- Retourne un module enregistré selon son ID. Si aucun module n'existe sous
--- cet ID, retourne `nil`.
---@see GameData.getModule_nilsafe
---@param id string
---@return Module?
function GameData.getModule(id)
	local id = tostring(id);
	if GameData.hasModule(id) then
		return GameData._modules[id];
	end
end

--- Retourne un module enregistré selon son ID. Si aucun module n'existe sous
--- cet identifiant, lève une erreur.
---
--- _@throws_ `UnknownIdError` - Le module n'existe pas
---@see GameData.getModule
---@param id string
---@return Module
function GameData.getModule_nilsafe(id)
	local id = tostring(id);
	local m = GameData.getModule(id);
	if not m then
		throw(Errors.UnknownId, 'Unknown Module ID : "' .. id .. '"');
	end
	return m;
end

--- Retourne une référence vers la liste des modules chargés. La clef
--- représente l'ID du module, et la valeur le module en lui-même.
---@return table<string, Module>
function GameData.getModules()
	return GameData._modules;
end

--- Charge le fichier `module.xml` d'un module.
---@param xmldata string Le contenu du fichier
---@param folderName string Le nom du dossier du module
---@return Module
function GameData.loadModuleFile(xmldata, folderName)
	local xmldata = tostring(xmldata);
	local LOG_HEADER = "[Loading Module in " .. folderName .. "] : ";
	loginf(LOG_HEADER .. "Loading module.xml");

	local doc = dom.DOMDocument:createFromXML(xmldata); ---@type DOMDocument
	local idNode = doc:getRoot():getFirstElementByName("Id");
	if not idNode then
		throw(Errors.MarkupNotFound, 'Cannot find markup "Id" in module file');
	end

	local idNodeContent = idNode:getShallowTextContent();
	if idNodeContent == "" then
		throw(Errors.EmptyMarkup, 'Empty markup : "Id", in module file');
	end

	local generatedModule = Module(idNodeContent); ---@type Module

	generatedModule:setName(generatedModule:getId());
	if doc:getRoot():hasElementWithName("Name") then
		local nameContent = doc:getRoot():getFirstElementByName("Name"):getShallowTextContent();
		if nameContent ~= "" then
			generatedModule:setName(nameContent);
		end
	end

	if doc:getRoot():hasElementWithName("Version") then
		local versionContent = doc:getRoot():getFirstElementByName("Version"):getShallowTextContent();
		if versionContent ~= "" then
			generatedModule:setVersion(versionContent);
		end
	end

	if doc:getRoot():hasElementWithName("Authors") then
		for author in list.stream(doc:getRoot():getFirstElementByName("Authors"):getElementsByName("Author")) do
			local authorContent = author:getShallowTextContent();
			if authorContent ~= "" then
				generatedModule:addAuthor(authorContent);
			else
				logwrn(LOG_HEADER .. 'Ignored empty Author markup in module file');
			end
		end
	end

	loginf(LOG_HEADER .. "Module recognized : " .. generatedModule:getId());
	return generatedModule;
end

--- Lit des données XML de langue et en tire un dictionnaire contenant les
--- clefs de traduction définies par ces données. Retourne ce dictionnaire,
--- avec en clef l'ID de la traduction et en valeur sa valeur traduite.
--- Retourne aussi, en deuxième retour, le code de la langue. Si les
--- données ne spécifient pas de code de langue, ou qu'elles sont
--- malformées, lève une erreur.
---@param xmldata string
---@param mod Module Le module qui définit ces données de langue
---@return table<string,string> data
---@return string code
function GameData.loadLocale(xmldata, mod)
	local xmldata = tostring(xmldata);
	local data = {}; ---@type table<string,string>
	local doc = dom.DOMDocument:createFromXML(xmldata); ---@type DOMDocument

	if doc:getRoot():getName() ~= "Locale" then
		throw(Errors.WrongMarkupName, 'Malformed Locale : the root markup must be "Locale"', {
			module = mod:getId(),
			expected = '"Locale"',
			got = '"' .. doc:getRoot():getName() .. '"'
		});
	end

	if not doc:getRoot():hasNonEmptyAttributeWithName("Code") then
		throw(Errors.MarkupAttributeNotFound, 'Malformed Locale : Locale root markup must have a "Code" attribute', {
			module = mod:getId()
		});
	end
	local code = doc:getRoot():getFirstAttributeByName("Code"):getValue();
	local LOG_HEADER = "[Loading Module " .. mod:getId() .. " (Locale " .. code .. ")] : ";
	loginf(LOG_HEADER .. "Loading locale");

	for key in list.stream(doc:getRoot():getElementsByName("Key")) do
		if not key:hasNonEmptyAttributeWithName("Id") then
			logwrn(LOG_HEADER .. "Ignored key without Id");
			goto continue;
		end
		local key_id = key:getFirstAttributeByName("Id"):getValue();
		if not ids.isValid(key_id) then
			logwrn(LOG_HEADER .. "Ignored key with invalid Id : \"" .. key_id .. "\"");
			goto continue;
		end
		data[key_id] = key:getShallowTextContent();
		loginf(LOG_HEADER .. "Registering key " .. key_id);
		::continue::
	end

	return data, code;
end

--- Charge les données de langue contenues dans le module. Lève une erreur si
--- le chargement d'une des langues a échoué.
---@param moduleFolder string
---@param mod Module
function GameData.loadLocales(moduleFolder, mod)
	local localesFolderName = moduleFolder .. "locales/";
	local LOG_HEADER = "[Loading Module " .. mod:getId() .. " (Locales)] : ";
	do
		local tmp = love.filesystem.getInfo(localesFolderName);
		if tmp == nil or tmp.type ~= "directory" then
			loginf(LOG_HEADER .. "No locale found.");
			return nil;
		end
	end;

	for filename in list.stream(love.filesystem.getDirectoryItems(localesFolderName)) do
		if love.filesystem.getInfo(localesFolderName .. filename).type == "file" then
			loginf(LOG_HEADER .. "Locale found : " .. filename);
			local success, data, code = pcall(GameData.loadLocale, love.filesystem.read(localesFolderName .. filename), mod);
			if not success then
				---@cast data ErrorObject
				throw(Errors.LoadingFailed, 'Failed to load locale file', {
					file = filename,
					module = mod:getId()
				}, data);
			end

			local referencedLocale = LocalizationManager.getLocale(code) or Locale():setCode(code);

			for key, value in pairs(data) do
				if key == "locale.name" then
					referencedLocale:setName(value);
				else
					referencedLocale:set(ids.ensureIdModule(key), value);
				end
			end

			if not LocalizationManager.hasLocale(referencedLocale:getCode()) then
				LocalizationManager.registerLocale(referencedLocale);
			end
		end
	end
end

--- Charge les données d'items d'un module.
---@param xmldata string
---@param mod Module
function GameData.loadItems(xmldata, mod)
	local LOG_HEADER = "[Loading Module " .. mod:getId() .. " (Items)] : ";
	loginf(LOG_HEADER .. "Loading items");
	local doc = dom.DOMDocument:createFromXML(xmldata); ---@type DOMDocument
	for i in list.stream(doc:getRoot():getElementsByName("Item")) do
		if not i:hasNonEmptyAttributeWithName("Id") then
			throw(Errors.MarkupAttributeNotFound, 'Malformed Item : Item declaration with no ID', {
				module = mod:getId()
			});
		end
		local id = i:getFirstAttributeByName("Id"):getValue();
		loginf(LOG_HEADER .. "Loading item " .. id);

		if not i:hasElementWithName("Type") then
			throw(Errors.MarkupNotFound, 'Malformed Item : Item declaration with no item type', {
				module = mod:getId(),
				item = id
			});
		end
		local itemType_tc = i:getFirstElementByName("Type"):getShallowTextContent();
		if itemType_tc == "" then
			throw(Errors.EmptyMarkup, 'Malformed Item : Item declaration with no item type', {
				module = mod:getId(),
				item = id
			});
		end
		local itemType = ItemType.fromString(itemType_tc, true);

		if not i:hasElementWithName('Unit') then
			logwrn(LOG_HEADER .. 'Ignored item without measurement unit', {
				Module = mod:getId(),
				Item = id
			});
			goto continue;
		end
		local itemMeasurementUnit = i:getFirstElementByName('Unit'):getShallowTextContent():lower();
		if not ItemUnit.isValid(itemMeasurementUnit) then
			logwrn(LOG_HEADER .. 'Ignored item with invalid measurement unit', {
				Module = mod:getId(),
				Item = id,
				Unit = itemMeasurementUnit
			});
			goto continue;
		end

		local item = ItemData(id, itemType, mod, itemMeasurementUnit); ---@type ItemData

		-- On cherche la taille a	après avoir créé l'item, comme ça si l'item ne
		-- définit pas de taille dans le fichier XML, on utilisera la taille par
		-- défaut.
		if i:hasElementWithName('Size') then
			local itemSize = tonumber(i:getFirstElementByName('Size'):getShallowTextContent());
			if itemSize ~= nil then
				item:setSize(itemSize);
			end
		end

		if i:hasElementWithName("Icon") then
			local icon = i:getFirstElementByName("Icon"):getShallowTextContent();
			if icon ~= "" then
				item:setIcon(icon);
			end
		end

		if i:hasElementWithName("Options") then
			for option in list.stream(i:getFirstElementByName("Options"):getElementsByName("Option")) do
				if not option:hasNonEmptyAttributeWithName("Id") then
					logwrn(LOG_HEADER .. "Ignored option with no ID in item " .. id);
					goto continue;
				end
				local optionId = option:getFirstAttributeByName("Id"):getValue();
				if item:hasOption(optionId) then
					logwrn(string.format("%sIgnored option %s with duplicated ID in item %s", LOG_HEADER, optionId, id));
					goto continue;
				end
				loginf(LOG_HEADER .. "Reading option " .. optionId);

				if not option:hasNonEmptyAttributeWithName("Type") then
					logwrn(string.format('%sIgnored option %s with no type in item %s', LOG_HEADER, optionId, id));
					goto continue;
				end
				local optionTypeStr = option:getFirstAttributeByName("Type"):getValue();
				local success, optionType = pcall(ItemOptionType.fromString, optionTypeStr);
				if not success then
					---@cast optionType ErrorObject
					logerr(optionType);
					logwrn(string.format('%sIgnored option %s with invalid type in item %s', LOG_HEADER, optionId, id));
					goto continue;
				end

				if optionType == ItemOptionType.RANGE then
					if not option:hasNonEmptyAttributeWithName("Range") then
						logwrn(string.format('%sIgnored option %s with no Range in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end
					local optionRange = string.split(option:getFirstAttributeByName("Range"):getValue(), ';');
					if not #optionRange == 2 then
						logwrn(string.format('%sIgnored option %s with invalid range in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end
					local optionRangeMin = tonumber(optionRange[1]);
					local optionRangeMax = tonumber(optionRange[2]);
					if not optionRangeMin or not optionRangeMax then
						logwrn(string.format('%sIgnored option %s with invalid range in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end

					local optionFloat = false;
					if option:hasNonEmptyAttributeWithName("Float") then
						local optionFloatStr = option:getFirstAttributeByName("Float"):getValue():lower();
						if optionFloatStr == 'yes' then optionFloat = true;
						elseif optionFloatStr ~= 'no' then
							logwrn(string.format('%sIgnored option %s with invalid Float value in item %s', LOG_HEADER, optionId, id));
							goto continue;
						end
					end
					if not optionFloat and (not isInteger(optionRangeMin) or not isInteger(optionRangeMax)) then
						logwrn(string.format('%sIgnored option %s with unauthorized float range in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end

					if not option:hasNonEmptyAttributeWithName("Default") then
						logwrn(string.format('%sIgnored option %s with no default value in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end
					local optionDefault = tonumber(option:getFirstAttributeByName("Default"):getValue());
					if not optionDefault then
						logwrn(string.format('%sIgnored option %s with invalid default value in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end
					if optionDefault < optionRangeMin or optionDefault > optionRangeMax then
						logwrn(string.format('%sIgnored option %s with out of range default value in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end
					if not optionFloat and not isInteger(optionDefault) then
						logwrn(string.format('%sIgnored option %s with unauthorized float default value in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end

					local opt = ItemOption(optionId, optionType); ---@type ItemOption
					opt:setRange(optionRangeMin, optionRangeMax, optionFloat);
					item:registerOption(opt);
				elseif optionType == ItemOptionType.ENUM then
					local optionValues = {}; ---@type string[]
					for value in list.stream(option:getElementsByName("Value")) do
						local valueStr = value:getShallowTextContent();
						if not list.contains(optionValues, valueStr) then
							optionValues[#optionValues+1] = valueStr;
						else
							logwrn(string.format('%sIgnored duplicate of value %s in option %s in item %s', LOG_HEADER, valueStr, optionId, id));
						end
					end
					if #optionValues == 0 then
						logwrn(string.format('%sIgnored option %s with no values in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end

					if not option:hasNonEmptyAttributeWithName("Default") then
						logwrn(string.format('%sIgnored option %s with no default value in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end
					local optionDefault = option:getFirstAttributeByName("Default"):getValue();

					if not list.contains(optionValues, optionDefault) then
						logwrn(string.format('%sIgnored option %s with unknown default value in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end

					local opt = ItemOption(optionId, optionType); ---@type ItemOption
					opt:setValues(optionValues);
					opt:setDefault(optionDefault);
					item:registerOption(opt);
				elseif optionType == ItemOptionType.SWITCH then
					if not option:hasNonEmptyAttributeWithName("Default") then
						logwrn(string.format('%sIgnored option %s with no default value in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end
					local optionDefault = option:getFirstAttributeByName("Default"):getValue():lower();
					if optionDefault == 'yes' then optionDefault = true;
					elseif optionDefault == 'no' then optionDefault = false;
					else
						logwrn(string.format('%sIgnored option %s with invalid default value in item %s', LOG_HEADER, optionId, id));
						goto continue;
					end

					local opt = ItemOption(optionId, optionType); ---@type ItemOption
					opt:setDefault(optionDefault);
					item:registerOption(opt);
				end
				::continue::
			end
		end

		ItemsManager.registerItem(item);
		::continue::
	end
end

--- Charge les types de machines décrites dans des données XML.
---@param xmldata string
---@param mod Module
function GameData.loadMachinesTypes(xmldata, mod)
	local LOG_HEADER = "[Loading Module " .. mod:getId() .. " (Machines Types)] : ";
	loginf(LOG_HEADER .. "Loading machines types");

	local doc = dom.DOMDocument:createFromXML(xmldata); ---@type DOMDocument

	for mtype in list.stream(doc:getRoot():getElementsByName("MachineType")) do
		if not mtype:hasNonEmptyAttributeWithName("Id") then
			logwrn(string.format('%sIgnored Machine Type with no ID in module %s', LOG_HEADER, mod:getId()));
			goto continue;
		end
		local generatedMachineType = MachineType(mtype:getFirstAttributeByName("Id"):getValue(), mod); ---@type MachineType
		loginf(LOG_HEADER .. "Loading machine type " .. generatedMachineType:getId());

		if mtype:hasElementWithName("OperatorsAmount") then
			local op = mtype:getFirstElementByName("OperatorsAmount"):getShallowTextContent();
			if op ~= "" then
				generatedMachineType:setOperators(math.abs(tonumber(op) or generatedMachineType:getOperators()));
			end
		end

		if mtype:hasElementWithName("Tools") then
			for tool in list.stream(mtype:getFirstElementByName("Tools"):getElementsByName("Tool")) do
				if not tool:hasNonEmptyAttributeWithName("Id") then
					logwrn(string.format('%sIgnored machine type tool with no ID', LOG_HEADER), {
						['Machine type'] = generatedMachineType:getId(),
						Module = mod:getId()
					});
					goto continue;
				end
				local toolslot = MachineToolSlot(tool:getFirstAttributeByName("Id"):getValue(), generatedMachineType); ---@type MachineToolSlot

				if tool:hasElementWithName("Accepts") then
					local accepts = tool:getFirstElementByName("Accepts");
					if accepts:hasElementWithName("Id") then
						for accepted in list.stream(accepts:getElementsByName("Id")) do
							if not accepted:hasNonEmptyAttributeWithName("Type") then
								logwrn(LOG_HEADER .. 'Ignored machine type tool acceptation with no ID', {
									Tool = toolslot:getId(),
									['Machine type'] = generatedMachineType:getId(),
									Module = mod:getId()
								});
								goto continue;
							end
							local accepted_type = accepted:getFirstAttributeByName("Type"):getValue();
							local accepted_id   = ids.ensureIdModule(accepted:getShallowTextContent());
							if not ids.isValid(accepted_id) then
								logwrn(LOG_HEADER .. 'Ignored machine type tool acceptation with invalid ID : ' .. accepted_id, {
									['Machine type'] = generatedMachineType:getId(),
									Tool = toolslot:getId(),
									Module = mod:getId()
								});
								goto continue;
							end
							if accepted_type == "item" then
								if ItemsManager.hasItem(accepted_id) then
									toolslot:accept(ItemsManager.getItem(accepted_id));
								else
									logwrn(LOG_HEADER .. 'Ignored machine type tool acceptation pointing to the unknown item ' .. accepted_id, {
										['Machine type'] = generatedMachineType:getId(),
										Tool = toolslot:getId(),
										Module = mod:getId()
									});
								end
							elseif accepted_type == "tag" then
								if TagsManager.hasTag(accepted_id) then
									toolslot:accept(TagsManager.getTag(accepted_id));
								else
									logwrn(LOG_HEADER .. 'Ignored machine type tool acceptation pointing to the unknown tag ' .. accepted_id, {
										['Machine type'] = generatedMachineType:getId(),
										Tool = toolslot:getId(),
										Module = mod:getId()
									});
								end
							else
								logwrn(LOG_HEADER .. 'Ignored machine type tool acceptation with unknown type ' .. accepted_type, {
									['Machine type'] = generatedMachineType:getId(),
									Module = mod:getId(),
									Tool = toolslot:getId()
								});
							end
							::continue::
						end
					else
						logwrn(string.format('%sIgnored machine type tool %s that does not accepts any item', LOG_HEADER, toolslot:getId()), {
							['Machine type'] = generatedMachineType:getId(),
							Module = mod:getId()
						});
					end
				else
					logwrn(string.format(
						'%sIgnored machine type tool %s that does not accepts any item',
						LOG_HEADER,
						toolslot:getId()
					), {
						['Machine type'] = generatedMachineType:getId(),
						Module = mod:getId()
					});
				end

				if tool:hasElementWithName("Amount") then
					local toolAmount = tool:getFirstElementByName("Amount"):getShallowTextContent();
					if toolAmount ~= "" then
						toolslot:setAmount(math.abs(tonumber(toolAmount) or toolslot:getAmount()));
					end
				end

				generatedMachineType:addToolSlot(toolslot);
				::continue::
			end
		end

		MachinesManager.registerMachineType(generatedMachineType);
		::continue::
	end
end

--- Charge les données de modèles de machines contenus d'un module.
---@param xmldata string
---@param mod Module
function GameData.loadMachinesModels(xmldata, mod)
	local LOG_HEADER = "[Loading Module " .. mod:getId() .. " (Machines Models)] : ";
	loginf(LOG_HEADER .. "Loading machines models");

	local doc = dom.DOMDocument:createFromXML(xmldata); ---@type DOMDocument

	for mmodel in list.stream(doc:getRoot():getElementsByName("MachineModel")) do
		if not mmodel:hasNonEmptyAttributeWithName("Id") then
			logwrn(LOG_HEADER .. 'Ignored machine model with no ID', {
				Module = mod:getId()
			});
			goto continue;
		end
		local id = mmodel:getFirstAttributeByName("Id"):getValue();
		loginf(LOG_HEADER .. "Loading machine model " .. id);

		if not mmodel:hasElementWithName("MachineType") then
			logwrn(string.format(
				'%sIgnored machine model %s with no machine type',
				LOG_HEADER,
				id
			), {
				Module = mod:getId()
			});
			goto continue;
		end
		local mtypeId = mmodel:getFirstElementByName("MachineType"):getShallowTextContent();
		if not ids.isValid(mtypeId) then
			logwrn(string.format('%sIgnored machine model %s with invalid machine type ID %s', LOG_HEADER, id, mtypeId), {
				Module = mod:getId()
			});
			goto continue;
		end
		if not MachinesManager.hasMachineType(mtypeId) then
			logwrn(string.format(
				'%sIgnored machine model %s pointing to the unknown machine type %s',
				LOG_HEADER,
				id,
				mtypeId
			), {
				Module = mod:getId()
			});
			goto continue;
		end

		local generatedMachineModel = MachineModel(id, mod, MachinesManager.getMachineType(mtypeId)); ---@type MachineModel

		if mmodel:hasElementWithName("Price") then
			local sPrice = mmodel:getFirstElementByName("Price"):getShallowTextContent();
			if sPrice ~= "" then
				generatedMachineModel:setPrice(math.abs(tonumber(sPrice) or generatedMachineModel:getPrice()));
			end
		end

		MachinesManager.registerMachineModel(generatedMachineModel);
		::continue::
	end
end

--- Fonction interne pour parser le contenu d'un tag ou d'un exTag. Retourne
--- un `string[]` contenant les ID des objets.
---
--- _@throws_ `Errors.TypesIncompatibility` - `content` n'est pas d'un type
---           compatible avec le tag
---
--- _@throws_ `Errors.UnknownTag` - le tag `content` n'existe pas
---
--- _@throws_ `Errors.InvalidEnumValue` - `contentType` n'est pas reconnu
---@param contentType "obj"|"tag"
---@param content string
---@param tagType Specifier.Tag
---@return string[]
function GameData.internalParseTagContent(contentType, content, tagType)
	local id = ids.ensureIdModule(content);
	if contentType == "obj" then
		if ids.Specifier.isQualifierValidTagContent(tagType, ids.extractId(id).qualifier) then
			return {id};
		else
			throw(Errors.TypesIncompatibility, string.format('Types incompatibility in a tag\'s content : %s object cannot be part of a tag of type %s', id, tagType));
		end
	elseif contentType == "tag" then
		if TagsManager.hasTag(id) then
			return TagsManager.getTag_nilsafe(id):getContent();
		else
			throw(Errors.UnknownTag, 'Unknown tag : ' .. id);
		end
	else
		throw(Errors.InvalidEnumValue, 'Invalid content type : ' .. contentType);
	end
end

--- Fonction interne qui charge les tags. Cette fonction charge les tags
--- contenus dans `els`, enregistre les balises `<Tag/>`, et modifie les
--- balises `<ExTag/>`. Il faut lui spécifier le type de ces tags. Elle est
--- appelée par toutes les fonctions qui chargent les tags (`loadItemsTags`,
--- `loadMachinesTags`, ...).
---@param els DOMElement[] Les tags
---@param type Specifier.Tag Le type des tags
---@param mod Module Le module qui définit ces tags
---@param LOG_HEADER string L'en-tête des messages de log
function GameData._loadTags(els, type, mod, LOG_HEADER)
	for node in list.stream(els) do
		if node:getName() == "Tag" then
			-- On crée un nouveau tag
			if not node:hasNonEmptyAttributeWithName("Id") then
				logwrn(LOG_HEADER .. 'Ignored tag with no ID', {
					Module = mod:getId()
				});
				goto continue;
			end
			local id = node:getFirstAttributeByName("Id"):getValue();

			loginf(LOG_HEADER .. "Loading tag " .. id);
			local generatedTag = Tag(id, type, mod); ---@type Tag

			if node:hasElementWithName("Contains") then
				-- il a du contenu
				local content = node:getFirstElementByName("Contains"):getElementsByName("Content");
				for i in list.stream(content) do
					if not i:hasNonEmptyAttributeWithName("Type") then
						logwrn(LOG_HEADER .. 'Ignored tag content with no type', {
							Module = mod:getId(),
							Tag = id
						});
						goto continue;
					end
					local ctype = i:getFirstAttributeByName("Type"):getValue();
					local ccontent = i:getShallowTextContent();
					if ccontent == "" then
						logwrn(LOG_HEADER .. 'Ignored empty tag content', {
							Module = mod:getId(),
							Tag = id
						});
						goto continue;
					end
					local tagContent = GameData.internalParseTagContent(ctype, ccontent, type);
					if #tagContent == 1 then
						local success, ret = pcall(Tag.add, generatedTag, tagContent[1]);
						if not success then
							---@cast ret ErrorObject
							logwrn(string.format('%sIgnored tag content %s', LOG_HEADER, tagContent[1]), {
								Module = mod:getId(),
								Tag = id,
								Error = ret.message
							});
							goto continue;
						end
					else
						for x in list.stream(tagContent) do
							local success, ret = pcall(Tag.add, generatedTag, x);
							if not success then
								---@cast ret ErrorObject
								logwrn(string.format('%sIgnored tag content %s', LOG_HEADER, x), {
									Module = mod:getId(),
									Tag = id,
									Error = ret.message
								});
								goto continue;
							end
						end
					end
					::continue::
				end
			end

			if node:hasElementWithName("Unseen") then
				generatedTag:setUnseen(true);
			end

			TagsManager.registerTag(generatedTag);
		elseif node:getName() == "ExTag" then
			-- On ajoute du contenu à un tag préexistant
			if not node:hasNonEmptyAttributeWithName("Id") then
				logwrn(LOG_HEADER .. 'Ignored ExTag with no ID', {
					Module = mod:getId()
				});
				goto continue;
			end
			local id = ids.ensureIdModule(node:getFirstAttributeByName("Id"):getValue());

			loginf(LOG_HEADER .. "Appending to tag " .. id);

			local referencedTag = TagsManager.getTag(id);
			if not referencedTag then
				logwrn(LOG_HEADER .. 'Ignored ExTag pointing to the unknown tag ' .. id, {
					Module = mod:getId()
				});
				goto continue;
			end

			if node:hasElementWithName("Contains") then
				-- il y a du contenu
				local content = node:getFirstElementByName("Contains"):getElementsByName("Content");
				for i in list.stream(content) do
					if not i:hasNonEmptyAttributeWithName("Type") then
						logwrn(LOG_HEADER .. 'Ignored ExTag content with no type', {
							Module = mod:getId(),
							Tag = id
						});
						goto continue;
					end
					local ctype = i:getFirstAttributeByName("Type"):getValue();
					local ccontent = i:getShallowTextContent();
					if ccontent == "" then
						logwrn(LOG_HEADER .. 'Ignored empty content in ExTag ' .. id, {
							Module = mod:getId()
						});
						goto continue;
					end
					local tagContent = GameData.internalParseTagContent(ctype, ccontent, type);
					if #tagContent == 1 then
						local success, ret = pcall(Tag.add, referencedTag, tagContent[1]);
						if not success then
							---@cast ret ErrorObject
							logwrn(string.format('%sIgnored ExTag content %s', LOG_HEADER, tagContent[1]), {
								Module = mod:getId(),
								Tag = id,
								Error = ret.message
							});
							goto continue;
						end
					else
						for x in list.stream(tagContent) do
							local success, ret = pcall(Tag.add, referencedTag, x);
							if not success then
								---@cast ret ErrorObject
								logwrn(string.format('%sIgnored ExTag content %s', LOG_HEADER, x), {
									Module = mod:getId(),
									Tag = id,
									Error = ret.message
								});
								goto continue;
							end
							::continue::
						end
					end
					::continue::
				end
			end
		end
		::continue::
	end
end

--- Charge les données de tags d'items d'un module.
---@param xmldata string
---@param mod Module
function GameData.loadItemsTags(xmldata, mod)
	local LOG_HEADER = "[Loading Module " .. mod:getId() .. " (Items Tags)] : ";
	loginf(LOG_HEADER .. "Loading items tags");
	local doc = dom.DOMDocument:createFromXML(xmldata); ---@type DOMDocument
	if not doc:getRoot():hasElementWithName("Items") then
		loginf(LOG_HEADER .. "No item tags found.");
		return;
	end
	GameData._loadTags(doc:getRoot():getFirstElementByName("Items"):getElementsByName("Tag", "ExTag"), ids.Specifier.Tag.ITEMS, mod, LOG_HEADER);
end

--- Charge les données de tags de machines d'un module.
---@param xmldata string
---@param mod Module
function GameData.loadMachinesTags(xmldata, mod)
	local LOG_HEADER = "[Loading Module " .. mod:getId() .. " (Machines Tags)] : ";
	loginf(LOG_HEADER .. "Loading machines tags");
	local doc = dom.DOMDocument:createFromXML(xmldata); ---@type DOMDocument
	if not doc:getRoot():hasElementWithName("Machines") then
		loginf(LOG_HEADER .. "No machines tags found.");
		return;
	end
	GameData._loadTags(doc:getRoot():getFirstElementByName("Machines"):getElementsByName("Tag", "ExTag"), ids.Specifier.Tag.MACHINES, mod, LOG_HEADER);
end

--- Fonction interne pour parser la valeur `v` autorisée de l'option de type
--- `t`.
---
--- _@throws_ `Errors.InvalidValue` - `v` ne possède pas une valeur valide au
---           regard de son type (_range_)
---
--- _@throws_ `Errors.InvalidEnumValue` - `v` ne possède pas une valeur
---           autorisée au regard de son type (_switch_)
---@param v string
---@param t ItemOptionType
---@return string|number|number[]|boolean
function GameData._parseRecipeInputOptionValue(v, t)
	if t == ItemOptionType.ENUM then
		return v;
	elseif t == ItemOptionType.RANGE then
		local min, max = unpack(string.split(v, ';'));
		min = tonumber(min);
		max = tonumber(max);
		if not min then
			throw(Errors.InvalidValue, 'Invalid value for a range : ' .. v, {
				value = v
			});
		end
		if not max then
			return min;
		else
			return {min, max};
		end
	elseif t == ItemOptionType.SWITCH then
		local v = v:lower();
		if v == 'yes' then
			return true;
		elseif v == 'no' then
			return false;
		else
			throw(Errors.InvalidEnumValue, 'Unauthorized value for a switch : ' .. v, {
				value = v,
				expected = '"yes"|"no"'
			});
		end
	end
end

--- Charge les recettes contenues dans les données XML, pour le module `mod`.
---@param xmldata string
---@param mod Module
function GameData.loadRecipes(xmldata, mod)
	local LOG_HEADER = "[Loading module " .. mod:getId() .. " (Recipes)] : ";
	loginf(LOG_HEADER .. "Loading recipes");

	local doc = dom.DOMDocument:createFromXML(xmldata); ---@type DOMDocument

	for recipe in list.stream(doc:getRoot():getElementsByName("Recipe")) do
		if not recipe:hasNonEmptyAttributeWithName("Id") then
			logwrn(LOG_HEADER .. 'Ignored recipe with no ID', {
				Module = mod:getId()
			});
			goto continue;
		end
		local r_id = recipe:getFirstAttributeByName("Id"):getValue();
		local generatedRecipe = Recipe(r_id, mod); ---@type Recipe
		loginf(LOG_HEADER .. "Loading recipe " .. r_id);

		-- Les machines
		if recipe:hasElementWithName("Machines") then
			for m in list.stream(recipe:getFirstElementByName('Machines'):getElementsByName("Machine")) do
				if not m:hasNonEmptyAttributeWithName("Type") then
					logwrn(LOG_HEADER .. 'Ignored machine with no ID', {
						Module = mod:getId(),
						Recipe = r_id
					});
					goto continue;
				end
				local m_type = string.lower(m:getFirstAttributeByName("Type"):getValue());
				local m_i = m:getShallowTextContent();
				if m_i == "" then
					logwrn(LOG_HEADER .. 'Ignored machine with no ID', {
						Module = mod:getId(),
						Recipe = r_id
					});
				else
					if m_type == "machine" then
						local mQualifier = ids.extractId(m_i).qualifier;
						if mQualifier == ids.Qualifier.MACHINE_TYPE then
							if MachinesManager.hasMachineType(m_type) then
								generatedRecipe:setAvailableFor(MachinesManager.getMachineType(m_i));
							else
								logwrn(string.format('%sIgnored machine pointing to the unknown machine type %s', LOG_HEADER, m_i), {
									Module = mod:getId(),
									Recipe = r_id
								});
							end
						elseif mQualifier == ids.Qualifier.MACHINE_MODEL then
							if MachinesManager.hasMachineModel(m_type) then
								generatedRecipe:setAvailableFor(MachinesManager.getMachineModel(m_i));
							else
								logwrn(string.format('%sIgnored machine pointing to the unknown machine model %s', LOG_HEADER, m_i), {
									Module = mod:getId(),
									Recipe = r_id
								});
							end
						else
							logwrn(string.format('%sIgnored machine because of bad types match : expecting a machine type or a machine model, but got %s', LOG_HEADER, m_i), {
								Module = mod:getId(),
								Recipe = r_id
							});
						end
					elseif m_type == "tag" then
						local mQualifier, mSpecifier;
						do
							local tmp = ids.extractId(m_i);
							mQualifier = tmp.qualifier;
							mSpecifier = tmp.specifier;
						end;
						if mQualifier == ids.Qualifier.TAG then
							if mSpecifier == ids.Specifier.Tag.MACHINES then
								if TagsManager.hasTag(m_i) then
									generatedRecipe:setAvailableFor(TagsManager.getTag(m_i));
								else
									logwrn(string.format('%sIgnored machine pointing to the unknown tag %s', LOG_HEADER, m_i), {
										Module = mod:getId(),
										Recipe = r_id
									});
								end
							else
								logwrn(string.format('%sIgnored machine pointing to the invalid tag %s', LOG_HEADER, m_i), {
									Module = mod:getId(),
									Recipe = r_id
								});
							end
						else
							logwrn(string.format(
								'%sIgnored machine because of bad types match : expecting a machine tag, but got %s',
								LOG_HEADER,
								m_i
							), {
								Module = mod:getId(),
								Recipe = r_id
							});
						end
					else
						logwrn(string.format('%sIgnored machine with unknown type %s', LOG_HEADER, m_type), {
							Recipe = r_id,
							Module = mod:getId()
						});
					end
				end
				::continue::
			end
		end

		-- Les inputs
		if recipe:hasElementWithName("Inputs") then
			for input in list.stream(recipe:getFirstElementByName('Inputs'):getElementsByName("Input")) do
				if not input:hasNonEmptyAttributeWithName('Id') then
					logwrn(LOG_HEADER .. 'Ignored input with no Id attribute', {
						Module = mod:getId(),
						Recipe = r_id
					});
					goto continue;
				end
				if not input:hasElementWithName("Id") then
					logwrn(LOG_HEADER .. 'Ignored input with no ID', {
						Module = mod:getId(),
						Recipe = r_id
					});
					goto continue;
				end
				local inputId = input:getFirstAttributeByName('Id'):getValue();
				local inputItems = {}; ---@type ItemData[]
				for itm in list.stream(input:getElementsByName("Id")) do
					if not itm:hasNonEmptyAttributeWithName("Type") then
						logwrn(LOG_HEADER .. 'Ignored input ID with no type', {
							Module = mod:getId(),
							Recipe = r_id
						});
						goto continue;
					end
					local itmType = itm:getFirstAttributeByName("Type"):getValue():lower();
					local itmValue = itm:getShallowTextContent();
					if itmValue == "" then
						logwrn(LOG_HEADER .. 'Ignored input ID without content', {
							Module = mod:getId(),
							Recipe = r_id
						});
						goto continue;
					elseif not ids.isValid(itmValue) then
						logwrn(string.format('%sIgnored input ID with invalid content %s', LOG_HEADER, itmValue), {
							Module = mod:getId(),
							Recipe = r_id
						});
						goto continue;
					end
					itmValue = ids.ensureIdModule(itmValue);
					if itmType == "item" then
						if ItemsManager.hasItem(itmValue) then
							inputItems[#inputItems+1] = ItemsManager.getItem(itmValue);
						else
							logwrn(string.format('%sIgnored input ID pointing to the unknown item %s', LOG_HEADER, itmValue), {
								Recipe = r_id,
								Module = mod:getId()
							});
						end
					elseif itmType == "tag" then
						if ids.extractId(itmValue).specifier == ids.Specifier.Tag.ITEMS then
							if TagsManager.hasTag(itmValue) then
								for x in TagsManager.getTag(itmValue):stream() do
									inputItems[#inputItems+1] = ItemsManager.getItem(x);
								end
							else
								logwrn(string.format('%sIgnored input ID pointing to the unknown tag %s', LOG_HEADER, itmValue), {
									Module = mod:getId(),
									Recipe = r_id
								});
							end
						else
							logwrn(string.format(
								'%sIgnored input ID because of bad match : expected an item tag, but got %s',
								LOG_HEADER,
								itmValue
							), {
								Recipe = r_id,
								Module = mod:getId()
							});
						end
					else
						logwrn(string.format('%sIgnored input ID with unknown type %s', LOG_HEADER, itmType), {
							Module = mod:getId(),
							Recipe = r_id
						});
					end
					::continue::
				end

				local inputAmount = 1;
				if input:hasElementWithName("Amount") then
					local x = input:getFirstElementByName("Amount"):getShallowTextContent();
					if x ~= "" then
						inputAmount = math.abs(tonumber(x) or inputAmount);
					else
						logwrn(LOG_HEADER .. 'Ignored input amount without value', {
							Recipe = r_id,
							Module = mod:getId()
						});
					end
				end

				local inputPersistent = input:hasElementWithName("Persistent");

				local recipeInput = RecipeInput(inputId, inputItems, inputAmount, inputPersistent); ---@type RecipeInput

				-- Les options d'ingrédients
				if input:hasElementWithName('Options') then
					for option in list.stream(input:getFirstElementByName('Options'):getElementsByName('Option')) do
						if not option:hasNonEmptyAttributeWithName('Id') then
							logwrn(LOG_HEADER .. 'Ignored input option with no ID', {
								Module = mod:getId(),
								Recipe = r_id
							});
							goto continue;
						end
						local optionId = option:getFirstAttributeByName('Id'):getValue();

						loginf(LOG_HEADER .. 'Reading recipe input option ' .. optionId);

						if not option:hasNonEmptyAttributeWithName('Type') then
							logwrn(LOG_HEADER .. 'Ignored input option with no ID', {
								Module = mod:getId(),
								Recipe = r_id
							});
							goto continue;
						end
						local success, ret = pcall(ItemOptionType.fromString, option:getFirstAttributeByName('Type'):getValue());
						if not success then
							logwrn(
								string.format(
									'%sIgnored input option %s with unknown type %s',
									LOG_HEADER,
									optionId,
									option:getFirstAttributeByName('Type'):getValue()
								), {
									Module = mod:getId(),
									Recipe = r_id
								}
							);
							goto continue;
						end
						local optionType = ret;

						local optionValues;
						if option:hasNonEmptyAttributeWithName('Value') then
							local s = option:getFirstAttributeByName('Value'):getValue();
							local success, ret = pcall(GameData._parseRecipeInputOptionValue, s, optionType);
							if not success then
								logwrn(string.format('%sIgnored input option %s. Values parsing failed.', LOG_HEADER, optionId), {
									Recipe = r_id,
									Module = mod:getId(),
									Error = ret.message
								});
								goto continue;
							end
							optionValues = ret;
						else
							if option:hasElementWithName('Value') then
								optionValues = {};
								for value in list.stream(option:getElementsByName('Value')) do
									local success, ret = pcall(GameData._parseRecipeInputOptionValue, value:getShallowTextContent(), optionType);
									if not success then
										logwrn(string.format('%sIgnored input option %s. Values parsing failed.', LOG_HEADER, optionId), {
											Recipe = r_id,
											Module = mod:getId(),
											Error = ret.message
										});
										goto continue;
									end
									optionValues[#optionValues+1] = ret;
									::continue::
								end
							else
								logwrn(string.format('%sIgnored input option %s with no value', LOG_HEADER, optionId), {
									Recipe = r_id,
									Module = mod:getId()
								});
								goto continue;
							end
						end

						recipeInput:setOption(optionId, optionType, optionValues);
						::continue::
					end
				end

				generatedRecipe:addInput(recipeInput);
				::continue::
			end
		end

		-- Le temps d'usinage
		if recipe:hasElementWithName("ProcessingTime") then
			local processingTime = recipe:getFirstElementByName("ProcessingTime"):getShallowTextContent();
			if processingTime ~= "" then
				processingTime = tonumber(processingTime);
				if not processingTime then
					logwrn(string.format('%sIgnored recipe %s with invalid processing time', LOG_HEADER, r_id), {Module = mod:getId()});
					goto continue;
				end
				generatedRecipe:setProcessingTime(math.abs(processingTime));
			else
				logwrn(string.format('%sIgnored recipe %s with no processing time', LOG_HEADER, r_id), {Module = mod:getId()});
				goto continue;
			end
		else
			logwrn(string.format('%sIgnored recipe %s with not processing time', LOG_HEADER, r_id), {Module = mod:getId()});
			goto continue;
		end

		-- Les outputs
		if recipe:hasElementWithName("Outputs") then
			if not recipe:getFirstElementByName("Outputs"):hasElementWithName("Output") then
				logwrn(string.format('%sIgnored recipe %s with no output', LOG_HEADER, r_id), {Module = mod:getId()});
				goto continue;
			end
			for output in list.stream(recipe:getFirstElementByName("Outputs"):getElementsByName("Output")) do
				if not output:hasElementWithName("Id") then
					logwrn(LOG_HEADER .. 'Ignored recipe output with no ID', {
						Module = mod:getId(),
						Recipe = r_id
					});
					goto continue;
				end

				local outputId = output:getFirstElementByName("Id"):getShallowTextContent();
				if outputId == "" then
					logwrn(LOG_HEADER .. 'Ignored recipe output with no ID', {
						Recipe = r_id,
						Module = mod:getId()
					});
					goto continue;
				end
				outputId = ids.ensureIdModule(outputId);

				local outputAmounts = {}; ---@type RecipeOutputAmount[]
				if output:hasElementWithName("Amounts") and output:getFirstElementByName("Amounts"):hasElementWithName("Amount") then
					for outputAmount in list.stream(output:getFirstElementByName("Amounts"):getElementsByName("Amount")) do
						local outputAmountValueLow, outputAmountValueHigh, outputAmountPercentage;
						if outputAmount:hasNonEmptyAttributeWithName("Value") then
							local values = string.split(outputAmount:getFirstAttributeByName('Value'):getValue(), ';');
							outputAmountValueLow = math.abs(tonumber(values[1]));
							if #values == 1 then
								outputAmountValueHigh = outputAmountValueLow;
							else
								outputAmountValueHigh = math.abs(tonumber(values[2]));
							end

							if not outputAmountValueLow then
								logwrn(string.format('%sIgnored recipe output amount with invalid low value', LOG_HEADER), {
									Module = mod:getId(),
									Recipe = r_id,
									Output = outputId,
									LowValue = outputAmountValueLow
								});
								goto continue;
							end
						end
						if outputAmount:hasNonEmptyAttributeWithName("Percentage") then
							outputAmountPercentage = math.abs(tonumber(outputAmount:getFirstAttributeByName("Percentage"):getValue()) or 2);
						end
						outputAmounts[#outputAmounts+1] = RecipeOutputAmount(outputAmountValueLow, outputAmountValueHigh, ternary(outputAmountPercentage == 2, -1, outputAmountPercentage));
						::continue::
					end
				else
					outputAmounts[#outputAmounts+1] = RecipeOutputAmount(1.0);
				end

				if not ItemsManager.hasItem(outputId) then
					logwrn(string.format('%sIgnored recipe output with unknown item %s', LOG_HEADER, outputId), {
						Module = mod:getId(),
						Recipe = r_id
					});
					goto continue;
				end

				local recipeOutput = RecipeOutput(ItemsManager.getItem(outputId), outputAmounts); ---@type RecipeOutput

				-- Options de sortie
				if output:hasElementWithName("Options") then
					local item = recipeOutput:getItem();
					for option in list.stream(output:getFirstElementByName("Options"):getElementsByName("Option")) do
						if not option:hasNonEmptyAttributeWithName("Id") then
							logwrn(LOG_HEADER .. 'Ignored recipe output option with no ID', {
								Module = mod:getId(),
								Recipe = r_id,
								Output = outputId
							});
							goto continue;
						end
						local optionId = option:getFirstAttributeByName("Id"):getValue();
						loginf(LOG_HEADER .. "Parsing option " .. optionId .. " in output " .. outputId .. " of recipe " .. r_id);
						if not option:hasNonEmptyAttributeWithName("Value") then
							logwrn(string.format('%sIgnored recipe output option %s with no value', LOG_HEADER, optionId), {
								Module = mod:getId(),
								Recipe = r_id,
								Output = outputId
							});
							goto continue;
						end
						local optionValue = option:getFirstAttributeByName("Value"):getValue();
						if not item:hasOption(optionId) then
							logwrn(string.format('%sIgnored recipe output option pointing to the unknown item %s', LOG_HEADER, optionId), {
								Module = mod:getId(),
								Recipe = r_id,
								Output = outputId
							});
							goto continue;
						end
						if not item:getOption(optionId):accepts(optionValue) then
							logwrn(string.format('%sIgnored recipe output option %s with unaccepted item', LOG_HEADER, optionId), {
								Module = mod:getId(),
								Recipe = r_id,
								Output = outputId
							});
							goto continue;
						end

						recipeOutput:setOption(optionId, optionValue);
						::continue::
					end
				end

				generatedRecipe:addOutput(recipeOutput);
				::continue::
			end
		else
			logwrn(string.format('%sIgnored recipe %s with no output', LOG_HEADER, r_id), {Module = mod:getId()});
		end

		RecipesManager.registerRecipe(generatedRecipe);
		::continue::
	end
end

--- Charge les polices de caractères d'un module.
---@param moduleFolder string Le chemin d'accès au dossier-racine du module
---@param mod Module Le module
function GameData.loadFonts(moduleFolder, mod)
	local fontsFolder = fs.path(moduleFolder, 'resources/fonts');
	local LOG_HEADER = '[Loading module ' .. mod:getId() .. ' (Fonts)] : ';
	loginf(LOG_HEADER .. 'Loading fonts');

	do
		local tmp = love.filesystem.getInfo(fontsFolder);
		if tmp == nil or tmp.type ~= "directory" then
			loginf(LOG_HEADER .. "No font found.");
		end
	end;

	local availablesFontsFiles = {};
	for fontfile in list.stream(love.filesystem.getDirectoryItems(fontsFolder)) do
		availablesFontsFiles[fs.getFileCorename(fontfile)] = fontfile;
	end

	for expectedFont, expectedFontInfos in pairs(ResourcesManager.fontsInfos) do
		if map.contains(availablesFontsFiles, expectedFontInfos[1]) then
			loginf(string.format('%sLoading font %s from file %s', LOG_HEADER, expectedFont, availablesFontsFiles[expectedFontInfos[1]]));
			local font = love.graphics.newFont(
				fs.path(fontsFolder, availablesFontsFiles[expectedFontInfos[1]]),
				expectedFontInfos[2],
				expectedFontInfos[3]);
			ResourcesManager.fonts[expectedFont] = font;
		end
	end
end

--- Charge un module.
---
--- _@throws_ `Errors.FolderNotFound` - `folderName` n'existe pas ou n'est pas
---           un dossier
---@param folderName string Le nom du dossier du module
function GameData.loadModule(folderName)
	local folderInfos = love.filesystem.getInfo("modules/" .. folderName);
	if folderInfos == nil or folderInfos.type ~= "directory" then
		throw(Errors.FolderNotFound, string.format('The folder %s does not exists, or is not a folder', folderName), {
			folder = folderName
		});
	end

	local moduleFolder = "modules/" .. folderName .. "/";

	-- Chargement de module.xml
	local file = moduleFolder .. "module.xml";
	local exists = love.filesystem.getInfo(file);
	if exists == nil or exists.type ~= "file" then
		throw(Errors.FileNotFound, string.format('The %s file cannot be found', file), {
			filename = file
		});
	end
	local currentModule = GameData.loadModuleFile(love.filesystem.read(file), moduleFolder); ---@type Module

	-- Chargement des items (items.xml)
	file = moduleFolder .. 'items.xml';
	exists = love.filesystem.getInfo(file);
	if exists ~= nil and exists.type == "file" then
		GameData.loadItems(love.filesystem.read(file), currentModule);
	end

	-- Chargement des tags d'items (tags.xml)
	file = moduleFolder .. 'tags.xml';
	exists = love.filesystem.getInfo(file);
	local tagsContent = "";
	if exists ~= nil and exists.type == "file" then
		tagsContent = love.filesystem.read(file);
		GameData.loadItemsTags(tagsContent, currentModule);
	end

	-- Chargement des types de machines (machinesTypes.xml)
	file = moduleFolder .. 'machinesTypes.xml';
	exists = love.filesystem.getInfo(file);
	if exists ~= nil and exists.type == "file" then
		GameData.loadMachinesTypes(love.filesystem.read(file), currentModule);
	end

	-- Chargement des modèles de machines (machines.xml)
	file = moduleFolder .. 'machines.xml';
	exists = love.filesystem.getInfo(file);
	if exists ~= nil and exists.type == "file" then
		GameData.loadMachinesModels(love.filesystem.read(file), currentModule);
	end

	-- Chargement des tags de machines (tags.xml)
	if tagsContent ~= "" then
		GameData.loadMachinesTags(tagsContent, currentModule);
	end

	-- Chargement des recettes (recipes.xml)
	file = moduleFolder .. 'recipes.xml';
	exists = love.filesystem.getInfo(file);
	if exists ~= nil and exists.type == "file" then
		GameData.loadRecipes(love.filesystem.read(file), currentModule);
	end

	-- Chargement des langues
	GameData.loadLocales(moduleFolder, currentModule);

	-- Chargement des polices
	GameData.loadFonts(moduleFolder, currentModule);

	GameData.registerModule(currentModule:getId(), currentModule);
end

--- Fonction interne qui affiche les statistiques sur le contenu chargé.
function GameData._logRecap()
	local text = "[Loading Modules] : Loaded " .. tostring(map.size(GameData._modules)) .. " module(s) :\n";
	for mid, m in pairs(GameData._modules) do
		text = text .. " - " .. m:getName() .. " " .. m:getVersion() .. " (" .. mid .. ")\n";
		text = text .. "\tItems : " .. tostring(ItemsManager.countItems(m:getId())) .. "\n";
		text = text .. "\tMachines Types : " .. tostring(MachinesManager.countMachinesType(m:getId())) .. "\n";
		text = text .. "\tMachines Models : " .. tostring(MachinesManager.countMachinesModels(m:getId())) .. "\n";
		text = text .. "\tTags : " .. tostring(TagsManager.countTags(m:getId())) .. "\n";
		text = text .. "\tRecipes : " .. tostring(RecipesManager.countRecipes(m:getId())) .. "\n";
	end
	loginf(text);
end

--- Charge les modules présents dans le dossier adéquat.
---
--- _@throws_ `Errors.LoadingFailed` - Le chargement d'un module a échoué
---@param printRecap? boolean Doit-on afficher le compte-rendu du chargement des modules ? `true` par défaut
function GameData.loadModules(printRecap)
	if printRecap == nil then
		printRecap = true;
	end
	local LOG_HEADER = "[Loading Modules] : ";
	loginf(LOG_HEADER .. "Loading modules");

	-- Chargement du module de base
	local success, ret = pcall(GameData.loadModule, GameData.BASE_MODULE_NAME);
	if not success then
		---@cast ret ErrorObject
		throw(Errors.LoadingFailed, 'Impossible de charger le module. Voir output.log pour plus d\'informations', {
			['module folder'] = GameData.BASE_MODULE_NAME
		}, ret);
	end

	-- Chargement des autres modules
	local files = love.filesystem.getDirectoryItems('modules');
	table.sort(files);
	for filename in list.fstream(files, function(f)
		local finfo = love.filesystem.getInfo('modules/' .. f);
		return f ~= GameData.BASE_MODULE_NAME and finfo.type == "directory";
	end) do
		local hasModuleDotXmlFile = false;
		local infos = love.filesystem.getInfo('modules/' .. filename .. '/module.xml');
		if infos ~= nil and infos.type == "file" then
			hasModuleDotXmlFile = true;
		end

		if hasModuleDotXmlFile then
			local success, ret = pcall(GameData.loadModule, filename);
			if not success then
				---@cast ret ErrorObject
				throw(Errors.LoadingFailed, 'Impossible de charger le module. Voir output.log pour plus d\'informations', {
					['module folder'] = filename
				}, ret);
			end
		else
			loginf(LOG_HEADER .. 'Ignored non-module folder ' .. filename);
		end
	end

	if printRecap then
		GameData._logRecap();
	end
end


return GameData;