require('calypso.class')
local scenes = require('calypso.scenes')
local ui     = require('calypso.old_ui')


-- *****************************************************************************
-- * WarehousesScene
-- *
-- *  class WarehousesScene extends Scene
-- *****************************************************************************

---@class WarehousesScene:Scene
local WarehousesScene = class('WarehousesScene', scenes.Scene)

function WarehousesScene:initialize()
	WarehousesScene.super.initialize(self)

	self._win    = ui.Window()    ---@type Window
	self._footer = ui.FooterBar() ---@type FooterBar
	self._table  = ui.Table()     ---@type Table
	self._tableColumns = { ---@type TableColumn[]
		ui.TableColumn():setName("name"):setDisplayedName("Nom de l'Entrepôt"):setSize(50, 0),
		ui.TableColumn():setName("capacity"):setDisplayedName("Capacité"):setSize(50, 0)
	}

	self._tableContent = {
		{"Le nom de l'entrepôt", "251 / 318"} -- nom, niveau de remplissage
	}

	self._win:getTitlebar():setText("Entrepôts")
	self._win:getTitlebar():onCloseButtonReleased(function (btn, istouch)
		scenes.ScenesManager.setCurrentScene('central_board')
	end)


	self._win:setContent(ui.ComponentsGroup())

	self._table:setPosition(0, 0)
	self._table:addColumn(self._tableColumns[1], 1)
	self._table:addColumn(self._tableColumns[2], 2)
	self._table:setContent(self._tableContent, ui.TableReadingMethod.VALUE)
	self._win:getContent():registerComponent(self._table)

	self._footer:setCentered(false)
	self._footer:setText("[F1] Retour")
	self._win:getContent():registerComponent(self._footer)

	self:registerUIComponent(self._win)
end

function WarehousesScene:draw()
	-- pour s'assurer que la taille et la position du footer soient bien correctes.
	-- Sinon il se trace en prenant comme référence le Rect de son groupe au
	-- moment de l'initialisation, quand il valait 0,0,0,0
	local footerLocation = self._footer:getLocation() ---@type Rect
	local groupLocation = self._win:getContent():getLocation() ---@type Rect
	if footerLocation.width ~= groupLocation.width then
		self._footer:setSize(groupLocation.width, footerLocation.height)
	end
	if footerLocation.y ~= groupLocation.height - footerLocation.height then
		self._footer:setPosition(footerLocation.x, groupLocation.height - footerLocation.height)
	end

	
	local tableLocation = self._table:getLocation() ---@type Rect
	if tableLocation.width ~= groupLocation.width then
		self._table:setSize(groupLocation.width, groupLocation.height - footerLocation.height)
	end
end

function WarehousesScene:onKeyReleased(key)
	if key == 'f1' then
		scenes.ScenesManager.setCurrentScene('central_board')
	end
end

return WarehousesScene