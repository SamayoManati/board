require('calypso.class')
local scenes = require('calypso.scenes')
local ui     = require('calypso.old_ui')
local colors = require('calypso.colors')
require('foundation.log')


-- *****************************************************************************
-- * CentralViewScene
-- *
-- *  class CentralViewScene extends Scene
-- *****************************************************************************

---@class CentralViewScene:Scene
local CentralViewScene = class('CentralViewScene', scenes.Scene)

function CentralViewScene:initialize()
	scenes.Scene.initialize(self)
	self._win = ui.Window() ---@type Window
	self._texts = {
		{colors.yellow, "[F1]", colors.white, " Quitter"},
		{colors.yellow, "[F2]", colors.white, " Voir les Lignes de Production"},
		{colors.yellow, "[F3]", colors.white, " Voir les Entrepôts"}
	}

	self._leftPane   = ui.VerticalComponentsStack() ---@type VerticalComponentsStack
	self._centerPane = ui.VerticalComponentsStack() ---@type VerticalComponentsStack
	self._rightPane  = ui.VerticalComponentsStack() ---@type VerticalComponentsStack

	self._btnViewProductionLines = ui.Button() ---@type Button
	self._btnViewWarehouses      = ui.Button() ---@type Button
	self._btnGameContent         = ui.Button() ---@type Button

	self._win:setContent(ui.HorizontalComponentsStack())

	self._win:getTitlebar():setText("Central Board")
	self._win:getTitlebar():onCloseButtonReleased(function (btn, istouch)
		if btn == 1 then
			love.event.quit(0)
		end
	end)

	self._btnViewProductionLines:setText("[F2] Voir les Lignes de Production")
	self._btnViewProductionLines:setPosition(5, 5)
	self._btnViewProductionLines:setSize(300, 50)
	self._btnViewProductionLines:setStyleTarget("central_board.main_button")
	self._btnViewProductionLines:onMouseRelease(function (x, y, button, istouch)
		if button == 1 then
			scenes.ScenesManager.setCurrentScene('production_lines_view')
		end
	end)
	self._btnViewWarehouses:setText("[F3] Voir les Entrepôts")
	self._btnViewWarehouses:setPosition(5, 60)
	self._btnViewWarehouses:setSize(300, 50)
	self._btnViewWarehouses:setStyleTarget("central_board.main_button")
	self._btnViewWarehouses:onMouseRelease(function (x, y, button, istouch)
		if button == 1 then
			scenes.ScenesManager.setCurrentScene('warehouses_view')
		end
	end)
	self._btnGameContent:setText("Contenu du Jeu")
	self._btnGameContent:setSize(300, 50)
	self._btnGameContent:setStyleTarget("central_board.main_button")
	self._btnGameContent:onMouseRelease(function (x, y, button, istouch)
		if button == 1 then
			scenes.ScenesManager.setCurrentScene('all_game_content')
		end
	end)
	self._leftPane:registerComponent(self._btnViewProductionLines)
	self._leftPane:registerComponent(self._btnViewWarehouses)
	self._leftPane:registerComponent(self._btnGameContent)
	self._leftPane:setMaxSize(50)

	self._win:getContent():registerComponent(self._leftPane)
	self._win:getContent():registerComponent(self._centerPane)
	self._win:getContent():registerComponent(self._rightPane)
	
	self:registerUIComponent(self._win)
end

function CentralViewScene:draw()
	CentralViewScene.super.draw(self)
	local ww, wh = love.graphics.getDimensions()
	local fontHeight = love.graphics.getFont():getHeight()
	local tierw = math.floor(ww / 3)
	local y = math.floor((wh - (#self._texts * fontHeight)) / 2)
	for _, line in ipairs(self._texts) do
		love.graphics.printf(line, tierw, y, tierw, "center")
		y = y + fontHeight
	end
end

function CentralViewScene:onKeyReleased(key)
	if key == 'f1' then
		love.event.quit(0)
	elseif key == 'f2' then
		scenes.ScenesManager.setCurrentScene('production_lines_view')
	elseif key == 'f3' then
		scenes.ScenesManager.setCurrentScene('warehouses_view')
	end
end


return CentralViewScene