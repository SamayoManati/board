require('calypso.core');
require('calypso.class');
require('foundation.log');
local scenes              = require('calypso.scenes');
local ui                  = require('calypso.old_ui');
local themes              = require('calypso.themes');
local ItemsManager        = require('global.items.ItemsManager');
local ItemType            = require('global.items.ItemType');
local LocalizationManager = require('global.locales.LocalizationManager');
local GameData            = require('gamedata');
local ids                 = require('foundation.ids');
local TagsManager         = require('global.tags.TagsManager');
local Tag                 = require('global.tags.Tag');
local MachinesManager     = require('global.machines.MachinesManager');
local MachineType         = require('global.machines.MachineType');
local RecipesManager      = require('global.recipes.RecipesManager');
local RecipeAttributes    = require('global.recipes.RecipeAttributes');


local function provide_items()
	local t = {}

	for id, item in pairs(ItemsManager._items) do
		t[#t+1] = {
			item:getModule():getName(),
			id,
			ItemType.toString(item:getType()),
			item:getSafeLocalizedName()
		}
	end

	return t
end

local function provide_tags()
	local t = {}

	for id, tag in pairs(TagsManager._tags) do
		t[#t+1] = {
			tag:getModule():getName(),
			id,
			tag:getSafeLocalizedName(),
			tag:getType(),
			tostring(#tag:getContent()) .. " élément(s)",
			ternary(tag:isUnseen(), "Oui", "Non")
		}
	end

	return t
end

local function provide_machines_types()
	local t = {}

	for id, mtype in pairs(MachinesManager._machinesTypes) do
		t[#t+1] = {
			mtype:getModule():getName(),
			id,
			mtype:getSafeLocalizedName(),
			tostring(mtype:getOperators()),
			tostring(map.size(mtype._tools))
		}
	end

	return t
end

local function provide_machines_models()
	local t = {}

	for id, mmodel in pairs(MachinesManager._machinesModels) do
		t[#t+1] = {
			mmodel:getModule():getName(),
			id,
			mmodel:getSafeLocalizedName(),
			tostring(mmodel:getPrice()),
			mmodel:getMachineType():getSafeLocalizedName()
		}
	end

	return t
end

local function provide_locales()
	local t = {}

	for code, l in pairs(LocalizationManager._locales) do
		t[#t+1] = {
			code,
			l:getName(),
			ternary(list.contains(LocalizationManager._activeLocales, code), "Oui", "Non"),
			tostring(map.size(l._keys)) .. " clef(s)"
		}
	end

	return t
end

local function provide_modules()
	local t = {}

	for id, m in pairs(GameData._modules) do
		t[#t+1] = {
			id,
			m:getName(),
			m:getVersion(),
			ternary(#m:getAuthors() > 1, m:getAuthors()[1] .. " et " .. tostring(#m:getAuthors() - 1) .. " autre(s)", m:getAuthors()[1]),
			tostring(ItemsManager.countItems(m:getId())),
			tostring(TagsManager.countTags(m:getId())),
			tostring(MachinesManager.countMachinesType(m:getId()))
		}
	end

	return t
end

local function provide_recipes()
	local t = {}

	for id, r in pairs(RecipesManager.getRecipes()) do
		t[#t+1] = {
			r:getModule():getName(),
			id,
			r:getSafeLocalizedAttribute(RecipeAttributes.NAME),
			tostring(#r:getAvailableFor()),
			tostring(#r:getInputs()),
			tostring(#r:getOutputs()),
			tostring(r:getProcessingTime())
		}
	end

	return t
end


-- *****************************************************************************
-- * CustomTableColumn
-- *
-- *  class CustomTableColumn extends TableColumn
-- *****************************************************************************

--- 	CustomTableColumn(name: string, property: Scene)
--- ---
---@class CustomTableColumn:TableColumn
local CustomTableColumn = class('CustomTableColumn', ui.TableColumn)

function CustomTableColumn:initialize(name, property)
	CustomTableColumn.super.initialize(self)
	self._scene = property
	self._content = {} ---@type love.Text[]
	self:setDisplayedName(name)
end

function CustomTableColumn:drawHeaderCell(x, y, width, height)
	love.graphics.setColor(themes.ThemesManager.supplyBackgroundColor(self:getStyleTarget() .. ".header"))
	love.graphics.rectangle("fill", x, y, width, height)

	love.graphics.setColor(themes.ThemesManager.supplyBorderColor(self:getStyleTarget() .. ".header"))
	love.graphics.line(x, y, x, y + height)
	love.graphics.line(x + width, y, x + width, y + height)

	if self._drawableColumnName == nil then
		self._drawableColumnName = love.graphics.newText(love.graphics.getFont(), self:getDisplayedName())
	end

	love.graphics.setColor(themes.ThemesManager.supplyForegroundColor(self:getStyleTarget() .. ".header"))
	love.graphics.draw(self._drawableColumnName, ui.centered(self._drawableColumnName:getWidth(), width, x), ui.centered(self._drawableColumnName:getHeight(), height, y))
end

function CustomTableColumn:drawCell(x, y, width, height, content, lineIndex)
	love.graphics.setColor(themes.ThemesManager.supplySafeBackgroundColor(self:getStyleTarget(), ternary(self._scene._currentlySelected == lineIndex, themes.TargetState.SELECTED, themes.TargetState.DEFAULT)))
	love.graphics.rectangle("fill", x, y, width, height)

	love.graphics.setColor(themes.ThemesManager.supplySafeBorderColor(self:getStyleTarget(), ternary(self._scene._currentlySelected == lineIndex, themes.TargetState.SELECTED, themes.TargetState.DEFAULT)))
	love.graphics.line(x, y, x, y + height)
	love.graphics.line(x + width, y, x + width, y + height)

	if self._content[lineIndex] == nil then
		self._content[lineIndex] = love.graphics.newText(love.graphics.getFont(), " " .. tostring(content))
	end

	love.graphics.setColor(themes.ThemesManager.supplySafeForegroundColor(self:getStyleTarget(), ternary(self._scene._currentlySelected == lineIndex, themes.TargetState.SELECTED, themes.TargetState.DEFAULT)))
	love.graphics.draw(self._content[lineIndex], x, ui.centered(self._content[lineIndex]:getHeight(), height, y))
end


-- *****************************************************************************
-- * AllGameContentScene
-- *
-- *  class AllGameContentScene extends Scene
-- *****************************************************************************

---@class AllGameContentScene:Scene
local AllGameContentScene = class('AllGameContentScene', scenes.Scene)

function AllGameContentScene:initialize()
	AllGameContentScene.super.initialize(self)

	self._currentlyShown = 0
	self._currentlySelected = 1
	self._view = 'main'

	self._win               = ui.Window() ---@type Window
	self._topPane           = ui.HorizontalComponentsStack() ---@type HorizontalComponentsStack
	self._bottomPane        = ui.HorizontalComponentsStack() ---@type HorizontalComponentsStack
	self._btnItems          = ui.Button() ---@type Button
	self._btnTags           = ui.Button() ---@type Button
	self._btnMachinesTypes  = ui.Button() ---@type Button
	self._btnMachinesModels = ui.Button() ---@type Button
	self._btnLocales        = ui.Button() ---@type Button
	self._btnModules        = ui.Button() ---@type Button
	self._btnRecipes        = ui.Button() ---@type Button
	self._contentTable      = ui.Table()  ---@type Table

	self._winTagContent   = ui.Window() ---@type Window
	self._tagContentTable = ui.Table()  ---@type Table

	self._winLocaleContent   = ui.Window() ---@type Window
	self._localeContentTable = ui.Table()  ---@type Table

	self._winMachineTypeContent   = ui.Window() ---@type Window
	self._machineTypeContentTable = ui.Table()  ---@type Table

	self._winRecipeContent    = ui.Window() ---@type Window
	self._recipeMachinesTable = ui.Table()  ---@type Table
	self._recipeInputsTable   = ui.Table()  ---@type Table
	self._recipeOutputsTable  = ui.Table()  ---@type Table

	self._win:getTitlebar():setText("Contenu du Jeu")
	self._win:getTitlebar():onCloseButtonReleased(function (btn, istouch)
		if btn == 1 then
			scenes.ScenesManager.setCurrentScene('central_board')
		end
	end)
	self._win:setContent(ui.VerticalComponentsStack())

	self._btnItems:setText("[F1] Items")
	self._btnItems:onMouseRelease(function(x, y, btn)
		if btn == 1 then
			self:_displayItems()
		end
	end)
	self._btnTags:setText("[F2] Tags")
	self._btnTags:onMouseRelease(function (x, y, btn)
		if btn == 1 then
			self:_displayTags()
		end
	end)
	self._btnMachinesTypes:setText("[F3] Types de Machines")
	self._btnMachinesTypes:onMouseRelease(function (x, y, btn)
		if btn == 1 then
			self:_displayMachinesTypes()
		end
	end)
	self._btnMachinesModels:setText("[F4] Modèles de Machines")
	self._btnMachinesModels:onMouseRelease(function (_, _, btn)
		if btn == 1 then
			self:_displayMachinesModels()
		end
	end)
	self._btnLocales:setText("[F5] Langues")
	self._btnLocales:onMouseRelease(function (x, y, btn)
		if btn == 1 then
			self:_displayLocales()
		end
	end)
	self._btnModules:setText("[F6] Modules")
	self._btnModules:onMouseRelease(function (x, y, btn)
		if btn == 1 then
			self:_displayModules()
		end
	end)
	self._btnRecipes:setText("[F7] Recettes")
	self._btnRecipes:onMouseRelease(function(x, y, btn)
		if btn == 1 then
			self:_displayRecipes()
		end
	end)

	self._topPane:setInternalMargin(20)
	self._topPane:registerComponent(self._btnItems)
	self._topPane:registerComponent(self._btnTags)
	self._topPane:registerComponent(self._btnMachinesTypes)
	self._topPane:registerComponent(self._btnMachinesModels)
	self._topPane:registerComponent(self._btnLocales)
	self._topPane:registerComponent(self._btnModules)
	self._topPane:registerComponent(self._btnRecipes)

	self._bottomPane:registerComponent(self._contentTable)

	self._win:getContent():registerComponent(self._topPane)
	self._win:getContent():registerComponent(self._bottomPane)
	self._win:getContent():setInternalMargin(20)

	self._winTagContent:getTitlebar():setText("Contenu du Tag")
	self._winTagContent:getTitlebar():onCloseButtonReleased(function (btn, istouch)
		if btn == 1 then
			self:_setView('main')
		end
	end)
	self._winTagContent:setContent(ui.VerticalComponentsStack())

	self._tagContentTable:addColumn(CustomTableColumn("Module", self))
	self._tagContentTable:addColumn(CustomTableColumn("ID", self))
	self._tagContentTable:addColumn(CustomTableColumn("Nom", self))
	self._winTagContent:getContent():registerComponent(self._tagContentTable)

	self._winLocaleContent:getTitlebar():setText("Contenu de la Langue")
	self._winLocaleContent:getTitlebar():onCloseButtonReleased(function (btn)
		if btn == 1 then
			self:_setView('main')
		end
	end)
	self._winLocaleContent:setContent(ui.VerticalComponentsStack())

	self._localeContentTable:addColumn(ui.TableColumn():setDisplayedName("Clef"))
	self._localeContentTable:addColumn(ui.TableColumn():setDisplayedName("Valeur"))
	self._winLocaleContent:getContent():registerComponent(self._localeContentTable)

	self._winMachineTypeContent:getTitlebar():setText("Outils du Type de Machine")
	self._winMachineTypeContent:getTitlebar():onCloseButtonReleased(function (btn)
		if btn == 1 then
			self:_setView('main')
		end
	end)
	self._winMachineTypeContent:setContent(ui.VerticalComponentsStack())

	self._machineTypeContentTable:addColumn(CustomTableColumn("ID", self))
	self._machineTypeContentTable:addColumn(CustomTableColumn("Nom", self))
	self._machineTypeContentTable:addColumn(CustomTableColumn("Accepte", self))
	self._machineTypeContentTable:addColumn(CustomTableColumn("Quantité", self))
	self._winMachineTypeContent:getContent():registerComponent(self._machineTypeContentTable)

	self._winRecipeContent:getTitlebar():setText("Recette")
	self._winRecipeContent:getTitlebar():onCloseButtonReleased(function (btn, istouch)
		if btn == 1 then
			self:_setView('main')
		end
	end)
	self._winRecipeContent:setContent(ui.VerticalComponentsStack())

	self._recipeMachinesTable:addColumn(CustomTableColumn("Type", self))
	self._recipeMachinesTable:addColumn(CustomTableColumn("ID", self))
	self._recipeMachinesTable:addColumn(CustomTableColumn("Nom", self))
	self._recipeInputsTable:addColumn(CustomTableColumn("Nombre d'Entrées", self))
	self._recipeInputsTable:addColumn(CustomTableColumn("Quantité", self))
	self._recipeInputsTable:addColumn(CustomTableColumn("Persistant ?", self))
	self._recipeOutputsTable:addColumn(CustomTableColumn("Module", self))
	self._recipeOutputsTable:addColumn(CustomTableColumn("ID", self))
	self._recipeOutputsTable:addColumn(CustomTableColumn("Nom", self))
	self._recipeOutputsTable:addColumn(CustomTableColumn("Quantités", self))
	self._winRecipeContent:getContent():registerComponent(self._recipeMachinesTable)
	self._winRecipeContent:getContent():registerComponent(self._recipeInputsTable)
	self._winRecipeContent:getContent():registerComponent(self._recipeOutputsTable)

	self:_setView('main')
end

function AllGameContentScene:onKeyReleased(key)
	AllGameContentScene.super.onKeyReleased(self, key)

	if self._view == 'main' then
		if key == 'f1' then
			self:_displayItems()
		elseif key == 'f2' then
			self:_displayTags()
		elseif key == 'f3' then
			self:_displayMachinesTypes()
		elseif key == 'f4' then
			self:_displayMachinesModels()
		elseif key == 'f5' then
			self:_displayLocales()
		elseif key == 'f6' then
			self:_displayModules()
		elseif key == 'f7' then
			self:_displayRecipes()
		elseif key == 'backspace' then
			scenes.ScenesManager.setCurrentScene('central_board')
		elseif key == 'down' then
			self._currentlySelected = ternary(self._currentlySelected + 1 > #self._contentTable:getContent(), 1, self._currentlySelected + 1)
		elseif key == 'up' then
			self._currentlySelected = ternary(self._currentlySelected - 1 == 0, #self._contentTable:getContent(), self._currentlySelected - 1)
		elseif key == 'right' then
			if self._currentlyShown == 0 then
				self:_displayItems()
			elseif self._currentlyShown == 1 then
				self:_displayTags()
			elseif self._currentlyShown == 2 then
				self:_displayMachinesTypes()
			elseif self._currentlyShown == 3 then
				self:_displayMachinesModels()
			elseif self._currentlyShown == 4 then
				self:_displayLocales()
			elseif self._currentlyShown == 5 then
				self:_displayModules()
			elseif self._currentlyShown == 6 then
				self:_displayRecipes()
			else
				self:_displayItems()
			end
		elseif key == 'left' then
			if self._currentlyShown == 7 then
				self:_displayModules()
			elseif self._currentlyShown == 6 then
				self:_displayLocales()
			elseif self._currentlyShown == 5 then
				self:_displayMachinesModels()
			elseif self._currentlyShown == 4 then
				self:_displayMachinesTypes()
			elseif self._currentlyShown == 3 then
				self:_displayTags()
			elseif self._currentlyShown == 2 then
				self:_displayItems()
			else
				self:_displayRecipes()
			end
		elseif key == 'return' then
			if self._currentlyShown == 2 then
				local selectedTag = self._contentTable:getContent()[self._currentlySelected][2]
				self:_populateTagContent(selectedTag)
				self._winTagContent:getTitlebar():setText("Contenu du Tag " .. TagsManager.getTag_nilsafe(selectedTag):getSafeLocalizedName())
				self:_setView('tag')
			elseif self._currentlyShown == 3 then
				local selectedMType = self._contentTable:getContent()[self._currentlySelected][2]
				self:_populateMachineTypeContent(selectedMType)
				self._winMachineTypeContent:getTitlebar():setText("Outils du Type de Machines " .. MachinesManager.getMachineType(selectedMType):getSafeLocalizedName())
				self:_setView('machineType')
			elseif self._currentlyShown == 5 then
				local selectedLocale = self._contentTable:getContent()[self._currentlySelected][1]
				self:_populateLocaleContent(selectedLocale)
				self._winLocaleContent:getTitlebar():setText("Contenu de la Langue " .. LocalizationManager._locales[selectedLocale]:getName())
				self:_setView('locale')
			elseif self._currentlyShown == 7 then
				local selectedRecipe = self._contentTable:getContent()[self._currentlySelected][2]
				self:_populateRecipeContent(selectedRecipe)
				self._winRecipeContent:getTitlebar():setText("Contenu de la Recette " .. RecipesManager.getRecipe_nilsafe(selectedRecipe):getSafeLocalizedAttribute("name"))
				self:_setView('recipe')
			end
		end
	elseif self._view == 'tag' then
		if key == 'backspace' then
			self:_setView('main')
		end
	elseif self._view == 'locale' then
		if key == 'backspace' then
			self:_setView('main')
		end
	elseif self._view == 'machineType' then
		if key == 'backspace' then
			self:_setView('main')
		elseif key == 'down' then
			self._currentlySelected = ternary(self._currentlySelected + 1 > #self._machineTypeContentTable:getContent(), 1, self._currentlySelected + 1)
		elseif key == 'up' then
			self._currentlySelected = ternary(self._currentlySelected - 1 == 0, #self._machineTypeContentTable:getContent(), self._currentlySelected - 1)
		end
	elseif self._view == 'recipe' then
		if key == 'backspace' then
			self:_setView('main')
		end
	end
end

function AllGameContentScene:_updateSectionSelection()
	self._currentlySelected = 1
	self._btnItems:setSelected(self._currentlyShown == 1)
	self._btnTags:setSelected(self._currentlyShown == 2)
	self._btnMachinesTypes:setSelected(self._currentlyShown == 3)
	self._btnMachinesModels:setSelected(self._currentlyShown == 4)
	self._btnLocales:setSelected(self._currentlyShown == 5)
	self._btnModules:setSelected(self._currentlyShown == 6)
	self._btnRecipes:setSelected(self._currentlyShown == 7)
end

function AllGameContentScene:_displayItems()
	self._currentlyShown = 1
	self:_updateSectionSelection()
	self._contentTable._columns = {}
	self._contentTable:addColumn(CustomTableColumn("Module", self))
	self._contentTable:addColumn(CustomTableColumn("ID", self))
	self._contentTable:addColumn(CustomTableColumn("Type", self))
	self._contentTable:addColumn(CustomTableColumn("Nom", self))
	self._contentTable:setContent(provide_items(), ui.TableReadingMethod.VALUE)
end

function AllGameContentScene:_displayTags()
	self._currentlyShown = 2
	self:_updateSectionSelection()
	self._contentTable._columns = {}
	self._contentTable:addColumn(CustomTableColumn("Module", self))
	self._contentTable:addColumn(CustomTableColumn("ID", self))
	self._contentTable:addColumn(CustomTableColumn("Nom", self))
	self._contentTable:addColumn(CustomTableColumn("Type", self))
	self._contentTable:addColumn(CustomTableColumn("Nombre d'Entrée(s)", self))
	self._contentTable:addColumn(CustomTableColumn("Invisible ?", self))
	self._contentTable:setContent(provide_tags(), ui.TableReadingMethod.VALUE)
end

function AllGameContentScene:_displayMachinesTypes()
	self._currentlyShown = 3
	self:_updateSectionSelection()
	self._contentTable._columns = {}
	self._contentTable:addColumn(CustomTableColumn("Module", self))
	self._contentTable:addColumn(CustomTableColumn("ID", self))
	self._contentTable:addColumn(CustomTableColumn("Nom", self))
	self._contentTable:addColumn(CustomTableColumn("Nombre d'Opérateur(s)", self))
	self._contentTable:addColumn(CustomTableColumn("Nombre d'Outil(s)", self))
	self._contentTable:setContent(provide_machines_types(), ui.TableReadingMethod.VALUE)
end

function AllGameContentScene:_displayMachinesModels()
	self._currentlyShown = 4
	self:_updateSectionSelection()
	self._contentTable._columns = {}
	self._contentTable:addColumn(CustomTableColumn("Module", self))
	self._contentTable:addColumn(CustomTableColumn("ID", self))
	self._contentTable:addColumn(CustomTableColumn("Nom", self))
	self._contentTable:addColumn(CustomTableColumn("Prix", self))
	self._contentTable:addColumn(CustomTableColumn("Type de Machine", self))
	self._contentTable:setContent(provide_machines_models(), ui.TableReadingMethod.VALUE)
end

function AllGameContentScene:_displayLocales()
	self._currentlyShown = 5
	self:_updateSectionSelection()
	self._contentTable._columns = {}
	self._contentTable:addColumn(CustomTableColumn("Code", self))
	self._contentTable:addColumn(CustomTableColumn("Nom", self))
	self._contentTable:addColumn(CustomTableColumn("Active ?", self))
	self._contentTable:addColumn(CustomTableColumn("Nombre d'Entrée(s)", self))
	self._contentTable:setContent(provide_locales(), ui.TableReadingMethod.VALUE)
end

function AllGameContentScene:_displayModules()
	self._currentlyShown = 6
	self:_updateSectionSelection()
	self._contentTable._columns = {}
	self._contentTable:addColumn(CustomTableColumn("ID", self))
	self._contentTable:addColumn(CustomTableColumn("Nom", self))
	self._contentTable:addColumn(CustomTableColumn("Version", self))
	self._contentTable:addColumn(CustomTableColumn("Auteur(s)", self))
	self._contentTable:addColumn(CustomTableColumn("Nombre d'Item(s)", self))
	self._contentTable:addColumn(CustomTableColumn("Nombre de Tag(s)", self))
	self._contentTable:addColumn(CustomTableColumn("Nombre de Type(s) de Machines", self))
	self._contentTable:setContent(provide_modules(), ui.TableReadingMethod.VALUE)
end

function AllGameContentScene:_displayRecipes()
	self._currentlyShown = 7
	self:_updateSectionSelection()
	self._contentTable._columns = {}
	self._contentTable:addColumn(CustomTableColumn("Module", self))
	self._contentTable:addColumn(CustomTableColumn("ID", self))
	self._contentTable:addColumn(CustomTableColumn("Nom", self))
	self._contentTable:addColumn(CustomTableColumn("Nbr de Machines", self))
	self._contentTable:addColumn(CustomTableColumn("Nbr d'Inputs", self))
	self._contentTable:addColumn(CustomTableColumn("Nbr d'Outputs", self))
	self._contentTable:addColumn(CustomTableColumn("Temps", self))
	self._contentTable:setContent(provide_recipes(), ui.TableReadingMethod.VALUE)
end

function AllGameContentScene:_populateTagContent(tagid)
	local t = {}
	for _, i in ipairs(TagsManager.getTag_nilsafe(tagid):getContent()) do
		if TagsManager.getTag_nilsafe(tagid):getType() == ids.Specifier.Tag.ITEMS then
			local x = ItemsManager.getItem(i)
			t[#t+1] = {
				x:getModule():getName(),
				i,
				x:getSafeLocalizedName()
			}
		elseif TagsManager.getTag_nilsafe(tagid):getType() == ids.Specifier.Tag.MACHINES then
			local x = MachinesManager.getMachineType_nilsafe(i)
			t[#t+1] = {
				x:getModule():getName(),
				i,
				x:getSafeLocalizedName()
			}
		end
	end

	self._tagContentTable:setContent(t, ui.TableReadingMethod.VALUE)
end

function AllGameContentScene:_populateLocaleContent(code)
	local t = {}

	for i, j in pairs(LocalizationManager._locales[code]._keys) do
		t[#t+1] = {
			i,
			j
		}
	end

	self._localeContentTable:setContent(t, ui.TableReadingMethod.VALUE)
end

function AllGameContentScene:_populateMachineTypeContent(id)
	local t = {}

	for toolid, tool in pairs(MachinesManager.getMachineType_nilsafe(id)._tools) do
		t[#t+1] = {
			toolid,
			tool:getSafeLocalizedName(),
			tostring(#tool._accepts) .. " Entrée(s)",
			tostring(tool:getAmount())
		}
	end

	self._machineTypeContentTable:setContent(t, ui.TableReadingMethod.VALUE)
end

function AllGameContentScene:_populateRecipeContent(id)
	local tm = {} -- type, id, nom
	local ti = {} -- nbr, qté, persistant?
	local to = {} -- module, id, nom, qtés

	for m in list.stream(RecipesManager.getRecipe_nilsafe(id):getAvailableFor()) do
		local t = ternary(m:instanceof(Tag),
			'Tag',
			ternary(m:instanceof(MachineType),
				'Type de Machines',
				'Modèle de Machines'))
		tm[#tm+1] = {
			t,
			m:getFullyQualifiedId(),
			m:getSafeLocalizedName()
		}
	end

	for input in list.stream(RecipesManager.getRecipe_nilsafe(id):getInputs()) do
		ti[#ti+1] = {
			tostring(#input._items),
			tostring(input:getAmount()),
			ternary(input:isPersistent(), "Oui", "Non")
		}
	end

	for output in list.stream(RecipesManager.getRecipe_nilsafe(id):getOutputs()) do
		to[#to+1] = {
			output:getItem():getModule():getName(),
			output:getItem():getFullyQualifiedId(),
			output:getItem():getSafeLocalizedName(),
			tostring(#output:getAmounts())
		}
	end

	self._recipeMachinesTable:setContent(tm, ui.TableReadingMethod.VALUE)
	self._recipeInputsTable:setContent(ti, "VALUE")
	self._recipeOutputsTable:setContent(to, "VALUE")
end

function AllGameContentScene:_setView(view)
	self._currentlySelected = 1
	if view == 'main' then
		self._view = 'main'
		self:unregisterUIComponent(1)
		self:registerUIComponent(self._win)
	elseif view == 'tag' then
		self._view = 'tag'
		self:unregisterUIComponent(1)
		self:registerUIComponent(self._winTagContent)
	elseif view == 'locale' then
		self._view = 'locale'
		self:unregisterUIComponent(1)
		self:registerUIComponent(self._winLocaleContent)
	elseif view == 'machineType' then
		self._view = 'machineType'
		self:unregisterUIComponent(1)
		self:registerUIComponent(self._winMachineTypeContent)
	elseif view == 'recipe' then
		self._view = 'recipe'
		self:unregisterUIComponent(1)
		self:registerUIComponent(self._winRecipeContent)
	end
end


return
{
	AllGameContentScene = AllGameContentScene
}