require('calypso.class')
local scenes = require('calypso.scenes')
local ui = require('calypso.old_ui')


local _test_table_content =
{
	{"prout", "caca"},
	{"prout", "caca"}
}


-- *****************************************************************************
-- * ProductionLinesScene
-- *
-- *  class ProductionLinesScene extends Scene
-- *****************************************************************************

---@class ProductionLinesScene:Scene
local ProductionLinesScene = class('ProductionLinesScene', scenes.Scene)

function ProductionLinesScene:initialize()
	scenes.Scene.initialize(self)
	self.classname = "scenes.ProductionLinesScene:ProductionLinesScene"

	self._win     = ui.Window()    ---@type Window
	self._footer  = ui.FooterBar() ---@type FooterBar
	self._table   = ui.Table()     ---@type Table

	self._win:getTitlebar():setText("Lignes de Production")
	self._win:getTitlebar():onCloseButtonReleased(function (btn, istouch)
		scenes.ScenesManager.setCurrentScene('central_board')
	end)

	self._win:setContent(ui.ComponentsGroup())

	self._footer:setText("[F1] Retour")
	self._footer:setCentered(false)
	self._footer:setPosition(0, self._win:getContent():getHeight() - self._footer:getHeight())
	self._win:getContent():registerComponent(self._footer)

	self._tableColonne1 = self._table:addColumn(ui.TableColumn():setName("colonne1"):setDisplayedName("Colonne 1"):setSize(50, 5))
	self._tableColonne2 = self._table:addColumn(ui.TableColumn():setName("colonne2"):setDisplayedName("Colonne 2"):setSize(50, 5))
	self._table:setLocation(ui.Rect(0, 0, 0, 0))
	self._table:setContent(_test_table_content, ui.TableReadingMethod.VALUE)
	self._win:getContent():registerComponent(self._table)

	self:registerUIComponent(self._win)
end

function ProductionLinesScene:onKeyReleased(key)
	if key == 'f1' then
		scenes.ScenesManager.setCurrentScene('central_board')
	end
end

function ProductionLinesScene:draw()
	-- pour s'assurer que la taille et la position du footer soient bien correctes.
	-- Sinon il se trace en prenant comme référence le Rect de son groupe au
	-- moment de l'initialisation, quand il valait 0,0,0,0
	local footerLocation = self._footer:getLocation() ---@type Rect
	local groupLocation = self._win:getContent():getLocation() ---@type Rect
	if footerLocation.width ~= groupLocation.width then
		self._footer:setSize(groupLocation.width, footerLocation.height)
	end
	if footerLocation.y ~= groupLocation.height - footerLocation.height then
		self._footer:setPosition(footerLocation.x, groupLocation.height - footerLocation.height)
	end

	local tableLocation = self._table:getLocation() ---@type Rect
	if tableLocation.width ~= groupLocation.width then
		self._table:setSize(groupLocation.width, groupLocation.height - footerLocation.height)
	end
end

return ProductionLinesScene