require('calypso.class')
local csc = require('calypso.scenes')
local ui  = require('calypso.old_ui')
require('foundation.log')


-- *****************************************************************************
-- * TestScene
-- *
-- *  class TestScene extends Scene
-- *****************************************************************************

---@class TestScene:Scene
local TestScene = class('TestScene', csc.Scene)

function TestScene:initialize()
	csc.Scene.initialize(self)
	self.classname = "scenes.test:TestScene"
	self._win = ui.Window() ---@type Window
	self._footerBar = ui.FooterBar() ---@type FooterBar
	self._testButton = ui.Button() ---@type Button

	self._win:getTitlebar():setText("Central Board")
	self._win:getTitlebar():onCloseButtonClicked(function (btn, istouch)
		love.event.quit(0)
	end)
	
	self._footerBar:setText("prout")
	self._footerBar:setCentered(false)
	
	self._testButton:setPosition(150, 150)
	self._testButton:setSize(150, 50)
	self._testButton:setText("Bouton !")
	self._testButton:onMouseClick(function(button, istouch)
		csc.ScenesManager.setCurrentScene('central_board')
	end)

	self._win:getContent():registerComponent(self._footerBar)
	self._win:getContent():registerComponent(self._testButton)
	
	self:registerUIComponent(self._win)
end


return TestScene