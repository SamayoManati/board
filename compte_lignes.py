import os
import os.path
import re


IGNORED_FOLDERS = [
	r"^\.\\\.git\\.*$",
	r"^\.\\\.vscode\\.*$"
]

WIDTH = 80

def is_in(l:list, s:str):
	for i in l:
		if bool(re.match(i, s)):
			return True
	return False


def fillstr(width, c=' '):
	s = ''
	for i in range(1, width + 1):
		s += c
	return s

def bxprint(s,spacing=True):
	if spacing:
		printedStr = '| ' + s
		print(printedStr + fillstr(WIDTH - len(printedStr)) + ' |')
	else:
		printedStr = '|' + s
		print(printedStr + fillstr(WIDTH - len(printedStr)) + '|')

def bxinline_separator(c='='):
	bxprint(fillstr(WIDTH, c), False)

f = []
for (dirpath, dirnames, filenames) in os.walk("."):
	f.extend([os.path.join(dirpath, i) for i in filenames])

sorted_files = []
ln = 0
fnbr = 0
for x in f:
	if not is_in(IGNORED_FOLDERS, x):
		with open(x, 'r') as file:
			try:
				thisfileln = len(file.readlines())
				ln += thisfileln
				fnbr += 1
				sorted_files.append([x, thisfileln])
			except:
				continue

sorted_files.sort(key=lambda i: i[1])

print('+' + fillstr(WIDTH, '-') + '+')

for x in sorted_files:
	bxprint(x[0] + fillstr(60-len(x[0]), '.') + "(" + str(x[1]) + " ligne(s))")

bxprint("")
bxinline_separator()
bxprint("Total :")
bxprint("    " + str(ln) + " lignes en " + str(fnbr) + " fichier(s)")

print('+' + fillstr(80, '-') + '+')
